# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Hermann Benedek <bence@intercom.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:40+0200\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Hermann Benedek <bence@intercom.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "nologin"
msgstr "nologin"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "2022. október 30"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "nologin - prevent non-root users from logging into the system"
msgid "nologin - prevent unprivileged users from logging into the system"
msgstr "nologin - a root felhasználón kívül letiltja az összes belépést"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the file B</etc/nologin> exists, B<login>(1)  will allow access only "
#| "to root. Other users will be shown the contents of this file and their "
#| "logins refused."
msgid ""
"If the file I</etc/nologin> exists and is readable, B<login>(1)  will allow "
"access only to root.  Other users will be shown the contents of this file "
"and their logins will be refused.  This provides a simple way of temporarily "
"disabling all unprivileged logins."
msgstr ""
"Ha létezik az B</etc/nologin> fájl, akkor a B<login>(1)  csak a root "
"felhasználó belépését engedélyezi. A többi felhasználónak kiírja a "
"B<nologin> fájl tartalmát, és megtagadja a belépésüket."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FÁJLOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I</etc/nologin>"
msgstr "I</etc/nologin>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<login>(1), B<shutdown>(8)"
msgstr "B<login>(1), B<shutdown>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "NOLOGIN"
msgstr "NOLOGIN"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "2017. szeptember 15"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programozói Kézikönyv"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
