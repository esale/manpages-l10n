# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Sztrepka Pál <szpal@firefly.szarvas.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-02-15 18:50+0100\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Sztrepka Pál <szpal@firefly.szarvas.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "FREE"
msgstr "FREE"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-06-03"
msgstr "2016. június 3"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr "Felhasználói parancsok"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: opensuse-leap-15-5
msgid "free - Display amount of free and used memory in the system"
msgstr "free - Kiírja a rendszerben szabad és elhasznált memória mennyiségét"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÖSSZEGZÉS"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<free> [I<options>]"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "B<free> displays the total amount of free and used physical and swap "
#| "memory in the system, as well as the shared memory and buffers used by "
#| "the kernel."
msgid ""
"B<free> displays the total amount of free and used physical and swap memory "
"in the system, as well as the buffers and caches used by the kernel. The "
"information is gathered by parsing /proc/meminfo. The displayed columns are:"
msgstr ""
"A B<free> kiírja a rendszerben a szabad, az elhasznált fizikai, és a swap "
"memória összmennyiségét, valamint a kernel által használt osztott memóriát "
"és a buffereket."

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<--total>"
msgid "B<total>"
msgstr "B<--total>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Total installed memory (MemTotal and SwapTotal in /proc/meminfo)"
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<user>"
msgid "B<used>"
msgstr "B<user>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Used memory (calculated as B<total> - B<available>)"
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<free>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Unused memory (MemFree and SwapFree in /proc/meminfo)"
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<shared>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Memory used (mostly) by tmpfs (Shmem in /proc/meminfo)"
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<buffers>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Memory used by kernel buffers (Buffers in /proc/meminfo)"
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<casefold>"
msgid "B<cache>"
msgstr "B<casefold>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Memory used by the page cache and slabs (Cached and SReclaimable in /proc/"
"meminfo)"
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<buff/cache>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Sum of B<buffers> and B<cache>"
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<available>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Estimation of how much memory is available for starting new applications, "
"without swapping. Unlike the data provided by the B<cache> or B<free> "
"fields, this field takes into account page cache and also that not all "
"reclaimable memory slabs will be reclaimed due to items being in use "
"(MemAvailable in /proc/meminfo, available on kernels 3.14, emulated on "
"kernels 2.6.27+, otherwise the same as B<free>)"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "KAPCSOLÓK"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-b>, B<--bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "free - Display amount of free and used memory in the system"
msgid "Display the amount of memory in bytes."
msgstr "free - Kiírja a rendszerben szabad és elhasznált memória mennyiségét"

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-k>, B<--kibibytes>"
msgid "B<-k>, B<--kibi>"
msgstr "B<-k>, B<--kibibytes>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display the amount of memory in kibibytes.  This is the default."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-m>, B<--merge>"
msgid "B<-m>, B<--mebi>"
msgstr "B<-m>, B<--merge>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "free - Display amount of free and used memory in the system"
msgid "Display the amount of memory in mebibytes."
msgstr "free - Kiírja a rendszerben szabad és elhasznált memória mennyiségét"

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-8>, B<--8-bit>"
msgid "B<-g>, B<--gibi>"
msgstr "B<-8>, B<--8-bit>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "free - Display amount of free and used memory in the system"
msgid "Display the amount of memory in gibibytes."
msgstr "free - Kiírja a rendszerben szabad és elhasznált memória mennyiségét"

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<--test>"
msgid "B<--tebi>"
msgstr "B<--test>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "free - Display amount of free and used memory in the system"
msgid "Display the amount of memory in tebibytes."
msgstr "free - Kiírja a rendszerben szabad és elhasznált memória mennyiségét"

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<--posix>"
msgid "B<--pebi>"
msgstr "B<--posix>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "free - Display amount of free and used memory in the system"
msgid "Display the amount of memory in pebibytes."
msgstr "free - Kiírja a rendszerben szabad és elhasznált memória mennyiségét"

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<--iso>"
msgid "B<--kilo>"
msgstr "B<--iso>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display the amount of memory in kilobytes. Implies --si."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<--message>"
msgid "B<--mega>"
msgstr "B<--message>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display the amount of memory in megabytes. Implies --si."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<--iso>"
msgid "B<--giga>"
msgstr "B<--iso>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display the amount of memory in gigabytes. Implies --si."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<--tag>"
msgid "B<--tera>"
msgstr "B<--tag>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display the amount of memory in terabytes. Implies --si."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<--total>"
msgid "B<--peta>"
msgstr "B<--total>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display the amount of memory in petabytes. Implies --si."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-h>, B<--human-readable>"
msgid "B<-h>, B<--human>"
msgstr "B<-h>, B<--human-readable>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Show all output fields automatically scaled to shortest three digit unit and "
"display the units of print out.  Following units are used."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"  B = bytes\n"
"  K = kibibyte\n"
"  M = mebibyte\n"
"  G = gibibyte\n"
"  T = tebibyte\n"
"  P = pebibyte\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"If unit is missing, and you have exbibyte of RAM or swap, the number is in "
"tebibytes and columns might not be aligned with header."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-w>, B<--warn>"
msgid "B<-w>, B<--wide>"
msgstr "B<-w>, B<--warn>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Switch to the wide mode. The wide mode produces lines longer than 80 "
"characters. In this mode B<buffers> and B<cache> are reported in two "
"separate columns."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-f>, B<--force>"
msgid "B<-C>, B<--full-cache>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Add to the plain B<Cached> in-memory cache for files also more cache lines "
"as the B<NFS_Unstable> pages sent to the server, but not yet committed to "
"stable storage and the B<SwapCached> memory that once was swapped out but is "
"swapped back.  Can be also enabled by the environment variable "
"B<PS_FULL_CACHE>."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-c>, B<--count>"
msgid "B<-c>, B<--count> I<count>"
msgstr "B<-c>, B<--count>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display the result I<count> times.  Requires the B<-s> option."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-l>, B<--login>"
msgid "B<-l>, B<--lohi>"
msgstr "B<-l>, B<--login>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Show detailed low and high memory statistics."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-s>, B<--silent>, B<--quiet>"
msgid "B<-s>, B<--seconds> I<delay>"
msgstr "B<-s>, B<--silent>, B<--quiet>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "The B<-s> switch activates continuous polling I<delay> seconds apart. You "
#| "may actually specify any floating point number for I<delay>, "
#| "B<usleep>(3)  is used for microsecond resolution delay times."
msgid ""
"Continuously display the result I<delay> seconds apart.  You may actually "
"specify any floating point number for I<delay> using either . or , for "
"decimal point.  B<usleep>(3)  is used for microsecond resolution delay times."
msgstr ""
"A B<-s> kapcsoló aktiválja a folyamatos számlálást I<delay> másodpercenként. "
"Valójában bármilyen lebegőpontos számot megadhatsz a I<delay>-nek, az "
"B<usleep>(3)-et a mikroszekundumos időkésleltetésekre használják."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<--si>"
msgstr "B<--si>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Use kilo, mega, giga etc (power of 1000) instead of kibi, mebi, gibi (power "
"of 1024)."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "B<-t>, B<--text>"
msgid "B<-t>, B<--total>"
msgstr "B<-t>, B<--text>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "The B<-t> switch displays a line containing the totals."
msgid "Display a line showing the column totals."
msgstr "A B<-t> kapcsoló kiír egy összegzéseket tartalmazó sort."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Print help."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information."
msgstr "A programváltozatról ír ki információt."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "FILES"
msgstr "FÁJLOK"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "/proc/meminfo"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "I</proc/meminfo>-- memory information"
msgid "memory information"
msgstr "I</proc/meminfo>-- memória információ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "BUGS"
msgstr "HIBÁK"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The value for the B<shared> column is not available from kernels before "
"2.6.32 and is displayed as zero."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Please send bug reports to"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "E<.UR procps@freelists.org> E<.UE>"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<ps>(1), B<slabtop>(1), B<top>(1), B<vmstat>(8)."
msgstr ""
