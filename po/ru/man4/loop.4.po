# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Artyom Kunyov <artkun@guitarplayer.ru>, 2012.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2012, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Konstantin Shvaykovskiy <kot.shv@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:35+0200\n"
"PO-Revision-Date: 2019-09-21 08:19+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "loop"
msgstr "loop"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "loop, loop-control - loop devices"
msgstr "loop, loop-control - закольцованные устройства"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "#include E<lt>linux/loop.hE<gt>\n"
msgstr "#include E<lt>linux/loop.hE<gt>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The loop device is a block device that maps its data blocks not to a "
"physical device such as a hard disk or optical disk drive, but to the blocks "
"of a regular file in a filesystem or to another block device.  This can be "
"useful for example to provide a block device for a filesystem image stored "
"in a file, so that it can be mounted with the B<mount>(8)  command.  You "
"could do"
msgstr ""
"Закольцованное устройство — это блочное устройство, которое отображает блоки "
"данных обычного файла в файловой системе или другое блочное устройство. Это "
"может быть полезно, например, для получения образа файловой системы, "
"хранящегося в файле, в виде блочного устройства, которое может быть "
"смонтировано с помощью команды B<mount>(8). Это можно сделать так:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<dd if=/dev/zero of=file.img bs=1MiB count=10>\n"
"$ B<sudo losetup /dev/loop4 file.img>\n"
"$ B<sudo mkfs -t ext4 /dev/loop4>\n"
"$ B<sudo mkdir /myloopdev>\n"
"$ B<sudo mount /dev/loop4 /myloopdev>\n"
msgstr ""
"$ B<dd if=/dev/zero of=file.img bs=1MiB count=10>\n"
"$ B<sudo losetup /dev/loop4 file.img>\n"
"$ B<sudo mkfs -t ext4 /dev/loop4>\n"
"$ B<sudo mkdir /myloopdev>\n"
"$ B<sudo mount /dev/loop4 /myloopdev>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<losetup>(8)  for another example."
msgstr "Другой пример смотрите в B<losetup>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A transfer function can be specified for each loop device for encryption and "
"decryption purposes."
msgstr ""
"Для шифрования и расшифровки каждому закольцованному устройству может быть "
"назначена функция обмена."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following B<ioctl>(2)  operations are provided by the loop block device:"
msgstr ""
"Для закольцованного блочного устройства доступны следующие операции "
"B<ioctl>(2):"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_SET_FD>"
msgstr "B<LOOP_SET_FD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Associate the loop device with the open file whose file descriptor is passed "
"as the (third)  B<ioctl>(2)  argument."
msgstr ""
"Связывает закольцованное устройство с открытым файлом, чей файловый "
"дескриптор передаётся в третьем аргументе B<ioctl>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_CLR_FD>"
msgstr "B<LOOP_CLR_FD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Disassociate the loop device from any file descriptor."
msgstr "Отвязывает закольцованное устройство от файлового дескриптора."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_SET_STATUS>"
msgstr "B<LOOP_SET_STATUS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the status of the loop device using the (third)  B<ioctl>(2)  argument.  "
"This argument is a pointer to a I<loop_info> structure, defined in "
"I<E<lt>linux/loop.hE<gt>> as:"
msgstr ""
"Назначает состояние (передаваемое в третьем аргументе B<ioctl>(2)) "
"закольцованному устройству. Данный аргумент представляет собой указатель на "
"структуру I<loop_info>, определённую в I<E<lt>linux/loop.hE<gt>> следующим "
"образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "struct loop_info {\n"
#| "    int           lo_number;            /* ioctl r/o */\n"
#| "    dev_t         lo_device;            /* ioctl r/o */\n"
#| "    unsigned long lo_inode;             /* ioctl r/o */\n"
#| "    dev_t         lo_rdevice;           /* ioctl r/o */\n"
#| "    int           lo_offset;\n"
#| "    int           lo_encrypt_type;\n"
#| "    int           lo_encrypt_key_size;  /* ioctl w/o */\n"
#| "    int           lo_flags;             /* ioctl r/o */\n"
#| "    char          lo_name[LO_NAME_SIZE];\n"
#| "    unsigned char lo_encrypt_key[LO_KEY_SIZE];\n"
#| "                                        /* ioctl w/o */\n"
#| "    unsigned long lo_init[2];\n"
#| "    char          reserved[4];\n"
#| "};\n"
msgid ""
"struct loop_info {\n"
"    int           lo_number;      /* ioctl r/o */\n"
"    dev_t         lo_device;      /* ioctl r/o */\n"
"    unsigned long lo_inode;       /* ioctl r/o */\n"
"    dev_t         lo_rdevice;     /* ioctl r/o */\n"
"    int           lo_offset;\n"
"    int           lo_encrypt_type;\n"
"    int           lo_encrypt_key_size;  /* ioctl w/o */\n"
"    int           lo_flags;       /* ioctl r/w (r/o before\n"
"                                     Linux 2.6.25) */\n"
"    char          lo_name[LO_NAME_SIZE];\n"
"    unsigned char lo_encrypt_key[LO_KEY_SIZE];\n"
"                                  /* ioctl w/o */\n"
"    unsigned long lo_init[2];\n"
"    char          reserved[4];\n"
"};\n"
msgstr ""
"struct loop_info {\n"
"    int           lo_number;            /* только чтение из ioctl */\n"
"    dev_t         lo_device;            /* только чтение из ioctl */\n"
"    unsigned long lo_inode;             /* только чтение из ioctl */\n"
"    dev_t         lo_rdevice;           /* только чтение из ioctl */\n"
"    int           lo_offset;\n"
"    int           lo_encrypt_type;\n"
"    int           lo_encrypt_key_size;  /* только запись ioctl */\n"
"    int           lo_flags;             /* только чтение из ioctl */\n"
"    char          lo_name[LO_NAME_SIZE];\n"
"    unsigned char lo_encrypt_key[LO_KEY_SIZE];\n"
"                                        /* только запись ioctl */\n"
"    unsigned long lo_init[2];\n"
"    char          reserved[4];\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The encryption type (I<lo_encrypt_type>)  should be one of B<LO_CRYPT_NONE>, "
"B<LO_CRYPT_XOR>, B<LO_CRYPT_DES>, B<LO_CRYPT_FISH2>, B<LO_CRYPT_BLOW>, "
"B<LO_CRYPT_CAST128>, B<LO_CRYPT_IDEA>, B<LO_CRYPT_DUMMY>, "
"B<LO_CRYPT_SKIPJACK>, or (since Linux 2.6.0)  B<LO_CRYPT_CRYPTOAPI>."
msgstr ""
"Типом шифрования (I<lo_encrypt_type>) должно быть одно из значений: "
"B<LO_CRYPT_NONE>, B<LO_CRYPT_XOR>, B<LO_CRYPT_DES>, B<LO_CRYPT_FISH2>, "
"B<LO_CRYPT_BLOW>, B<LO_CRYPT_CAST128>, B<LO_CRYPT_IDEA>, B<LO_CRYPT_DUMMY>, "
"B<LO_CRYPT_SKIPJACK> или B<LO_CRYPT_CRYPTOAPI> (начиная с Linux 2.6.0)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<lo_flags> field is a bit mask that can include zero or more of the "
"following:"
msgstr ""
"Поле I<lo_flags> представляет собой битовую маску, в которой может быть ноль "
"или несколько следующих значений:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LO_FLAGS_READ_ONLY>"
msgstr "B<LO_FLAGS_READ_ONLY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The loopback device is read-only."
msgstr "Закольцованное устройство доступно только для чтения."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LO_FLAGS_AUTOCLEAR> (since Linux 2.6.25)"
msgstr "B<LO_FLAGS_AUTOCLEAR> (начиная с Linux 2.6.25)"

#.  commit 96c5865559cee0f9cbc5173f3c949f6ce3525581
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The loopback device will autodestruct on last close."
msgstr "Закольцованное устройство автоматически уничтожится после закрытия."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LO_FLAGS_PARTSCAN> (since Linux 3.2)"
msgstr "B<LO_FLAGS_PARTSCAN> (начиная с Linux 3.2)"

#.  commit e03c8dd14915fabc101aa495828d58598dc5af98
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Allow automatic partition scanning."
msgstr "Разрешено автоматическое сканирования разделов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<LO_FLAGS_DIRECT_IO> (since Linux 4.10)"
msgstr "B<LO_FLAGS_DIRECT_IO> (начиная с Linux 4.10)"

#.  commit 2e5ab5f379f96a6207c45be40c357ebb1beb8ef3
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Use direct I/O mode to access the backing file."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The only I<lo_flags> that can be modified by B<LOOP_SET_STATUS> are "
"B<LO_FLAGS_AUTOCLEAR> and B<LO_FLAGS_PARTSCAN>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_GET_STATUS>"
msgstr "B<LOOP_GET_STATUS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Get the status of the loop device.  The (third)  B<ioctl>(2)  argument must "
"be a pointer to a I<struct loop_info>."
msgstr ""
"Получить состояние закольцованного устройства. В третьем аргументе "
"B<ioctl>(2) должен быть задан указатель на структуру I<struct loop_info>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_CHANGE_FD> (since Linux 2.6.5)"
msgstr "B<LOOP_CHANGE_FD> (начиная с Linux 2.6.5)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Switch the backing store of the loop device to the new file identified file "
"descriptor specified in the (third)  B<ioctl>(2)  argument, which is an "
"integer.  This operation is possible only if the loop device is read-only "
"and the new backing store is the same size and type as the old backing store."
msgstr ""
"Поменять источник данных (backing store) закольцованного устройства на новый "
"файл, определяемый файловым дескриптором, указанным в третьем аргументе "
"B<ioctl>(2), представленный целым числом. Данная операция допустима только, "
"если закольцованное устройство доступно только на чтение и новый источник "
"данных имеет тот же размер и тип, использованный ранее."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_SET_CAPACITY> (since Linux 2.6.30)"
msgstr "B<LOOP_SET_CAPACITY> (начиная с Linux 2.6.30)"

#.  commit 53d6660836f233df66490707365ab177e5fb2bb4
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Resize a live loop device.  One can change the size of the underlying "
"backing store and then use this operation so that the loop driver learns "
"about the new size.  This operation takes no argument."
msgstr ""
"Изменить размер используемого (live) закольцованного устройства. Можно "
"изменить размер используемого источника данных, а затем применить эту "
"операцию для того, чтобы драйвер закольцованных устройств учёл новый размер. "
"У этой операции нет аргументов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_SET_DIRECT_IO> (since Linux 4.10)"
msgstr "B<LOOP_SET_DIRECT_IO> (начиная с Linux 4.10)"

#.  commit ab1cb278bc7027663adbfb0b81404f8398437e11
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set DIRECT I/O mode on the loop device, so that it can be used to open "
"backing file.  The (third)  B<ioctl>(2)  argument is an unsigned long "
"value.  A nonzero represents direct I/O mode."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_SET_BLOCK_SIZE> (since Linux 4.14)"
msgstr "B<LOOP_SET_BLOCK_SIZE> (начиная с Linux 4.14)"

#.  commit 89e4fdecb51cf5535867026274bc97de9480ade5
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the block size of the loop device.  The (third)  B<ioctl>(2)  argument "
"is an unsigned long value.  This value must be a power of two in the range "
"[512,pagesize]; otherwise, an B<EINVAL> error results."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_CONFIGURE> (since Linux 5.8)"
msgstr "B<LOOP_CONFIGURE> (начиная с Linux 5.8)"

#.  commit 3448914e8cc550ba792d4ccc74471d1ca4293aae
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Set the status of the loop device using the (third)  B<ioctl>(2)  "
#| "argument.  This argument is a pointer to I<loop_info> structure, defined "
#| "in I<E<lt>linux/loop.hE<gt>> as:"
msgid ""
"Setup and configure all loop device parameters in a single step using the "
"(third)  B<ioctl>(2)  argument.  This argument is a pointer to a "
"I<loop_config> structure, defined in I<E<lt>linux/loop.hE<gt>> as:"
msgstr ""
"Назначает состояние (передаваемое в третьем аргументе B<ioctl>(2)) "
"закольцованному устройству. Данный аргумент представляет собой указатель на "
"структуру I<loop_info>, определённую в I<E<lt>linux/loop.hE<gt>> следующим "
"образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"struct loop_config {\n"
"    __u32               fd;\n"
"    __u32               block_size;\n"
"    struct loop_info64  info;\n"
"    __u64               __reserved[8];\n"
"};\n"
msgstr ""
"struct loop_config {\n"
"    __u32               fd;\n"
"    __u32               block_size;\n"
"    struct loop_info64  info;\n"
"    __u64               __reserved[8];\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In addition to doing what B<LOOP_SET_STATUS> can do, B<LOOP_CONFIGURE> can "
"also be used to do the following:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"set the correct block size immediately by setting I<loop_config.block_size>;"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"explicitly request direct I/O mode by setting B<LO_FLAGS_DIRECT_IO> in "
"I<loop_config.info.lo_flags>; and"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"explicitly request read-only mode by setting B<LO_FLAGS_READ_ONLY> in "
"I<loop_config.info.lo_flags>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Since Linux 2.6, there are two new B<ioctl>(2)  operations:"
msgstr "Начиная с Linux 2.6, появилось две новые операции B<ioctl>(2):"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_SET_STATUS64>, B<LOOP_GET_STATUS64>"
msgstr "B<LOOP_SET_STATUS64>, B<LOOP_GET_STATUS64>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These are similar to B<LOOP_SET_STATUS> and B<LOOP_GET_STATUS> described "
"above but use the I<loop_info64> structure, which has some additional fields "
"and a larger range for some other fields:"
msgstr ""
"Они подобны описанным выше B<LOOP_SET_STATUS> и B<LOOP_GET_STATUS>, но "
"используют структуру I<loop_info64>, в которой есть несколько дополнительных "
"полей, а некоторым другим полям назначены типы с большим диапазоном значений:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "struct loop_info64 {\n"
#| "    uint64_t lo_device;                   /* ioctl r/o */\n"
#| "    uint64_t lo_inode;                    /* ioctl r/o */\n"
#| "    uint64_t lo_rdevice;                  /* ioctl r/o */\n"
#| "    uint64_t lo_offset;\n"
#| "    uint64_t lo_sizelimit;/* bytes, 0 == max available */\n"
#| "    uint32_t lo_number;                   /* ioctl r/o */\n"
#| "    uint32_t lo_encrypt_type;\n"
#| "    uint32_t lo_encrypt_key_size;         /* ioctl w/o */\n"
#| "    uint32_t lo_flags;                    /* ioctl r/o */\n"
#| "    uint8_t  lo_file_name[LO_NAME_SIZE];\n"
#| "    uint8_t  lo_crypt_name[LO_NAME_SIZE];\n"
#| "    uint8_t  lo_encrypt_key[LO_KEY_SIZE]; /* ioctl w/o */\n"
#| "    uint64_t lo_init[2];\n"
#| "};\n"
msgid ""
"struct loop_info64 {\n"
"    uint64_t lo_device;           /* ioctl r/o */\n"
"    uint64_t lo_inode;            /* ioctl r/o */\n"
"    uint64_t lo_rdevice;          /* ioctl r/o */\n"
"    uint64_t lo_offset;\n"
"    uint64_t lo_sizelimit;  /* bytes, 0 == max available */\n"
"    uint32_t lo_number;           /* ioctl r/o */\n"
"    uint32_t lo_encrypt_type;\n"
"    uint32_t lo_encrypt_key_size; /* ioctl w/o */\n"
"    uint32_t lo_flags; i          /* ioctl r/w (r/o before\n"
"                                     Linux 2.6.25) */\n"
"    uint8_t  lo_file_name[LO_NAME_SIZE];\n"
"    uint8_t  lo_crypt_name[LO_NAME_SIZE];\n"
"    uint8_t  lo_encrypt_key[LO_KEY_SIZE]; /* ioctl w/o */\n"
"    uint64_t lo_init[2];\n"
"};\n"
msgstr ""
"struct loop_info64 {\n"
"    uint64_t lo_device;                   /* только чтение из ioctl */\n"
"    uint64_t lo_inode;                    /* только чтение из ioctl */\n"
"    uint64_t lo_rdevice;                  /* только чтение из ioctl */\n"
"    uint64_t lo_offset;\n"
"    uint64_t lo_sizelimit;/* в байтах, 0 == макс. доступный */\n"
"    uint32_t lo_number;                   /* только чтение из ioctl */\n"
"    uint32_t lo_encrypt_type;\n"
"    uint32_t lo_encrypt_key_size;         /* только запись из ioctl */\n"
"    uint32_t lo_flags;                    /* только чтение из ioctl */\n"
"    uint8_t  lo_file_name[LO_NAME_SIZE];\n"
"    uint8_t  lo_crypt_name[LO_NAME_SIZE];\n"
"    uint8_t  lo_encrypt_key[LO_KEY_SIZE]; /* только запись из ioctl */\n"
"    uint64_t lo_init[2];\n"
"};\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "/dev/loop-control"
msgstr "/dev/loop-control"

#.  commit 770fe30a46a12b6fb6b63fbe1737654d28e84844
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Since Linux 3.1, the kernel provides the I</dev/loop-control> device, which "
"permits an application to dynamically find a free device, and to add and "
"remove loop devices from the system.  To perform these operations, one first "
"opens I</dev/loop-control> and then employs one of the following "
"B<ioctl>(2)  operations:"
msgstr ""
"Начиная с Linux 3.1, ядро предоставляет устройство I</dev/loop-control>, "
"которое позволяет приложению динамически находить свободное устройство, "
"добавлять и удалять закольцованные устройства из системы. Для выполнения "
"этих операций сначала открывается I</dev/loop-control>, а затем выполняется "
"одна из следующих операций B<ioctl>(2):"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_CTL_GET_FREE>"
msgstr "B<LOOP_CTL_GET_FREE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Allocate or find a free loop device for use.  On success, the device number "
"is returned as the result of the call.  This operation takes no argument."
msgstr ""
"Выделяет или ищет свободное закольцованное устройства для работы. При "
"успешном выполнении возвращается номер устройства. У операции нет аргументов."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_CTL_ADD>"
msgstr "B<LOOP_CTL_ADD>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Add the new loop device whose device number is specified as a long integer "
"in the third B<ioctl>(2)  argument.  On success, the device index is "
"returned as the result of the call.  If the device is already allocated, the "
"call fails with the error B<EEXIST>."
msgstr ""
"Добавляет новое закольцованное устройство; номер устройства передаётся в "
"виде длинного целого в третьем аргументе B<ioctl>(2). При успешном "
"выполнении возвращается индекс устройства. Если устройство уже выделено, то "
"вызов завершается с ошибкой B<EEXIST>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOOP_CTL_REMOVE>"
msgstr "B<LOOP_CTL_REMOVE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Remove the loop device whose device number is specified as a long integer in "
"the third B<ioctl>(2)  argument.  On success, the device number is returned "
"as the result of the call.  If the device is in use, the call fails with the "
"error B<EBUSY>."
msgstr ""
"Удаляет закольцованное устройство; номер устройства передаётся в виде "
"длинного целого в третьем аргументе B<ioctl>(2). При успешном выполнении "
"возвращается номер устройства. Если устройство используется, то вызов "
"завершается с ошибкой B<EBUSY>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</dev/loop*>"
msgstr "I</dev/loop*>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The loop block special device files."
msgstr "Специальные закольцованные файлы устройств."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The program below uses the I</dev/loop-control> device to find a free loop "
"device, opens the loop device, opens a file to be used as the underlying "
"storage for the device, and then associates the loop device with the backing "
"store.  The following shell session demonstrates the use of the program:"
msgstr ""
"Программа, представленная ниже, используется устройство I</dev/loop-control> "
"для поиска свободного закольцованного устройства, открывает закольцованное "
"устройство, открывает файл, который нужно использовать в качестве источника "
"данных, и связывает закольцованное устройство с источником. Демонстрация "
"работы программы:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<dd if=/dev/zero of=file.img bs=1MiB count=10>\n"
"10+0 records in\n"
"10+0 records out\n"
"10485760 bytes (10 MB) copied, 0.00609385 s, 1.7 GB/s\n"
"$ B<sudo ./mnt_loop file.img>\n"
"loopname = /dev/loop5\n"
msgstr ""
"$ B<dd if=/dev/zero of=file.img bs=1MiB count=10>\n"
"10+0 records in\n"
"10+0 records out\n"
"10485760 bytes (10 MB) copied, 0.00609385 s, 1.7 GB/s\n"
"$ B<sudo ./mnt_loop file.img>\n"
"loopname = /dev/loop5\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Исходный код программы"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>linux/loop.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>linux/loop.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"
msgstr ""
"#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\e\n"
"                        } while (0)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int loopctlfd, loopfd, backingfile;\n"
"    long devnr;\n"
"    char loopname[4096];\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int loopctlfd, loopfd, backingfile;\n"
"    long devnr;\n"
"    char loopname[4096];\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Usage: %s backing-file\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 2) {\n"
"        fprintf(stderr, \"Использование: %s файл-источник\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    loopctlfd = open(\"/dev/loop-control\", O_RDWR);\n"
"    if (loopctlfd == -1)\n"
"        errExit(\"open: /dev/loop-control\");\n"
msgstr ""
"    loopctlfd = open(\"/dev/loop-control\", O_RDWR);\n"
"    if (loopctlfd == -1)\n"
"        errExit(\"open: /dev/loop-control\");\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    devnr = ioctl(loopctlfd, LOOP_CTL_GET_FREE);\n"
"    if (devnr == -1)\n"
"        errExit(\"ioctl-LOOP_CTL_GET_FREE\");\n"
msgstr ""
"    devnr = ioctl(loopctlfd, LOOP_CTL_GET_FREE);\n"
"    if (devnr == -1)\n"
"        errExit(\"ioctl-LOOP_CTL_GET_FREE\");\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    sprintf(loopname, \"/dev/loop%ld\", devnr);\n"
"    printf(\"loopname = %s\\en\", loopname);\n"
msgstr ""
"    sprintf(loopname, \"/dev/loop%ld\", devnr);\n"
"    printf(\"loopname = %s\\en\", loopname);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    loopfd = open(loopname, O_RDWR);\n"
"    if (loopfd == -1)\n"
"        errExit(\"open: loopname\");\n"
msgstr ""
"    loopfd = open(loopname, O_RDWR);\n"
"    if (loopfd == -1)\n"
"        errExit(\"open: loopname\");\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    backingfile = open(argv[1], O_RDWR);\n"
"    if (backingfile == -1)\n"
"        errExit(\"open: backing-file\");\n"
msgstr ""
"    backingfile = open(argv[1], O_RDWR);\n"
"    if (backingfile == -1)\n"
"        errExit(\"open: backing-file\");\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (ioctl(loopfd, LOOP_SET_FD, backingfile) == -1)\n"
"        errExit(\"ioctl-LOOP_SET_FD\");\n"
msgstr ""
"    if (ioctl(loopfd, LOOP_SET_FD, backingfile) == -1)\n"
"        errExit(\"ioctl-LOOP_SET_FD\");\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<losetup>(8), B<mount>(8)"
msgstr "B<losetup>(8), B<mount>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "LOOP"
msgstr "LOOP"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "#include E<lt>linux/loop.hE<gt>"
msgstr "#include E<lt>linux/loop.hE<gt>"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"$ B<dd if=/dev/zero of=file.img bs=1MiB count=10>\n"
"$ B<sudo losetup /dev/loop4 file.img >\n"
"$ B<sudo mkfs -t ext4 /dev/loop4>\n"
"$ B<sudo mkdir /myloopdev>\n"
"$ B<sudo mount /dev/loop4 /myloopdev>\n"
msgstr ""
"$ B<dd if=/dev/zero of=file.img bs=1MiB count=10>\n"
"$ B<sudo losetup /dev/loop4 file.img >\n"
"$ B<sudo mkfs -t ext4 /dev/loop4>\n"
"$ B<sudo mkdir /myloopdev>\n"
"$ B<sudo mount /dev/loop4 /myloopdev>\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Set the status of the loop device using the (third)  B<ioctl>(2)  argument.  "
"This argument is a pointer to I<loop_info> structure, defined in "
"I<E<lt>linux/loop.hE<gt>> as:"
msgstr ""
"Назначает состояние (передаваемое в третьем аргументе B<ioctl>(2)) "
"закольцованному устройству. Данный аргумент представляет собой указатель на "
"структуру I<loop_info>, определённую в I<E<lt>linux/loop.hE<gt>> следующим "
"образом:"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"struct loop_info {\n"
"    int           lo_number;            /* ioctl r/o */\n"
"    dev_t         lo_device;            /* ioctl r/o */\n"
"    unsigned long lo_inode;             /* ioctl r/o */\n"
"    dev_t         lo_rdevice;           /* ioctl r/o */\n"
"    int           lo_offset;\n"
"    int           lo_encrypt_type;\n"
"    int           lo_encrypt_key_size;  /* ioctl w/o */\n"
"    int           lo_flags;             /* ioctl r/o */\n"
"    char          lo_name[LO_NAME_SIZE];\n"
"    unsigned char lo_encrypt_key[LO_KEY_SIZE];\n"
"                                        /* ioctl w/o */\n"
"    unsigned long lo_init[2];\n"
"    char          reserved[4];\n"
"};\n"
msgstr ""
"struct loop_info {\n"
"    int           lo_number;            /* только чтение из ioctl */\n"
"    dev_t         lo_device;            /* только чтение из ioctl */\n"
"    unsigned long lo_inode;             /* только чтение из ioctl */\n"
"    dev_t         lo_rdevice;           /* только чтение из ioctl */\n"
"    int           lo_offset;\n"
"    int           lo_encrypt_type;\n"
"    int           lo_encrypt_key_size;  /* только запись ioctl */\n"
"    int           lo_flags;             /* только чтение из ioctl */\n"
"    char          lo_name[LO_NAME_SIZE];\n"
"    unsigned char lo_encrypt_key[LO_KEY_SIZE];\n"
"                                        /* только запись ioctl */\n"
"    unsigned long lo_init[2];\n"
"    char          reserved[4];\n"
"};\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"struct loop_info64 {\n"
"    uint64_t lo_device;                   /* ioctl r/o */\n"
"    uint64_t lo_inode;                    /* ioctl r/o */\n"
"    uint64_t lo_rdevice;                  /* ioctl r/o */\n"
"    uint64_t lo_offset;\n"
"    uint64_t lo_sizelimit;/* bytes, 0 == max available */\n"
"    uint32_t lo_number;                   /* ioctl r/o */\n"
"    uint32_t lo_encrypt_type;\n"
"    uint32_t lo_encrypt_key_size;         /* ioctl w/o */\n"
"    uint32_t lo_flags;                    /* ioctl r/o */\n"
"    uint8_t  lo_file_name[LO_NAME_SIZE];\n"
"    uint8_t  lo_crypt_name[LO_NAME_SIZE];\n"
"    uint8_t  lo_encrypt_key[LO_KEY_SIZE]; /* ioctl w/o */\n"
"    uint64_t lo_init[2];\n"
"};\n"
msgstr ""
"struct loop_info64 {\n"
"    uint64_t lo_device;                   /* только чтение из ioctl */\n"
"    uint64_t lo_inode;                    /* только чтение из ioctl */\n"
"    uint64_t lo_rdevice;                  /* только чтение из ioctl */\n"
"    uint64_t lo_offset;\n"
"    uint64_t lo_sizelimit;/* в байтах, 0 == макс. доступный */\n"
"    uint32_t lo_number;                   /* только чтение из ioctl */\n"
"    uint32_t lo_encrypt_type;\n"
"    uint32_t lo_encrypt_key_size;         /* только запись из ioctl */\n"
"    uint32_t lo_flags;                    /* только чтение из ioctl */\n"
"    uint8_t  lo_file_name[LO_NAME_SIZE];\n"
"    uint8_t  lo_crypt_name[LO_NAME_SIZE];\n"
"    uint8_t  lo_encrypt_key[LO_KEY_SIZE]; /* только запись из ioctl */\n"
"    uint64_t lo_init[2];\n"
"};\n"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИМЕР"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
