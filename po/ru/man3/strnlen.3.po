# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:53+0200\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strnlen>()"
msgid "strnlen"
msgstr "B<strnlen>()"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "strnlen - determine the length of a fixed-size string"
msgstr "strnlen - вычисление длины строки постоянного размера"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<size_t strnlen(const char *>I<s>B<, size_t >I<maxlen>B<);>\n"
msgid "B<size_t strnlen(const char >I<s>B<[.>I<maxlen>B<], size_t >I<maxlen>B<);>\n"
msgstr "B<size_t strnlen(const char *>I<s>B<, size_t >I<maxlen>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<strnlen>():"
msgstr "B<strnlen>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Начиная с glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    До glibc 2.10:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strnlen>()  function returns the number of bytes in the string "
#| "pointed to by I<s>, excluding the terminating null byte (\\(aq\\e0\\(aq), "
#| "but at most I<maxlen>.  In doing this, B<strnlen>()  looks only at the "
#| "first I<maxlen> characters in the string pointed to by I<s> and never "
#| "beyond I<s+maxlen>."
msgid ""
"The B<strnlen>()  function returns the number of bytes in the string pointed "
"to by I<s>, excluding the terminating null byte (\\[aq]\\e0\\[aq]), but at "
"most I<maxlen>.  In doing this, B<strnlen>()  looks only at the first "
"I<maxlen> characters in the string pointed to by I<s> and never beyond "
"I<s[maxlen-1]>."
msgstr ""
"Функция B<strnlen>() возвращает количество байт в строке, на которую "
"указывает I<s>, не считая завершающего байта null (\\(aq\\e0\\(aq), но не "
"более I<maxlen>. При этом B<strnlen>() обращается только к первым I<maxlen> "
"символам, на которые указывает I<s>, и никогда не выходит дальше I<s+maxlen>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strnlen>()  function returns I<strlen(s)>, if that is less than "
#| "I<maxlen>, or I<maxlen> if there is no null terminating (\\(aq\\e0\\(aq) "
#| "among the first I<maxlen> characters pointed to by I<s>."
msgid ""
"The B<strnlen>()  function returns I<strlen(s)>, if that is less than "
"I<maxlen>, or I<maxlen> if there is no null terminating (\\[aq]\\e0\\[aq]) "
"among the first I<maxlen> characters pointed to by I<s>."
msgstr ""
"Функция B<strnlen>() возвращает I<strlen(s)>, если оно меньше I<maxlen>, или "
"I<maxlen>, если среди первых I<maxlen> байт, на которую указывает I<s>, не "
"встретился завершающий байт null (\\(aq\\e0\\(aq)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<strnlen>()"
msgstr "B<strnlen>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<strlen>(3)"
msgstr "B<strlen>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "STRNLEN"
msgstr "STRNLEN"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-03-15"
msgstr "15 марта 2016 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<size_t strnlen(const char *>I<s>B<, size_t >I<maxlen>B<);>\n"
msgstr "B<size_t strnlen(const char *>I<s>B<, size_t >I<maxlen>B<);>\n"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Начиная с glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-5
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "До glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-5
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "The B<strnlen>()  function returns the number of bytes in the string "
#| "pointed to by I<s>, excluding the terminating null byte (\\(aq\\e0\\(aq), "
#| "but at most I<maxlen>.  In doing this, B<strnlen>()  looks only at the "
#| "first I<maxlen> characters in the string pointed to by I<s> and never "
#| "beyond I<s+maxlen>."
msgid ""
"The B<strnlen>()  function returns the number of characters in the string "
"pointed to by I<s>, excluding the terminating null byte (\\(aq\\e0\\(aq), "
"but at most I<maxlen>.  In doing this, B<strnlen>()  looks only at the first "
"I<maxlen> characters in the string pointed to by I<s> and never beyond "
"I<s+maxlen>."
msgstr ""
"Функция B<strnlen>() возвращает количество байт в строке, на которую "
"указывает I<s>, не считая завершающего байта null (\\(aq\\e0\\(aq), но не "
"более I<maxlen>. При этом B<strnlen>() обращается только к первым I<maxlen> "
"символам, на которые указывает I<s>, и никогда не выходит дальше I<s+maxlen>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<strnlen>()  function returns I<strlen(s)>, if that is less than "
"I<maxlen>, or I<maxlen> if there is no null terminating (\\(aq\\e0\\(aq) "
"among the first I<maxlen> characters pointed to by I<s>."
msgstr ""
"Функция B<strnlen>() возвращает I<strlen(s)>, если оно меньше I<maxlen>, или "
"I<maxlen>, если среди первых I<maxlen> байт, на которую указывает I<s>, не "
"встретился завершающий байт null (\\(aq\\e0\\(aq)."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
