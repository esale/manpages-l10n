# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:28+0200\n"
"PO-Revision-Date: 2019-10-05 08:06+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<gamma>():"
msgid "gamma"
msgstr "B<gamma>():"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "gamma, gammaf, gammal - (logarithm of the) gamma function"
msgstr "gamma, gammaf, gammal - логарифм гамма-функции"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] double gamma(double >I<x>B<);>\n"
"B<[[deprecated]] float gammaf(float >I<x>B<);>\n"
"B<[[deprecated]] long double gammal(long double >I<x>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<gamma>():"
msgstr "B<gamma>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE\n"
"        || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<gammaf>(), B<gammal>():"
msgstr "B<gammaf>(), B<gammal>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "_XOPEN_SOURCE E<gt>= 600 || (_XOPEN_SOURCE && _ISOC99_SOURCE)\n"
#| "    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgid ""
"    _XOPEN_SOURCE E<gt>= 600 || (_XOPEN_SOURCE && _ISOC99_SOURCE)\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE E<gt>= 600 || (_XOPEN_SOURCE && _ISOC99_SOURCE)\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions are deprecated: instead, use either the B<tgamma>(3)  or the "
"B<lgamma>(3)  functions, as appropriate."
msgstr ""
"Данные функции устарели. Используйте вместо них, по возможности, "
"B<tgamma>(3) либо B<lgamma>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For the definition of the Gamma function, see B<tgamma>(3)."
msgstr "Определение гамма-функции смотрите в B<tgamma>(3)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "*BSD version"
msgstr "Версия *BSD"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The libm in 4.4BSD and some versions of FreeBSD had a B<gamma>()  function "
"that computes the Gamma function, as one would expect."
msgstr ""
"В libm из 4.4BSD и некоторых версиях FreeBSD имеется функция B<gamma>(), "
"которая вычисляет гамма-функцию, как и ожидается."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "glibc version"
msgstr "Версия glibc"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Glibc has a B<gamma>()  function that is equivalent to B<lgamma>(3)  and "
#| "computes the natural logarithm of the Gamma function."
msgid ""
"glibc has a B<gamma>()  function that is equivalent to B<lgamma>(3)  and "
"computes the natural logarithm of the Gamma function."
msgstr ""
"В glibc есть функция B<gamma>(), которая эквивалентна B<lgamma>(3) и "
"вычисляет натуральный логарифм гамма-функции."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<lgamma>(3)."
msgstr "Смотрите B<lgamma>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<gamma>(),\n"
"B<gammaf>(),\n"
"B<gammal>()"
msgstr ""
"B<gamma>(),\n"
"B<gammaf>(),\n"
"B<gammal>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:signgam"
msgstr "MT-Unsafe race:signgam"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "None"
msgid "None."
msgstr "None"

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "SVID 2."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Because of historical variations in behavior across systems, this "
#| "function is not specified in any recent standard.  It was documented in "
#| "SVID 2."
msgid ""
"Because of historical variations in behavior across systems, this function "
"is not specified in any recent standard."
msgstr ""
"Из-за различий в поведении в различных системах эта функция не попала ни в "
"один современный стандарт. Она описана в SVID 2."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"4.2BSD had a B<gamma>()  that computed ln(|Gamma(|I<x>|)|), leaving the sign "
"of Gamma(|I<x>|)  in the external integer I<signgam>.  In 4.3BSD the name "
"was changed to B<lgamma>(3), and the man page promises"
msgstr ""
"В 4.2BSD функция B<gamma>() вычисляет ln(|Gamma(|I<x>|)|), записывая знак "
"Gamma(|I<x>|) во внешнее целое I<signgam>. В 4.3BSD было изменено имя на "
"B<lgamma>(3), и в справочной странице указано"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"At some time in the future the name gamma will be rehabilitated and used for "
"the Gamma function"
msgstr ""
"Через некоторое время имя gamma было восстановлено и использовано для гаммы-"
"функции"

#.  The FreeBSD man page says about gamma() that it is like lgamma()
#.  except that is does not set signgam.
#.  Also, that 4.4BSD has a gamma() that computes the true gamma function.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This did indeed happen in 4.4BSD, where B<gamma>()  computes the Gamma "
"function (with no effect on I<signgam>).  However, this came too late, and "
"we now have B<tgamma>(3), the \"true gamma\" function."
msgstr ""
"В действительности это произошло в 4.4BSD, где B<gamma>() вычисляла гамма-"
"функцию (без влияния на I<signgam>). Однако, это случилось слишком поздно и "
"теперь мы имеем B<tgamma>(3), «настоящую» гамма-функцию."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<lgamma>(3), B<signgam>(3), B<tgamma>(3)"
msgstr "B<lgamma>(3), B<signgam>(3), B<tgamma>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Because of historical variations in behavior across systems, this function "
"is not specified in any recent standard.  It was documented in SVID 2."
msgstr ""
"Из-за различий в поведении в различных системах эта функция не попала ни в "
"один современный стандарт. Она описана в SVID 2."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: SS
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "History"
msgstr "История"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GAMMA"
msgstr "GAMMA"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>math.hE<gt>>"
msgstr "B<#include E<lt>math.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<double gamma(double >I<x>B<);>"
msgstr "B<double gamma(double >I<x>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<float gammaf(float >I<x>B<);>"
msgstr "B<float gammaf(float >I<x>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<long double gammal(long double >I<x>B<);>"
msgstr "B<long double gammal(long double >I<x>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Link with I<-lm>."
msgstr "Компонуется при указании параметра I<-lm>."

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"_XOPEN_SOURCE\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"_XOPEN_SOURCE E<gt>= 600 || (_XOPEN_SOURCE && _ISOC99_SOURCE)\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE E<gt>= 600 || (_XOPEN_SOURCE && _ISOC99_SOURCE)\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* версии glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Glibc has a B<gamma>()  function that is equivalent to B<lgamma>(3)  and "
"computes the natural logarithm of the Gamma function."
msgstr ""
"В glibc есть функция B<gamma>(), которая эквивалентна B<lgamma>(3) и "
"вычисляет натуральный логарифм гамма-функции."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
