# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:26+0200\n"
"PO-Revision-Date: 2019-09-27 19:31+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<fgetpwent>()"
msgid "fgetpwent"
msgstr "B<fgetpwent>()"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "fgetpwent - get password file entry"
msgstr "fgetpwent - получает записи из файла паролей"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>stdio.hE<gt>>\n"
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>stdio.hE<gt>>\n"
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<struct passwd *fgetpwent(FILE *>I<stream>B<);>\n"
msgstr "B<struct passwd *fgetpwent(FILE *>I<stream>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<fgetpwent>()"
msgid "B<fgetpwent>():"
msgstr "B<fgetpwent>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _SVID_SOURCE\n"
msgstr ""
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 и старее:\n"
"        _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<fgetpwent>()  function returns a pointer to a structure containing the "
"broken out fields of a line in the file I<stream>.  The first time it is "
"called it returns the first entry; thereafter, it returns successive "
"entries.  The file referred to by I<stream> must have the same format as I</"
"etc/passwd> (see B<passwd>(5))."
msgstr ""
"Функция B<fgetpwent>() возвращает указатель на структуру, содержащую "
"отдельные поля строки файла I<stream>. Во время первого вызова функция "
"возвращает первую запись; в дальнейшем она возвращает все последующие "
"записи. Формат файла I<stream> должен быть таким же как у I</etc/passwd> "
"(смотрите B<passwd>(5))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The I<passwd> structure is defined in I<E<lt>pwd.hE<gt>> as follows:"
msgstr "Структура I<passwd> определена в I<E<lt>pwd.hE<gt>> таким образом:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct passwd {\n"
"    char   *pw_name;       /* username */\n"
"    char   *pw_passwd;     /* user password */\n"
"    uid_t   pw_uid;        /* user ID */\n"
"    gid_t   pw_gid;        /* group ID */\n"
"    char   *pw_gecos;      /* real name */\n"
"    char   *pw_dir;        /* home directory */\n"
"    char   *pw_shell;      /* shell program */\n"
"};\n"
msgstr ""
"struct passwd {\n"
"    char   *pw_name;       /* имя пользователя */\n"
"    char   *pw_passwd;     /* пароль пользователя */\n"
"    uid_t   pw_uid;        /* идентификатор пользователя */\n"
"    gid_t   pw_gid;        /* идентификатор группы */\n"
"    char   *pw_gecos;      /* настоящее имя */\n"
"    char   *pw_dir;        /* домашний каталог */\n"
"    char   *pw_shell;      /* программная оболочка */\n"
"};\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<fgetpwent>()  function returns a pointer to a I<passwd> structure, "
#| "or NULL if there are no more entries or an error occurs.  In the event of "
#| "an error, I<errno> is set to indicate the cause."
msgid ""
"The B<fgetpwent>()  function returns a pointer to a I<passwd> structure, or "
"NULL if there are no more entries or an error occurs.  In the event of an "
"error, I<errno> is set to indicate the error."
msgstr ""
"Функция B<fgetpwent>() возвращает указатель на структуру I<passwd> или NULL, "
"если больше паролей не найдено или произошла ошибка. При ошибке I<errno> "
"присваивается соответствующее значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Insufficient memory to allocate I<passwd> structure."
msgstr "Недостаточно памяти для структуры I<passwd>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛЫ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/passwd>"
msgstr "I</etc/passwd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "password database file"
msgstr "файл, содержащий базу паролей"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fgetpwent>()"
msgstr "B<fgetpwent>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:fgetpwent"
msgstr "MT-Unsafe race:fgetpwent"

#.  FIXME: The marking is different from that in the glibc manual,
#.  which has:
#.     fgetpwent: MT-Unsafe race:fpwent
#.  We think race:fpwent in glibc maybe hard for users to understand,
#.  and have sent a patch to the GNU libc community for changing it to
#.  race:fgetpwent, however, something about the copyright impeded the
#.  progress.
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "None"
msgid "None."
msgstr "None"

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "SVr4."
msgstr "SVr4."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<endpwent>(3), B<fgetpwent_r>(3), B<fopen>(3), B<getpw>(3), B<getpwent>(3), "
"B<getpwnam>(3), B<getpwuid>(3), B<putpwent>(3), B<setpwent>(3), B<passwd>(5)"
msgstr ""
"B<endpwent>(3), B<fgetpwent_r>(3), B<fopen>(3), B<getpw>(3), B<getpwent>(3), "
"B<getpwnam>(3), B<getpwuid>(3), B<putpwent>(3), B<setpwent>(3), B<passwd>(5)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "FGETPWENT"
msgstr "FGETPWENT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2018-02-02"
msgstr "2 февраля 2018 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<fgetpwent>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _SVID_SOURCE\n"
msgstr ""
"B<fgetpwent>():\n"
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc 2.19 и старее:\n"
"        _SVID_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<fgetpwent>()  function returns a pointer to a I<passwd> structure, or "
"NULL if there are no more entries or an error occurs.  In the event of an "
"error, I<errno> is set to indicate the cause."
msgstr ""
"Функция B<fgetpwent>() возвращает указатель на структуру I<passwd> или NULL, "
"если больше паролей не найдено или произошла ошибка. При ошибке I<errno> "
"присваивается соответствующее значение."

#.  FIXME: The marking is different from that in the glibc manual,
#.  which has:
#.     fgetpwent: MT-Unsafe race:fpwent
#.  We think race:fpwent in glibc maybe hard for users to understand,
#.  and have sent a patch to the GNU libc community for changing it to
#.  race:fgetpwent, however, something about the copyright impeded the
#.  progress.
#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
