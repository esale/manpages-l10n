# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 20:00+0200\n"
"PO-Revision-Date: 2019-10-15 18:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "unlocked_stdio"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"getc_unlocked, getchar_unlocked, putc_unlocked, putchar_unlocked - "
"nonlocking stdio functions"
msgstr ""
"getc_unlocked, getchar_unlocked, putc_unlocked, putchar_unlocked - "
"неблокирующие функции стандартного ввода-вывода"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getc_unlocked(FILE *>I<stream>B<);>\n"
"B<int getchar_unlocked(void);>\n"
"B<int putc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
"B<int putchar_unlocked(int >I<c>B<);>\n"
msgstr ""
"B<int getc_unlocked(FILE *>I<stream>B<);>\n"
"B<int getchar_unlocked(void);>\n"
"B<int putc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
"B<int putchar_unlocked(int >I<c>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int getc_unlocked(FILE *>I<stream>B<);>\n"
#| "B<int getchar_unlocked(void);>\n"
#| "B<int putc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
#| "B<int putchar_unlocked(int >I<c>B<);>\n"
msgid ""
"B<void clearerr_unlocked(FILE *>I<stream>B<);>\n"
"B<int feof_unlocked(FILE *>I<stream>B<);>\n"
"B<int ferror_unlocked(FILE *>I<stream>B<);>\n"
"B<int fileno_unlocked(FILE *>I<stream>B<);>\n"
"B<int fflush_unlocked(FILE *>I<stream>B<);>\n"
msgstr ""
"B<int getc_unlocked(FILE *>I<stream>B<);>\n"
"B<int getchar_unlocked(void);>\n"
"B<int putc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
"B<int putchar_unlocked(int >I<c>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
#| "B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"
msgid ""
"B<int fgetc_unlocked(FILE *>I<stream>B<);>\n"
"B<int fputc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
msgstr ""
"B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
"B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
#| "B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"
msgid ""
"B<size_t fread_unlocked(void >I<ptr>B<[restrict .>I<size>B< * .>I<n>B<],>\n"
"B<                      size_t >I<size>B<, size_t >I<n>B<,>\n"
"B<                      FILE *restrict >I<stream>B<);>\n"
"B<size_t fwrite_unlocked(const void >I<ptr>B<[restrict .>I<size>B< * .>I<n>B<],>\n"
"B<                      size_t >I<size>B<, size_t >I<n>B<,>\n"
"B<                      FILE *restrict >I<stream>B<);>\n"
msgstr ""
"B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
"B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
#| "B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"
msgid ""
"B<char *fgets_unlocked(char >I<s>B<[restrict .>I<n>B<], int >I<n>B<, FILE *restrict >I<stream>B<);>\n"
"B<int fputs_unlocked(const char *restrict >I<s>B<, FILE *restrict >I<stream>B<);>\n"
msgstr ""
"B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
"B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr "B<#include E<lt>wchar.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int getc_unlocked(FILE *>I<stream>B<);>\n"
#| "B<int getchar_unlocked(void);>\n"
#| "B<int putc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
#| "B<int putchar_unlocked(int >I<c>B<);>\n"
msgid ""
"B<wint_t getwc_unlocked(FILE *>I<stream>B<);>\n"
"B<wint_t getwchar_unlocked(void);>\n"
"B<wint_t fgetwc_unlocked(FILE *>I<stream>B<);>\n"
msgstr ""
"B<int getc_unlocked(FILE *>I<stream>B<);>\n"
"B<int getchar_unlocked(void);>\n"
"B<int putc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
"B<int putchar_unlocked(int >I<c>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int getc_unlocked(FILE *>I<stream>B<);>\n"
#| "B<int getchar_unlocked(void);>\n"
#| "B<int putc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
#| "B<int putchar_unlocked(int >I<c>B<);>\n"
msgid ""
"B<wint_t fputwc_unlocked(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"
"B<wint_t putwc_unlocked(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"
"B<wint_t putwchar_unlocked(wchar_t >I<wc>B<);>\n"
msgstr ""
"B<int getc_unlocked(FILE *>I<stream>B<);>\n"
"B<int getchar_unlocked(void);>\n"
"B<int putc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
"B<int putchar_unlocked(int >I<c>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
#| "B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"
msgid ""
"B<wchar_t *fgetws_unlocked(wchar_t >I<ws>B<[restrict .>I<n>B<], int >I<n>B<,>\n"
"B<                      FILE *restrict >I<stream>B<);>\n"
"B<int fputws_unlocked(const wchar_t *restrict >I<ws>B<,>\n"
"B<                      FILE *restrict >I<stream>B<);>\n"
msgstr ""
"B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
"B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<getc_unlocked>(), B<getchar_unlocked>(), B<putc_unlocked>(), "
"B<putchar_unlocked>():"
msgstr ""
"B<getc_unlocked>(), B<getchar_unlocked>(), B<putc_unlocked>(), "
"B<putchar_unlocked>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "/* Since glibc 2.24: */ _POSIX_C_SOURCE\\ E<gt>=\\ 199309L\n"
#| "    || /* Glibc versions E<lt>= 2.23: */ _POSIX_C_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgid ""
"    /* glibc E<gt>= 2.24: */ _POSIX_C_SOURCE E<gt>= 199309L\n"
"        || /* glibc E<lt>= 2.23: */ _POSIX_C_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"/* начиная с glibc 2.24: */ _POSIX_C_SOURCE\\ E<gt>=\\ 199309L\n"
"    || /* Glibc versions E<lt>= 2.23: */ _POSIX_C_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<clearerr_unlocked>(), B<feof_unlocked>(), B<ferror_unlocked>(), "
"B<fileno_unlocked>(), B<fflush_unlocked>(), B<fgetc_unlocked>(), "
"B<fputc_unlocked>(), B<fread_unlocked>(), B<fwrite_unlocked>():"
msgstr ""
"B<clearerr_unlocked>(), B<feof_unlocked>(), B<ferror_unlocked>(), "
"B<fileno_unlocked>(), B<fflush_unlocked>(), B<fgetc_unlocked>(), "
"B<fputc_unlocked>(), B<fread_unlocked>(), B<fwrite_unlocked>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"        || /* glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<fgets_unlocked>(), B<fputs_unlocked>(), B<getwc_unlocked>(), "
"B<getwchar_unlocked>(), B<fgetwc_unlocked>(), B<fputwc_unlocked>(), "
"B<putwchar_unlocked>(), B<fgetws_unlocked>(), B<fputws_unlocked>():"
msgstr ""
"B<fgets_unlocked>(), B<fputs_unlocked>(), B<getwc_unlocked>(), "
"B<getwchar_unlocked>(), B<fgetwc_unlocked>(), B<fputwc_unlocked>(), "
"B<putwchar_unlocked>(), B<fgetws_unlocked>(), B<fputws_unlocked>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Each of these functions has the same behavior as its counterpart without the "
"\"_unlocked\" suffix, except that they do not use locking (they do not set "
"locks themselves, and do not test for the presence of locks set by others) "
"and hence are thread-unsafe.  See B<flockfile>(3)."
msgstr ""
"Все эти функции ведут себя так же, как и их аналоги без суффикса "
"«_unlocked», за исключением того, что они не используют блокировку (они не "
"устанавливают блокировки самостоятельно и не проверяют наличие других "
"блокировок) и, таким образом, не являются нитебезопасными. Смотрите "
"B<flockfile>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<getc_unlocked>(),\n"
"B<putc_unlocked>(),\n"
"B<clearerr_unlocked>(),\n"
"B<fflush_unlocked>(),\n"
"B<fgetc_unlocked>(),\n"
"B<fputc_unlocked>(),\n"
"B<fread_unlocked>(),\n"
"B<fwrite_unlocked>(),\n"
"B<fgets_unlocked>(),\n"
"B<fputs_unlocked>(),\n"
"B<getwc_unlocked>(),\n"
"B<fgetwc_unlocked>(),\n"
"B<fputwc_unlocked>(),\n"
"B<putwc_unlocked>(),\n"
"B<fgetws_unlocked>(),\n"
"B<fputws_unlocked>()"
msgstr ""
"B<getc_unlocked>(),\n"
"B<putc_unlocked>(),\n"
"B<clearerr_unlocked>(),\n"
"B<fflush_unlocked>(),\n"
"B<fgetc_unlocked>(),\n"
"B<fputc_unlocked>(),\n"
"B<fread_unlocked>(),\n"
"B<fwrite_unlocked>(),\n"
"B<fgets_unlocked>(),\n"
"B<fputs_unlocked>(),\n"
"B<getwc_unlocked>(),\n"
"B<fgetwc_unlocked>(),\n"
"B<fputwc_unlocked>(),\n"
"B<putwc_unlocked>(),\n"
"B<fgetws_unlocked>(),\n"
"B<fputws_unlocked>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe race:stream"
msgstr "MT-Safe race:stream"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<getchar_unlocked>(),\n"
"B<getwchar_unlocked>()"
msgstr ""
"B<getchar_unlocked>(),\n"
"B<getwchar_unlocked>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:stdin"
msgstr "MT-Unsafe race:stdin"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<putchar_unlocked>(),\n"
"B<putwchar_unlocked>()"
msgstr ""
"B<putchar_unlocked>(),\n"
"B<putwchar_unlocked>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:stdout"
msgstr "MT-Unsafe race:stdout"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<feof_unlocked>(),\n"
"B<ferror_unlocked>(),\n"
"B<fileno_unlocked>()"
msgstr ""
"B<feof_unlocked>(),\n"
"B<ferror_unlocked>(),\n"
"B<fileno_unlocked>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getsockname>(2)"
msgid "B<getc_unlocked>()"
msgstr "B<getsockname>(2)"

#. type: TQ
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<getchar_unlocked>(),\n"
#| "B<getwchar_unlocked>()"
msgid "B<getchar_unlocked>()"
msgstr ""
"B<getchar_unlocked>(),\n"
"B<getwchar_unlocked>()"

#. type: TQ
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<fputwc>(3), B<unlocked_stdio>(3)"
msgid "B<putc_unlocked>()"
msgstr "B<fputwc>(3), B<unlocked_stdio>(3)"

#. type: TQ
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<putchar_unlocked>(),\n"
#| "B<putwchar_unlocked>()"
msgid "B<putchar_unlocked>()"
msgstr ""
"B<putchar_unlocked>(),\n"
"B<putwchar_unlocked>()"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<ethers>"
msgid "Others:"
msgstr "B<ethers>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "None"
msgid "None."
msgstr "None"

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#.  E.g., in HP-UX 10.0. In HP-UX 10.30 they are called obsolescent, and
#.  moved to a compatibility library.
#.  Available in HP-UX 10.0: clearerr_unlocked, fclose_unlocked,
#.  feof_unlocked, ferror_unlocked, fflush_unlocked, fgets_unlocked,
#.  fgetwc_unlocked, fgetws_unlocked, fileno_unlocked, fputs_unlocked,
#.  fputwc_unlocked, fputws_unlocked, fread_unlocked, fseek_unlocked,
#.  ftell_unlocked, fwrite_unlocked, getc_unlocked, getchar_unlocked,
#.  getw_unlocked, getwc_unlocked, getwchar_unlocked, putc_unlocked,
#.  putchar_unlocked, puts_unlocked, putws_unlocked, putw_unlocked,
#.  putwc_unlocked, putwchar_unlocked, rewind_unlocked, setvbuf_unlocked,
#.  ungetc_unlocked, ungetwc_unlocked.
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<flockfile>(3), B<stdio>(3)"
msgstr "B<flockfile>(3), B<stdio>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"The four functions B<getc_unlocked>(), B<getchar_unlocked>(), "
"B<putc_unlocked>(), B<putchar_unlocked>()  are in POSIX.1-2001 and "
"POSIX.1-2008."
msgstr ""
"Четыре функции B<getc_unlocked>(), B<getchar_unlocked>(), B<putc_unlocked>() "
"и B<putchar_unlocked>() являются частью стандартов POSIX.1-2001 и "
"POSIX.1-2008."

#.  E.g., in HP-UX 10.0. In HP-UX 10.30 they are called obsolescent, and
#.  moved to a compatibility library.
#.  Available in HP-UX 10.0: clearerr_unlocked, fclose_unlocked,
#.  feof_unlocked, ferror_unlocked, fflush_unlocked, fgets_unlocked,
#.  fgetwc_unlocked, fgetws_unlocked, fileno_unlocked, fputs_unlocked,
#.  fputwc_unlocked, fputws_unlocked, fread_unlocked, fseek_unlocked,
#.  ftell_unlocked, fwrite_unlocked, getc_unlocked, getchar_unlocked,
#.  getw_unlocked, getwc_unlocked, getwchar_unlocked, putc_unlocked,
#.  putchar_unlocked, puts_unlocked, putws_unlocked, putw_unlocked,
#.  putwc_unlocked, putwchar_unlocked, rewind_unlocked, setvbuf_unlocked,
#.  ungetc_unlocked, ungetwc_unlocked.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"The nonstandard B<*_unlocked>()  variants occur on a few UNIX systems, and "
"are available in recent glibc.  They should probably not be used."
msgstr ""
"В некоторых систем UNIX и последних версиях glibc встречаются нестандартные "
"варианты функций B<*_unlocked>(). Вероятно, не стоит их использовать."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "UNLOCKED_STDIO"
msgstr "UNLOCKED_STDIO"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<void clearerr_unlocked(FILE *>I<stream>B<);>\n"
"B<int feof_unlocked(FILE *>I<stream>B<);>\n"
"B<int ferror_unlocked(FILE *>I<stream>B<);>\n"
"B<int fileno_unlocked(FILE *>I<stream>B<);>\n"
"B<int fflush_unlocked(FILE *>I<stream>B<);>\n"
"B<int fgetc_unlocked(FILE *>I<stream>B<);>\n"
"B<int fputc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
"B<size_t fread_unlocked(void *>I<ptr>B<, size_t >I<size>B<, size_t >I<n>B<,>\n"
"B<                      FILE *>I<stream>B<);>\n"
"B<size_t fwrite_unlocked(const void *>I<ptr>B<, size_t >I<size>B<, size_t >I<n>B<,>\n"
"B<                      FILE *>I<stream>B<);>\n"
msgstr ""
"B<void clearerr_unlocked(FILE *>I<stream>B<);>\n"
"B<int feof_unlocked(FILE *>I<stream>B<);>\n"
"B<int ferror_unlocked(FILE *>I<stream>B<);>\n"
"B<int fileno_unlocked(FILE *>I<stream>B<);>\n"
"B<int fflush_unlocked(FILE *>I<stream>B<);>\n"
"B<int fgetc_unlocked(FILE *>I<stream>B<);>\n"
"B<int fputc_unlocked(int >I<c>B<, FILE *>I<stream>B<);>\n"
"B<size_t fread_unlocked(void *>I<ptr>B<, size_t >I<size>B<, size_t >I<n>B<,>\n"
"B<                      FILE *>I<stream>B<);>\n"
"B<size_t fwrite_unlocked(const void *>I<ptr>B<, size_t >I<size>B<, size_t >I<n>B<,>\n"
"B<                      FILE *>I<stream>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
"B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"
msgstr ""
"B<char *fgets_unlocked(char *>I<s>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
"B<int fputs_unlocked(const char *>I<s>B<, FILE *>I<stream>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<wint_t getwc_unlocked(FILE *>I<stream>B<);>\n"
"B<wint_t getwchar_unlocked(void);>\n"
"B<wint_t fgetwc_unlocked(FILE *>I<stream>B<);>\n"
"B<wint_t fputwc_unlocked(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"
"B<wint_t putwc_unlocked(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"
"B<wint_t putwchar_unlocked(wchar_t >I<wc>B<);>\n"
"B<wchar_t *fgetws_unlocked(wchar_t *>I<ws>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
"B<int fputws_unlocked(const wchar_t *>I<ws>B<, FILE *>I<stream>B<);>\n"
msgstr ""
"B<wint_t getwc_unlocked(FILE *>I<stream>B<);>\n"
"B<wint_t getwchar_unlocked(void);>\n"
"B<wint_t fgetwc_unlocked(FILE *>I<stream>B<);>\n"
"B<wint_t fputwc_unlocked(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"
"B<wint_t putwc_unlocked(wchar_t >I<wc>B<, FILE *>I<stream>B<);>\n"
"B<wint_t putwchar_unlocked(wchar_t >I<wc>B<);>\n"
"B<wchar_t *fgetws_unlocked(wchar_t *>I<ws>B<, int >I<n>B<, FILE *>I<stream>B<);>\n"
"B<int fputws_unlocked(const wchar_t *>I<ws>B<, FILE *>I<stream>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"/* Since glibc 2.24: */ _POSIX_C_SOURCE\\ E<gt>=\\ 199309L\n"
"    || /* Glibc versions E<lt>= 2.23: */ _POSIX_C_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"/* начиная с glibc 2.24: */ _POSIX_C_SOURCE\\ E<gt>=\\ 199309L\n"
"    || /* Glibc versions E<lt>= 2.23: */ _POSIX_C_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"/* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"/* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версиях glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
