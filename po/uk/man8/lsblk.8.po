# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-07-25 19:41+0200\n"
"PO-Revision-Date: 2022-06-25 22:44+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "LSBLK"
msgstr "LSBLK"

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "2022-08-04"
msgstr "4 серпня 2022 року"

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "lsblk - list block devices"
msgstr "lsblk — виведення списку блокових пристроїв"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<lsblk> [options] [I<device>...]"
msgstr "B<lsblk> [параметри] [I<пристрій>...]"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<lsblk> lists information about all available or the specified block "
"devices. The B<lsblk> command reads the B<sysfs> filesystem and B<udev db> "
"to gather information. If the udev db is not available or B<lsblk> is "
"compiled without udev support, then it tries to read LABELs, UUIDs and "
"filesystem types from the block device. In this case root permissions are "
"necessary."
msgstr ""
"B<lsblk> виводить список відомостей про усі доступні або вказані блокові "
"пристрої. Команда B<lsblk> читає файлову систему B<sysfs> і B<базу даних "
"udev> для збирання відомостей. Якщо база даних udev є недоступною або "
"B<lsblk> зібрано без підтримки udev, програма намагається прочитати з "
"блокового пристрою LABEL, UUID та типи файлових систем. У цьому випадку "
"обов'язковими є права доступу root."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The command prints all block devices (except RAM disks) in a tree-like "
"format by default. Use B<lsblk --help> to get a list of all available "
"columns."
msgstr ""
"Команда типово виводить усі блокові пристрої (окрім дисків в оперативній "
"пам'яті) у деревоподібному форматі. Скористайтеся командою B<lsblk --help>, "
"щоб ознайомитися зі списком усіх доступних стовпчиків."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The default output, as well as the default output from options like B<--fs> "
"and B<--topology>, is subject to change. So whenever possible, you should "
"avoid using default outputs in your scripts. Always explicitly define "
"expected columns by using B<--output> I<columns-list> and B<--list> in "
"environments where a stable output is required."
msgstr ""
"Типовий формат виведення, а також типовий формат виведення для параметрів, "
"подібних до B<--fs> та B<--topology>, може бути змінено авторами програми. "
"Тому, коли це можливо, вам слід уникати обробки типових виведених даних у "
"ваших скриптах. Завжди явно визначайте очікувані стовпчики за допомогою "
"параметра B<--output> I<список-стовпчиків> разом зі списком стовпчиків у "
"середовищах, де потрібне виведення стабільного набору даних."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Note that B<lsblk> might be executed in time when B<udev> does not have all "
"information about recently added or modified devices yet. In this case it is "
"recommended to use B<udevadm settle> before B<lsblk> to synchronize with "
"udev."
msgstr ""
"Зауважте, що B<lsblk> може бути запущено у момент, коли B<udev> не має усіх "
"даних щодо нещодавно доданих або змінених пристроїв. У цьому випадку "
"рекомендуємо скористатися командою B<udevadm settle> до команди B<lsblk>, "
"щоб синхронізувати дані udev."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The relationship between block devices and filesystems is not always one-to-"
"one. The filesystem may use more block devices, or the same filesystem may "
"be accessible by more paths. This is the reason why B<lsblk> provides "
"MOUNTPOINT and MOUNTPOINTS (pl.) columns. The column MOUNTPOINT displays "
"only one mount point (usually the last mounted instance of the filesystem), "
"and the column MOUNTPOINTS displays by multi-line cell all mount points "
"associated with the device."
msgstr ""
"Зв'язок між блоковими пристроями і файловими системами не завжди є "
"однозначним. Для файлової системи може бути використано декілька блокових "
"пристроїв, а одна файлова система може бути доступною за декількома шляхами. "
"Ось чому B<lsblk> показує стовпчики MOUNTPOINT та MOUNTPOINTS (множина). У "
"стовпчику MOUNTPOINT буде показано лише одну точку монтування (зазвичай, "
"останній змонтований екземпляр файлової системи), а у стовпчику MOUNTPOINTS "
"буде показано багаторядкову комірку усіх точок монтування, які пов'язано із "
"пристроєм."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<-A>, B<--noempty>"
msgstr "B<-A>, B<--noempty>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Don\\(cqt print empty devices."
msgstr "Не виводити даних для порожніх пристроїв."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"Disable all built-in filters and list all empty devices and RAM disk devices "
"too."
msgstr ""
"Вимкнути усі вбудовані фільтри і вивести список усіх порожніх пристроїв, а "
"також дискових пристрої в оперативній пам'яті."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-b>, B<--bytes>"
msgstr "B<-b>, B<--bytes>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr "Вивести розмір у байтах, а не у зручному для читання форматі."

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""
"Типово, одиницею, у якій показано розміри, є байт, а префікси одиниць є "
"степенями 2^10 (1024). Для забезпечення зручності читання відбувається "
"скорочення позначень до першої літери запису; приклади: «1 КіБ» та «1 МіБ» "
"буде показано як «1 K» та «1 M», із вилученням «іБ», яке є частиною цих "
"скорочень."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-D>, B<--discard>"
msgstr "B<-D>, B<--discard>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Print information about the discarding capabilities (TRIM, UNMAP) for each "
"device."
msgstr ""
"Вивести відомості щодо можливостей відкидання (TRIM, UNMAP) для кожного з "
"пристроїв."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-d>, B<--nodeps>"
msgstr "B<-d>, B<--nodeps>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Do not print holder devices or slaves. For example, B<lsblk --nodeps /dev/"
"sda> prints information about the sda device only."
msgstr ""
"Не виводити пристрої-утримувачі або підлеглі пристрої. Наприклад, *lsblk --"
"nodeps /dev/sda* виведе дані лише щодо пристрою sda."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-E>, B<--dedup> I<column>"
msgstr "B<-E>, B<--dedup> I<стовпчик>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Use I<column> as a de-duplication key to de-duplicate output tree. If the "
"key is not available for the device, or the device is a partition and "
"parental whole-disk device provides the same key than the device is always "
"printed."
msgstr ""
"Скористатися I<стовпчиком> як ключем для усування дублікатів в ієрархії "
"виведених даних. Якщо ключ виявиться недоступним для пристрою або пристрій "
"буде розділом і батьківський пристрій усього диска надають той самий ключ, "
"буде завжди виведено дані пристрою."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The usual use case is to de-duplicate output on system multi-path devices, "
"for example by B<-E WWN>."
msgstr ""
"Звичайним використанням є усування дублікатів з виведення для системних "
"пристроїв із багатьма шляхами, наприклад, за допомогою B<-E WWN>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-e>, B<--exclude> I<list>"
msgstr "B<-e>, B<--exclude> I<список>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Exclude the devices specified by the comma-separated I<list> of major device "
"numbers. Note that RAM disks (major=1) are excluded by default if B<--all> "
"is not specified. The filter is applied to the top-level devices only. This "
"may be confusing for B<--list> output format where hierarchy of the devices "
"is not obvious."
msgstr ""
"Виключити пристрої, які вказано у I<списку> номерів основних пристроїв, які "
"відокремлено комами. Зауважте, що диски в оперативній пам'яті (основний=1) "
"типово виключено, якщо не вказано B<--all>. Фільтр буде застосовано лише до "
"пристроїв верхнього рівня. Це може заплутати показ у форматі виведення B<--"
"list>, де визначення ієрархії пристроїв не є очевидним."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-f>, B<--fs>"
msgstr "B<-f>, B<--fs>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Output info about filesystems. This option is equivalent to B<-o NAME,FSTYPE,"
"FSVER,LABEL,UUID,FSAVAIL,FSUSE%,MOUNTPOINTS>. The authoritative information "
"about filesystems and raids is provided by the B<blkid>(8) command."
msgstr ""
"Вивести дані щодо файлових систем. Цей параметр є еквівалентним до B<-o NAME,"
"FSTYPE,FSVER,LABEL,UUID,FSAVAIL,FSUSE%,MOUNTPOINTS>. Надійні дані щодо "
"файлових систем та RAID буде надано командою B<blkid>(8)."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-I>, B<--include> I<list>"
msgstr "B<-I>, B<--include> I<список>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Include devices specified by the comma-separated I<list> of major device "
"numbers. The filter is applied to the top-level devices only. This may be "
"confusing for B<--list> output format where hierarchy of the devices is not "
"obvious."
msgstr ""
"Включити пристрої, які вказано у I<списку> номерів основних пристроїв, які "
"відокремлено комами. Фільтр буде застосовано лише до пристроїв верхнього "
"рівня. Це може заплутати показ у форматі виведення B<--list>, де визначення "
"ієрархії пристроїв не є очевидним."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-i>, B<--ascii>"
msgstr "B<-i>, B<--ascii>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Use ASCII characters for tree formatting."
msgstr "Використати символи ASCII для форматування ієрархії."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-J>, B<--json>"
msgstr "B<-J>, B<--json>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Use JSON output format. It\\(cqs strongly recommended to use B<--output> and "
"also B<--tree> if necessary."
msgstr ""
"Скористатися форматом виведення JSON. Наполегливо рекомендуємо скористатися "
"B<--output>, а також B<--tree>, якщо потрібно."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-l>, B<--list>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Produce output in the form of a list. The output does not provide "
"information about relationships between devices and since version 2.34 every "
"device is printed only once if B<--pairs> or B<--raw> not specified (the "
"parsable outputs are maintained in backwardly compatible way)."
msgstr ""
"Вивести дані у форматі списку. У виведених даних не буде надано відомостей "
"щодо взаємозв'язок між пристроями, а починаючи з версії 2.34, запис кожного "
"пристрою буде виведено лише раз, якщо не вказано B<--pairs> або B<--raw> "
"(супровід придатного до обробки виведення передбачено лише для зворотної "
"сумісності)."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-M>, B<--merge>"
msgstr "B<-M>, B<--merge>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Group parents of sub-trees to provide more readable output for RAIDs and "
"Multi-path devices. The tree-like output is required."
msgstr ""
"Згрупувати батьківські записи підлеглих ієрархій з метою створення "
"зручнішого для читання виведення для RAID і пристроїв із багатьма шляхами. "
"Потрібне деревоподібне виведення."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-m>, B<--perms>"
msgstr "B<-m>, B<--perms>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Output info about device owner, group and mode. This option is equivalent to "
"B<-o NAME,SIZE,OWNER,GROUP,MODE>."
msgstr ""
"Вивести дані щодо власника, групи і режиму доступу до пристрою. Цей параметр "
"еквівалентний до  B<-o NAME,SIZE,OWNER,GROUP,MODE>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-n>, B<--noheadings>"
msgstr "B<-n>, B<--noheadings>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Do not print a header line."
msgstr "Не виводити рядок заголовка."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-o>, B<--output> I<list>"
msgstr "B<-o>, B<--output> I<список>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns. The columns may affect tree-like output. The default is "
"to use tree for the column \\(aqNAME\\(aq (see also B<--tree>)."
msgstr ""
"Визначити, які стовпчики слід використовувати для виведення. Скористайтеся "
"параметром B<--help>, щоб переглянути список підтримуваних стовпчиків*). "
"Вибрані стовпчики можуть впливати на деревоподібне виведення. Типовим "
"варіантом є використання ієрархії для стовпчика «NAME» (див. також B<--"
"tree>)."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format I<+list> (e.g., B<lsblk -o +UUID>)."
msgstr ""
"Типовий список стовпчиків може бути розширено, якщо I<список> вказано у "
"форматі I<+список> (наприклад, B<lsblk -o +UUID>)"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-O>, B<--output-all>"
msgstr "B<-O>, B<--output-all>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Output all available columns."
msgstr "Вивести список усіх доступних стовпчиків."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-P>, B<--pairs>"
msgstr "B<-P>, B<--pairs>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"Produce output in the form of key=\"value\" pairs. The output lines are "
"still ordered by dependencies. All potentially unsafe value characters are "
"hex-escaped (\\(rsxE<lt>codeE<gt>). See also option B<--shell>."
msgstr ""
"Вивести дані у формі пар ключ=\"значення\". Рядки виведення буде "
"упорядковано за залежностями. Усі потенційно небезпечні символи значень буде "
"екрановано (\\(rsxE<lt>кодE<gt>). Див. також параметр B<--shell>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-p>, B<--paths>"
msgstr "B<-p>, B<--paths>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Print full device paths."
msgstr "Виводити шляхи до пристроїв повністю."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-r>, B<--raw>"
msgstr "B<-r>, B<--raw>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Produce output in raw format. The output lines are still ordered by "
"dependencies. All potentially unsafe characters are hex-escaped "
"(\\(rsxE<lt>codeE<gt>) in the NAME, KNAME, LABEL, PARTLABEL and MOUNTPOINT "
"columns."
msgstr ""
"Вивести дані у необробленому форматі. Рядки виведення буде упорядковано за "
"залежностями. Усі потенційно небезпечні символи значень буде екрановано "
"(\\(rsxE<lt>кодE<gt>) у стовпчиках NAME, KNAME, LABEL, PARTLABEL і "
"MOUNTPOINT."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-S>, B<--scsi>"
msgstr "B<-S>, B<--scsi>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Output info about SCSI devices only. All partitions, slaves and holder "
"devices are ignored."
msgstr ""
"Вивести дані лише щодо пристроїв SCSI. Усі розділи, допоміжні пристрої та "
"пристрої утримання буде проігноровано."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-s>, B<--inverse>"
msgstr "B<-s>, B<--inverse>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Print dependencies in inverse order. If the B<--list> output is requested "
"then the lines are still ordered by dependencies."
msgstr ""
"Вивести залежності в оберненому порядку. Якщо буде надіслано запит щодо "
"виведення B<--list>, рядки все одно буде упорядковано за залежностями."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-T>, B<--tree>[B<=>I<column>]"
msgstr "B<-T>, B<--tree>[B<=>I<стовпчик>]"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Force tree-like output format. If I<column> is specified, then a tree is "
"printed in the column. The default is NAME column."
msgstr ""
"Примусово використати деревоподібний формат виведення. Якщо вказано "
"I<стовпчик>, ієрархію буде виведено у стовпчик. Типовим є стовпчик NAME."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-t>, B<--topology>"
msgstr "B<-t>, B<--topology>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Output info about block-device topology. This option is equivalent to"
msgstr ""
"Вивести дані щодо топології блокового пристрою. Цей параметр є еквівалентним "
"до такого"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<-o NAME,ALIGNMENT,MIN-IO,OPT-IO,PHY-SEC,LOG-SEC,ROTA,SCHED,RQ-SIZE,RA,"
"WSAME>."
msgstr ""
"B<-o NAME,ALIGNMENT,MIN-IO,OPT-IO,PHY-SEC,LOG-SEC,ROTA,SCHED,RQ-SIZE,RA,"
"WSAME>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-w>, B<--width> I<number>"
msgstr "B<-w>, B<--width> I<число>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Specifies output width as a number of characters. The default is the number "
"of the terminal columns, and if not executed on a terminal, then output "
"width is not restricted at all by default. This option also forces B<lsblk> "
"to assume that terminal control characters and unsafe characters are not "
"allowed. The expected use-case is for example when B<lsblk> is used by the "
"B<watch>(1) command."
msgstr ""
"Вказує ширину виведення у кількості символів. Типовою шириною є кількість "
"стовпчиків у терміналі, а якщо програму запущено не у терміналі, ширину "
"виведення типово взагалі не обмежено. Цей параметр примушує B<lsblk> до "
"припущення, що не можна використовувати символи керування терміналом та "
"небезпечні символи. Прикладом застосування є використання B<lsblk> командою "
"B<watch>(1)."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-x>, B<--sort> I<column>"
msgstr "B<-x>, B<--sort> I<стовпчик>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Sort output lines by I<column>. This option enables B<--list> output format "
"by default. It is possible to use the option B<--tree> to force tree-like "
"output and than the tree branches are sorted by the I<column>."
msgstr ""
"Упорядкувати рядки виведення за I<стовпчиком>. Цей параметр типово вмикає "
"формат виведення B<--list>. Можна скористатися параметром B<--tree> для "
"примусового використання деревоподібного виведення, а гілки дерева буде "
"упорядковано за I<стовпчиком>."

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<-y>, B<--shell>"
msgstr "B<-y>, B<--shell>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"The column name will be modified to contain only characters allowed for "
"shell variable identifiers, for example, MIN_IO and FSUSE_PCT instead of MIN-"
"IO and FSUSE%. This is usable, for example, with B<--pairs>. Note that this "
"feature has been automatically enabled for B<--pairs> in version 2.37, but "
"due to compatibility issues, now it\\(cqs necessary to request this behavior "
"by B<--shell>."
msgstr ""
"Назву стовпчика буде змінено так, щоб вона містила лише символи, які є "
"дозволеними для ідентифікаторів змінних командної оболонки, наприклад, "
"MINI_IO і FSUSE_PCT замість MIN-IO і FSUSE%. Це корисно, зокрема, з B<--"
"pairs>. Зауважте, що цю можливість було автоматично увімкнено для B<--pairs> "
"у версії 2.37, але через проблеми із сумісністю у нових версіях потрібно "
"викликати таку поведінку за допомогою параметра B<--shell>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-z>, B<--zoned>"
msgstr "B<-z>, B<--zoned>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print the zone related information for each device."
msgstr "Вивести пов'язані із зоною дані для кожного пристрою."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<--sysroot> I<directory>"
msgstr "B<--sysroot> I<каталог>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Gather data for a Linux instance other than the instance from which the "
"B<lsblk> command is issued. The specified directory is the system root of "
"the Linux instance to be inspected. The real device nodes in the target "
"directory can be replaced by text files with udev attributes."
msgstr ""
"Зібрати дані для екземпляра Linux, відмінного від екземпляра, з якого "
"запущено команду B<lsblk>. Вказаний каталог є кореневим каталогом системи "
"екземпляра Linux, ревізію якого слід виконати. Справжні вузли пристроїв у "
"каталозі призначення може бути замінено текстовими файлами з атрибутами udev."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "EXIT STATUS"
msgstr "СТАН ВИХОДУ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "0"
msgstr "0"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "success"
msgstr "успіх"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "1"
msgstr "1"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "failure"
msgstr "невдача"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "32"
msgstr "32"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "none of specified devices found"
msgstr "не знайдено вказаних пристроїв"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "64"
msgstr "64"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "some specified devices found, some not found"
msgstr "знайдено якісь із вказаних пристроїв, деякі не знайдено"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "ENVIRONMENT"
msgstr "СЕРЕДОВИЩЕ"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LSBLK_DEBUG>=all"
msgstr "B<LSBLK_DEBUG>=all"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "enables B<lsblk> debug output."
msgstr "вмикає діагностичне виведення у B<lsblk>."

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LIBBLKID_DEBUG>=all"
msgstr "B<LIBBLKID_DEBUG>=all"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "enables B<libblkid> debug output."
msgstr "вмикає показ діагностичних повідомлень B<libblkid>."

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LIBMOUNT_DEBUG>=all"
msgstr "B<LIBMOUNT_DEBUG>=all"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "enables B<libmount> debug output."
msgstr "вмикає показ діагностичних повідомлень B<libmount>."

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LIBSMARTCOLS_DEBUG>=all"
msgstr "B<LIBSMARTCOLS_DEBUG>=all"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "enables B<libsmartcols> debug output."
msgstr "вмикає показ діагностичних повідомлень B<libsmartcols>."

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LIBSMARTCOLS_DEBUG_PADDING>=on"
msgstr "B<LIBSMARTCOLS_DEBUG_PADDING>=on"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "use visible padding characters."
msgstr "використати символи візуального доповнення."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "ПРИМІТКИ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"For partitions, some information (e.g., queue attributes) is inherited from "
"the parent device."
msgstr ""
"Для розділів деякі дані (наприклад атрибути черги) успадковуються від "
"батьківського пристрою."

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"The B<lsblk> command needs to be able to look up each block device by major:"
"minor numbers, which is done by using I</sys/dev/block>. This sysfs block "
"directory appeared in kernel 2.6.27 (October 2008). In case of problems with "
"a new enough kernel, check that B<CONFIG_SYSFS> was enabled at the time of "
"the kernel build."
msgstr ""
"Команда B<lsblk> повинна мати можливість шукати усі блокові пристрої за "
"числами основний:підлеглий. Це завдання виконується за допомогою I</sys/dev/"
"block>. Цей блоковий каталог sysfs з'явився у ядрі 2.6.27 (жовтень 2008 "
"року). Якщо виникнуть проблеми із достатньо новим ядром, перевірте, чи було "
"увімкнено B<CONFIG_SYSFS> під час збирання ядра."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<ls>(1), B<blkid>(8), B<findmnt>(8)"
msgstr "B<ls>(1), B<blkid>(8), B<findmnt>(8)"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<lsblk> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<lsblk> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Also list empty devices and RAM disk devices."
msgstr ""
"Також вивести список усіх порожніх пристроїв і дискових пристроїв в "
"оперативній пам'яті."

#. type: Plain text
#: opensuse-leap-15-5
msgid "Print the SIZE column in bytes rather than in a human-readable format."
msgstr ""
"Вивести стовпчик розміру у байтах, а не у зручному для читання форматі."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Produce output in the form of key=\"value\" pairs. The output lines are "
"still ordered by dependencies. All potentially unsafe value characters are "
"hex-escaped (\\(rsxE<lt>codeE<gt>). The key (variable name) will be modified "
"to contain only characters allowed for a shell variable identifiers, for "
"example, MIN_IO and FSUSE_PCT instead of MIN-IO and FSUSE%."
msgstr ""
"Вивести дані у формі пар ключ=\"значення\". Рядки виведення буде "
"упорядковано за залежностями. Усі потенційно небезпечні символи значень буде "
"екрановано (\\(rsxE<lt>кодE<gt>). Ключ (назву змінної) буде змінено так, щоб "
"він містив лише символи, які є дозволеними для ідентифікаторів змінних "
"командної оболонки, наприклад, MINI_IO і FSUSE_PCT замість MIN-IO і FSUSE%."

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: Plain text
#: opensuse-leap-15-5
msgid "Print the zone model for each device."
msgstr "Вивести пов'язані із зоною дані для кожного пристрою."

#. type: Plain text
#: opensuse-leap-15-5
msgid "LSBLK_DEBUG=all"
msgstr "LSBLK_DEBUG=all"

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBBLKID_DEBUG=all"
msgstr "LIBBLKID_DEBUG=all"

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBMOUNT_DEBUG=all"
msgstr "LIBMOUNT_DEBUG=all"

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBSMARTCOLS_DEBUG=all"
msgstr "LIBSMARTCOLS_DEBUG=all"

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBSMARTCOLS_DEBUG_PADDING=on"
msgstr "LIBSMARTCOLS_DEBUG_PADDING=on"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<lsblk> command needs to be able to look up each block device by major:"
"minor numbers, which is done by using I</sys/dev/block>. This sysfs block "
"directory appeared in kernel 2.6.27 (October 2008). In case of problems with "
"a new enough kernel, check that CONFIG_SYSFS was enabled at the time of the "
"kernel build."
msgstr ""
"Команда B<lsblk> повинна мати можливість шукати усі блокові пристрої за "
"числами основний:підлеглий. Це завдання виконується за допомогою I</sys/dev/"
"block>. Цей блоковий каталог sysfs з'явився у ядрі 2.6.27 (жовтень 2008 "
"року). Якщо виникнуть проблеми із достатньо новим ядром, перевірте, чи було "
"увімкнено B<CONFIG_SYSFS> під час збирання ядра."
