# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-07-25 19:26+0200\n"
"PO-Revision-Date: 2022-07-31 18:48+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "BLKZONE"
msgstr "BLKZONE"

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr "Керування системою"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "blkzone - run zone command on a device"
msgstr "blkzone — запуск команди зони на пристрої"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<blkzone> I<command> [options] I<device>"
msgstr "B<blkzone> I<команда> [параметри] I<пристрій>"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<blkzone> is used to run zone command on device that support the Zoned "
"Block Commands (ZBC) or Zoned-device ATA Commands (ZAC). The zones to "
"operate on can be specified using the offset, count and length options."
msgstr ""
"B<blkzone> використовують для запуску команд обробки зон на пристрої, де "
"передбачено підтримку Zoned Block Commands (ZBC) або Zoned-device ATA "
"Commands (ZAC). Зони, з якими слід працювати, може бути вказано за допомогою "
"параметрів відступу, кількості та довжину."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "The I<device> argument is the pathname of the block device."
msgstr "Аргумент I<пристрій> визначає шлях до блокового пристрою."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "COMMANDS"
msgstr "КОМАНДИ"

#. type: SS
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "report"
msgstr "report"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The command B<blkzone report> is used to report device zone information."
msgstr ""
"Команду B<blkzone report> використовують для створення звітів щодо зон "
"пристрою."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"By default, the command will report all zones from the start of the block "
"device. Options may be used to modify this behavior, changing the starting "
"zone or the size of the report, as explained below."
msgstr ""
"Типово, команда повідомить про усі зони від початку блокового пристрою. "
"Параметрами можна скористатися для внесення змін до цієї поведінки, зміни "
"початкової зони або розмір звіту, як це описано нижче."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Report output:"
msgstr "Виведення звіту:"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid ".sp\n"
msgstr ".sp\n"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "start"
msgstr "start"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Zone start sector"
msgstr "Початковий сектор зони"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "len"
msgstr "len"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Zone length in number of sectors"
msgstr "Довжина зони у секторах"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "cap"
msgstr "cap"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Zone capacity in number of sectors"
msgstr "Місткість зони у кількості секторів"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "wptr"
msgstr "wptr"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Zone write pointer position"
msgstr "Позиція вказівника запису зони"

#. type: SS
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "reset"
msgstr "reset"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Reset write pointer recommended"
msgstr "Рекомендоване скидання вказівника запису"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "non-seq"
msgstr "non-seq"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Non-sequential write resources active"
msgstr "Активними є ресурси непослідовного запису"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "cond"
msgstr "cond"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Zone condition"
msgstr "Стан зони"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "type"
msgstr "type"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Zone type"
msgstr "Тип зони"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Zone conditions:"
msgstr "Стани зони:"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "cl"
msgstr "cl"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Closed"
msgstr "Закрита"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "nw"
msgstr "nw"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Not write pointer"
msgstr "Не є вказівником запису"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "em"
msgstr "em"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Empty"
msgstr "Порожня"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "fu"
msgstr "fu"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Full"
msgstr "Повна"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "oe"
msgstr "oe"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Explicitly opened"
msgstr "Явно відкрита"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "oi"
msgstr "oi"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Implicitly opened"
msgstr "Неявно відкрита"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "ol"
msgstr "ol"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Offline"
msgstr "Не працює"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "ro"
msgstr "ro"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Read only"
msgstr "Лише читання"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "x?"
msgstr "x?"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Reserved conditions (should not be reported)"
msgstr "Зарезервовані стани (не має повідомлятися)"

#. type: SS
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "capacity"
msgstr "capacity"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The command B<blkzone capacity> is used to report device capacity "
"information."
msgstr ""
"Команду B<blkzone capacity> використовують для створення звітів щодо "
"місткості."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"By default, the command will report the sum, in number of sectors, of all "
"zone capacities on the device. Options may be used to modify this behavior, "
"changing the starting zone or the size of the report, as explained below."
msgstr ""
"Типово, команда повідомляє про суму, у кількості секторів, усіх можливостей "
"зон на пристрої. Параметрами можна скористатися для внесення змін до цієї "
"поведінки, зміни початкової зони або розмір звіту, як це описано нижче."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The command B<blkzone reset> is used to reset one or more zones. Unlike "
"B<sg_reset_wp>(8), this command operates from the block layer and can reset "
"a range of zones."
msgstr ""
"Команду B<blkzone reset> використовують для скидання однієї або декількох "
"зон. На відміну від B<sg_reset_wp>(8), ця команда працює з шару блоків та "
"може скидати діапазон зон."

#. type: SS
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "open"
msgstr "open"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The command B<blkzone open> is used to explicitly open one or more zones. "
"Unlike B<sg_zone>(8), open action, this command operates from the block "
"layer and can open a range of zones."
msgstr ""
"Команду B<blkzone open> використовують для явного відкриття однієї або "
"декількох зон. На відміну від B<sg_zone>(), дії з відкриття, ця команда "
"працює з шару блоків та може відкривати діапазон зон."

#. type: SS
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "close"
msgstr "close"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The command B<blkzone close> is used to close one or more zones. Unlike "
"B<sg_zone>(8), close action, this command operates from the block layer and "
"can close a range of zones."
msgstr ""
"Команду B<blkzone close> використовують для закриття однієї або декількох "
"зон. На відміну від B<sg_zone>(), дії з закриття, ця команда працює з шару "
"блоків та може закривати діапазон зон."

#. type: SS
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "finish"
msgstr "finish"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The command B<blkzone finish> is used to finish (transition to full "
"condition) one or more zones. Unlike B<sg_zone>(8), finish action, this "
"command operates from the block layer and can finish a range of zones."
msgstr ""
"Команду B<blkzone finish> використовують для завершення (переведення до "
"умови повноти) однієї або декількох зон. На відміну від B<sg_zone>(), дії з "
"завершення, ця команда працює з шару блоків та може завершувати діапазон зон."

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"By default, the B<reset>, B<open>, B<close> and B<finish> commands will "
"operate from the zone at device sector 0 and operate on all zones. Options "
"may be used to modify this behavior as explained below."
msgstr ""
"Типово, команди B<reset>, B<open>, B<close> і B<finish> працюватимуть з зони "
"на секторі 0 пристрою і з усіма зонами. Параметрами можна скористатися для "
"внесення змін до цієї поведінки, як це пояснено нижче."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The I<offset> and I<length> option arguments may be followed by the "
"multiplicative suffixes KiB (=1024), MiB (=1024*1024), and so on for GiB, "
"TiB, PiB, EiB, ZiB and YiB (the \"iB\" is optional, e.g., \"K\" has the same "
"meaning as \"KiB\") or the suffixes KB (=1000), MB (=1000*1000), and so on "
"for GB, TB, PB, EB, ZB and YB. Additionally, the 0x prefix can be used to "
"specify I<offset> and I<length> in hex."
msgstr ""
"Після аргументів I<відступ> і I<довжина> можна додавати суфікси одиниць KiB "
"(=1024), MiB (=1024*1024) тощо для GiB, TiB, PiB, EiB, ZiB та YiB (частина "
"«iB» є необов'язковою, наприклад, «K» є тим самим, що і «KiB») або суфікси "
"KB (=1000), MB (=1000*1000) тощо для GB, TB, PB, EB, ZB і YB. Крім того, "
"префіксом 0x можна скористатися для визначення I<відступу> і I<довжини> у "
"форматі шістнадцяткових чисел."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-o>, B<--offset> I<sector>"
msgstr "B<-o>, B<--offset> I<сектор>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The starting zone specified as a sector offset. The provided offset in "
"sector units (512 bytes) should match the start of a zone. The default value "
"is zero."
msgstr ""
"Початкова зона, яку вказано як відступ у секторах. Заданий відступ в "
"одиницях секторів (512 байтів) має відповідати початку зони. Типовим є "
"нульове значення."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-l>, B<--length> I<sectors>"
msgstr "B<-l>, B<--length> I<сектори>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The maximum number of sectors the command should operate on. The default "
"value is the number of sectors remaining after I<offset>. This option cannot "
"be used together with the option B<--count>."
msgstr ""
"Максимальна кількість секторів, з якими має працювати команда. Типовим є "
"значення кількості секторів, які лишаються після I<відступу>. Цим параметром "
"не можна користуватися у поєднанні із параметром B<--count>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-c>, B<--count> I<count>"
msgstr "B<-c>, B<--count> I<кількість>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The maximum number of zones the command should operate on. The default value "
"is the number of zones starting from I<offset>. This option cannot be used "
"together with the option B<--length>."
msgstr ""
"Максимальна кількість зон, з якими має працювати команда. Типовим є значення "
"кількості зон, починаючи зі значення I<відступу>. Цим параметром не можна "
"користуватися у поєднанні із параметром B<--length>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-f>, B<--force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Enforce commands to change zone status on block devices used by the system."
msgstr ""
"Наказати командам змінювати стан зон на блокових пристроях, які використано "
"системою."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Display the number of zones returned in the report or the range of sectors "
"reset."
msgstr ""
"Вивести кількість зон, які повернуто у звіті, або діапазон скинутих секторів."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<sg_rep_zones>(8)"
msgstr "B<sg_rep_zones>(8)"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<blkzone> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<blkzone> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"By default, the reset, open, close and finish commands will operate from the "
"zone at device sector 0 and operate on all zones. Options may be used to "
"modify this behavior as explained below."
msgstr ""
"Типово, команди B<reset>, B<open>, B<close> і B<finish> працюватимуть з зони "
"на секторі 0 пристрою і з усіма зонами. Параметрами можна скористатися для "
"внесення змін до цієї поведінки, як це пояснено нижче."

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."
