# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andriy Rysin <arysin@gmail.com>, 2022.
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-07-25 19:42+0200\n"
"PO-Revision-Date: 2022-06-23 17:27+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "MCOOKIE"
msgstr "MCOOKIE"

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr "11 травня 2022 року"

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr "Команди користувача"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "mcookie - generate magic cookies for xauth"
msgstr "mcookie — створення контрольних кук для xauth"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<mcookie> [options]"
msgstr "B<mcookie> [параметри]"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<mcookie> generates a 128-bit random hexadecimal number for use with the X "
"authority system. Typical usage:"
msgstr ""
"B<mcookie> породжує 128-бітові випадкове шістнадцяткове число для "
"використання у системі уповноваження графічного сервера X. Типове "
"використання:"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<xauth add :0 . >\\f(CRmcookie\\fR"
msgstr "B<xauth add :0 . >\\f(CRmcookie\\fR"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"\\f(CRThe \"random\" number generated is actually the MD5 message digest of "
"random information coming from one of the sources B<getrandom>\\f(CR(2) "
"system call, I</dev/urandom>\\f(CR, I</dev/random>\\f(CR, or the I<libc "
"pseudo-random functions>\\f(CR, in this preference order. See also the "
"option B<--file>\\f(CR.\\fR"
msgstr ""
"«Випадкове» число, насправді, буде породжено на основі контрольної суми "
"повідомлення MD5 на основі даних, що походять з системного виклику "
"B<getrandom>(2), I</dev/urandom>, I</dev/random>, або I<функцій породження "
"псевдовипадкових чисел libc>, саме у такому порядку. Див. також параметр B<--"
"file>."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "ПАРАМЕТРИ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-f>, B<--file> I<file>"
msgstr "B<-f>, B<--file> I<файл>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Use this I<file> as an additional source of randomness (for example I</dev/"
"urandom>). When I<file> is \\(aq-\\(aq, characters are read from standard "
"input."
msgstr ""
"Скористатися цим I<файлом> як додатковим джерелом випадковості (наприклад, "
"I</dev/urandom>). Якщо аргументом I<файл> є «-», символи буде прочитано зі "
"стандартного джерела вхідних даних."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-m>, B<--max-size> I<number>"
msgstr "B<-m>, B<--max-size> I<число>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Read from I<file> only this I<number> of bytes. This option is meant to be "
"used when reading additional randomness from a file or device."
msgstr ""
"Прочитати з I<файла> лише вказане I<число> байтів. Цей параметр призначено "
"для використання при читанні додаткових даних випадковості з файла або "
"пристрою."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The I<number> argument may be followed by the multiplicative suffixes "
"KiB=1024, MiB=1024*1024, and so on for GiB, TiB, PiB, EiB, ZiB and YiB (the "
"\"iB\" is optional, e.g., \"K\" has the same meaning as \"KiB\") or the "
"suffixes KB=1000, MB=1000*1000, and so on for GB, TB, PB, EB, ZB and YB."
msgstr ""
"Після аргументу I<число> можна додавати суфікси одиниць KiB (=1024), MiB "
"(=1024*1024) тощо для GiB, TiB, PiB, EiB, ZiB та YiB (частина «iB» є "
"необов'язковою, наприклад, «K» є тим самим, що і «KiB») або суфікси KB "
"(=1000), MB (=1000*1000) тощо для GB, TB, PB, EB, ZB і YB."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Inform where randomness originated, with amount of entropy read from each "
"source."
msgstr ""
"Вивести дані щодо походження випадковості із величиною ентропії, прочитаною "
"з кожного джерела."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr "Вивести текст довідки і завершити роботу."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print version and exit."
msgstr "Вивести дані щодо версії і завершити роботу."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "FILES"
msgstr "ФАЙЛИ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "I</dev/urandom>"
msgstr "I</dev/urandom>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "I</dev/random>"
msgstr "I</dev/random>"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "BUGS"
msgstr "ВАДИ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "It is assumed that none of the randomness sources will block."
msgstr ""
"У програмі зроблено припущення, що жодне із джерел випадковості не "
"блокується."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<md5sum>(1), B<X>(7), B<xauth>(1), B<rand>(3)"
msgstr "B<md5sum>(1), B<X>(7), B<xauth>(1), B<rand>(3)"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<mcookie> command is part of the util-linux package which can be "
"downloaded from"
msgstr "B<mcookie> є частиною пакунка util-linux, який можна отримати з"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14 лютого 2022 року"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Вивести дані щодо версії і завершити роботу."
