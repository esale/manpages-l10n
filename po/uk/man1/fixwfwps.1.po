# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Andrij Mizyk <andm1zyk@proton.me>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:27+0200\n"
"PO-Revision-Date: 2022-09-24 23:25+0300\n"
"Last-Translator: Andrij Mizyk <andm1zyk@proton.me>\n"
"Language-Team: Ukrainian\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FIXWFWPS"
msgstr "FIXWFWPS"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Release 1 Patchlevel 17"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "fixwfwps - filter to fix Word for Windows documents so PSUtils work"
msgstr ""
"fixwfwps - фільтр для виправлення документів Word для Windows, щоб працювали "
"з PSUtils"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<fixwfwps> E<lt> I<WordforWindows.ps> E<gt> I<Fixed.ps>"
msgstr "B<fixwfwps> E<lt> I<WordforWindows.ps> E<gt> I<Fixed.ps>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"I<Fixwfwps> is a I<perl> filter which \"fixes\" PostScript from Word for "
"Windows so that it works correctly with Angus Duggan's B<psutils> package."
msgstr ""
"I<Fixwfwps> — це фільтр на I<perl>, що \"виправляє\" PostScript із Word для "
"Windows, щоб він коректно працював із пакунком B<psutils> від Angus Duggan."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "АВТОР"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Авторські права (C) Angus J. C. Duggan 1991-1995"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"B<psbook>(1), B<psselect>(1), B<pstops>(1), B<epsffit>(1), B<psnup>(1), "
"B<psresize>(1), B<psmerge>(1), B<fixscribeps>(1), B<getafm>(1), "
"B<fixdlsrps>(1), B<fixfmps>(1), B<fixpsditps>(1), B<fixpspps>(1), "
"B<fixtpps>(1), B<fixwfwps>(1), B<fixwpps>(1), B<fixwwps>(1), "
"B<extractres>(1), B<includeres>(1), B<showchar>(1)"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "TRADEMARKS"
msgstr "ТОРГОВА МАРКА"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr "B<PostScript> є торговою маркою Adobe Systems Incorporated."
