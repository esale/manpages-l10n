# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:35+0200\n"
"PO-Revision-Date: 2022-07-13 20:17+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "LIBBLKID"
msgstr "LIBBLKID"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-08-04"
msgstr "4 серпня 2022 року"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Programmer\\(aqs Manual"
msgstr "Підручник програміста"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "libblkid - block device identification library"
msgstr "libblkid — бібліотека ідентифікації блокових пристроїв"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "B<#include E<lt>blkid.hE<gt>>"
msgstr "B<#include E<lt>blkid.hE<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "B<cc> I<file.c> B<-lblkid>"
msgstr "B<cc> I<file.c> B<-lblkid>"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"The B<libblkid> library is used to identify block devices (disks) as to "
"their content (e.g., filesystem type) as well as extracting additional "
"information such as filesystem labels/volume names, unique identifiers/"
"serial numbers. A common use is to allow use of B<LABEL=> and B<UUID=> tags "
"instead of hard-coding specific block device names into configuration files."
msgstr ""
"Бібліотеку B<libblkid> використовують для ідентифікації блокових пристроїв "
"(дисків) за їхнім вмістом (наприклад, типом файлової системи), а також "
"видобування додаткових відомостей, зокрема міток або назв томів файлової "
"системи, унікальних ідентифікаторів та серійних номерів. Типовим "
"використанням є уможливлення використання міток B<LABEL=>і B<UUID=> замість "
"жорсткого вписування назв певних блокових пристроїв до файлів налаштувань."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"The low-level part of the library also allows the extraction of information "
"about partitions and block device topology."
msgstr ""
"Низькорівнева частина бібліотеки також уможливлює видобування відомостей "
"щодо розділів і топології блокового пристрою."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"The high-level part of the library keeps information about block devices in "
"a cache file and is verified to still be valid before being returned to the "
"user (if the user has read permission on the raw block device, otherwise "
"not). The cache file also allows unprivileged users (normally anyone other "
"than root, or those not in the \"disk\" group) to locate devices by label/"
"id. The standard location of the cache file can be overridden by the "
"environment variable B<BLKID_FILE>."
msgstr ""
"Високорівнева частина бібліотеки зберігає відомості щодо блокових пристроїв "
"у файлі кешу і перевіряє чинність даних до повернення їх користувачеві (якщо "
"користувач має права доступу до читання простого блокового пристрою, інакше "
"не перевіряє). Файл кешу також уможливлює для непривілейованих користувачів "
"(зазвичай, це користувачі, відмінні від root, або користувачі поза групою "
"«disk») шукати пристрої за міткою або ідентифікатором. Стандартне місце "
"файла кешу можна перевизначити за допомогою змінної середовища B<BLKID_FILE>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"In situations where one is getting information about a single known device, "
"it does not impact performance whether the cache is used or not (unless you "
"are not able to read the block device directly)."
msgstr ""
"У ситуаціях, де користувач програми отримує відомості щодо окремого відомого "
"пристрою, використання кешу не впливає значно на швидкодію (якщо ви не "
"можете читати з блокового пристрою  безпосередньо)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"The high-level part of the library supports two methods to determine B<LABEL/"
"UUID>. It reads information directly from a block device or read information "
"from /dev/disk/by-* udev symlinks. The udev is preferred method by default."
msgstr ""
"У високорівневій частині бібліотеки передбачено підтримку двох методів "
"визначення B<LABEL/UUID>. Вона читає відомості безпосередньо з блокового "
"пристрою або читає відомості з символічних посилань udev /dev/disk/by-*. "
"Типово, пріоритетним є метод з udev."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"If you are dealing with multiple devices, use of the cache is highly "
"recommended (even if empty) as devices will be scanned at most one time and "
"the on-disk cache will be updated if possible."
msgstr ""
"Якщо ви маєте справу із декількома пристроями, наполегливо рекомендуємо "
"користуватися кешем (навіть якщо він порожній), оскільки сканування "
"пристроїв буде виконуватися не більше одного разу, а кеш на диску буде "
"оновлюватися, якщо це можливо."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"In some cases (modular kernels), block devices are not even visible until "
"after they are accessed the first time, so it is critical that there is some "
"way to locate these devices without enumerating only visible devices, so the "
"use of the cache file is B<required> in this situation."
msgstr ""
"У деяких випадках (модульні ядра), блокові пристрої не є навіть видимими аж "
"до першого доступу, тому критичним є існування певного способу пошуку цих "
"пристроїв без нумерації лише видимих пристроїв, тому використання файла кешу "
"у цьому випадку є *обов'язковим*."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "CONFIGURATION FILE"
msgstr "ФАЙЛ НАЛАШТУВАНЬ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"The standard location of the I</etc/blkid.conf> config file can be "
"overridden by the environment variable B<BLKID_CONF>. For more details about "
"the config file see B<blkid>(8) man page."
msgstr ""
"Стандартне розташування файла налаштувань I</etc/blkid.conf> можна "
"перевизначити змінною середовища B<BLKID_CONF>. Щоб дізнатися більше про "
"файл налаштувань, ознайомтеся зі сторінкою підручника B<blkid>(8)."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr "АВТОРИ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"B<libblkid> was written by Andreas Dilger for the ext2 filesystem utilities, "
"with input from Ted Ts\\(cqo. The library was subsequently heavily modified "
"by Ted Ts\\(cqo."
msgstr ""
"Бібліотеку B<libblkid> було написано Andreas Dilger для допоміжних "
"інструментів файлової системи ext2 на основі вхідних даних від Ted Ts'o. "
"Потім бібліотеку було значно переписано Ted Ts'o."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "The low-level probing code was rewritten by Karel Zak."
msgstr "Низькорівневий код зондування було переписано Karel Zak."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "COPYING"
msgstr "КОПІЮВАННЯ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"B<libblkid> is available under the terms of the GNU Library General Public "
"License (LGPL), version 2 (or at your discretion any later version)."
msgstr ""
"Розповсюдження B<libblkid> відбувається за умов дотримання GNU Library "
"General Public License (LGPL) версії 2 (або, якщо хочете, будь-якої пізнішої "
"версії)."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "B<blkid>(8), B<findfs>(8)"
msgstr "B<blkid>(8), B<findfs>(8)"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ЗВІТИ ПРО ВАДИ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "For bug reports, use the issue tracker at"
msgstr "Для звітування про вади використовуйте систему стеження помилками на"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "ДОСТУПНІСТЬ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"The B<libblkid> library is part of the util-linux package since version "
"2.15. It can be downloaded from"
msgstr ""
"B<libblkid> є частиною пакунка util-linux, починаючи з версії 2.15. Пакунок "
"можна отримати з"
