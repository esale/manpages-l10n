# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
# Juan Piernas <piernas@ditec.um.es>, 1998.
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:54+0200\n"
"PO-Revision-Date: 2005-01-21 19:55+0200\n"
"Last-Translator: Miguel Pérez Ibars <mpi79470@alu.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<system>()"
msgid "system"
msgstr "B<system>()"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "system - execute a shell command"
msgstr "system - ejecuta una orden del intérprete de órdenes (shell)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int system(const char *>I<command>B<);>\n"
msgstr "B<int system(const char *>I<command>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<system>()  library function behaves as if it used B<fork>(2)  to "
"create a child process that executed the shell command specified in "
"I<command> using B<execl>(3)  as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "execl(\"/bin/sh\", \"sh\", \"-c\", command, (char *) NULL);\n"
msgstr "execl(\"/bin/sh\", \"sh\", \"-c\", command, (char *) NULL);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<system>()  returns after the command has been completed."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"During execution of the command, B<SIGCHLD> will be blocked, and B<SIGINT> "
"and B<SIGQUIT> will be ignored, in the process that calls B<system>().  "
"(These signals will be handled according to their defaults inside the child "
"process that executes I<command>.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the value of I<string> is B<NULL>, B<system()> returns nonzero if the "
#| "shell is available, and zero if not."
msgid ""
"If I<command> is NULL, then B<system>()  returns a status indicating whether "
"a shell is available on the system."
msgstr ""
"Si el valor de I<string> es B<NULL>, B<system()> devuelve un número distinto "
"de cero si hay un intérprete de órdenes disponible, y cero si no."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The return value of B<system>()  is one of the following:"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<command> is NULL, then a nonzero value if a shell is available, or 0 if "
"no shell is available."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If a child process could not be created, or its status could not be "
"retrieved, the return value is -1 and I<errno> is set to indicate the error."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If a shell could not be executed in the child process, then the return value "
"is as though the child shell terminated by calling B<_exit>(2)  with the "
"status 127."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If all system calls succeed, then the return value is the termination status "
"of the child shell used to execute I<command>.  (The termination status of a "
"shell is the termination status of the last command it executes.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In the last two cases, the return value is a \"wait status\" that can be "
"examined using the macros described in B<waitpid>(2).  (i.e., "
"B<WIFEXITED>(), B<WEXITSTATUS>(), and so on)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<system>()  does not affect the wait status of any other children."
msgstr ""
"B<system>() no afecta al estado de espera de cualquier otro proceso hijo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<system>()  can fail with any of the same errors as B<fork>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<system>()"
msgstr "B<system>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, C99."
msgid "POSIX.1-2001, C89."
msgstr "POSIX.1-2001, C99."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<system>()  provides simplicity and convenience: it handles all of the "
"details of calling B<fork>(2), B<execl>(3), and B<waitpid>(2), as well as "
"the necessary manipulations of signals; in addition, the shell performs the "
"usual substitutions and I/O redirections for I<command>.  The main cost of "
"B<system>()  is inefficiency: additional system calls are required to create "
"the process that runs the shell and to execute the shell."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the B<_XOPEN_SOURCE> feature test macro is defined (before including "
"I<any> header files), then the macros described in B<waitpid>(2)  "
"(B<WEXITSTATUS>(), etc.) are made available when including I<E<lt>stdlib."
"hE<gt>>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"As mentioned, B<system>()  ignores B<SIGINT> and B<SIGQUIT>.  This may make "
"programs that call it from a loop uninterruptible, unless they take care "
"themselves to check the exit status of the child.  For example:"
msgstr ""
"Como se mencionó, B<system>() ignora B<SIGINT> y B<SIGQUIT>. Esto puede "
"hacer que los programas que invocan a esta función desde un bucle sean "
"ininterrumpibles, a menos que se preocupen ellos mismos de comprobar el "
"estado de salida del hijo. P.e."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"while (something) {\n"
"    int ret = system(\"foo\");\n"
msgstr ""
"while (something) {\n"
"    int ret = system(\"foo\");\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (WIFSIGNALED(ret) &&\n"
"        (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))\n"
"            break;\n"
"}\n"
msgstr ""
"    if (WIFSIGNALED(ret) &&\n"
"        (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))\n"
"            break;\n"
"}\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"According to POSIX.1, it is unspecified whether handlers registered using "
"B<pthread_atfork>(3)  are called during the execution of B<system>().  In "
"the glibc implementation, such handlers are not called."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Before glibc 2.1.3, the check for the availability of I</bin/sh> was not "
"actually performed if I<command> was NULL; instead it was always assumed to "
"be available, and B<system>()  always returned 1 in this case.  Since glibc "
"2.1.3, this check is performed because, even though POSIX.1-2001 requires a "
"conforming implementation to provide a shell, that shell may not be "
"available or executable if the calling program has previously called "
"B<chroot>(2)  (which is not specified by POSIX.1-2001)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"It is possible for the shell command to terminate with a status of 127, "
"which yields a B<system>()  return value that is indistinguishable from the "
"case where a shell could not be executed in the child process."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Caveats"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Do not use B<system>()  from a privileged program (a set-user-ID or set-"
"group-ID program, or a program with capabilities)  because strange values "
"for some environment variables might be used to subvert system integrity.  "
"For example, B<PATH> could be manipulated so that an arbitrary program is "
"executed with privilege.  Use the B<exec>(3)  family of functions instead, "
"but not B<execlp>(3)  or B<execvp>(3)  (which also use the B<PATH> "
"environment variable to search for an executable)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<system>()  will not, in fact, work properly from programs with set-user-ID "
"or set-group-ID privileges on systems on which I</bin/sh> is bash version 2: "
"as a security measure, bash 2 drops privileges on startup.  (Debian uses a "
"different shell, B<dash>(1), which does not do this when invoked as B<sh>.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Any user input that is employed as part of I<command> should be I<carefully> "
"sanitized, to ensure that unexpected shell commands or command options are "
"not executed.  Such risks are especially grave when using B<system>()  from "
"a privileged program."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#.  [BUG 211029](https://bugzilla.kernel.org/show_bug.cgi?id=211029)
#.  [glibc bug](https://sourceware.org/bugzilla/show_bug.cgi?id=27143)
#.  [POSIX bug](https://www.austingroupbugs.net/view.php?id=1440)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If the command name starts with a hyphen, B<sh>(1)  interprets the command "
"name as an option, and the behavior is undefined.  (See the B<-c> option to "
"B<sh>(1).)  To work around this problem, prepend the command with a space as "
"in the following call:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    system(\" -unfortunate-command-name\");\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<sh>(1), B<execve>(2), B<fork>(2), B<sigaction>(2), B<sigprocmask>(2), "
"B<wait>(2), B<exec>(3), B<signal>(7)"
msgstr ""
"B<sh>(1), B<execve>(2), B<fork>(2), B<sigaction>(2), B<sigprocmask>(2), "
"B<wait>(2), B<exec>(3), B<signal>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "SYSTEM"
msgstr "SYSTEM"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<system>()  library function uses B<fork>(2)  to create a child process "
"that executes the shell command specified in I<command> using B<execl>(3)  "
"as follows:"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "execl(\"/bin/sh\", \"sh\", \"-c\", command, (char *) NULL);\n"
msgid "    execl(\"/bin/sh\", \"sh\", \"-c\", command, (char *) 0);\n"
msgstr "execl(\"/bin/sh\", \"sh\", \"-c\", command, (char *) NULL);\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"During execution of the command, B<SIGCHLD> will be blocked, and B<SIGINT> "
"and B<SIGQUIT> will be ignored, in the process that calls B<system>()  "
"(these signals will be handled according to their defaults inside the child "
"process that executes I<command>)."
msgstr ""

#. type: IP
#: opensuse-leap-15-5
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"If a child process could not be created, or its status could not be "
"retrieved, the return value is -1."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: Plain text
#: opensuse-leap-15-5
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"In versions of glibc before 2.1.3, the check for the availability of I</bin/"
"sh> was not actually performed if I<command> was NULL; instead it was always "
"assumed to be available, and B<system>()  always returned 1 in this case.  "
"Since glibc 2.1.3, this check is performed because, even though POSIX.1-2001 "
"requires a conforming implementation to provide a shell, that shell may not "
"be available or executable if the calling program has previously called "
"B<chroot>(2)  (which is not specified by POSIX.1-2001)."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."
