# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Juan Piernas <piernas@ditec.um.es>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:53+0200\n"
"PO-Revision-Date: 2000-04-23 19:55+0200\n"
"Last-Translator: Juan Piernas <piernas@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<stpncpy>()"
msgid "stpncpy"
msgstr "B<stpncpy>()"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"stpncpy, strncpy - zero a fixed-width buffer and copy a string into a "
"character sequence with truncation and zero the rest of it"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid ""
"B<char *strncpy(char >I<dst>B<[restrict .>I<sz>B<], const char *restrict >I<src>B<,>\n"
"B<               size_t >I<sz>B<);>\n"
"B<char *stpncpy(char >I<dst>B<[restrict .>I<sz>B<], const char *restrict >I<src>B<,>\n"
"B<               size_t >I<sz>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<stpncpy>():"
msgstr "B<stpncpy>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Desde glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Antes de glibc 2.10:\n"
"        _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These functions copy the string pointed to by I<src> into a null-padded "
"character sequence at the fixed-width buffer pointed to by I<dst>.  If the "
"destination buffer, limited by its size, isn't large enough to hold the "
"copy, the resulting character sequence is truncated.  For the difference "
"between the two functions, see RETURN VALUE."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "An implementation of these functions might be:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *stpncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>\n"
msgid ""
"char *\n"
"strncpy(char *restrict dst, const char *restrict src, size_t sz)\n"
"{\n"
"    stpncpy(dst, src, sz);\n"
"    return dst;\n"
"}\n"
msgstr "B<char *stpncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"char *\n"
"stpncpy(char *restrict dst, const char *restrict src, size_t sz)\n"
"{\n"
"    bzero(dst, sz);\n"
"    return mempcpy(dst, src, strnlen(src, sz));\n"
"}\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<stpncpy>()"
msgid "B<strncpy>()"
msgstr "B<stpncpy>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<return> [I<n>]"
msgid "returns I<dst>."
msgstr "B<return> [I<n>]"

#. #-#-#-#-#  archlinux: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-38: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-5: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<stpncpy>()"
msgstr "B<stpncpy>()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"returns a pointer to one after the last character in the destination "
"character sequence."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<strncpy>(3), B<wcpncpy>(3)"
msgid ""
"B<stpncpy>(),\n"
"B<strncpy>()"
msgstr "B<strncpy>(3), B<wcpncpy>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. #-#-#-#-#  archlinux: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before that, it was a GNU extension.
#.  It first appeared in glibc 1.07 in 1993.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before that, it was a GNU extension.
#.  It first appeared in glibc 1.07 in 1993.
#. type: Plain text
#. #-#-#-#-#  fedora-38: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before that, it was a GNU extension.
#.  It first appeared in glibc 1.07 in 1993.
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Before that, it was a GNU extension.
#.  It first appeared in glibc 1.07 in 1993.
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgid "C89, POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "glibc 2.1.  C99, POSIX.1-2001."
msgid "glibc 1.07.  POSIX.1-2008."
msgstr "glibc 2.1.  C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The name of these functions is confusing.  These functions produce a null-"
"padded character sequence, not a string (see B<string_copying>(7))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"It's impossible to distinguish truncation by the result of the call, from a "
"character sequence that just fits the destination buffer; truncation should "
"be detected by comparing the length of the input string with the size of the "
"destination buffer."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If you're going to use this function in chained calls, it would be useful to "
"develop a similar function that accepts a pointer to the end (one after the "
"last element) of the destination buffer instead of its size."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#include E<lt>stdio.hE<gt>>\n"
#| "B<#include E<lt>sys/types.hE<gt>>\n"
#| "B<#include E<lt>pwd.hE<gt>>\n"
msgid ""
"#include E<lt>err.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"B<#include E<lt>stdio.hE<gt>>\n"
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    char    *p;\n"
"    char    buf1[20];\n"
"    char    buf2[20];\n"
"    size_t  len;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (sizeof(buf2) E<lt> strlen(\"Hello world!\"))\n"
"        warnx(\"strncpy: truncating character sequence\");\n"
"    strncpy(buf2, \"Hello world!\", sizeof(buf2));\n"
"    len = strnlen(buf2, sizeof(buf2));\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    printf(\"[len = %zu]: \", len);\n"
"    printf(\"%.*s\\en\", (int) len, buf2);  // \"Hello world!\"\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (sizeof(buf1) E<lt> strlen(\"Hello world!\"))\n"
"        warnx(\"stpncpy: truncating character sequence\");\n"
"    p = stpncpy(buf1, \"Hello world!\", sizeof(buf1));\n"
"    len = p - buf1;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    printf(\"[len = %zu]: \", len);\n"
"    printf(\"%.*s\\en\", (int) len, buf1);  // \"Hello world!\"\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: stpncpy.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<strncpy>(3), B<wcpncpy>(3)"
msgid "B<wcpncpy>(3), B<string_copying>(7)"
msgstr "B<strncpy>(3), B<wcpncpy>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-01-26"
msgstr "26 Enero 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid ""
"B<char *stpncpy(char >I<dst>B<[restrict .>I<sz>B<], const char *restrict >I<src>B<,>\n"
"B<               size_t >I<sz>B<);>\n"
"B<char *strncpy(char >I<dst>B<[restrict .>I<sz>B<], const char *restrict >I<src>B<,>\n"
"B<               size_t >I<sz>B<);>\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "STPNCPY"
msgstr "STPNCPY"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-03-15"
msgstr "15 Marzo 2016"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "stpncpy - copy a fixed-size string, returning a pointer to its end"
msgstr ""
"stpncpy - copia una cadena de tamaño fijo, devolviendo un puntero a su final"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<char *stpncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>\n"
msgstr "B<char *stpncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>\n"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Desde glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-5
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Antes de glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-5
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "The B<stpncpy> function copies at most I<n> characters from the string "
#| "pointed to by I<src>, including the terminating '\\e0' character, to the "
#| "array pointed to by I<dest>. Exactly I<n> characters are written at "
#| "I<dest>. If the length I<strlen(src)> is smaller than I<n>, the remaining "
#| "characters in the array pointed to by I<dest> are filled with '\\e0' "
#| "characters. If the length I<strlen(src)> is greater or equal to I<n>, the "
#| "string pointed to by I<dest> will not be '\\e0' terminated."
msgid ""
"The B<stpncpy>()  function copies at most I<n> characters from the string "
"pointed to by I<src>, including the terminating null byte (\\(aq\\e0\\(aq), "
"to the array pointed to by I<dest>.  Exactly I<n> characters are written at "
"I<dest>.  If the length I<strlen(src)> is smaller than I<n>, the remaining "
"characters in the array pointed to by I<dest> are filled with null bytes "
"(\\(aq\\e0\\(aq), If the length I<strlen(src)> is greater than or equal to "
"I<n>, the string pointed to by I<dest> will not be null-terminated."
msgstr ""
"La función B<stpncpy> copia, como mucho, I<n> caracteres de la cadena "
"apuntada por I<src>, incluyendo el carácter terminador '\\e0', al array "
"apuntado por I<dest>. Se escriben exactamente I<n> caracteres en I<dest>. Si "
"la longitud I<strlen(src)> es más pequeña que I<n>, los restantes caracteres "
"del array apuntado por I<dest> se rellenan con caracteres '\\e0'. Si la "
"longitud I<strlen(src)> es mayor o igual que I<n>, la cadena apuntada por "
"I<dest> no terminará en '\\e0'."

#. type: Plain text
#: opensuse-leap-15-5
msgid "The strings may not overlap."
msgstr "Las cadenas no se pueden solapar."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The programmer must ensure that there is room for at least I<n> characters "
"at I<dest>."
msgstr ""
"El programador debe garantizar que haya espacio para, al menos, I<n> "
"caracteres en I<dest>."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "B<stpncpy> returns a pointer to the terminating null in I<dest>, or, if "
#| "I<dest> is not null-terminated, I<dest + n>."
msgid ""
"B<stpncpy>()  returns a pointer to the terminating null byte in I<dest>, or, "
"if I<dest> is not null-terminated, I<dest>+I<n>."
msgstr ""
"B<stpncpy> devuelve un puntero al carácter nulo que termina la cadena "
"I<dest> o, si I<dest> no termina en nulo, I<dest + n>."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This function was added to POSIX.1-2008.  Before that, it was a GNU "
"extension.  It first appeared in version 1.07 of the GNU C library in 1993."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<strncpy>(3), B<wcpncpy>(3)"
msgstr "B<strncpy>(3), B<wcpncpy>(3)"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."
