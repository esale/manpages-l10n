# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Frédéric Zulian <zulian@free.fr>, 2006.
# Grégory Colpart <reg@evolix.fr>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006, 2007.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Florentin Duneau <fduneau@gmail.com>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006, 2007.
# Florentin Duneau <fduneau@gmail.com>, 2008-2010.
# David Prévot <david@tilapin.org>, 2010-2013.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra\n"
"POT-Creation-Date: 2022-10-03 15:49+0200\n"
"PO-Revision-Date: 2023-03-23 09:35-0100\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.4\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "PWDX"
msgstr "PWDX"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "June 2011"
msgstr "Juin 2011"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr "Commandes de l'utilisateur"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: opensuse-leap-15-5
msgid "pwdx - report current working directory of a process"
msgstr "pwdx - Afficher le répertoire de travail d'un processus"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<pwdx> [I<options>] I<pid> [...]"
msgstr "B<pwdx> [I<options>] I<PID> [...]"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Output version information and exit."
msgstr "afficher les informations de version et quitter."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Output help screen and exit."
msgstr "Afficher un écran d'aide puis quitter."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<ps>(1), B<pgrep>(1)"
msgstr "B<ps>(1), B<pgrep>(1)"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: opensuse-leap-15-5
msgid "No standards apply, but pwdx looks an awful lot like a SunOS command."
msgstr ""
"Aucune norme n'est respectée mais B<pwdx> ressemble fortement à une commande "
"SunOS."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: opensuse-leap-15-5
msgid "E<.UR nmiell@gmail.com> Nicholas Miell E<.UE> wrote pwdx in 2004."
msgstr "Nicholas Miell E<lt>I<nmiell@gmail.com>E<gt> a écrit pwdx en 2004."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Merci d'envoyer un rapport de bogue à E<.UR procps@freelists.org> E<.UE>"
