# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-06-27 19:54+0200\n"
"PO-Revision-Date: 2023-01-15 10:40+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sysfs"
msgstr "sysfs"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "sysfs - get filesystem type information"
msgstr "sysfs - Obtenir des informations sur les types de systèmes de fichiers"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] int sysfs(int >I<option>B<, const char *>I<fsname>B<);>\n"
"B<[[deprecated]] int sysfs(int >I<option>B<, unsigned int >I<fs_index>B<, char *>I<buf>B<);>\n"
"B<[[deprecated]] int sysfs(int >I<option>B<);>\n"
msgstr ""
"B<[[obsolète]] int sysfs(int >I<option>B<, const char *>I<fsname>B<);>\n"
"B<[[obsolète]] int sysfs(int >I<option>B<, unsigned int >I<fs_index>B<, char *>I<buf>B<);>\n"
"B<[[obsolète]] int sysfs(int >I<option>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<Note>: if you are looking for information about the B<sysfs> filesystem "
"that is normally mounted at I</sys>, see B<sysfs>(5)."
msgstr ""
"B<Note> : si vous recherchez des informations sur le système de fichiers "
"B<sysfs> qui est normalement monté sur I</sys>, consultez B<sysfs>(5)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The (obsolete)  B<sysfs>()  system call returns information about the "
"filesystem types currently present in the kernel.  The specific form of the "
"B<sysfs>()  call and the information returned depends on the I<option> in "
"effect:"
msgstr ""
"L'appel système (obsolète) B<sysfs>() renvoie des informations concernant "
"les types de systèmes de fichiers actuellement connus par le noyau. La forme "
"particulière de l'appel à B<sysfs>() et les informations renvoyées dépendent "
"dans les faits de l'argument I<option>\\ :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<1>"
msgstr "B<1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Translate the filesystem identifier string I<fsname> into a filesystem type "
"index."
msgstr ""
"Traduit l'identifiant textuel du système de fichiers I<fsname> en un index."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<2>"
msgstr "B<2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Translate the filesystem type index I<fs_index> into a null-terminated "
"filesystem identifier string.  This string will be written to the buffer "
"pointed to by I<buf>.  Make sure that I<buf> has enough space to accept the "
"string."
msgstr ""
"Traduit l'index de type de système de fichiers I<fs_index> en une chaîne de "
"caractères terminée par NULL. La chaîne sera écrite dans le tampon pointé "
"par I<buf>. Assurez-vous que I<buf> contient suffisamment de place pour la "
"chaîne entière."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<3>"
msgstr "B<3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Return the total number of filesystem types currently present in the kernel."
msgstr ""
"Renvoie le nombre total de types de systèmes de fichiers reconnus par le "
"noyau."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The numbering of the filesystem type indexes begins with zero."
msgstr "La numérotation des index de systèmes de fichiers commence à zéro."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, B<sysfs>()  returns the filesystem index for option B<1>, zero "
"for option B<2>, and the number of currently configured filesystems for "
"option B<3>.  On error, -1 is returned, and I<errno> is set to indicate the "
"error."
msgstr ""
"S'il réussit B<sysfs>() renvoie l'index du système de fichiers pour l'option "
"B<1>, zéro pour l'option B<2> et le nombre de systèmes de fichiers connus "
"pour l'option B<3>. En cas d'erreur, B<-1> est renvoyé et I<errno> est "
"défini pour préciser l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Either I<fsname> or I<buf> is outside your accessible address space."
msgstr ""
"I<fsname> ou I<buf> pointent en dehors de l'espace d'adressage accessible."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<fsname> is not a valid filesystem type identifier; I<fs_index> is out-of-"
"bounds; I<option> is invalid."
msgstr ""
"I<fsname> n'est pas un identifiant de système de fichiers. I<fs_index> est "
"hors limites. I<option> n'est pas valable."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "SVr4."
msgstr "SVr4."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This System-V derived system call is obsolete; don't use it.  On systems "
"with I</proc>, the same information can be obtained via I</proc>; use that "
"interface instead."
msgstr ""
"Cet appel système dérivé de System V est obsolète, ne l'utilisez pas. Sur "
"des systèmes avec I</proc>, la même information peut être obtenue avec I</"
"proc> ; utilisez plutôt cette interface."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"There is no libc or glibc support.  There is no way to guess how large "
"I<buf> should be."
msgstr ""
"Il n'y a pas de prise en charge dans la libc ou la glibc. On ne peut pas "
"savoir de quelle taille doit être I<buf>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<proc>(5), B<sysfs>(5)"
msgstr "B<proc>(5), B<sysfs>(5)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "SYSFS"
msgstr "SYSFS"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int sysfs(int >I<option>B<, const char *>I<fsname>B<);>"
msgstr "B<int sysfs(int >I<option>B<, const char *>I<fsname>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<int sysfs(int >I<option>B<, unsigned int >I<fs_index>B<, char *>I<buf>B<);>"
msgstr ""
"B<int sysfs(int >I<option>B<, unsigned int >I<fs_index>B<, char *>I<buf>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int sysfs(int >I<option>B<);>"
msgstr "B<int sysfs(int >I<option>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, B<sysfs>()  returns the filesystem index for option B<1>, zero "
"for option B<2>, and the number of currently configured filesystems for "
"option B<3>.  On error, -1 is returned, and I<errno> is set appropriately."
msgstr ""
"S'il réussit B<sysfs>() renvoie l'index du système de fichiers pour l'option "
"B<1>, zéro pour l'option B<2>, et le nombre de systèmes de fichiers connus "
"pour l'option B<3>. En cas d'erreur, B<-1> est renvoyé et I<errno> contient "
"le code d'erreur."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This System-V derived system call is obsolete; don't use it.  On systems "
"with I</proc>, the same information can be obtained via I</proc/"
"filesystems>; use that interface instead."
msgstr ""
"Cet appel système dérivé de System V est obsolète, ne l'utilisez pas. Sur "
"des systèmes avec I</proc>, la même information peut être obtenue avec I</"
"proc/filesystems> ; utilisez plutôt cette interface."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
