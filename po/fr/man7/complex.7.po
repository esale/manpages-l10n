# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999,2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006
# Denis Barbier <barbier@debian.org>, 2006,2010.
# David Prévot <david@tilapin.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-06-27 19:23+0200\n"
"PO-Revision-Date: 2023-06-16 17:08+0200\n"
"Last-Translator: Thomas Vincent <tvincent@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 1.8.11\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "complex"
msgstr "complex"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "complex - basics of complex mathematics"
msgstr "complex - Bases des mathématiques concernant les nombres complexes"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>complex.hE<gt>>\n"
msgstr "B<#include E<lt>complex.hE<gt>>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Complex numbers are numbers of the form z = a+b*i, where a and b are real "
"numbers and i = sqrt(-1), so that i*i = -1."
msgstr ""
"Les nombres complexes sont des nombres de la forme z = a+b*i, où a et b sont "
"des nombres réels et i = sqrt(-1), de sorte que i*i = -1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"There are other ways to represent that number.  The pair (a,b) of real "
"numbers may be viewed as a point in the plane, given by X- and Y-"
"coordinates.  This same point may also be described by giving the pair of "
"real numbers (r,phi), where r is the distance to the origin O, and phi the "
"angle between the X-axis and the line Oz.  Now z = r*exp(i*phi) = "
"r*(cos(phi)+i*sin(phi))."
msgstr ""
"Il y a d'autres manières de représenter ce nombre. La paire de nombres réels "
"(a, b) peut être vue comme un point du plan, indiqué par ses coordonnées en "
"X et Y. Le même point peut aussi être décrit par une paire de nombres réels "
"(r, phi), où r est la distance à l'origine O et phi l'angle entre l'axe des "
"X et la ligne Oz. Ce qui donne z = r*exp(i*phi) = r*(cos(phi)+i*sin(phi))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The basic operations are defined on z = a+b*i and w = c+d*i as:"
msgstr ""
"Les opérations de base sur z = a+b*i et w = c+d*i sont définies comme suit :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<addition: z+w = (a+c) + (b+d)*i>"
msgstr "B<addition : z+w = (a+c) + (b+d)*i>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<multiplication: z*w = (a*c - b*d) + (a*d + b*c)*i>"
msgstr "B<multiplication : z*w = (a*c - b*d) + (a*d + b*c)*i>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<division: z/w = ((a*c + b*d)/(c*c + d*d)) + ((b*c - a*d)/(c*c + d*d))*i>"
msgstr "B<division : z/w = ((a*c + b*d)/(c*c + d*d)) + ((b*c - a*d)/(c*c + d*d))*i>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Nearly all math function have a complex counterpart but there are some "
"complex-only functions."
msgstr ""
"Presque toutes les fonctions mathématiques ont leur équivalent complexe, "
"mais il existe aussi des fonctions qui ne s'appliquent qu'aux nombres "
"complexes."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Your C-compiler can work with complex numbers if it supports the C99 "
"standard.  Link with I<-lm>.  The imaginary unit is represented by I."
msgstr ""
"Votre compilateur C peut traiter les nombres complexes s'il est conforme à "
"la norme C99. Effectuez l'édition de liens avec I<-lm>. La partie imaginaire "
"est représentée par I."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"/* check that exp(i * pi) == -1 */\n"
"#include E<lt>math.hE<gt>        /* for atan */\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>complex.hE<gt>\n"
msgstr ""
"/* vérifier que exp(i * pi) == -1 */\n"
"#include E<lt>math.hE<gt>        /* pour atan */\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>complex.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    double pi = 4 * atan(1.0);\n"
"    double complex z = cexp(I * pi);\n"
"    printf(\"%f + %f * i\\en\", creal(z), cimag(z));\n"
"}\n"
msgstr ""
"int\n"
"main(void)\n"
"{\n"
"    double pi = 4 * atan(1.0);\n"
"    double complex z = cexp(I * pi);\n"
"    printf(\"%f + %f * i\\en\", creal(z), cimag(z));\n"
"}\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<cabs>(3), B<cacos>(3), B<cacosh>(3), B<carg>(3), B<casin>(3), "
"B<casinh>(3), B<catan>(3), B<catanh>(3), B<ccos>(3), B<ccosh>(3), "
"B<cerf>(3), B<cexp>(3), B<cexp2>(3), B<cimag>(3), B<clog>(3), B<clog10>(3), "
"B<clog2>(3), B<conj>(3), B<cpow>(3), B<cproj>(3), B<creal>(3), B<csin>(3), "
"B<csinh>(3), B<csqrt>(3), B<ctan>(3), B<ctanh>(3)"
msgstr ""
"B<cabs>(3), B<cacos>(3), B<cacosh>(3), B<carg>(3), B<casin>(3), "
"B<casinh>(3), B<catan>(3), B<catanh>(3), B<ccos>(3), B<ccosh>(3), "
"B<cerf>(3), B<cexp>(3), B<cexp2>(3), B<cimag>(3), B<clog>(3), B<clog10>(3), "
"B<clog2>(3), B<conj>(3), B<cpow>(3), B<cproj>(3), B<creal>(3), B<csin>(3), "
"B<csinh>(3), B<csqrt>(3), B<ctan>(3), B<ctanh>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "COMPLEX"
msgstr "COMPLEX"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2011-09-16"
msgstr "16 septembre 2011"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>complex.hE<gt>>"
msgstr "B<#include E<lt>complex.hE<gt>>"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
