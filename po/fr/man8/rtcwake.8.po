# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <ccb@club-internet.fr>, 1997, 2002, 2003.
# Michel Quercia <quercia AT cal DOT enst DOT fr>, 1997.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999.
# Frédéric Delanoy <delanoy_f@yahoo.com>, 2000.
# Thierry Vignaud <tvignaud@mandriva.com>, 2000.
# Christophe Sauthier <christophe@sauthier.com>, 2001.
# Sébastien Blanchet, 2002.
# Jérôme Perzyna <jperzyna@yahoo.fr>, 2004.
# Aymeric Nys <aymeric AT nnx POINT com>, 2004.
# Alain Portal <aportal@univ-montp2.fr>, 2005, 2006.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006.
# Yves Rütschlé <l10n@rutschle.net>, 2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006.
# Julien Cristau <jcristau@debian.org>, 2006.
# Philippe Piette <foudre-blanche@skynet.be>, 2006.
# Jean-Baka Domelevo-Entfellner <domelevo@gmail.com>, 2006.
# Nicolas Haller <nicolas@boiteameuh.org>, 2006.
# Sylvain Archenault <sylvain.archenault@laposte.net>, 2006.
# Valéry Perrin <valery.perrin.debian@free.fr>, 2006.
# Jade Alglave <jade.alglave@ens-lyon.org>, 2006.
# Nicolas François <nicolas.francois@centraliens.net>, 2007.
# Alexandre Kuoch <alex.kuoch@gmail.com>, 2008.
# Lyes Zemmouche <iliaas@hotmail.fr>, 2008.
# Florentin Duneau <fduneau@gmail.com>, 2006, 2008, 2009, 2010.
# Alexandre Normand <aj.normand@free.fr>, 2010.
# David Prévot <david@tilapin.org>, 2010-2015.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-util-linux\n"
"POT-Creation-Date: 2023-07-25 19:51+0200\n"
"PO-Revision-Date: 2022-05-12 17:29+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "RTCWAKE"
msgstr "RTCWAKE"

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr "11 mai 2022"

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr "Administration Système"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "rtcwake - enter a system sleep state until specified wakeup time"
msgstr ""
"rtcwake - Mettre le système en veille jusqu'à une date de réveil indiquée"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<rtcwake> [options] [B<-d> I<device>] [B<-m> I<standby_mode>] {B<-s> "
"I<seconds>|B<-t> I<time_t>}"
msgstr ""
"B<rtcwake> [options] [B<-d> I<périphérique>] [B<-m> I<mode>] {B<-s> "
"I<secondes>|B<-t> I<heure_h>}"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"This program is used to enter a system sleep state and to automatically wake "
"from it at a specified time."
msgstr ""
"Ce programme permet de mettre le système en veille et de le réveiller "
"automatiquement à une date indiquée."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"This uses cross-platform Linux interfaces to enter a system sleep state, and "
"leave it no later than a specified time. It uses any RTC framework driver "
"that supports standard driver model wakeup flags."
msgstr ""
"Il utilise des interfaces Linux multiplateformes pour mettre le système en "
"veille et ne pas l'y laisser au-delà d'une date indiquée. N'importe quel "
"environnement de pilote d'horloge matérielle (RTC) prenant en charge les "
"attributs de réveil normalisés peut être utilisé."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"This is normally used like the old B<apmsleep> utility, to wake from a "
"suspend state like ACPI S1 (standby) or S3 (suspend-to-RAM). Most platforms "
"can implement those without analogues of BIOS, APM, or ACPI."
msgstr ""
"Il est normalement utilisé comme l'ancien utilitaire B<apmsleep>, pour "
"réveiller à partir d'un état de veille comme les états S1 (pause/standby) ou "
"S3 (veille/suspend to RAM) d’ACPI. La plupart des plateformes peuvent les "
"implémenter en l’absence d’équivalent des BIOS, APM ou ACPI."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"On some systems, this can also be used like B<nvram-wakeup>, waking from "
"states like ACPI S4 (suspend to disk). Not all systems have persistent media "
"that are appropriate for such suspend modes."
msgstr ""
"Sur certains systèmes, il peut aussi être utilisé comme B<nvram-wakeup>, en "
"réveillant à partir d'états comme S4 (hibernation/suspend to disk) d’ACPI. "
"Tous les systèmes ne possèdent pas de support physique ininterruptible "
"permettant ces modes de veille."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Note that alarm functionality depends on hardware; not every RTC is able to "
"setup an alarm up to 24 hours in the future."
msgstr ""
"Remarquez que la fonction d’alarme dépend du matériel. Toutes les horloges "
"temps réel (RTC) peuvent ne pas déclencher une alarme au-delà d’une durée de "
"24 heures."

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"The suspend setup may be interrupted by active hardware; for example "
"wireless USB input devices that continue to send events for some fraction of "
"a second after the return key is pressed. B<rtcwake> tries to avoid this "
"problem and it waits to the terminal to settle down before entering a system "
"sleep."
msgstr ""
"La configuration de veille peut être interrompue par du matériel encore "
"actif. Par exemple, des périphériques d’entrée USB et sans-fil continuent "
"d’envoyer des évènements quelques fractions de seconde après que la touche "
"entrée soit pressée. B<rtcwake> essaie d’éviter ce problème et il attend que "
"le terminal soit désœuvré avant d’entamer la mise en veille du système."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-A>, B<--adjfile> I<file>"
msgstr "B<-A>, B<--adjfile> I<fichier>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Specify an alternative path to the adjust file."
msgstr "Indiquer un autre chemin vers le I<fichier> d’ajustement."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-a>, B<--auto>"
msgstr "B<-a>, B<--auto>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Read the clock mode (whether the hardware clock is set to UTC or local time) "
"from the I<adjtime> file, where B<hwclock>(8) stores that information. This "
"is the default."
msgstr ""
"Lire le mode d'horloge (si l'horloge matérielle est configurée en heure "
"universelle (UTC) ou en heure locale) depuis le fichier I<adjtime>, où "
"B<hwclock>(8) conserve ces renseignements. C'est le mode par défaut."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<--date> I<timestamp>"
msgstr "B<--date> I<instant>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Set the wakeup time to the value of the timestamp. Format of the timestamp "
"can be any of the following:"
msgstr ""
"Régler l’heure de réveil à la valeur d’I<instant>. Le format peut être "
"n’importe lequel des formats suivants :"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid ".sp\n"
msgstr ".sp\n"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "YYYYMMDDhhmmss"
msgstr "AAAAMMJJhhmmss"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "YYYY-MM-DD hh:mm:ss"
msgstr "AAAA-MM-JJ hh:mm:ss"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "YYYY-MM-DD hh:mm"
msgstr "AAAA-MM-JJ hh:mm"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "(seconds will be set to 00)"
msgstr "(les secondes sont définies à 00)"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "YYYY-MM-DD"
msgstr "AAAA-MM-JJ"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "(time will be set to 00:00:00)"
msgstr "(l’heure est définie à 00:00:00)"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "hh:mm:ss"
msgstr "hh:mm:ss"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "(date will be set to today)"
msgstr "(la date est définie à aujourd’hui)"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "hh:mm"
msgstr "hh:mm"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "(date will be set to today, seconds to 00)"
msgstr ""
"(la date est définie à aujourd’hui,\n"
"\t les secondes à 00)"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "tomorrow"
msgstr "tomorrow [demain]"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "(time is set to 00:00:00)"
msgstr "(l’heure est définie à 00:00:00)"

#. type: tbl table
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "+5min"
msgstr "+5min"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-d>, B<--device> I<device>"
msgstr "B<-d>, B<--device> I<périphérique>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Use the specified I<device> instead of B<rtc0> as realtime clock. This "
"option is only relevant if your system has more than one RTC. You may "
"specify B<rtc1>, B<rtc2>, ... here."
msgstr ""
"Utiliser le I<périphérique> indiqué au lieu de B<rtc0> comme horloge temps "
"réel. Cette option n'a de sens que si le système dispose de plus d'une "
"horloge matérielle. B<rtc1>, B<rtc2>, etc., peuvent être indiquées ici."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-l>, B<--local>"
msgstr "B<-l>, B<--local>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Assume that the hardware clock is set to local time, regardless of the "
"contents of the I<adjtime> file."
msgstr ""
"Considérer que l'horloge matérielle est définie en heure locale quelque soit "
"le contenu du fichier I<adjtime>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<--list-modes>"
msgstr "B<--list-modes>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "List available B<--mode> option arguments."
msgstr "Liste des arguments disponibles de l’option B<--mode>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-m>, B<--mode> I<mode>"
msgstr "B<-m>, B<--mode> I<mode>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Go into the given standby state. Valid values for I<mode> are:"
msgstr "Entrer dans le I<mode> de veille indiqué. Les valeurs possibles sont :"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<standby>"
msgstr "B<standby>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"ACPI state S1. This state offers minimal, though real, power savings, while "
"providing a very low-latency transition back to a working system. This is "
"the default mode."
msgstr ""
"État S1 de l'ACPI. Cet état propose l'économie minimale d'énergie, bien que "
"réelle, tout en fournissant une transition rapide au retour à l'état de "
"travail. C'est le mode par défaut."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<freeze>"
msgstr "B<freeze>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The processes are frozen, all the devices are suspended and all the "
"processors idled. This state is a general state that does not need any "
"platform-specific support, but it saves less power than Suspend-to-RAM, "
"because the system is still in a running state. (Available since Linux 3.9.)"
msgstr ""
"Les processus sont gelés, tous les périphériques sont en veille et tous les "
"processeurs en attente. Cet état est un état général qui n’a pas besoin de "
"prise en charge spécifique à la plateforme, mais il n’économise pas autant "
"de puissance qu’une mise en veille car le système est toujours dans un état "
"de fonctionnement (disponible depuis Linux 3.9)."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<mem>"
msgstr "B<mem>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"ACPI state S3 (Suspend-to-RAM). This state offers significant power savings "
"as everything in the system is put into a low-power state, except for "
"memory, which is placed in self-refresh mode to retain its contents."
msgstr ""
"État S3 de l'ACPI (veille/suspend to RAM). Cet état propose une économie "
"d'énergie significative puisque tout le système est placé en état "
"d'alimentation minimale, à l'exception de la mémoire qui est placée en mode "
"d'autorafraîchissement pour conserver son contenu."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<disk>"
msgstr "B<disk>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"ACPI state S4 (Suspend-to-disk). This state offers the greatest power "
"savings, and can be used even in the absence of low-level platform support "
"for power management. This state operates similarly to Suspend-to-RAM, but "
"includes a final step of writing memory contents to disk."
msgstr ""
"État S4 de l'ACPI (hibernation/suspend to disk). Cet état propose la plus "
"grande économie d'énergie et peut même être utilisé en l’absence de prise en "
"charge de bas niveau par la plateforme de la gestion d'alimentation. Cet "
"état fonctionne comme la veille, avec une dernière étape pour écrire le "
"contenu de la mémoire sur disque."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<off>"
msgstr "B<off>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"ACPI state S5 (Poweroff). This is done by calling \\(aq/sbin/shutdown\\(aq. "
"Not officially supported by ACPI, but it usually works."
msgstr ""
"État S5 de l'ACPI (extinction). C'est réalisé en appelant « /sbin/"
"shutdown ». N'est pas officiellement pris en charge par l'ACPI, mais "
"fonctionne habituellement."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<no>"
msgstr "B<no>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Don\\(cqt suspend, only set the RTC wakeup time."
msgstr ""
"Ne pas mettre en veille, seulement configurer l’heure de réveil de l’horloge "
"matérielle."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<on>"
msgstr "B<on>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Don\\(cqt suspend, but read the RTC device until an alarm time appears. This "
"mode is useful for debugging."
msgstr ""
"Ne pas mettre en veille, mais lire le périphérique d'horloge matérielle "
"jusqu'à l'apparition de l'heure d'alarme. Ce mode est pratique pour le "
"débogage."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<disable>"
msgstr "B<disable>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Disable a previously set alarm."
msgstr "Désactiver une alarme précédemment définie."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<show>"
msgstr "B<show>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Print alarm information in format: \"alarm: off|on E<lt>timeE<gt>\". The "
"time is in ctime() output format, e.g., \"alarm: on Tue Nov 16 04:48:45 "
"2010\"."
msgstr ""
"Afficher des renseignements sur l'alarme au format « alarme : arrêt|"
"déclenchement E<lt>heureE<gt> ». L'heure est au format de sortie B<ctime>(), "
"par exemple « alarme : déclenchement dimanche 23 octobre 2011, 16:59:10 »."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-n>, B<--dry-run>"
msgstr "B<-n>, B<--dry-run>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"This option does everything apart from actually setting up the alarm, "
"suspending the system, or waiting for the alarm."
msgstr ""
"Tout faire, sauf configurer vraiment l'alarme, mettre le système en veille "
"ou attendre l'alarme."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-s>, B<--seconds> I<seconds>"
msgstr "B<-s>, B<--seconds> I<secondes>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Set the wakeup time to I<seconds> in the future from now."
msgstr ""
"Définir l'heure de réveil à I<secondes> dans le futur à partir de maintenant."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-t>, B<--time> I<time_t>"
msgstr "B<-t>, B<--time> I<heure_h>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Set the wakeup time to the absolute time I<time_t>. I<time_t> is the time in "
"seconds since 1970-01-01, 00:00 UTC. Use the B<date>(1) tool to convert "
"between human-readable time and I<time_t>."
msgstr ""
"Définir l'heure de réveil à l'heure absolue I<heure_h>. I<heure_h> est "
"l'heure en seconde à partir de 00:00:00 1970-01-01 UTC. Utiliser l'outil "
"B<date>(1) pour convertir l'heure entre le format lisible et I<heure_h>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-u>, B<--utc>"
msgstr "B<-u>, B<--utc>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Assume that the hardware clock is set to UTC (Universal Time Coordinated), "
"regardless of the contents of the I<adjtime> file."
msgstr ""
"Considérer que l'horloge matérielle est définie en temps universel coordonné "
"(UTC) quelque soit le contenu du fichier I<adjtime>."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Be verbose."
msgstr "Sortie détaillée."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr "Afficher l’aide-mémoire puis quitter."

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print version and exit."
msgstr "Afficher la version puis quitter."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Some PC systems can\\(cqt currently exit sleep states such as B<mem> using "
"only the kernel code accessed by this driver. They need help from userspace "
"code to make the framebuffer work again."
msgstr ""
"Certains systèmes de PC ne peuvent actuellement pas sortir d'états de veille "
"comme B<mem> en n'utilisant que le code du noyau auquel accède ce pilote. "
"Ils ont besoin de l'aide de code en espace utilisateur pour que la mémoire "
"vidéo fonctionne encore."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "I</etc/adjtime>"
msgstr "I</etc/adjtime>"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

# NOTE: s/GIT/Git/
#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The program was posted several times on LKML and other lists before "
"appearing in kernel commit message for Linux 2.6 in the GIT commit "
"87ac84f42a7a580d0dd72ae31d6a5eb4bfe04c6d."
msgstr ""
"Le programme a été posté plusieurs fois sur LKML et d'autres listes avant "
"d'apparaître dans le message de commit du noyau pour Linux 2.6 dans le "
"commit 87ac84f42a7a580d0dd72ae31d6a5eb4bfe04c6d de Git."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "The program was written by"
msgstr "Le programme a été écrit par"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "and improved by"
msgstr "et amélioré par"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"This is free software. You may redistribute copies of it under the terms of "
"the"
msgstr ""
"C'est un logiciel libre. Vous pouvez en redistribuer des copies sous les "
"conditions de la "

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "There is NO WARRANTY, to the extent permitted by law."
msgstr "Il n'y a AUCUNE GARANTIE dans la mesure autorisée par la loi."

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<adjtime_config>(5), B<hwclock>(8), B<date>(1)"
msgstr "B<adjtime_config>(5), B<hwclock>(8), B<date>(1)"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "SIGNALER DES BOGUES"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""
"Pour envoyer un rapport de bogue, utilisez le système de gestion des "
"problèmes à l'adresse"

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr "DISPONIBILITÉ"

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<rtcwake> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"La commande B<rtcwake> fait partie du paquet util-linux, qui peut être "
"téléchargé de"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14 février 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The suspend setup may be interrupted by active hardware; for example "
"wireless USB input devices that continue to send events for some fraction of "
"a second after the return key is pressed. B<rtcwake> tries to avoid this "
"problem and it waits to terminal to settle down before entering a system "
"sleep."
msgstr ""
"La configuration de veille peut être interrompue par du matériel encore "
"actif. Par exemple, des périphériques d’entrée USB et sans-fil continuent "
"d’envoyer des évènements quelques fractions de seconde après que la touche "
"entrée soit pressée. B<rtcwake> essaie d’éviter ce problème et il attend que "
"le terminal soit désœuvré avant d’entamer la mise en veille du système."

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Afficher le nom et la version du logiciel et quitter."

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<hwclock>(8), B<date>(1)"
msgstr "B<hwclock>(8), B<date>(1)"
