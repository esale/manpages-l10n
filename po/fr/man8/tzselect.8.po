# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2012-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2020, 2021
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr\n"
"POT-Creation-Date: 2023-06-27 19:59+0200\n"
"PO-Revision-Date: 2021-01-17 23:46+0100\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: vim\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<tzselect>"
msgid "tzselect"
msgstr "B<tzselect>"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Time Zone Database"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "tzselect - select a timezone"
msgstr "tzselect - Sélectionner un fuseau horaire"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: ds d
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid " degrees"
msgstr ""

#. type: ds m
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid " minutes"
msgstr ""

#. type: ds s
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "the second."
msgid " seconds"
msgstr "le second."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"B<tzselect> [ B<\\*-c> I<coord> ] [ B<\\*-n> I<limit> ] [ B<\\*-\\*-help> ] "
"[ B<\\*-\\*-version> ]"
msgstr ""
"B<tzselect> [ B<\\*-c> I<coord> ] [ B<\\*-n> I<limite> ] [ B<\\*-\\*-help> ] "
"[ B<\\*-\\*-version> ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"The B<tzselect> program asks the user for information about the current "
"location, and outputs the resulting timezone to standard output.  The output "
"is suitable as a value for the TZ environment variable."
msgstr ""
"Le programme B<tzselect> demande à l'utilisateur des informations sur son "
"emplacement géographique et fournit la description du fuseau horaire sur la "
"sortie standard. Cette sortie est utilisable comme valeur pour la variable "
"d'environnement B<TZ>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"All interaction with the user is done via standard input and standard error."
msgstr ""
"Toutes les interactions avec l'utilisateur se font par l'entrée standard et "
"la sortie d'erreur."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<\\*-c >I<coord>"
msgstr "B<\\*-c >I<coord>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Instead of asking for continent and then country and then city, ask for "
#| "selection from time zones whose largest cities are closest to the "
#| "location with geographical coordinates I<coord.> Use ISO 6709 notation "
#| "for I<coord,> that is, a latitude immediately followed by a longitude.  "
#| "The latitude and longitude should be signed integers followed by an "
#| "optional decimal point and fraction: positive numbers represent north and "
#| "east, negative south and west.  Latitudes with two and longitudes with "
#| "three integer digits are treated as degrees; latitudes with four or six "
#| "and longitudes with five or seven integer digits are treated as I<DDMM, "
#| "DDDMM, DDMMSS,> or I<DDDMMSS> representing I<DD> or I<DDD> degrees, I<MM> "
#| "minutes, and zero or I<SS> seconds, with any trailing fractions represent "
#| "fractional minutes or (if I<SS> is present) seconds.  The decimal point "
#| "is that of the current locale.  For example, in the (default) C locale, "
#| "B<\\*-c\\ +40.689\\*-074.045> specifies 40.689\\(de\\|N, 74.045\\(de\\|W, "
#| "B<\\*-c\\ +4041.4\\*-07402.7> specifies 40\\(de\\|41.4\\(fm\\|N, "
#| "74\\(de\\|2.7\\(fm\\|W, and B<\\*-c\\ +404121\\*-0740240> specifies "
#| "40\\(de\\|41\\(fm\\|21\\(sd\\|N, 74\\(de\\|2\\(fm\\|40\\(sd\\|W.  If "
#| "I<coord> is not one of the documented forms, the resulting behavior is "
#| "unspecified."
msgid ""
"Instead of asking for continent and then country and then city, ask for "
"selection from time zones whose largest cities are closest to the location "
"with geographical coordinates I<coord.> Use ISO 6709 notation for I<coord,> "
"that is, a latitude immediately followed by a longitude.  The latitude and "
"longitude should be signed integers followed by an optional decimal point "
"and fraction: positive numbers represent north and east, negative south and "
"west.  Latitudes with two and longitudes with three integer digits are "
"treated as degrees; latitudes with four or six and longitudes with five or "
"seven integer digits are treated as I<DDMM, DDDMM, DDMMSS,> or I<DDDMMSS> "
"representing I<DD> or I<DDD> degrees, I<MM> minutes, and zero or I<SS> "
"seconds, with any trailing fractions represent fractional minutes or (if "
"I<SS> is present) seconds.  The decimal point is that of the current "
"locale.  For example, in the (default) C locale, B<\\*-c\\ "
"+40.689\\*-074.045> specifies 40.689\\*d\\*_N, 74.045\\*d\\*_W, B<\\*-c\\ "
"+4041.4\\*-07402.7> specifies 40\\*d\\*_41.4\\*m\\*_N, "
"74\\*d\\*_2.7\\*m\\*_W, and B<\\*-c\\ +404121\\*-0740240> specifies "
"40\\*d\\*_41\\*m\\*_21\\*s\\*_N, 74\\*d\\*_2\\*m\\*_40\\*s\\*_W.  If "
"I<coord> is not one of the documented forms, the resulting behavior is "
"unspecified."
msgstr ""
"Au lieu de demander le continent, le pays puis la ville, demander une "
"sélection à partir de zones horaires dont les plus grandes villes sont "
"proches de l’emplacement de coordonnées géographiques I<coord>. Utiliser la "
"notation ISO\\ 6709 pour I<coord>, c'est-à-dire, une latitude suivie "
"immédiatement par une longitude. La latitude et la longitude doivent être "
"des nombres entiers suivis facultativement par un point décimal et une "
"partie fractionnaire. Les nombres positifs représentent le nord et l’est, "
"ceux négatifs le sud et l’ouest. Les latitudes avec deux chiffres et les "
"longitudes avec trois chiffres sont traitées comme des degrés. Les latitudes "
"avec quatre ou six chiffres et les longitudes avec cinq ou sept chiffres "
"sont traitées comme I<DDMM>, I<DDDMM>, I<DDMMSS> ou I<DDDMMSS> représentant "
"I<DD> ou I<DDD>\\ degrés, I<MM>\\ minutes et zéro ou I<SS>\\ secondes, avec "
"n’importe quelle partie fractionnaire représentant des minutes ou (si I<SS> "
"est présent) de secondes. Le point décimal est celui de la régionalisation "
"utilisée. Par exemple, avec la locale\\ C (celle par défaut),  B<\\*-c\\ "
"+40.689\\*-074.045> indique 40.689\\(de\\|N, 74.045\\(de\\|W, B<\\*-c\\ "
"+4041.4\\*-07402.7> indique 40\\(de\\|41.4\\(fm\\|N, 74\\(de\\|2.7\\(fm\\|W, "
"and B<\\*-c\\ +404121\\*-0740240> indique 40\\(de\\|41\\(fm\\|21\\(sd\\|N, "
"74\\(de\\|2\\(fm\\|40\\(sd\\|W. Si I<coord> n’est pas une des formes "
"répertoriées, le comportement résultant n’est pas défini."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<\\*-n >I<limit>"
msgstr "B<\\*-n >I<limite>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"When B<\\*-c> is used, display the closest I<limit> locations (default 10)."
msgstr ""
"Lorsque B<\\*-c> est utilisé, afficher les I<limite> emplacements les plus "
"proches (par défaut\\ 10)."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<\\*-\\*-help>"
msgstr "B<\\*-\\*-help>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Output help information and exit."
msgstr "Afficher l’aide et terminer."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<\\*-\\*-version>"
msgstr "B<\\*-\\*-version>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Output version information and exit."
msgstr "afficher les informations de version et quitter."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT VARIABLES"
msgstr "VARIABLES D'ENVIRONNEMENT"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<AWK>"
msgstr "B<AWK>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Name of a Posix-compliant B<awk> program (default: B<awk>)."
msgstr ""
"Nom de l'interpréteur I<awk> compatible POSIX disponible (par défaut\\ : "
"B<awk>)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<TZDIR>"
msgstr "B<TZDIR>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Name of the directory containing timezone data files (default: B</usr/share/"
"zoneinfo>)."
msgstr ""
"Nom du répertoire contenant les fichiers de données des fuseaux horaires "
"(par défaut\\ : I</usr/share/zoneinfo>)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "I<TZDIR>B</iso3166.tab>"
msgstr "I<TZDIR>B</iso3166.tab>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Table of ISO 3166 2-letter country codes and country names."
msgstr "Table des noms de pays et des codes sur deux lettres (ISO\\ 3166)."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "I<TZDIR>B</zone1970.tab>"
msgstr "I<TZDIR>B</zone1970.tab>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Table of country codes, latitude and longitude, timezones, and descriptive "
"comments."
msgstr ""
"Table des codes, des latitudes et longitudes, des fuseaux horaires pour des "
"régions et leurs commentaires descriptifs."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "I<TZDIR>B</>I<TZ>"
msgstr "I<TZDIR>B</>I<TZ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Timezone data file for timezone I<TZ>."
msgstr "Fichier de données du fuseau horaire I<TZ>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "CODE DE RETOUR"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"The exit status is zero if a timezone was successfully obtained from the "
"user, nonzero otherwise."
msgstr ""
"Le code de retour vaut zéro si un fuseau horaire a été obtenu correctement "
"et est différent de zéro dans le cas contraire."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "newctime(3), tzfile(5), zdump(8), zic(8)"
msgstr "B<newctime>(3), B<tzfile>(5), B<zdump>(8), B<zic>(8)"

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Applications should not assume that B<tzselect>'s output matches the user's "
"political preferences."
msgstr ""
"Les applications ne doivent pas présumer que la sortie de B<tzselect> "
"corresponde à des préférences politiques."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30 octobre 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, fuzzy, no-wrap
#| msgid "B<tzselect>"
msgid "B<tzselect>\n"
msgstr "B<tzselect>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"The B<tzselect> program asks the user for information about the current "
"location, and outputs the resulting timezone description to standard "
"output.  The output is suitable as a value for the B<TZ> environment "
"variable."
msgstr ""
"Le programme B<tzselect> demande à l'utilisateur des informations sur son "
"emplacement géographique et fournit la description du fuseau horaire sur la "
"sortie standard. Cette sortie est utilisable comme valeur pour la variable "
"d'environnement B<TZ>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"The exit status is zero if a timezone was successfully obtained from the "
"user, and is nonzero otherwise."
msgstr ""
"Le code de retour vaut zéro si un fuseau horaire a été obtenu correctement "
"et est différent de zéro dans le cas contraire."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENVIRONNEMENT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "Name of a POSIX-compliant I<awk> program (default: B<awk>)."
msgstr ""
"Nom de l'interpréteur I<awk> compatible POSIX disponible (par défaut\\ : "
"B<awk>)."

#.  or perhaps /usr/local/etc/zoneinfo in some older systems.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Name of the directory containing timezone data files (default: I</usr/share/"
"zoneinfo>)."
msgstr ""
"Nom du répertoire contenant les fichiers de données des fuseaux horaires "
"(par défaut\\ : I</usr/share/zoneinfo>)."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<TZDIR>I</iso3166.tab>"
msgstr "B<TZDIR>I</iso3166.tab>"

#. type: TP
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<TZDIR>I</zone.tab>"
msgstr "B<TZDIR>I</zone.tab>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Table of country codes, latitude and longitude, TZ values, and descriptive "
"comments."
msgstr ""
"Table des codes de pays, latitudes, longitudes, fuseaux horaires et leurs "
"commentaires descriptifs."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<TZDIR>I</>I<TZ>"
msgstr "B<TZDIR>I</>I<TZ>"

#. #-#-#-#-#  debian-bookworm: tzselect.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: tzselect.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-38: tzselect.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: tzselect.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: tzselect.8.pot (PACKAGE VERSION)  #-#-#-#-#
#.  @(#)tzselect.8	1.3
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<tzfile>(5), B<zdump>(8), B<zic>(8)"
msgstr "B<tzfile>(5), B<zdump>(8), B<zic>(8)"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "TZSELECT"
msgstr "TZSELECT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2007-05-18"
msgstr "18 mai 2007"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux System Administration"
msgstr "Manuel de l'administrateur Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<tzselect>"
msgstr "B<tzselect>"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
