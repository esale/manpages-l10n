# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# bubu <bubub@no-log.org>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-06-27 19:45+0200\n"
"PO-Revision-Date: 2023-03-18 19:41+0100\n"
"Last-Translator: bubu <bubub@no-log.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Poedit 3.0.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "rcmd"
msgstr "rcmd"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"rcmd, rresvport, iruserok, ruserok, rcmd_af, rresvport_af, iruserok_af, "
"ruserok_af - routines for returning a stream to a remote command"
msgstr ""
"rcmd, rresvport, iruserok, ruserok, rcmd_af, rresvport_af, iruserok_af, "
"ruserok_af - Routines renvoyant un flux de données pour une commande "
"invoquée à distance"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>netdb.hE<gt>    >/* Or E<lt>unistd.hE<gt> on some systems */\n"
msgstr "B<#include E<lt>netdb.hE<gt>    >/* Ou E<lt>unistd.hE<gt> sur certains systèmes */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int rcmd(char **restrict >I<ahost>B<, unsigned short >I<inport>B<,>\n"
"B<            const char *restrict >I<locuser>B<,>\n"
"B<            const char *restrict >I<remuser>B<,>\n"
"B<            const char *restrict >I<cmd>B<, int *restrict >I<fd2p>B<);>\n"
msgstr ""
"B<int rcmd(char **restrict >I<ahost>B<, unsigned short >I<inport>B<,>\n"
"B<            const char *restrict >I<locuser>B<,>\n"
"B<            const char *restrict >I<remuser>B<,>\n"
"B<            const char *restrict >I<cmd>B<, int *restrict >I<fd2p>B<);>\n"
"\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int rresvport(int *>I<port>B<);>\n"
msgstr "B<int rresvport(int *>I<port>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int iruserok(uint32_t >I<raddr>B<, int >I<superuser>B<,>\n"
"B<            const char *>I<ruser>B<, const char *>I<luser>B<);>\n"
"B<int ruserok(const char *>I<rhost>B<, int >I<superuser>B<,>\n"
"B<            const char *>I<ruser>B<, const char *>I<luser>B<);>\n"
msgstr ""
"B<int iruserok(uint32_t >I<raddr>B<, int >I<superuser>B<,>\n"
"B<            const char *>I<ruser>B<, const char *>I<luser>B<);>\n"
"B<int ruserok(const char *>I<rhost>B<, int >I<superuser>B<,>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int rcmd_af(char **restrict >I<ahost>B<, unsigned short >I<inport>B<,>\n"
"B<            const char *restrict >I<locuser>B<,>\n"
"B<            const char *restrict >I<remuser>B<,>\n"
"B<            const char *restrict >I<cmd>B<, int *restrict >I<fd2p>B<,>\n"
"B<            sa_family_t >I<af>B<);>\n"
msgstr ""
"B<int rcmd_af(char **restrict >I<ahost>B<, unsigned short >I<inport>B<,>\n"
"B<            const char *restrict >I<locuser>B<,>\n"
"B<            const char *restrict >I<remuser>B<,>\n"
"B<            const char *restrict >I<cmd>B<, int *restrict >I<fd2p>B<,>\n"
"B<            sa_family_t >I<af>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int rresvport_af(int *>I<port>B<, sa_family_t >I<af>B<);>\n"
msgstr "B<int rresvport_af(int *>I<port>B<, sa_family_t >I<af>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int iruserok_af(const void *restrict >I<raddr>B<, int >I<superuser>B<,>\n"
"B<            const char *restrict >I<ruser>B<, const char *restrict >I<luser>B<,>\n"
"B<            sa_family_t >I<af>B<);>\n"
"B<int ruserok_af(const char *>I<rhost>B<, int >I<superuser>B<,>\n"
"B<            const char *>I<ruser>B<, const char *>I<luser>B<,>\n"
"B<            sa_family_t >I<af>B<);>\n"
msgstr ""
"B<int iruserok_af(const void *restrict >I<raddr>B<, int >I<superuser>B<,>\n"
"B<            const char *restrict >I<ruser>B<, const char *restrict >I<luser>B<,>\n"
"B<            sa_family_t >I<af>B<);>\n"
"B<int ruserok_af(const char *>I<rhost>B<, int >I<superuser>B<,>\n"
"B<            const char *>I<ruser>B<, const char *>I<luser>B<,>\n"
"B<            sa_family_t >I<af>B<);>\n"
"\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<rcmd>(), B<rcmd_af>(), B<rresvport>(), B<rresvport_af>(), B<iruserok>(), "
"B<iruserok_af>(), B<ruserok>(), B<ruserok_af>():"
msgstr ""
"B<rcmd>(), B<rcmd_af>(), B<rresvport>(), B<rresvport_af>(), B<iruserok>(), "
"B<iruserok_af>(), B<ruserok>(), B<ruserok_af>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.19 :\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 et antérieures :\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<rcmd>()  function is used by the superuser to execute a command on a "
"remote machine using an authentication scheme based on privileged port "
"numbers.  The B<rresvport>()  function returns a file descriptor to a socket "
"with an address in the privileged port space.  The B<iruserok>()  and "
"B<ruserok>()  functions are used by servers to authenticate clients "
"requesting service with B<rcmd>().  All four functions are used by the "
"B<rshd>(8)  server (among others)."
msgstr ""
"La fonction B<rcmd>() est utilisée par le superutilisateur pour exécuter une "
"commande sur une machine distante, en utilisant un schéma d'identification "
"basé sur des numéros de ports privilégiés. La fonction B<rresvport>() "
"renvoie un descripteur de fichier sur un socket se trouvant dans l'espace "
"des numéros de ports privilégiés. Les fonctions B<iruserok>() et "
"B<ruserok>() sont utilisées par les serveurs pour authentifier les clients "
"demandant un service avec B<rcmd>(). Les quatre fonctions sont utilisées "
"(entre autres) par le serveur B<rshd>(8)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "rcmd()"
msgstr "rcmd()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<rcmd>()  function looks up the host I<*ahost> using "
"B<gethostbyname>(3), returning -1 if the host does not exist.  Otherwise, "
"I<*ahost> is set to the standard name of the host and a connection is "
"established to a server residing at the well-known Internet port I<inport>."
msgstr ""
"La fonction B<rcmd>() recherche l'hôte I<*ahost> en utilisant "
"B<gethostbyname>(3), puis renvoie B<-1> si cet hôte n'existe pas. Sinon, "
"I<*ahost> est rempli avec le nom standard de cet hôte et une connexion est "
"établie avec un serveur se trouvant sur l'un des ports bien connus I<inport>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the connection succeeds, a socket in the Internet domain of type "
"B<SOCK_STREAM> is returned to the caller, and given to the remote command as "
"I<stdin> and I<stdout>.  If I<fd2p> is nonzero, then an auxiliary channel to "
"a control process will be set up, and a file descriptor for it will be "
"placed in I<*fd2p>.  The control process will return diagnostic output from "
"the command (unit 2) on this channel, and will also accept bytes on this "
"channel as being UNIX signal numbers, to be forwarded to the process group "
"of the command.  If I<fd2p> is 0, then the I<stderr> (unit 2 of the remote "
"command) will be made the same as the I<stdout> and no provision is made for "
"sending arbitrary signals to the remote process, although you may be able to "
"get its attention by using out-of-band data."
msgstr ""
"Si la connexion réussit, un socket dans le domaine Internet de type "
"B<SOCK_STREAM> est renvoyé à l'appelant et est fourni à la commande distante "
"en guise de I<stdin> et I<stdout>. Si I<fd2p> est non nul, un canal "
"auxiliaire est créé pour un processus de contrôle, et son descripteur sera "
"placé dans I<*fd2p>. Le processus de contrôle renverra les sorties de "
"diagnostic (numéro 2) sur ce canal, et acceptera également des octets qu'il "
"considérera comme des numéros de signal UNIX à envoyer au groupe de "
"processus de la commande. Si I<fd2p> est nul, alors I<stderr> (sortie "
"numéro 2 de la commande distante) sera renvoyée sur I<stdout> et rien n'est "
"prévu pour l'envoi de signaux arbitraires au processus distant, bien que "
"vous puissiez y parvenir en utilisant des données hors-bande."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The protocol is described in detail in B<rshd>(8)."
msgstr "Le protocole est décrit en détail dans B<rshd>(8)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "rresvport()"
msgstr "rresvport()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<rresvport>()  function is used to obtain a socket with a privileged "
"port bound to it.  This socket is suitable for use by B<rcmd>()  and several "
"other functions.  Privileged ports are those in the range 0 to 1023.  Only a "
"privileged process (on Linux, a process that has the B<CAP_NET_BIND_SERVICE> "
"capability in the user namespace governing its network namespace)  is "
"allowed to bind to a privileged port.  In the glibc implementation, this "
"function restricts its search to the ports from 512 to 1023.  The I<port> "
"argument is value-result: the value it supplies to the call is used as the "
"starting point for a circular search of the port range; on (successful) "
"return, it contains the port number that was bound to."
msgstr ""
"La fonction B<rresvport>() est utilisée pour obtenir un socket attaché à un "
"port privilégié. Ce socket est utilisable ensuite pour B<rcmd>() et "
"plusieurs autres fonctions. Les numéros de ports privilégiés se trouvent "
"dans l'intervalle 0 à 1023. Seul un processus privilégié "
"(B<CAP_NET_BIND_SERVICE>) a le droit de s'associer à un port privilégié. "
"Dans l'implémentation de la glibc, cette fonction restreint sa recherche du "
"port 512 au port 1023. L'argument I<port> est valeur-résultat : la valeur "
"qu'il fournit à l'appel est utilisée comme point de départ pour une "
"recherche circulaire de l'intervalle de port ; au retour (en cas de "
"réussite), il contient le numéro de port avec lequel il a été associé."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "iruserok() and ruserok()"
msgstr "iruserok() et ruserok()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<iruserok>()  and B<ruserok>()  functions take a remote host's IP "
"address or name, respectively, two usernames and a flag indicating whether "
"the local user's name is that of the superuser.  Then, if the user is I<not> "
"the superuser, it checks the I</etc/hosts.equiv> file.  If that lookup is "
"not done, or is unsuccessful, the I<.rhosts> in the local user's home "
"directory is checked to see if the request for service is allowed."
msgstr ""
"Les fonctions B<iruserok>() et B<ruserok>() prennent respectivement en "
"argument un nom ou une adresse IP d'hôte distant, deux noms d'utilisateurs "
"et un drapeau indiquant si l'utilisateur local est le superutilisateur "
"« I<superuser> ». Ainsi, si l'utilisateur n'est I<pas> le superutilisateur, "
"elles vérifient le fichier I</etc/hosts.equiv>. Si cela échoue, le fichier "
"I<.rhosts> est recherché dans le répertoire personnel de l'utilisateur "
"local, afin de voir si la requête est autorisée."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If this file does not exist, is not a regular file, is owned by anyone other "
"than the user or the superuser, is writable by anyone other than the owner, "
"or is hardlinked anywhere, the check automatically fails.  Zero is returned "
"if the machine name is listed in the I<hosts.equiv> file, or the host and "
"remote username are found in the I<.rhosts> file; otherwise B<iruserok>()  "
"and B<ruserok>()  return -1.  If the local domain (as obtained from "
"B<gethostname>(2))  is the same as the remote domain, only the machine name "
"need be specified."
msgstr ""
"Si ce fichier n'existe pas, s'il ne s'agit pas d'un fichier ordinaire, s'il "
"appartient à quelqu'un d'autre que l'utilisateur local ou le "
"superutilisateur, ou encore s'il est accessible en écriture par quelqu'un "
"d'autre que son propriétaire, le test échoue automatiquement. Si la machine "
"est listée dans le fichier I<hosts.equiv>, ou si les noms d'hôte et "
"d'utilisateur distants sont trouvés dans le fichier I<.rhosts>, "
"B<iruserok>() et B<ruserok>() renvoient zéro. Sinon elles renvoient B<-1>. "
"Si le domaine local (obtenu par l'intermédiaire de B<gethostname>(2)) est le "
"même que le domaine distant, seul le nom de machine a besoin d'être indiqué."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the IP address of the remote host is known, B<iruserok>()  should be used "
"in preference to B<ruserok>(), as it does not require trusting the DNS "
"server for the remote host's domain."
msgstr ""
"Si l'adresse IP de l'hôte distant est connue, B<iruserok>() sera préférée à "
"B<ruserok>(), car elle ne nécessite pas d'avoir un serveur DNS digne de "
"confiance pour le domaine distant."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "*_af() variants"
msgstr "Variantes *_af()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"All of the functions described above work with IPv4 (B<AF_INET>)  sockets.  "
"The \"_af\" variants take an extra argument that allows the socket address "
"family to be specified.  For these functions, the I<af> argument can be "
"specified as B<AF_INET> or B<AF_INET6>.  In addition, B<rcmd_af>()  supports "
"the use of B<AF_UNSPEC>."
msgstr ""
"Toutes les fonctions décrites ci-dessus fonctionnent avec des sockets IPv4 "
"(B<AF_INET>). Les variantes « _af » prennent un argument supplémentaire qui "
"permet d'indiquer la famille d'adresse de socket. Pour ces fonctions, "
"l'argument I<af> peut être indiqué comme B<AF_INET> ou B<AF_INET6>. De plus, "
"B<rcmd_af>() permet d'utiliser B<AF_UNSPEC>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<rcmd>()  function returns a valid socket descriptor on success.  It "
"returns -1 on error and prints a diagnostic message on the standard error."
msgstr ""
"La fonction B<rcmd>() renvoie un descripteur de socket valable si elle "
"réussit, sinon elle renvoie B<-1> et affiche un message de diagnostic sur sa "
"sortie d'erreur standard."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<rresvport>()  function returns a valid, bound socket descriptor on "
"success.  On failure, it returns -1 and sets I<errno> to indicate the "
"error.  The error code B<EAGAIN> is overloaded to mean: \"All network ports "
"in use\"."
msgstr ""
"La fonction B<rresvport>() renvoie un descripteur de socket valable, attaché "
"à une adresse privilégiée si elle réussit. Elle renvoie B<-1> sinon, et "
"I<errno> contient le code d'erreur. Le code d'erreur B<EAGAIN> signifie en "
"réalité «\\ Tous les ports réseau sont déjà utilisés\\ »."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For information on the return from B<ruserok>()  and B<iruserok>(), see "
"above."
msgstr ""
"Voir ci-dessus pour des renseignements sur les valeurs renvoyées par "
"B<ruserok>() et B<iruserok>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<rcmd>(),\n"
"B<rcmd_af>()"
msgstr ""
"B<rcmd>(),\n"
"B<rcmd_af>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe"
msgstr "MT-Unsafe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<rresvport>(),\n"
"B<rresvport_af>()"
msgstr ""
"B<rresvport>(),\n"
"B<rresvport_af>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<iruserok>(),\n"
"B<ruserok>(),\n"
"B<iruserok_af>(),\n"
"B<ruserok_af>()"
msgstr ""
"B<iruserok>(),\n"
"B<ruserok>(),\n"
"B<iruserok_af>(),\n"
"B<ruserok_af>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "BSD."
msgstr "BSD."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<iruserok_af>(),\n"
#| "B<ruserok_af>()"
msgid "B<iruserok_af>()"
msgstr ""
"B<iruserok_af>(),\n"
"B<ruserok_af>()"

#. type: TQ
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<rcmd>(),\n"
#| "B<rcmd_af>()"
msgid "B<rcmd_af>()"
msgstr ""
"B<rcmd>(),\n"
"B<rcmd_af>()"

#. type: TQ
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "rresvport()"
msgid "B<rresvport_af>()"
msgstr "rresvport()"

#. type: TQ
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<user_xattr>"
msgid "B<ruserok_af>()"
msgstr "B<user_xattr>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.2."
msgstr "glibc 2.2."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Not in POSIX.1.  Present on the BSDs, Solaris, and many other systems.  "
#| "These functions appeared in 4.2BSD.  The \"_af\" variants are more recent "
#| "additions, and are not present on as wide a range of systems."
msgid ""
"Solaris, 4.2BSD.  The \"_af\" variants are more recent additions, and are "
"not present on as wide a range of systems."
msgstr ""
"Pas dans POSIX.1-2001. Présentes sur les BSD, Solaris et beaucoup d'autres "
"systèmes. Ces fonctions sont apparues dans BSD 4.2. Les variantes « _af » "
"sont des ajouts plus récents, et ne sont pas présentes sur de nombreux "
"systèmes."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. #-#-#-#-#  archlinux: rcmd.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Bug filed 25 Nov 2007:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=5399
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: rcmd.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Bug filed 25 Nov 2007:
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=5399
#. type: Plain text
#. #-#-#-#-#  debian-unstable: rcmd.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Bug filed 25 Nov 2007:
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=5399
#. type: Plain text
#. #-#-#-#-#  fedora-38: rcmd.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Bug filed 25 Nov 2007:
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=5399
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: rcmd.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Bug filed 25 Nov 2007:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=5399
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: rcmd.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Bug filed 25 Nov 2007:
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=5399
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: rcmd.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  Bug filed 25 Nov 2007:
#.  https://www.sourceware.org/bugzilla/show_bug.cgi?id=5399
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<iruserok>()  and B<iruserok_af>()  are declared in glibc headers only "
"since glibc 2.12."
msgstr ""
"B<iruserok>() et B<iruserok_af>() ne sont déclarées dans les fichiers d'en-"
"tête de la glibc que depuis la glibc 2.12."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<rlogin>(1), B<rsh>(1), B<rexec>(3), B<rexecd>(8), B<rlogind>(8), B<rshd>(8)"
msgstr ""
"B<rlogin>(1), B<rsh>(1), B<rexec>(3), B<rexecd>(8), B<rlogind>(8), B<rshd>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 février 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"The functions B<iruserok_af>(), B<rcmd_af>(), B<rresvport_af>(), and "
"B<ruserok_af>()  functions are provided since glibc 2.2."
msgstr ""
"Les fonctions B<iruserok_af>(), B<rcmd_af>(), B<rresvport_af>() et "
"B<ruserok_af>() sont fournies depuis la glibc 2.2."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Not in POSIX.1.  Present on the BSDs, Solaris, and many other systems.  "
"These functions appeared in 4.2BSD.  The \"_af\" variants are more recent "
"additions, and are not present on as wide a range of systems."
msgstr ""
"Pas dans POSIX.1-2001. Présentes sur les BSD, Solaris et beaucoup d'autres "
"systèmes. Ces fonctions sont apparues dans BSD 4.2. Les variantes « _af » "
"sont des ajouts plus récents, et ne sont pas présentes sur de nombreux "
"systèmes."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "RCMD"
msgstr "RCMD"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<#include E<lt>netdb.hE<gt> \\ \\ >/* Or E<lt>unistd.hE<gt> on some systems */\n"
msgstr "B<#include E<lt>netdb.hE<gt> \\ \\ >/* Ou E<lt>unistd.hE<gt> sur certains systèmes */\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int rcmd(char **>I<ahost>B<, unsigned short >I<inport>B<, const char *>I<locuser>B<, >\n"
"B<         const char *>I<remuser>B<, const char *>I<cmd>B<, int *>I<fd2p>B<);>\n"
msgstr ""
"B<int rcmd(char **>I<ahost>B<, unsigned short >I<inport>B<, const char *>I<locuser>B<, >\n"
"B<         const char *>I<remuser>B<, const char *>I<cmd>B<, int *>I<fd2p>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int iruserok(uint32_t >I<raddr>B<, int >I<superuser>B<, >\n"
"B<             const char *>I<ruser>B<, const char *>I<luser>B<);>\n"
msgstr ""
"B<int iruserok(uint32_t >I<raddr>B<, int >I<superuser>B<, >\n"
"B<             const char *>I<ruser>B<, const char *>I<luser>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int ruserok(const char *>I<rhost>B<, int >I<superuser>B<, >\n"
"B<            const char *>I<ruser>B<, const char *>I<luser>B<);>\n"
msgstr ""
"B<int ruserok(const char *>I<rhost>B<, int >I<superuser>B<, >\n"
"B<            const char *>I<ruser>B<, const char *>I<luser>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int rcmd_af(char **>I<ahost>B<, unsigned short >I<inport>B<, const char *>I<locuser>B<, >\n"
"B<            const char *>I<remuser>B<, const char *>I<cmd>B<, int *>I<fd2p>B<,>\n"
"B<            sa_family_t >I<af>B<);>\n"
msgstr ""
"B<int rcmd_af(char **>I<ahost>B<, unsigned short >I<inport>B<, const char *>I<locuser>B<, >\n"
"B<            const char *>I<remuser>B<, const char *>I<cmd>B<, int *>I<fd2p>B<,>\n"
"B<            sa_family_t >I<af>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int iruserok_af(const void *>I<raddr>B<, int >I<superuser>B<, >\n"
"B<                const char *>I<ruser>B<, const char *>I<luser>B<, sa_family_t >I<af>B<);>\n"
msgstr ""
"B<int iruserok_af(const void *>I<raddr>B<, int >I<superuser>B<, >\n"
"B<                const char *>I<ruser>B<, const char *>I<luser>B<, sa_family_t >I<af>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int ruserok_af(const char *>I<rhost>B<, int >I<superuser>B<, >\n"
"B<               const char *>I<ruser>B<, const char *>I<luser>B<, sa_family_t >I<af>B<);>\n"
msgstr ""
"B<int ruserok_af(const char *>I<rhost>B<, int >I<superuser>B<, >\n"
"B<               const char *>I<ruser>B<, const char *>I<luser>B<, sa_family_t >I<af>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<rcmd>(),\n"
"B<rcmd_af>(),\n"
"B<rresvport>(),\n"
"B<rresvport_af>(),\n"
"B<iruserok>(),\n"
"B<iruserok_af>(),\n"
"B<ruserok>(),\n"
"B<ruserok_af>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"B<rcmd>(),\n"
"B<rcmd_af>(),\n"
"B<rresvport>(),\n"
"B<rresvport_af>(),\n"
"B<iruserok>(),\n"
"B<iruserok_af>(),\n"
"B<ruserok>(),\n"
"B<ruserok_af>():\n"
"    Depuis la glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 et précédentes :\n"
"        _BSD_SOURCE\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<rresvport>()  function is used to obtain a socket with a privileged "
"port bound to it.  This socket is suitable for use by B<rcmd>()  and several "
"other functions.  Privileged ports are those in the range 0 to 1023.  Only a "
"privileged process (on Linux: a process that has the B<CAP_NET_BIND_SERVICE> "
"capability in the user namespace governing its network namespace).  is "
"allowed to bind to a privileged port.  In the glibc implementation, this "
"function restricts its search to the ports from 512 to 1023.  The I<port> "
"argument is value-result: the value it supplies to the call is used as the "
"starting point for a circular search of the port range; on (successful) "
"return, it contains the port number that was bound to."
msgstr ""
"La fonction B<rresvport>() est utilisée pour obtenir un socket attaché à un "
"port privilégié. Ce socket est utilisable ensuite pour B<rcmd>() et "
"plusieurs autres fonctions. Les numéros de ports privilégiés se trouvent "
"dans l'intervalle 0 à 1023. Seul un processus privilégié "
"(B<CAP_NET_BIND_SERVICE>) a le droit de s'associer à un port privilégié. "
"Dans l'implémentation de la glibc, cette fonction restreint sa recherche du "
"port 512 au port 1023. L'argument I<port> est valeur-résultat : la valeur "
"qu'il fournit à l'appel est utilisée comme point de départ pour une "
"recherche circulaire de l'intervalle de port ; au retour (en cas de "
"réussite), il contient le numéro de port avec lequel il a été associé."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<rresvport>()  function returns a valid, bound socket descriptor on "
"success.  It returns -1 on error with the global value I<errno> set "
"according to the reason for failure.  The error code B<EAGAIN> is overloaded "
"to mean \"All network ports in use.\""
msgstr ""
"La fonction B<rresvport>() renvoie un descripteur de socket valable, attaché "
"à une adresse privilégiée si elle réussit. Elle renvoie B<-1> sinon, et "
"I<errno> contient le code d'erreur. Le code d'erreur B<EAGAIN> signifie en "
"réalité «\\ Tous les ports réseau sont déjà utilisés\\ »."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The functions B<iruserok_af>(), B<rcmd_af>(), B<rresvport_af>(), and "
"B<ruserok_af>()  functions are provide in glibc since version 2.2."
msgstr ""
"Les fonctions B<iruserok_af>(), B<rcmd_af>(), B<rresvport_af>() et "
"B<ruserok_af>() sont fournies dans la glibc depuis la version 2.2."

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<iruserok>(),\n"
"B<ruserok>(),\n"
msgstr ""
"B<iruserok>(),\n"
"B<ruserok>(),\n"

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<iruserok_af>(),\n"
"B<ruserok_af>()"
msgstr ""
"B<iruserok_af>(),\n"
"B<ruserok_af>()"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#.  Bug filed 25 Nov 2007:
#.  http://sources.redhat.com/bugzilla/show_bug.cgi?id=5399
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<iruserok>()  and B<iruserok_af>()  are declared in glibc headers only "
"since version 2.12."
msgstr ""
"B<iruserok>() et B<iruserok_af>() ne sont déclarées dans les fichiers d'en-"
"tête de la glibc que depuis la version 2.12."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<rlogin>(1), B<rsh>(1), B<intro>(2), B<rexec>(3), B<rexecd>(8), "
"B<rlogind>(8), B<rshd>(8)"
msgstr ""
"B<rlogin>(1), B<rsh>(1), B<intro>(2), B<rexec>(3), B<rexecd>(8), "
"B<rlogind>(8), B<rshd>(8)"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
