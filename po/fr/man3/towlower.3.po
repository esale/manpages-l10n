# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-06-27 19:59+0200\n"
"PO-Revision-Date: 2023-03-14 10:55+0100\n"
"Last-Translator: Thomas Vincent <tvincent@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 22.12.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "towlower"
msgstr "towlower"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "towlower, towlower_l - convert a wide character to lowercase"
msgstr "towlower, towlower_l - Conversion d'un caractère large en minuscule"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr "B<#include E<lt>wctype.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<wint_t towlower(wint_t >I<wc>B<);>\n"
"B<wint_t towlower_l(wint_t >I<wc>B<, locale_t >I<locale>B<);>\n"
msgstr ""
"B<wint_t towlower(wint_t >I<wc>B<);>\n"
"B<wint_t towlower_l(wint_t >I<wc>B<, locale_t >I<locale>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<towlower_l>():"
msgstr "B<towlower_l>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _XOPEN_SOURCE E<gt>= 700\n"
"    Before glibc 2.10:\n"
"        _GNU_SOURCE\n"
msgstr ""
"    Depuis la glibc 2.10 :\n"
"        _XOPEN_SOURCE E<gt>= 700\n"
"    Avant la glibc 2.10 :\n"
"        _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<towlower>()  function is the wide-character equivalent of the "
"B<tolower>(3)  function.  If I<wc> is an uppercase wide character, and there "
"exists a lowercase equivalent in the current locale, it returns the "
"lowercase equivalent of I<wc>.  In all other cases, I<wc> is returned "
"unchanged."
msgstr ""
"La fonction B<towlower>() est l'équivalent de B<tolower>(3) pour les "
"caractères larges. Si I<wc> est un caractère large majuscule et que son "
"équivalent en minuscule existe dans la locale utilisée, la fonction renvoie "
"l'équivalent en minuscule de I<wc>. Dans les autres cas, I<wc> est renvoyé "
"sans modification."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<towlower_l>()  function performs the same task, but performs the "
"conversion based on the character type information in the locale specified "
"by I<locale>.  The behavior of B<towlower_l>()  is undefined if I<locale> is "
"the special locale object B<LC_GLOBAL_LOCALE> (see B<duplocale>(3))  or is "
"not a valid locale object handle."
msgstr ""
"B<towlower_l>() effectue la même tâche, mais effectue la conversion en "
"utilisant le jeu de caractères de la locale indiquée par I<locale>. Le "
"comportement de B<towlower_l()> est indéfini si I<locale> est la locale "
"spéciale B<LC_GLOBAL_LOCALE> (voir B<duplocale>(3)) ou s'il ne s'agit pas "
"d'une locale valable."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The argument I<wc> must be representable as a I<wchar_t> and be a valid "
"character in the locale or be the value B<WEOF>."
msgstr ""
"Le paramètre I<wc> doit être un caractère descriptible avec le type "
"I<wchar_t> et valable dans la locale utilisée, ou valoir B<WEOF>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<wc> was convertible to lowercase, B<towlower>()  returns its lowercase "
"equivalent; otherwise it returns I<wc>."
msgstr ""
"Si I<wc> est convertible en minuscule, B<towlower>() renvoie son équivalent "
"en minuscule, sinon renvoie I<wc>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. #-#-#-#-#  archlinux: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-38: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-rawhide: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-leap-15-5: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<towlower>()"
msgstr "B<towlower>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. #-#-#-#-#  archlinux: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-38: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-rawhide: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-leap-15-5: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: towlower.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<towlower_l>()"
msgstr "B<towlower_l>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008 (XSI)."
msgstr "C11, POSIX.1-2008 (XSI)."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C99, POSIX.1-2001 (XSI).  Obsolete in POSIX.1-2008 (XSI)."
msgstr "C99, POSIX.1-2001 (XSI). Obsolète dans POSIX.1-2008 (XSI)."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.3.  POSIX.1-2008."
msgstr "glibc 2.3. POSIX.1-2008."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The behavior of these functions depends on the B<LC_CTYPE> category of the "
"locale."
msgstr ""
"Le comportement de ces fonctions dépend de la catégorie B<LC_CTYPE> de la "
"localisation."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These functions are not very appropriate for dealing with Unicode "
"characters, because Unicode knows about three cases: upper, lower, and title "
"case."
msgstr ""
"Ces fonctions ne sont pas très appropriées pour traiter les caractères "
"Unicode, car ils existent en trois casses\\ : «\\ upper\\ », «\\ lower\\ » "
"et «\\ title\\ »."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<iswlower>(3), B<towctrans>(3), B<towupper>(3), B<locale>(7)"
msgstr "B<iswlower>(3), B<towctrans>(3), B<towupper>(3), B<locale>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 décembre 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "The B<towlower_l>()  function first appeared in glibc 2.3."
msgstr "La fonction B<towlower_l>() est apparue dans la glibc 2.3."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"B<towlower>(): C99, POSIX.1-2001 (XSI); present as an XSI extension in "
"POSIX.1-2008, but marked obsolete."
msgstr ""
"B<towlower>() : C99, POSIX.1-2001 (XSI) ; présente dans POSIX.1-2008 en tant "
"qu'extension XS mais marquée obsolète."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<towlower_l>(): POSIX.1-2008."
msgstr "B<towlower_l>() : POSIX.1-2008."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "TOWLOWER"
msgstr "TOWLOWER"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<wint_t towlower(wint_t >I<wc>B<);>\n"
msgstr "B<wint_t towlower(wint_t >I<wc>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<wint_t towlower_l(wint_t >I<wc>B<, locale_t >I<locale>B<);>\n"
msgstr "B<wint_t towlower_l(wint_t >I<wc>B<, locale_t >I<locale>B<);>\n"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Depuis la glibc 2.10 :"

#. type: Plain text
#: opensuse-leap-15-5
msgid "_XOPEN_SOURCE\\ E<gt>=\\ 700"
msgstr "_XOPEN_SOURCE\\ E<gt>=\\ 700"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Avant la glibc 2.10 :"

#. type: Plain text
#: opensuse-leap-15-5
msgid "_GNU_SOURCE"
msgstr "_GNU_SOURCE"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"These functions are not very appropriate for dealing with Unicode "
"characters, because Unicode knows about three cases: upper, lower and title "
"case."
msgstr ""
"Ces fonctions ne sont pas très appropriées pour traiter les caractères "
"Unicode, car ils existent en trois casses\\ : «\\ upper\\ », «\\ lower\\ » "
"et «\\ title\\ »."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
