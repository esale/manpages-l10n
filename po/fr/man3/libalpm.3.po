# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marc Poiroud <marci1@archlinux.fr>, 2009.
# Jean-Jacques Brioist <jean.brioist@numericable.fr>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-09 16:37+0200\n"
"PO-Revision-Date: 2020-10-26 21:46+0100\n"
"Last-Translator: Jean-Jacques Brioist <jean.brioist@numericable.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.1\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "libalpm"
msgstr ""

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "May 2023"
msgid "Fri May 19 2023"
msgstr "Mai 2023"

#. type: SH
#: archlinux
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux
msgid "libalpm - The libalpm Public API"
msgstr ""

#. type: Plain text
#: archlinux
#, no-wrap
msgid " - libalpm is a package management library, primarily used by pacman\\&.  \n"
msgstr ""

#. type: SH
#: archlinux
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: SS
#: archlinux
#, no-wrap
msgid "Modules"
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "I<KeyFile=>"
msgid "B<Files>"
msgstr "I<KeyFile=>"

#. type: Plain text
#: archlinux
msgid "Functions for package files\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "B<group>"
msgid "B<Groups>"
msgstr "B<group>"

#. type: Plain text
#: archlinux
msgid "Functions for package groups\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<Error Codes>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Error codes returned by libalpm\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<Handle>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Functions to initialize and release libalpm\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid "B<Signature checking>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Functions to check signatures\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "I<Independent=>"
msgid "B<Dependency>"
msgstr "I<Independent=>"

#. type: Plain text
#: archlinux
msgid ""
"Functions dealing with libalpm's dependency and conflict information\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "I<FallbackDNS=>"
msgid "B<Callbacks>"
msgstr "I<FallbackDNS=>"

#. type: Plain text
#: archlinux
msgid "Functions and structures for libalpm's callbacks\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "B<alpm_databases>"
msgid "B<Database>"
msgstr "B<alpm_databases>"

#. type: Plain text
#: archlinux
msgid "Functions to query and manipulate the database of libalpm\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "Logging Functions"
msgid "B<Logging Functions>"
msgstr "Fonctions de journalisation"

#. type: Plain text
#: archlinux
msgid "Functions to log using libalpm\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "I<Options=>"
msgid "B<Options>"
msgstr "I<Options=>"

#. type: Plain text
#: archlinux
msgid "Libalpm option getters and setters\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "Package Functions"
msgid "B<Package Functions>"
msgstr "Fonctions opérant sur paquetage"

#. type: Plain text
#: archlinux
msgid "Functions to manipulate libalpm packages\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "B<sigaction>(2)"
msgid "B<Transaction>"
msgstr "B<sigaction>(2)"

#. type: Plain text
#: archlinux
msgid "Functions to manipulate libalpm transactions\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "Miscellaneous Functions"
msgid "B<Miscellaneous Functions>"
msgstr "Fonctions diverses"

#. type: Plain text
#: archlinux
msgid "Various libalpm functions\\&."
msgstr ""

#. type: Plain text
#: archlinux
#, fuzzy
#| msgid "B<alpm_list>"
msgid "B<libalpm_list(3)>"
msgstr "B<alpm_list>"

#. type: Plain text
#: archlinux
msgid "Functions to manipulate B<alpm_list_t> lists\\&."
msgstr ""

#. type: SH
#: archlinux
#, fuzzy, no-wrap
#| msgid "Description"
msgid "Detailed Description"
msgstr "Description"

#. type: Plain text
#: archlinux
msgid "libalpm is a package management library, primarily used by pacman\\&."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"For ease of access, the libalpm manual has been split up into several "
"sections\\&."
msgstr ""
"Pour un accès simplifié, le manuel de la libalpm a été scindé en différentes "
"section\\&."

#. type: SH
#: archlinux
#, fuzzy, no-wrap
#| msgid "See\\ Also"
msgid "See Also"
msgstr "Voir\\ Aussi"

#. type: Plain text
#: archlinux
msgid ""
"B<libalpm_list(3)>, B<libalpm_cb(3)>, B<libalpm_databases(3)>, "
"B<libalpm_depends(3)>, B<libalpm_errors(3)>, B<libalpm_files(3)>, "
"B<libalpm_groups(3)>, B<libalpm_handle(3)>, B<libalpm_log(3)>, "
"B<libalpm_misc(3)>, B<libalpm_options(3)>, B<libalpm_packages(3)>, "
"B<libalpm_sig(3)>, B<libalpm_trans(3)>"
msgstr ""

#. type: SH
#: archlinux
#, fuzzy, no-wrap
#| msgid "author"
msgid "Author"
msgstr "auteur"

#. type: Plain text
#: archlinux
msgid "Generated automatically by Doxygen for libalpm from the source code\\&."
msgstr ""
