# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-06-27 19:40+0200\n"
"PO-Revision-Date: 2023-07-10 08:45+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "nfsservctl"
msgstr "nfsservctl"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 martie 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "nfsservctl - syscall interface to kernel nfs daemon"
msgstr "nfsservctl - interfața syscall pentru demonul nfs din nucleu"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>linux/nfsd/syscall.hE<gt>>\n"
msgstr "B<#include E<lt>linux/nfsd/syscall.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<long nfsservctl(int >I<cmd>B<, struct nfsctl_arg *>I<argp>B<,>\n"
"B<                union nfsctl_res *>I<resp>B<);>\n"
msgstr ""
"B<long nfsservctl(int >I<cmd>B<, struct nfsctl_arg *>I<argp>B<,>\n"
"B<                union nfsctl_res *>I<resp>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<Note>: Since Linux 3.1, this system call no longer exists.  It has been "
"replaced by a set of files in the I<nfsd> filesystem; see B<nfsd>(7)."
msgstr ""
"I<Notă>: Începând cu Linux 3.1, acest apel de sistem nu mai există.  A fost "
"înlocuit cu un set de fișiere din sistemul de fișiere I<nfsd>; a se vedea "
"B<nfsd>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"/*\n"
" * These are the commands understood by nfsctl().\n"
" */\n"
"#define NFSCTL_SVC        0  /* This is a server process. */\n"
"#define NFSCTL_ADDCLIENT  1  /* Add an NFS client. */\n"
"#define NFSCTL_DELCLIENT  2  /* Remove an NFS client. */\n"
"#define NFSCTL_EXPORT     3  /* Export a filesystem. */\n"
"#define NFSCTL_UNEXPORT   4  /* Unexport a filesystem. */\n"
"#define NFSCTL_UGIDUPDATE 5  /* Update a client\\[aq]s UID/GID map\n"
"                                (only in Linux 2.4.x and earlier). */\n"
"#define NFSCTL_GETFH      6  /* Get a file handle (used by mountd(8))\n"
"                                (only in Linux 2.4.x and earlier). */\n"
msgstr ""
"/*\n"
" * Acestea sunt comenzile înțelese de nfsctl().\n"
" */\n"
"#define NFSCTL_SVC        0  /* Acesta este un proces de server. */\n"
"#define NFSCTL_ADDCLIENT  1  /* Adaugă un client NFS. */\n"
"#define NFSCTL_DELCLIENT  2  /* Elimină un client NFS. */\n"
"#define NFSCTL_EXPORT     3  /* Exportă un sistem de fișiere. */\n"
"#define NFSCTL_UNEXPORT   4  /* Anulează exportul unui sistem de fișiere. */\n"
"#define NFSCTL_UGIDUPDATE 5  /* Actualizarea corespondenței UID/GID a unui client\n"
"                                (numai în Linux 2.4.x și versiunile anterioare). */\n"
"#define NFSCTL_GETFH      6  /* Obține un gestionar de fișier (utilizat de mountd(8))\n"
"                                (numai în Linux 2.4.x și versiunile anterioare). */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct nfsctl_arg {\n"
"    int                       ca_version;     /* safeguard */\n"
"    union {\n"
"        struct nfsctl_svc     u_svc;\n"
"        struct nfsctl_client  u_client;\n"
"        struct nfsctl_export  u_export;\n"
"        struct nfsctl_uidmap  u_umap;\n"
"        struct nfsctl_fhparm  u_getfh;\n"
"        unsigned int          u_debug;\n"
"    } u;\n"
"}\n"
msgstr ""
"struct nfsctl_arg {\n"
"    int                       ca_version;     /* protecție „safeguard” */\n"
"    union {\n"
"        struct nfsctl_svc     u_svc;\n"
"        struct nfsctl_client  u_client;\n"
"        struct nfsctl_export  u_export;\n"
"        struct nfsctl_uidmap  u_umap;\n"
"        struct nfsctl_fhparm  u_getfh;\n"
"        unsigned int          u_debug;\n"
"    } u;\n"
"}\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"union nfsctl_res {\n"
"        struct knfs_fh          cr_getfh;\n"
"        unsigned int            cr_debug;\n"
"};\n"
msgstr ""
"union nfsctl_res {\n"
"        struct knfs_fh          cr_getfh;\n"
"        unsigned int            cr_debug;\n"
"};\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOAREA RETURNATĂ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"În caz de succes, se returnează zero.  În caz de eroare, se returnează -1, "
"iar I<errno> este configurată pentru a indica eroarea."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDE"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "ISTORIC"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Removed in Linux 3.1.  Removed in glibc 2.28."
msgstr "Eliminat în Linux 3.1.  Eliminat în glibc 2.28."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<nfsd>(7)"
msgstr "B<nfsd>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIUNI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"This system call was removed in Linux 3.1.  Library support was removed in "
"glibc 2.28."
msgstr ""
"Acest apel de sistem a fost eliminat în Linux 3.1.  Suportul pentru "
"bibliotecă a fost eliminat în glibc 2.28."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "This call is Linux-specific."
msgstr "Acest apel este specific pentru Linux."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "NFSSERVCTL"
msgstr "NFSSERVCTL"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembrie 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manualul programatorului Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"/*\n"
" * These are the commands understood by nfsctl().\n"
" */\n"
"#define NFSCTL_SVC        0  /* This is a server process. */\n"
"#define NFSCTL_ADDCLIENT  1  /* Add an NFS client. */\n"
"#define NFSCTL_DELCLIENT  2  /* Remove an NFS client. */\n"
"#define NFSCTL_EXPORT     3  /* Export a filesystem. */\n"
"#define NFSCTL_UNEXPORT   4  /* Unexport a filesystem. */\n"
"#define NFSCTL_UGIDUPDATE 5  /* Update a client's UID/GID map\n"
"                                (only in Linux 2.4.x and earlier). */\n"
"#define NFSCTL_GETFH      6  /* Get a file handle (used by mountd)\n"
"                                (only in Linux 2.4.x and earlier). */\n"
msgstr ""
"/*\n"
" * Acestea sunt comenzile înțelese de nfsctl().\n"
" */\n"
"#define NFSCTL_SVC        0  /* Acesta este un proces de server. */\n"
"#define NFSCTL_ADDCLIENT  1  /* Adaugă un client NFS. */\n"
"#define NFSCTL_DELCLIENT  2  /* Elimină un client NFS. */\n"
"#define NFSCTL_EXPORT     3  /* Exportă un sistem de fișiere. */\n"
"#define NFSCTL_UNEXPORT   4  /* Anulează exportul unui sistem de fișiere. */\n"
"#define NFSCTL_UGIDUPDATE 5  /* Actualizarea corespondenței UID/GID a unui client\n"
"                                (numai în Linux 2.4.x și versiunile anterioare). */\n"
"#define NFSCTL_GETFH      6  /* Obține un gestionar de fișier (utilizat de mountd)\n"
"                                (numai în Linux 2.4.x și versiunile anterioare). */\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"În caz de succes, se returnează zero.  În caz de eroare, se returnează -1, "
"iar I<errno> este configurată în mod corespunzător."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ÎN CONFORMITATE CU"

# R-GC, scrie:
# COLOPHON (în eng.) = COLOFON (în rom.)
# *****
# Colofon = Notă la sfârșitul unei publicații sau
# pe verso foii de titlu, cuprinzând datele editoriale.
# sau:
# Colofon =  Însemnare la sfârșitul unei cărți în
# epoca manuscriselor și incunabulelor, cuprinzând
# date privind tipograful, locul unde a lucrat,
# autorul și titlul lucrării.
# sau:
# Colofon = Notă, însemnare finală a unei cărți,
# care reproduce sau completează cele spuse în titlu.
# ===============
# M-am decis pentru:
# COLOFON -- NOTĂ FINALĂ
#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON -- NOTĂ FINALĂ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Această pagină face parte din versiunea 4.16 a proiectului Linux I<man-"
"pages>.  O descriere a proiectului, informații despre raportarea erorilor și "
"cea mai recentă versiune a acestei pagini pot fi găsite la \\%https://www."
"kernel.org/doc/man-pages/."
