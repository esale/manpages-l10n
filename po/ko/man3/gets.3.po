# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 류정욱 <compiler@kldp.org>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:29+0200\n"
"PO-Revision-Date: 2000-04-21 08:57+0900\n"
"Last-Translator: 류정욱 <compiler@kldp.org>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "gets"
msgstr "gets"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "gets - get a string from standard input (DEPRECATED)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *gets(char *>I<s>B<);>\n"
msgid "B<[[deprecated]] char *gets(char *>I<s>B<);>\n"
msgstr "B<char *gets(char *>I<s>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<Never use this function>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<gets()> reads a line from I<stdin> into the buffer pointed to by I<s> "
#| "until either a terminating newline or B<EOF>, which it replaces with "
#| "B<'\\e0'>.  No check for buffer overrun is performed (see B<BUGS> below)."
msgid ""
"B<gets>()  reads a line from I<stdin> into the buffer pointed to by I<s> "
"until either a terminating newline or B<EOF>, which it replaces with a null "
"byte (\\[aq]\\e0\\[aq]).  No check for buffer overrun is performed (see BUGS "
"below)."
msgstr ""
"B<gets()>는 표준 입력(I<stdin>)으로부터 개행 문자나 B<EOF>로 끝나는 한 줄을 "
"입력 받아서 I<s>가 가리키는 버퍼에 저장하고 마지막을 B<'\\e0'>로 변경한다. 그"
"러나 버퍼 오버플로우에 대한 검사를 하지 않는다.  (이 점에 대해서는 아래의 B<"
"버그>를 보기 바란다.)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<gets()> and B<fgets()> return I<s> on success, and B<NULL> on error or "
#| "when end of file occurs while no characters have been read."
msgid ""
"B<gets>()  returns I<s> on success, and NULL on error or when end of file "
"occurs while no characters have been read.  However, given the lack of "
"buffer overrun checking, there can be no guarantees that the function will "
"even return."
msgstr ""
"B<gets()>와 B<fgets()>는 성공했을 경우 문자열의 포인터 I<s>를 반환한다. 그리"
"고 에러가 발생하거나 파일의 마지막에 도달한 경우, 또한 아무런 문자도 입력 받"
"지 못한 경우에는 B<NULL>을 반환한다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "속성"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"이 섹션에서 사용되는 용어에 대한 설명은 B<attributes>(7)을 참조하십시오."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "상호 작용"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "속성"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "번호"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<gets>()"
msgstr "B<gets>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr "C89, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"LSB deprecates B<gets>().  POSIX.1-2008 marks B<gets>()  obsolescent.  ISO "
"C11 removes the specification of B<gets>()  from the C language, and since "
"glibc 2.16, glibc header files don't expose the function declaration if the "
"B<_ISOC11_SOURCE> feature test macro is defined."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "버그"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Never use B<gets>().  Because it is impossible to tell without knowing the "
"data in advance how many characters B<gets>()  will read, and because "
"B<gets>()  will continue to store characters past the end of the buffer, it "
"is extremely dangerous to use.  It has been used to break computer "
"security.  Use B<fgets>()  instead."
msgstr ""
"가급적 B<gets>()를 사용하지 마라. B<gets>()가 얼마나 많은 문자를 읽어들일 지 "
"사전에 알 수 없고, 그로 인해 B<gets>()가 버퍼의 용량을 넘어서는 범위에 문자"
"를 저장할 위험이 있다. 그러므로 B<gets>()를 사용하는 것은 컴퓨터 보안에 치명"
"적인 타격을 줄 수 있다.  (버퍼 오버플로우를 이용한 해킹이 가능하다. - 역주) "
"대신 B<fgets>()을 사용하는 것이 좋다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For more information, see CWE-242 (aka \"Use of Inherently Dangerous "
"Function\") at http://cwe.mitre.org/data/definitions/242.html"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<read>(2), B<write>(2), B<ferror>(3), B<fgetc>(3), B<fgets>(3), "
"B<fgetwc>(3), B<fgetws>(3), B<fopen>(3), B<fread>(3), B<fseek>(3), "
"B<getline>(3), B<getwchar>(3), B<puts>(3), B<scanf>(3), B<ungetwc>(3), "
"B<unlocked_stdio>(3), B<feature_test_macros>(7)"
msgstr ""
"B<read>(2), B<write>(2), B<ferror>(3), B<fgetc>(3), B<fgets>(3), "
"B<fgetwc>(3), B<fgetws>(3), B<fopen>(3), B<fread>(3), B<fseek>(3), "
"B<getline>(3), B<getwchar>(3), B<puts>(3), B<scanf>(3), B<ungetwc>(3), "
"B<unlocked_stdio>(3), B<feature_test_macros>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "2023년 2월 5일"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "C99, POSIX.1-2001."
msgstr "C99, POSIX.1-2001."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GETS"
msgstr "GETS"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "2017년 9월 15일"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<char *gets(char *>I<s>B<);>\n"
msgstr "B<char *gets(char *>I<s>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "B<gets()> reads a line from I<stdin> into the buffer pointed to by I<s> "
#| "until either a terminating newline or B<EOF>, which it replaces with "
#| "B<'\\e0'>.  No check for buffer overrun is performed (see B<BUGS> below)."
msgid ""
"B<gets>()  reads a line from I<stdin> into the buffer pointed to by I<s> "
"until either a terminating newline or B<EOF>, which it replaces with a null "
"byte (\\(aq\\e0\\(aq).  No check for buffer overrun is performed (see BUGS "
"below)."
msgstr ""
"B<gets()>는 표준 입력(I<stdin>)으로부터 개행 문자나 B<EOF>로 끝나는 한 줄을 "
"입력 받아서 I<s>가 가리키는 버퍼에 저장하고 마지막을 B<'\\e0'>로 변경한다. 그"
"러나 버퍼 오버플로우에 대한 검사를 하지 않는다.  (이 점에 대해서는 아래의 B<"
"버그>를 보기 바란다.)"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: Plain text
#: opensuse-leap-15-5
msgid "C89, C99, POSIX.1-2001."
msgstr "C89, C99, POSIX.1-2001."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"LSB deprecates B<gets>().  POSIX.1-2008 marks B<gets>()  obsolescent.  ISO "
"C11 removes the specification of B<gets>()  from the C language, and since "
"version 2.16, glibc header files don't expose the function declaration if "
"the B<_ISOC11_SOURCE> feature test macro is defined."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
