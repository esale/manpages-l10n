# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:23+0200\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COMM"
msgstr "COMM"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2023"
msgstr "Tháng 4 năm 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "Compare sorted files FILE1 and FILE2 line by line.\n"
msgid "comm - compare two sorted files line by line"
msgstr "So sánh các tập tin đã sắp xếp TẬP_TIN1 và TẬP_TIN2 theo từng dòng.\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<comm> [I<\\,OPTION\\/>]... I<\\,FILE1 FILE2\\/>"
msgstr "B<comm> [I<\\,TÙY_CHỌN\\/>]… I<\\,TẬP_TIN1 TẬP_TIN2\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Compare sorted files FILE1 and FILE2 line by line."
msgstr "So sánh các tập tin đã sắp xếp TẬP_TIN1 và TẬP_TIN2 theo từng dòng."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "When FILE1 or FILE2 (not both) is -, read standard input."
msgstr ""
"Khi TẬP_TIN1 hoặc TẬP_TIN2 (không phải cả hai) là “-”, thì đọc từ đầu vào "
"tiêu chuẩn."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"With no options, produce three-column output.  Column one contains lines "
"unique to FILE1, column two contains lines unique to FILE2, and column three "
"contains lines common to both files."
msgstr ""
"Khi không có tùy chọn, đưa ra kết quả trong ba cột.  Cột thứ nhất là những "
"dòng chỉ có trong TẬP_TIN1, cột thứ hai chứa những dòng chỉ có trong "
"TẬP_TIN2, và cột thứ ba chứa những dòng có chung trong chúng."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-1>"
msgstr "B<-1>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "suppress column 1 (lines unique to FILE1)"
msgstr "bỏ đi cột 1 (những dòng chỉ có trong TẬP_TIN1)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-2>"
msgstr "B<-2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "suppress column 2 (lines unique to FILE2)"
msgstr "bỏ đi cột 2 (những dòng chỉ có trong TẬP_TIN2)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-3>"
msgstr "B<-3>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "suppress column 3 (lines that appear in both files)"
msgstr "bỏ đi cột 3 (những dòng có trong cả hai tập tin)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--check-order>"
msgstr "B<--check-order>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"check that the input is correctly sorted, even if all input lines are "
"pairable"
msgstr ""
"kiểm tra dữ liệu đầu vào được sắp xếp đúng, thậm chí nếu mọi dòng đầu vào "
"đều có thể kết đôi được"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--nocheck-order>"
msgstr "B<--nocheck-order>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not check that the input is correctly sorted"
msgstr "không kiểm tra xem đầu vào đã xắp xếp đúng chưa"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--output-delimiter>=I<\\,STR\\/>"
msgstr "B<--output-delimiter>=I<\\,CHUỖI\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "separate columns with STR"
msgstr "phân cách các cột bằng chuỗi này"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--total>"
msgstr "B<--total>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output a summary"
msgstr "xuất tổng thể"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero-terminated>"
msgstr "B<-z>, B<--zero-terminated>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "line delimiter is NUL, not newline"
msgstr "bộ phân tách dòng là NUL, không phải ký tự dòng mới"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "hiển thị trợ giúp này rồi thoát"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "đưa ra thông tin phiên bản rồi thoát"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Note, comparisons honor the rules specified by 'LC_COLLATE'."
msgstr "Chú ý là so sánh tuân theo quy tắc quy định bởi “LC_COLLATE”."

# type: =head1
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "VÍ DỤ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "comm -12 file1 file2"
msgstr "comm -12 tin1 tin2"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Print only lines present in both file1 and file2."
msgstr "In ra chỉ những dòng nằm trong cả hai tập tin."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "comm -3 file1 file2"
msgstr "comm -3 tin1 tin2"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Print lines in file1 not in file2, and vice versa."
msgstr "In ra những dòng chỉ nằm trong một của hai tập tin này."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "TÁC GIẢ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Richard M. Stallman and David MacKenzie."
msgstr "Viết bởi Richard M. Stallman và David MacKenzie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Trợ giúp trực tuyến GNU coreutils: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Report %s translation bugs to <https://translationproject.org/team/>\n"
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Hãy thông báo lỗi dịch “%s” cho <https://translationproject.org/team/vi."
"html>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "BẢN QUYỀN"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Đây là phần mềm tự do: bạn có quyền sửa đổi và phát hành lại nó. KHÔNG CÓ "
"BẢO HÀNH GÌ CẢ, với điều khiển được pháp luật cho phép."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "join(1), uniq(1)"
msgid "B<join>(1), B<uniq>(1)"
msgstr "B<join>(1), B<uniq>(1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/commE<gt>"
msgstr ""
"Tài liệu đầy đủ có tại: E<lt>https://www.gnu.org/software/coreutils/commE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) comm invocation\\(aq"
msgstr ""
"hoặc sẵn có nội bộ thông qua: info \\(aq(coreutils) comm invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Tháng 9 năm 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: TH
#: fedora-38
#, no-wrap
msgid "January 2023"
msgstr "Tháng 1 năm 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Tháng 4 năm 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "Tháng 10 năm 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Giấy phép GPL pb3+ : "
"Giấy phép Công cộng GNU phiên bản 3 hay sau E<lt>https://gnu.org/licenses/"
"gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-5
msgid "join(1), uniq(1)"
msgstr "B<join>(1), B<uniq>(1)"
