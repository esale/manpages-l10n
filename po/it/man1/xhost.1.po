# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Marco Curreli <marcocurreli@tiscali.it>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-06-27 20:02+0200\n"
"PO-Revision-Date: 2020-05-23 09:16+0200\n"
"Last-Translator: Marco Curreli <marcocurreli@tiscali.it>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "XHOST"
msgstr "XHOST"

#. type: TH
#: archlinux mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "xhost 1.0.9"
msgstr "xhost 1.0.9"

#. type: TH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "X Version 11"
msgstr "X Version 11"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "xhost - server access control program for X"
msgstr "xhost - programma di controllo degli accessi al server X"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINTASSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<xhost> [[+-]name ...]"
msgstr "B<xhost> [[+-]nome ...]"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"The I<xhost> program is used to add and delete host names or user names to "
"the list allowed to make connections to the X server.  In the case of hosts, "
"this provides a rudimentary form of privacy control and security.  It is "
"only sufficient for a workstation (single user) environment, although it "
"does limit the worst abuses.  Environments which require more sophisticated "
"measures should implement the user-based mechanism or use the hooks in the "
"protocol for passing other authentication data to the server."
msgstr ""
"Il programma x<host> E' utilizzato per aggiungere e cancellare nomi di host "
"o nomi di utente dalla lista abilitata a connettersi al server X. Nel caso "
"degli host, ciò costituisce una rudimentale forma di controllo della privacy "
"e della sicurezza. E' sufficiente solo per un ambiente workstation (singolo "
"utilizzatore), sebbene esso limiti i peggiori abusi. Ambienti che richiedano "
"misure più sofisticate dovrebbero implementare il meccanismo \"user-based\" "
"o utilizzare gli hook nel protocollo per passare altri dati di "
"autenticazione al server."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPZIONI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<Xhost> accepts the following command line options described below.  For "
"security, the options that affect access control may only be run from the "
"\"controlling host\".  For workstations, this is the same machine as the "
"server.  For X terminals, it is the login host."
msgstr ""
"I<Xhost> accetta le seguenti opzioni a linea di comando descritte sotto. Per "
"sicurezza, le opzioni che effettuano il controllo di accesso possono essere "
"avviate solo dal \"host di controllo\". Per le workstation, questo è la "
"stessa macchina server. Per terminali X, è l'host di login."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-help>"
msgstr "B<-help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Prints a usage message."
msgstr "Stampa un messaggio sull'utilizzo."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<[+]>I<name>"
msgstr "B<[+]>I<nome>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"The given I<name> (the plus sign is optional)  is added to the list allowed "
"to connect to the X server.  The name can be a host name or a complete name "
"(See E<.SM> B<NAMES> for more details)."
msgstr ""
"Il dato I<nome> (il segno più è opzionale) è aggiunto alla lista abilitata "
"alla connessione al server X. Il \"nome\" può essere un nome di host o un "
"nome di utente."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<->I<name>"
msgstr "B<->I<nome>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"The given I<name> is removed from the list of allowed to connect to the "
"server.  The name can be a host name or a complete name (See E<.SM> B<NAMES> "
"for more details).  Existing connections are not broken, but new connection "
"attempts will be denied.  Note that the current machine is allowed to be "
"removed; however, further connections (including attempts to add it back) "
"will not be permitted.  Resetting the server (thereby breaking all "
"connections)  is the only way to allow local connections again."
msgstr ""
"Il dato I<nome> è rimosso dalla lista degli abilitati alla connessione al "
"server X. Il \"nome\" può essere un nome di host o un nome di utente. Le "
"connessioni in corso non vengono interrotte ma le nuove connessioni vengono "
"negate. Notare che la macchina utilizzata per la connessione è abilitata ad "
"essere rimossa; tuttavia ulteriori connessioni (incluso il tentativo di "
"aggiungerla alla lista) non saranno permesse. Riavviare il server "
"(interrompendo così tutte le connessioni in corso) è l'unica via per "
"riattivare le connessioni locali."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<\\+>"
msgstr "B<\\+>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Access is granted to everyone, even if they aren't on the list (i.e., access "
"control is turned off)."
msgstr ""
"L'accesso è possibile per chiunque, anche se non presente nell'elenco "
"(ovvero il controllo degli accessi è disabilitato)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<->"
msgstr "B<->"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Access is restricted to only those on the list (i.e., access control is "
"turned on)."
msgstr ""
"L'accesso è consentito solo a coloro che sono sulla lista di accesso (ovvero "
"il controllo degli accessi e attivato)."

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "I<nothing>"
msgstr "I<niente>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"If no command line arguments are given, a message indicating whether or not "
"access control is currently enabled is printed, followed by the list of "
"those allowed to connect.  This is the only option that may be used from "
"machines other than the controlling host."
msgstr ""
"Se non viene passato nessun argomento sulla linea di comando, un messaggio "
"stampato indica se o meno il controllo degli accessi è abilitato, seguito "
"dalla lista di quelli abilitati alla connnessione. Questa è l'unica opzione "
"che può essere utilizzata da macchine diverse dell'host di controllo."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAMES"
msgstr "NOMI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"A complete name has the syntax ``family:name'' where the families are as "
"follows:"
msgstr ""
"Un nome completo ha la sintassi ``famiglia:nome'' dove \"famiglia\" può "
"essere come le seguenti:"

#. type: ta
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "1i"
msgstr "1i"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy, no-wrap
msgid ""
"inet\tInternet host (IPv4)\n"
"inet6\tInternet host (IPv6)\n"
"dnet\tDECnet host\n"
"nis\tSecure RPC network name\n"
"krb\tKerberos V5 principal\n"
"local\tcontains only one name, the empty string\n"
"si\tServer Interpreted\n"
msgstr ""
"inet\tInternet host (IPv4)\n"
"inet6\tInternet host (IPv6)\n"
"dnet\tDECnet host\n"
"nis\tSecure RPC network name\n"
"krb\tKerberos V5 principale\n"
"local\tcontiene solo un nome, la stringa vuota\n"
"si\tServer Interpreted (interpretato dal server)\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The family is case insensitive.  The format of the name varies with the "
"family."
msgstr ""
"La \"famiglia\" non è sensibile alle maiuscole. Il formato del \"nome\" "
"varia a seconda della \"famiglia\"."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"When Secure RPC is being used, the network independent netname (e.g., \"nis:"
"unix.I<uid>@I<domainname>\") can be specified, or a local user can be "
"specified with just the username and a trailing at-sign (e.g., \"nis:pat@\")."
msgstr ""
"Quando viene utilizzata una connessione Secure RPC può essere specificato "
"l'indirizzo relativo della rete (es., \"nis:unix.I<uid>@I<domainname>\"), o "
"può essere specificato un utente locale con solo il nome utente seguito dal "
"simbolo at (es., \"nis:pat@\")."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"For backward compatibility with pre-R6 I<xhost>, names that contain an at-"
"sign (@) are assumed to be in the nis family.  Otherwise they are assumed to "
"be Internet addresses. If compiled to support IPv6, then all IPv4 and IPv6 "
"addresses returned by getaddrinfo(3) are added to the access list in the "
"appropriate inet or inet6 family."
msgstr ""
"Per la compatibilità all'indietro con i nomi di host Pre-R6 che contengono "
"il simbolo at (@), I<xhost> presume che appartengano alla famiglia nis. "
"Altrimenti si presume che siano indirizzi internet. Se compilato con il "
"supporto IPv6, allora tutti gli indirizzi IPv4 e IPv6 ricevuti da "
"getaddrinfo(3) sono aggiunti alla lista degli accessi nella famiglia inet o "
"inet6 appropriata."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"The local family specifies all the local connections at once. However, the "
"server interpreted address \"si:localuser:I<username>\" can be used to "
"specify a single local user. (See the I<Xsecurity>(7) manual page for more "
"details.)"
msgstr ""
"Gli indirizzi interpretati dal server sono composti da caratteri case-"
"sensitive, tag e stringhe rappresentanti un valore dato, separati da \":\". "
"Per esempio, \"si:hostname:almas\" è l'indirizzo interrpretato di un server "
"di tipo I<hostname>, con valore di I<almas>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Server interpreted addresses consist of a case-sensitive type tag and a "
"string representing a given value, separated by a colon.  For example, \"si:"
"hostname:almas\" is a server interpreted address of type I<hostname>, with a "
"value of I<almas>.  For more information on the available forms of server "
"interpreted addresses, see the I<Xsecurity>(7)  manual page."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The initial access control list for display number B<n> may be set by the "
"file I</etc/X>B<n>I<.hosts>, where B<n> is the display number of the "
"server.  See I<Xserver>(1)  for details."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DIAGNOSTICS"
msgstr "DIAGNOSTICA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"For each name added to the access control list, a line of the form \"I<name> "
"being added to access control list\" is printed.  For each name removed from "
"the access control list, a line of the form \"I<name> being removed from "
"access control list\" is printed."
msgstr ""
"Per ogni nome aggiunto alla lista di controllo degli accessi viene stampata "
"una linea della forma \"I<nome> being added to access control list\". Per "
"ogni nome rimosso dalla lista di controllo degli accessi, viene stampata una "
"linea della forma \"I<nome> being removed from access control list\"."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

# FIXME X(7), Xsecurity(7), Xserver(1), xdm(1), xauth(1), getaddrinfo(3) → B<X>(7), B<Xsecurity>(7), B<Xserver>(1), B<xdm>(1), B<xauth>(1), B<getaddrinfo>(3)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "X(7), Xsecurity(7), Xserver(1), xdm(1), xauth(1), getaddrinfo(3)"
msgstr ""
"B<X>(7), B<Xsecurity>(7), B<Xserver>(1), B<xdm>(1), B<xauth>(1), "
"B<getaddrinfo>(3)"

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "AMBIENTE"

#. type: TP
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<DISPLAY>"
msgstr "B<DISPLAY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "to get the default host and display to use."
msgstr "per assegnare l'host e il display predefiniti da usare."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"You can't specify a display on the command line because B<-display> is a "
"valid command line argument (indicating that you want to remove the machine "
"named I<``display''> from the access list)."
msgstr ""
"Non si può specificare un display sulla linea di comando, poichè B<-display> "
"è un argomento valido per la linea di comando (che indica che si vuole "
"rimuovere la macchina chiamata I<``display''> dalla lista di controllo degli "
"accessi.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, fuzzy
msgid ""
"The X server stores network addresses, not host names, unless you use the "
"server-interpreted hostname type address.  If somehow you change a host's "
"network address while the server is still running, and you are using a "
"network-address based form of authentication, I<xhost> must be used to add "
"the new address and/or remove the old address."
msgstr ""
"Il server X carica gli indirizzi di rete, non i nomi degli host, a meno che "
"non si usino gli indirizzi degli host \"server-interpreted\". Se in qualche "
"modo si cambia un indirizzo di rete di un host mentre il server è ancora in "
"funzione, e si sta utilizzando un form di autenticazione basato sugli "
"indirizzi di rete, I<xhost> può essere usato per aggiungere il nuovo "
"indirizzo e/o rimuovere il vecchio indirizzo."

#. type: SH
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Bob Scheifler, MIT Laboratory for Computer Science,"
msgstr "Bob Scheifler, MIT Laboratory for Computer Science,"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Jim Gettys, MIT Project Athena (DEC)."
msgstr "Jim Gettys, MIT Project Athena (DEC)."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "xhost 1.0.8"
msgstr "xhost 1.0.8"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "xhost 1.0.7"
msgstr "xhost 1.0.7"
