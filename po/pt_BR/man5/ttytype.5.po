# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2000.
# Carlos Augusto Horylka <horylka@conectiva.com.br>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:59+0200\n"
"PO-Revision-Date: 2000-05-31 17:26+0200\n"
"Last-Translator: Carlos Augusto Horylka <horylka@conectiva.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ttytype"
msgstr "ttytype"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-01-22"
msgstr "22 janeiro 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ttytype - terminal device to default terminal type mapping"
msgstr ""
"ttytype - dispositivo de terminal para mapeamento padrão do tipo de terminal"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I</etc/ttytype> file associates termcap/terminfo terminal type names "
#| "with tty lines.  Each line consists of a terminal type, followed by "
#| "whitespace, followed by a tty name (a device name without the I</dev/>) "
#| "prefix."
msgid ""
"The I</etc/ttytype> file associates B<termcap>(5)  and B<terminfo>(5)  "
"terminal type names with tty lines.  Each line consists of a terminal type, "
"followed by whitespace, followed by a tty name (a device name without the I</"
"dev/> prefix)."
msgstr ""
"O arquivo I</etc/ttytype> associa nomes de tipo de terminal termcap/terminfo "
"com as linhas tty. Cada linha consiste do tipo de terminal, seguido por "
"espaço, seguido pelo um nome tty (um nome dispositivo de sem o prefixo I</"
"dev/>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This association is used by the program B<tset>(1)  to set the environment "
"variable B<TERM> to the default terminal name for the user's current tty."
msgstr ""
"Esta associação é usada pelo programa B<tset>(1) para selecionar a varável "
"de ambiente B<TERM> para o nome de terminal padrão para o usuários atuais da "
"tty."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This facility was designed for a traditional time-sharing environment "
#| "featuring character-cell terminals hardwired to a Unix minicomputer.  It "
#| "is little used on modern workstation and personal Unixes."
msgid ""
"This facility was designed for a traditional time-sharing environment "
"featuring character-cell terminals hardwired to a UNIX minicomputer.  It is "
"little used on modern workstation and personal UNIX systems."
msgstr ""
"Esta facilidade foi projetada para um ambiente de associação de tempo "
"tradicional retratando uma estrututa de terminais em células de caracteres "
"para mini-computadores Unix. Isto é pouco usado em modernas Estações de "
"Trabalho e Unixes pessoais."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARQUIVOS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/ttytype>"
msgstr "I</etc/ttytype>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "the tty definitions file."
msgstr "o arquivo de definição tty."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "A typical I</etc/ttytype> is:"
msgstr "Um típico I</etc/ttytype> é:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"con80x25 tty1\n"
"vt320 ttys0\n"
msgstr ""
"con80x25 tty1\n"
"vt320 ttys0\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<termcap>(5), B<terminfo>(5), B<agetty>(8), B<mingetty>(8)"
msgstr "B<termcap>(5), B<terminfo>(5), B<agetty>(8), B<mingetty>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "TTYTYPE"
msgstr "TTYTYPE"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2012-12-31"
msgstr "31 dezembro 2012"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "The I</etc/ttytype> file associates termcap/terminfo terminal type names "
#| "with tty lines.  Each line consists of a terminal type, followed by "
#| "whitespace, followed by a tty name (a device name without the I</dev/>) "
#| "prefix."
msgid ""
"The I</etc/ttytype> file associates B<termcap>(5)/B<terminfo>(5)  terminal "
"type names with tty lines.  Each line consists of a terminal type, followed "
"by whitespace, followed by a tty name (a device name without the I</dev/>) "
"prefix."
msgstr ""
"O arquivo I</etc/ttytype> associa nomes de tipo de terminal termcap/terminfo "
"com as linhas tty. Cada linha consiste do tipo de terminal, seguido por "
"espaço, seguido pelo um nome tty (um nome dispositivo de sem o prefixo I</"
"dev/>)."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLO"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."
