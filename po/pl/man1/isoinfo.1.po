# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Bartłomiej Sowa <bartowl@kki.net.pl>, 2000.
# Michał Kułach <michal.kulach@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-06-27 19:33+0200\n"
"PO-Revision-Date: 2022-01-28 22:50+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ISOINFO"
msgstr "ISOINFO"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "04/06/01"
msgstr "04/06/01"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Version 2.0"
msgstr "Wersja 2.0"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"devdump, isoinfo, isovfy, isodump - Utility programs for dumping and "
"verifying iso9660 images."
msgstr ""
"devdump, isoinfo, isovfy, isodump - Programy użytkowe do zrzucania i "
"weryfikacji obrazów iso9660."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<devdump> I<isoimage>"
msgstr "B<devdump> I<obraz_iso>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<isodump> I<isoimage>"
msgstr "B<isodump> I<obraz_iso>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"B<isoinfo> [ B<-d> ] [ B<-h> ] [ B<-R> ] [ B<-J> ] [ B<-j> I<charset> ] [ B<-"
"f> ] [ B<-l> ] [ B<-p> ] [ B<-T> I<sector> ] [ B<-N> I<sector> ] [ B<-i> "
"I<isoimage> ] [ B<-x> I<path> ]"
msgstr ""
"B<isoinfo> [ B<-d> ] [ B<-h> ] [ B<-R> ] [ B<-J> ] [ B<-j> "
"I<zestaw_znaków> ] [ B<-f> ] [ B<-l> ] [ B<-p> ] [ B<-T> I<sektor> ] [ B<-N> "
"I<sektor> ] [ B<-i> I<obraz_iso> ] [ B<-x> I<ścieżka> ]"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<isovfy> I<isoimage>"
msgstr "B<isovfy> I<obraz_iso>"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"B<devdump> is a crude utility to interactively display the contents of "
"device or filesystem images.  The initial screen is a display of the first "
"256 bytes of the first 2048 byte sector.  The commands are the same as with "
"B<isodump>."
msgstr ""
"B<devdump> jest prostym narzędziem interaktywnie wyświetlającym zawartość "
"urządzenia lub obraz systemu plików.  Początkowy obraz to pierwsze 256 "
"bajtów pierwszego 2048-bajtowego sektora.  Polecenia są takie same jak w "
"przypadku B<isodump>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"B<isodump> is a crude utility to interactively display the contents of "
"iso9660 images in order to verify directory integrity.  The initial screen "
"is a display of the first part of the root directory, and the prompt shows "
"you the extent number and offset in the extent."
msgstr ""
"B<isodump> jest prostym narzędziem interaktywnie wyświetlającym zawartość "
"obrazów iso9660 w celu weryfikacji integralności katalogów.  Początkowy "
"obraz to pierwsza część katalogu głównego, i znak zachęty wyświetlający "
"zakres i przesunięcie w tym zakresie."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"You can use the 'a' and 'b' commands to move backwards and forwards within "
"the image. The 'g' command allows you to goto an arbitrary extent, and the "
"'f' command specifies a search string to be used. The '+' command searches "
"forward for the next instance of the search string, and the 'q' command "
"exits B<devdump> or B<isodump>."
msgstr ""
"Można użyć poleceń 'a' i 'b' aby poruszać się w przód i w tył wewnątrz "
"obrazu. Polecenie 'g' pozwala przeskoczyć do konkretnego miejsca, a "
"polecenie 'f' pozwala na wpisanie frazy wyszukiwania. Polecenie '+' "
"przeszukuje w przód aż do następnego wystąpienia frazy wyszukiwania, a "
"polecenie 'q' kończy działanie programu B<devdump> lub B<isodump>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"B<isoinfo> is a utility to perform directory like listings of iso9660 images."
msgstr ""
"B<isoinfo> jest narzędziem pozwalającym na listowanie zawartości obrazu "
"iso9660 tak jak katalogu."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"B<isovfy> is a utility to verify the integrity of an iso9660 image. Most of "
"the tests in B<isovfy> were added after bugs were discovered in early "
"versions of B<genisoimage.> It isn't all that clear how useful this is "
"anymore, but it doesn't hurt to have this around."
msgstr ""
"B<isovfy> jest narzędziem do weryfikacji integralności obrazu iso9660. "
"Większość testów w B<isovfy> zostało dodanych po tym jak odnaleziono błędy "
"we wczesnych wersjach B<genisoimage>. Trudno określić jak użyteczne jest to "
"aktualnie, ale nie zaszkodzi spróbować."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The options common to all programs are B<-help>,B<-h>,B<-version>, "
"B<i>I<=name,>B<dev>I<=name.> The B<isoinfo> program has additional command "
"line options. The options are:"
msgstr ""
"Opcjami wspólnymi dla wszystkich programów są B<-help>, B<-h>, B<-version>, "
"B<i=>I<nazwa>, B<dev=>I<nazwa>. Program B<isoinfo> ma dodatkowe opcje."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-help>"
msgstr "B<-help>"

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "print a summary of all options."
msgstr "wyświetla podsumowanie wszystkich opcji."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-d>"
msgstr "B<-d>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Print information from the primary volume descriptor (PVD) of the iso9660 "
"image. This includes information about Rock Ridge, Joliet extensions and "
"Eltorito boot information if present."
msgstr ""
"Wypisuje informacje z podstawowego deskryptora woluminu (ang. primary volume "
"descriptor - PVD) obrazu iso9660. Obejmuje to informacje o Rock Ridge, "
"rozszerzeniach Joliet i informacjach rozruchowych Eltorito - jeśli są obecne."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"generate output as if a 'find . -print' command had been run on the iso9660 "
"image. You should not use the B<-l> image with the B<-f> option."
msgstr ""
"tworzy taki wynik, jakby ktoś wykonał 'find . -print' na obrazie iso9660. "
"Nie należy używać obrazu B<-l> razem z opcją B<-f>."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-i iso_image>"
msgstr "B<-i obraz_iso>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Specifies the path of the iso9660 image that we wish to examine.  The "
"options B<-i> and B<dev=>I<target> are mutual exclusive."
msgstr ""
"Określa ścieżkę do obrazu iso9660, który chcemy sprawdzić. Opcje B<-i> oraz "
"B<dev=>I<cel> wzajemnie się wykluczają."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dev=>I<target>"
msgstr "B<dev=>I<cel>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Sets the SCSI target for the drive, see notes above.  A typical device "
"specification is B<dev=>I<6,0> \\&.  If a filename must be provided together "
"with the numerical target specification, the filename is implementation "
"specific.  The correct filename in this case can be found in the system "
"specific manuals of the target operating system.  On a I<FreeBSD> system "
"without I<CAM> support, you need to use the control device (e.g.  I</dev/"
"rcd0.ctl>).  A correct device specification in this case may be B<dev=>I</"
"dev/rcd0.ctl:@> \\&."
msgstr ""
"Ustawia cel SCSI jako napęd, zob. uwagi powyżej. Zwykle ma on postać "
"B<dev=>I<6,0> \\&. Jeśli konieczne jest podanie nazwy pliku z określeniem "
"numerycznym, nazwa pliku zależy od implementacji. Prawidłową nazwę pliku "
"można wówczas znaleźć w podręczniku systemowym docelowego systemu "
"operacyjnego. W systemie I<FreeBSD> bez obsługi I<CAM>, konieczne jest "
"użycia urządzenia sterującego (np.  I</dev/rcd0.ctl>).  Prawidłowa postać "
"I<celu> może być wówczas następująca: B<dev=>I</dev/rcd0.ctl:@> \\&."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"On Linux, drives connected to a parallel port adapter are mapped to a "
"virtual SCSI bus. Different adapters are mapped to different targets on this "
"virtual SCSI bus."
msgstr ""
"W Linuksie, napędy podłączone do adaptera portu równoległego są przypisane "
"wirtualnej szynie SCSI. Różne adaptery są mapowane do różnych celów tejże "
"szyny."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"If no I<dev> option is present, the program will try to get the device from "
"the B<CDR_DEVICE> environment."
msgstr ""
"Jeśli nie poda się opcji B<dev>, program spróbuje uzyskać urządzenie ze "
"zmiennej środowiskowej B<CDR_DEVICE>."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"If the argument to the B<dev=> option does not contain the characters ',', "
"'/', '@' or ':', it is interpreted as an label name that may be found in the "
"file /etc/wodim.conf (see FILES section)."
msgstr ""
"Jeśli argument podany opcji B<dev=> nie zawiera następujących znaków: \",\", "
"\"/\", \"@\" lub \":\", jest interpretowany jako nazwa etykiety, taka jak w "
"pliku /etc/wodim.conf (zob. rozdział PLIKI)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "The options B<-i> and B<dev=>I<target> are mutual exclusive."
msgstr "Opcje B<-i> i B<dev=>I<cel> wzajemnie się wykluczają."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"generate output as if a 'ls -lR' command had been run on the iso9660 image.  "
"You should not use the B<-f> image with the B<-l> option."
msgstr ""
"tworzy taki wynik, jakby ktoś wykonał 'ls -lR' na obrazie iso9660. Nie "
"należy używać obrazu B<-f> razem z opcją B<-l>."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-N sector>"
msgstr "B<-N sektor>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Quick hack to help examine single session disc files that are to be written "
"to a multi-session disc. The sector number specified is the sector number at "
"which the iso9660 image should be written when send to the cd-writer. Not "
"used for the first session on the disc."
msgstr ""
"Szybki sposób na analizowanie jednosesyjnych obrazów, która mają być "
"wypalone na płycie wielosesyjnej. Podany numer sektora jest sektorem, od "
"którego obraz iso9660 powinien być zapisany przy wysyłaniu do nagrywarki. "
"Opcja nie służy do nagrywania pierwszej sesji na dysku."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Print path table information."
msgstr "Wyświetla informację o tablicy ścieżek."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-R>"
msgstr "B<-R>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Extract information from Rock Ridge extensions (if present) for permissions, "
"file names and ownerships."
msgstr ""
"Wyciąga informacje z rozszerzenia Rock Ridge (jeśli jest obecne) o prawach, "
"nazwach plików i właścicielu."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-J>"
msgstr "B<-J>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Extract information from Joliet extensions (if present) for file names."
msgstr ""
"Wyciąga informacje z rozszerzenia Joliet (jeśli jest obecne) o nazwach "
"plików."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-j charset>"
msgstr "B<-j zestaw_znaków>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Convert Joliet file names (if present) to the supplied charset. See "
"B<genisoimage>(8)  for details."
msgstr ""
"Przekształca nazwy plików Joliet (jeśli są obecne) do podanego zestawu "
"znaków. Więcej informacji w podręczniku B<genisoimage>(8) "

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-T sector>"
msgstr "B<-T sektor>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Quick hack to help examine multi-session images that have already been "
"burned to a multi-session disc. The sector number specified is the sector "
"number for the start of the session we wish to display."
msgstr ""
"Szybki sposób na analizowanie wielosesyjnych obrazów, które zostały już "
"wypalone na płycie wielosesyjnej. Numer sektora podany tutaj to numer "
"sektora, w którym zaczyna się dana sesja na płycie."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-x pathname>"
msgstr "B<-x ścieżka>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Extract specified file to stdout."
msgstr "Wyodrębnia podany plik na standardowe wyjście."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The author of the original sources (1993 .\\|.\\|. 1998) is Eric Youngdale "
"E<lt>ericy@gnu.ai.mit.eduE<gt> or E<lt>eric@andante.jic.comE<gt> is to blame "
"for these shoddy hacks.  J\\*org Schilling wrote the SCSI transport library "
"and its adaptation layer to the programs and newer parts (starting from "
"1999) of the utilities, this makes them Copyright (C) 1999-2004 J\\*org "
"Schilling.  Patches to improve general usability would be gladly accepted."
msgstr ""
"O te liche dodatki należy winić Erica Youngdale'a E<lt>ericy@gnu.ai.mit."
"eduE<gt> lub E<lt>eric@andante.jic.comE<gt> - autora oryginalnych źródeł "
"(1993 .\\|.\\|. 1998) . J\\*org Schilling utworzył bibliotekę transportową "
"SCSI i jej warstwę kompatybilności do programów, a także nowsze części "
"(począwszy od 1999) narzędzi, co upoważnia go do opatrzenia ich etykietą "
"Copyright (C) 1999-2004 J\\*org Schilling. Patche poprawiające używalność "
"byłyby mile widziane."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"This manpage describes the program implementation of B<isoinfo> as shipped "
"by the cdrkit distribution. See B<http://alioth.debian.org/projects/debburn/"
"> for details. It is a spinoff from the original program distributed in the "
"cdrtools package [1]. However, the cdrtools developers are not involved in "
"the development of this spinoff and therefore shall not be made responsible "
"for any problem caused by it. Do not try to get support for this program by "
"contacting the original author(s)."
msgstr ""
"Niniejsza strona podręcznika systemowego opisuje implementację B<isoinfo> "
"dostarczaną przez dystrybucję cdrkit. Więcej informacji pod adresem B<http://"
"alioth.debian.org/projects/debburn/>. Jest to odmiana oryginalnego programu "
"dystrybuowana z pakietem cdrtools[1]. Deweloperzy cdrtools nie są jednak "
"odpowiedzialni za jej rozwój, ani za problemy wynikłe z jej działania. Z "
"tego powodu prosimy o niekontaktowanie się z oryginalnymi autorami, w celu "
"uzyskania wsparcia."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "If you have support questions, send them to"
msgstr ""
"Pytania dotyczące programu można kierować (w języku angielskim) pod adres"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<debburn-devel@lists.alioth.debian.org>"
msgstr "B<debburn-devel@lists.alioth.debian.org>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "If you have definitely found a bug, send a mail to this list or to"
msgstr ""
"Jeśli natomiast odkryło się niewątpliwy błąd, należy wysłać wiadomość (w "
"języku angielskim) na tę listę lub pod adres"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<submit@bugs.debian.org>"
msgstr "B<submit@bugs.debian.org>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"writing at least a short description into the Subject and \"Package: "
"cdrkit\" into the first line of the mail body."
msgstr ""
"z co najmniej krótkim opisem w tytule; przy czym pierwszy wiersz wiadomości "
"musi brzmieć \"Package: cdrkit\"."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr "BŁĘDY"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "The user interface really sucks."
msgstr "Interfejs użytkownika jest do bani."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FUTURE IMPROVEMENTS"
msgstr "PRZYSZŁE UDOSKONALENIA"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"These utilities are really quick hacks, which are very useful for debugging "
"problems in genisoimage or in an iso9660 filesystem. In the long run, it "
"would be nice to have a daemon that would NFS export a iso9660 image."
msgstr ""
"Te narzędzia to tylko szybkie sztuczki, które są bardzo użyteczne podczas "
"odpluskwiania problemów w genisoimage lub systemie plików iso9660. Na "
"dłuższą metę, byłoby miło mieć demon, który by eksportował obraz iso9660 po "
"NFS-ie."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The isoinfo program is probably the program that is of the most use to the "
"general user."
msgstr ""
"Isoinfo jest programem, który jest prawdopodobnie najbardziej użyteczny dla "
"zwykłego użytkownika."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AVAILABILITY"
msgstr "DOSTĘPNOŚĆ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"These utilities come with the B<cdrkit> package, and the primary download "
"site is http://debburn.alioth.debian.org/ and FTP mirrors of distributions.  "
"Despite the name, the software is not beta."
msgstr ""
"Te narzędzia zostały dostarczone wraz z pakietem B<cdrtools>, a główną "
"stroną do pobrań jest https://sourceforge.net/projects/cdrtools/files/ oraz "
"serwery lustrzane FTP dystrybucji. Pomimo nazwy, oprogramowanie to nie jest "
"wersją beta."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ŚRODOWISKO"

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<CDR_DEVICE>"
msgstr "B<CDR_DEVICE>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"This may either hold a device identifier that is suitable to the open call "
"of the SCSI transport library or a label in the file /etc/wodim.conf."
msgstr ""
"Zmienna może zawierać albo identyfikator urządzenia zdolnego do otwierania "
"wywołań biblioteki transportowej SCSI lub etykietę z pliku /etc/wodim.conf."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<RSH>"
msgstr "B<RSH>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"If the B<RSH> environment is present, the remote connection will not be "
"created via B<rcmd>(3)  but by calling the program pointed to by B<RSH>.  "
"Use e.g.  B<RSH=>/usr/bin/ssh to create a secure shell connection."
msgstr ""
"Jeśli zmienna B<RSH> jest ustawiona, zdalne połączenia nie będą tworzone "
"poleceniem B<rcmd>(3) lecz wywołaniem programu, na który wskazuje zmienna "
"B<RSH>.  Przykładowo podanie B<RSH=>I</usr/bin/ssh> spowoduje zestawienie "
"bezpiecznego połączenia powłoki."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Note that this forces the program to create a pipe to the B<rsh(1)> program "
"and disallows the program to directly access the network socket to the "
"remote server.  This makes it impossible to set up performance parameters "
"and slows down the connection compared to a B<root> initiated B<rcmd(3)> "
"connection."
msgstr ""
"Proszę zauważyć, że zmusza to program do utworzenia potoku do programu "
"B<rsh(1)> i nie pozwala na bezpośredni dostęp do gniazda sieciowego zdalnego "
"serwera. W ten sposób niemożliwe jest ustawienie parametrów wydajnościowych, "
"co spowalnia połączenie w stosunku do połączenia B<rcmd(3)> zestawionego "
"przez B<root>-a."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<RSCSI>"
msgstr "B<RSCSI>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"If the B<RSCSI> environment is present, the remote SCSI server will not be "
"the program B</opt/schily/sbin/rscsi> but the program pointed to by "
"B<RSCSI>.  Note that the remote SCSI server program name will be ignored if "
"you log in using an account that has been created with a remote SCSI server "
"program as login shell."
msgstr ""
"Jeśli obecna jest zmienna środowiskowa B<RSCSI> zdalnym serwerem SCSI nie "
"będzie program I</opt/schily/sbin/rscsi>, lecz program na który wskazuje "
"zmienna B<RSCSI>.  Proszę zauważyć, że nazwa programu na zdalnym serwerze "
"zostanie zignorowana, jeśli logowanie nastąpi z konta, które zostało "
"utworzone z programem SCSI zdalnego serwera ustawionym jako powłoka "
"logowania."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "/etc/wodim.conf"
msgstr "I</etc/wodim.conf>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Default values can be set for the following options in /etc/wodim.conf."
msgstr ""
"Wartości domyślne poniższych opcji można ustawić w pliku /etc/wodim.conf."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CDR_DEVICE"
msgstr "CDR_DEVICE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"This may either hold a device identifier that is suitable to the open call "
"of the SCSI transport library or a label in the file /etc/wodim.conf that "
"allows to identify a specific drive on the system."
msgstr ""
"Zmienna może zawierać albo identyfikator urządzenia zdolnego do otwierania "
"wywołań biblioteki transportowej SCSI lub etykietę z pliku /etc/wodim.conf "
"pozwalającą na zidentyfikowanie określonego napędu w systemie."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Any other label"
msgstr "Dowolna inna etykieta"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"is an identifier for a specific drive on the system.  Such an identifier may "
"not contain the characters ',', '/', '@' or ':'."
msgstr ""
"jest identyfikatorem określonego napędu w systemie. Nie może zawierać "
"następujących znaków: \",\",\"/\", \"@\" i \":\"."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Each line that follows a label contains a TAB separated list of items.  "
"Currently, four items are recognized: the SCSI ID of the drive, the default "
"speed that should be used for this drive, the default FIFO size that should "
"be used for this drive and drive specific options. The values for I<speed> "
"and I<fifosize> may be set to -1 to tell the program to use the global "
"defaults.  The value for driveropts may be set to \"\" if no driveropts are "
"used.  A typical line may look this way:"
msgstr ""
"Każdy następny wiersz zawiera etykietę zawierającą oddzieloną tabulatorem "
"listę wpisów. Obecnie rozpoznawane są cztery wpisy: identyfikator SCSI "
"napędu; domyślna prędkość, która powinna być zastosowana dla tego napędu; "
"domyślny rozmiar FIFO, który powinien być zastosowany dla danego napędu; "
"opcje charakterystyczne dla napędu. Wartości B<speed> (prędkości) i "
"B<fifosize> (rozmiaru FIFO) można ustawić na -1, aby program użył globalnych "
"ustawień domyślnych. Jeśli wartość dla B<driveropts> będzie wynosiła \"\", "
"nie będą użyte żadne opcje charakterystyczne dla napędu. Typowy wiersz może "
"wyglądać następująco:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "teac1= 0,5,0\t4\t8m\t\"\""
msgstr "teac1= 0,5,0\t4\t8m\t\"\""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "yamaha= 1,6,0\t-1\t-1\tburnfree"
msgstr "yamaha= 1,6,0\t-1\t-1\tburnfree"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"This tells the program that a drive named I<teac1> is at scsibus 0, target "
"5, lun 0 and should be used with speed 4 and a FIFO size of 8 MB.  A second "
"drive may be found at scsibus 1, target 6, lun 0 and uses the default speed "
"and the default FIFO size."
msgstr ""
"Nakazuje to programowi użycie napędu o nazwie I<teac1> na szynie scsi 0, "
"celu 5, numerze logicznym (lun) 0 z prędkością 4 i rozmiarze FIFO 8 MB. "
"Drugi napęd znajduje się na szynie scsi 1, celu 6, lun 0 i używa domyślnych "
"wartości dla prędkości i rozmiaru FIFO."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<genisoimage>(1), B<wodim>(1), B<readcd>(1), B<ssh>(1)."
msgstr "B<genisoimage>(1), B<wodim>(1), B<readcd>(1), B<ssh>(1)."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SOURCES"
msgstr "ŹRÓDŁA"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "[1] Cdrtools 2.01.01a08 from May 2006, http://cdrecord.berlios.de"
msgstr "[1] Cdrtools 2.01.01a08 z maja 2006, http://cdrecord.berlios.de"
