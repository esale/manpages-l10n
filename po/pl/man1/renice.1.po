# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Michał Kułach <michal.kulach@gmail.com>, 2013.
# Szymon Lamkiewicz <s.lam@o2.pl>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-06-27 19:46+0200\n"
"PO-Revision-Date: 2020-06-24 09:58+0200\n"
"Last-Translator: Szymon Lamkiewicz <s.lam@o2.pl>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 2.2.1\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "RENICE"
msgstr "RENICE"

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr "11 maja 2022 r."

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr "Polecenia użytkownika"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "renice - alter priority of running processes"
msgstr "renice - zmienia priorytet działającego procesu"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<renice> [B<-n>] I<priority> [B<-g>|B<-p>|B<-u>] I<identifier>..."
msgstr "B<renice> [B<-n>] I<piorytet> [B<-g>|B<-p>|B<-u>] I<identyfikator>..."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "B<renice> alters the scheduling priority of one or more running "
#| "processes.  The first argument is the I<priority> value to be used.  The "
#| "other arguments are interpreted as process IDs (by default), process "
#| "group IDs, user IDs, or user names.  B<renice>'ing a process group causes "
#| "all processes in the process group to have their scheduling priority "
#| "altered.  B<renice>'ing a user causes all processes owned by the user to "
#| "have their scheduling priority altered."
msgid ""
"B<renice> alters the scheduling priority of one or more running processes. "
"The first argument is the I<priority> value to be used. The other arguments "
"are interpreted as process IDs (by default), process group IDs, user IDs, or "
"user names. B<renice>\\(aqing a process group causes all processes in the "
"process group to have their scheduling priority altered. B<renice>\\(aqing a "
"user causes all processes owned by the user to have their scheduling "
"priority altered."
msgstr ""
"B<renice> zmienia priorytet jednego lub większej liczby procesów. Pierwszym "
"parametrem jest wartość priorytetu I<priorytet>. Kolejne parametry są "
"interpretowane jako numery ID procesów, numery ID grup procesów, numery ID "
"użytkowników lub nazwy użytkowników. B<renice> wywołane na grupie procesów "
"sprawia, że wszystkie procesy w grupie mają ten sam priorytet. B<renice> "
"wywołane na użytkowniku sprawia, że wszystkie procesy, których właścicielem "
"jest użytkownik mają zmieniony priorytet."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-n>,B< --priority >I<priority>"
msgid "B<-n>, B<--priority> I<priority>"
msgstr "B<-n>,B< --priority >I<piorytet>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "Specify the scheduling I<priority> to be used for the process, process "
#| "group, or user.  Use of the option B<-n> or B<--priority> is optional, "
#| "but when used it must be the first argument."
msgid ""
"Specify the scheduling I<priority> to be used for the process, process "
"group, or user. Use of the option B<-n> or B<--priority> is optional, but "
"when used it must be the first argument."
msgstr ""
"Określa I<priorytet>, który ma być ustawiony dla procesu, grupy procesów lub "
"użytkownika. Użycie opcji B<-n> czy B<--priority> jest opcjonalne, lecz gdy "
"zostanie użyte musi być pierwszym parametrem."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-g>, B<--pgrp>"
msgstr "B<-g>, B<--pgrp>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Interpret the succeeding arguments as process group IDs."
msgstr "Interpretuj następujące argumenty jako numery ID grup procesów."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-p>, B<--pid>"
msgstr "B<-p>, B<--pid>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Interpret the succeeding arguments as process IDs (the default)."
msgstr "Interpretuj następujące argumenty jako numery ID procesów (domyślne)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-u>, B<--user>"
msgstr "B<-u>, B<--user>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Interpret the succeeding arguments as usernames or UIDs."
msgstr ""
"Interpretuj następujące argumenty jako nazwy użytkowników bądź ich numery ID."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr "Wyświetla ten tekst i kończy pracę."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "Print version and exit."
msgstr "Wyświetla informacje o wersji i kończy działanie."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "I</etc/passwd>"
msgstr "I</etc/passwd>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "to map user names to user IDs"
msgstr "do mapowania nazw użytkowników na ich identyfikatory."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "Users other than the superuser may only alter the priority of processes "
#| "they own.  Furthermore, an unprivileged user can only I<increase> the "
#| "``nice value'' (i.e., choose a lower priority)  and such changes are "
#| "irreversible unless (since Linux 2.6.12)  the user has a suitable "
#| "``nice'' resource limit (see B<ulimit>(1p)  and B<getrlimit>(2))."
msgid ""
"Users other than the superuser may only alter the priority of processes they "
"own. Furthermore, an unprivileged user can only I<increase> the \"nice "
"value\" (i.e., choose a lower priority) and such changes are irreversible "
"unless (since Linux 2.6.12) the user has a suitable \"nice\" resource limit "
"(see B<ulimit>(1p) and B<getrlimit>(2))."
msgstr ""
"Użytkownicy inni niż super użytkownik mogą zmieniać priorytet tyko własnych "
"procesów. Dodatkowo nieuprzywilejowany użytkownik może tylko I<podnieść> "
"wartość ``nice'' (obniżyć priorytet), a takie zmiany są nieodwracalne, chyba "
"że (od Linux 2.6.12) użytkownik ma odpowiedni limit zasobu ``nice'' (sprawdź "
"B<ulimit>(1p) oraz B<getrlimit>(2))."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "The superuser may alter the priority of any process and set the priority "
#| "to any value in the range -20 to 19.  Useful priorities are: 19 (the "
#| "affected processes will run only when nothing else in the system wants "
#| "to), 0 (the ``base'' scheduling priority), anything negative (to make "
#| "things go very fast)."
msgid ""
"The superuser may alter the priority of any process and set the priority to "
"any value in the range -20 to 19. Useful priorities are: 19 (the affected "
"processes will run only when nothing else in the system wants to), 0 (the "
"\"base\" scheduling priority), anything negative (to make things go very "
"fast)."
msgstr ""
"Super użytkownik może zmienić priorytet każdego procesu oraz ustawić dowolną "
"wartość z zakresu -20 do 19. Użyteczne priorytety to: 19 (afektowany proces "
"będzie wykonywany tylko wtedy, gdy nic innego w systemie obecnie nie "
"działa), 0 (podstawowy priorytet działania), wartości ujemne (by znacząco "
"przyspieszyć działanie)."

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "The B<renice> command appeared in 4.0BSD."
msgstr "Polecenie B<renice> pojawiło się w 4.0BSD."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLES"
msgstr "PRZYKŁADY"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The following command would change the priority of the processes with PIDs "
"987 and 32, plus all processes owned by the users daemon and root:"
msgstr ""
"Następująca komenda zmieni priorytety procesów o identyfikatorach 987 i 32 "
"oraz wszystkich procesów użytkowników daemon i root."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<renice +1 987 -u daemon root -p 32>"
msgstr "B<renice +1 987 -u daemon root -p 32>"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<nice>(1), B<chrt>(1), B<getpriority>(2), B<setpriority>(2), "
"B<credentials>(7), B<sched>(7)"
msgstr ""
"B<nice>(1), B<chrt>(1), B<getpriority>(2), B<setpriority>(2), "
"B<credentials>(7), B<sched>(7)"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ZGŁASZANIE BŁĘDÓW"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr "Raporty o błędach proszę zgłaszać pod adresem"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr "DOSTĘPNOŚĆ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The B<renice> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14 lutego 2022 r."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Wyświetla informacje o wersji i kończy działanie."
