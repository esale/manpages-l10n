# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1998.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:35+0200\n"
"PO-Revision-Date: 2022-08-07 09:07+0200\n"
"Last-Translator: Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 19.08.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "linkat()"
msgid "link"
msgstr "linkat()"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "link, linkat - make a new name for a file"
msgstr "link, linkat - utworzenie nowej nazwy dla pliku"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int link(const char *>I<oldpath>B<, const char *>I<newpath>B<);>\n"
msgstr "B<int link(const char *>I<oldpath>B<, const char *>I<newpath>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of B<AT_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definicja stałych B<AT_*> */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int linkat(int >I<olddirfd>B<, const char *>I<oldpath>B<,>\n"
"B<           int >I<newdirfd>B<, const char *>I<newpath>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int linkat(int >I<olddirfd>B<, const char *>I<oldpath>B<,>\n"
"B<           int >I<newdirfd>B<, const char *>I<newpath>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Wymagane ustawienia makr biblioteki glibc (patrz B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<linkat>():"
msgstr "B<linkat>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Before glibc 2.10:\n"
"        _ATFILE_SOURCE\n"
msgstr ""
"    Od glibc 2.10:\n"
"        _POSIX_C_SOURCE E<gt>= 200809L\n"
"    Przed glibc 2.10:\n"
"        _ATFILE_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<link>()  creates a new link (also known as a hard link) to an existing "
"file."
msgstr ""
"B<link>() tworzy nowe dowiązanie (nazywane też dowiązaniem twardym) do "
"istniejącego pliku."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If I<newpath> exists, it will I<not> be overwritten."
msgstr "Jeśli plik I<newpath> już istnieje, to I<nie> będzie nadpisany."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This new name may be used exactly as the old one for any operation; both "
"names refer to the same file (and so have the same permissions and "
"ownership) and it is impossible to tell which name was the \"original\"."
msgstr ""
"Ta nowa nazwa może być używana dokłądnie tak samo jak stara w dowolnych "
"operacjach; obie nazwy odnoszą się do tego samego pliku (i w związku z tym "
"mają te same prawa i właścicielstwo). Nie można też powiedzieć, która nazwa "
"jest `oryginalna'."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "linkat()"
msgstr "linkat()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<linkat>()  system call operates in exactly the same way as B<link>(), "
"except for the differences described here."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the pathname given in I<oldpath> is relative, then it is interpreted "
"relative to the directory referred to by the file descriptor I<olddirfd> "
"(rather than relative to the current working directory of the calling "
"process, as is done by B<link>()  for a relative pathname)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<oldpath> is relative and I<olddirfd> is the special value B<AT_FDCWD>, "
"then I<oldpath> is interpreted relative to the current working directory of "
"the calling process (like B<link>())."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If I<oldpath> is absolute, then I<olddirfd> is ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The interpretation of I<newpath> is as for I<oldpath>, except that a "
"relative pathname is interpreted relative to the directory referred to by "
"the file descriptor I<newdirfd>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following values can be bitwise ORed in I<flags>:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<AT_EMPTY_PATH> (since Linux 2.6.39)"
msgstr "B<AT_EMPTY_PATH> (od Linuksa 2.6.39)"

#.  commit 11a7b371b64ef39fc5fb1b6f2218eef7c4d035e3
#.  Before glibc 2.16, defining _ATFILE_SOURCE sufficed
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<oldpath> is an empty string, create a link to the file referenced by "
"I<olddirfd> (which may have been obtained using the B<open>(2)  B<O_PATH> "
"flag).  In this case, I<olddirfd> can refer to any type of file except a "
"directory.  This will generally not work if the file has a link count of "
"zero (files created with B<O_TMPFILE> and without B<O_EXCL> are an "
"exception).  The caller must have the B<CAP_DAC_READ_SEARCH> capability in "
"order to use this flag.  This flag is Linux-specific; define B<_GNU_SOURCE> "
"to obtain its definition."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<AT_SYMLINK_FOLLOW> (since Linux 2.6.18)"
msgstr "B<AT_SYMLINK_FOLLOW> (od Linuksa 2.6.18)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"By default, B<linkat>(), does not dereference I<oldpath> if it is a symbolic "
"link (like B<link>()).  The flag B<AT_SYMLINK_FOLLOW> can be specified in "
"I<flags> to cause I<oldpath> to be dereferenced if it is a symbolic link.  "
"If procfs is mounted, this can be used as an alternative to "
"B<AT_EMPTY_PATH>, like this:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"linkat(AT_FDCWD, \"/proc/self/fd/E<lt>fdE<gt>\", newdirfd,\n"
"       newname, AT_SYMLINK_FOLLOW);\n"
msgstr ""
"linkat(AT_FDCWD, \"/proc/self/fd/E<lt>fdE<gt>\", newdirfd,\n"
"       newname, AT_SYMLINK_FOLLOW);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Before Linux 2.6.18, the I<flags> argument was unused, and had to be "
"specified as 0."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<openat>(2)  for an explanation of the need for B<linkat>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"ustawiane jest I<errno> wskazując błąd."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Write access to the directory containing I<newpath> is not allowed for "
#| "the process's effective uid, or one of the directories in I<oldpath> or "
#| "I<newpath> did not allow search (execute) permission."
msgid ""
"Write access to the directory containing I<newpath> is denied, or search "
"permission is denied for one of the directories in the path prefix of "
"I<oldpath> or I<newpath>.  (See also B<path_resolution>(7).)"
msgstr ""
"Zapis do katalogu zawierającego I<newpath> nie jest dozwolony dla efekywnego "
"UID procesu, lub jeden z katalogów w I<oldpath> lub I<newpath> nie daje "
"uprawnień przeszukiwania (wykonania)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EDQUOT>"
msgstr "B<EDQUOT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The user's quota of disk blocks on the filesystem has been exhausted."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<newpath> already exists."
msgstr "I<newpath> już istnieje."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<oldpath> or I<newpath> points outside your accessible address space."
msgstr ""
"I<oldpath> lub I<newpath> wskazuje poza dostępną dla użyrkownika przestrzeń "
"adresową."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "An I/O error occurred."
msgstr "Wystąpił błąd wejścia/wyjścia."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Too many symbolic links were encountered in resolving I<oldpath> or "
"I<newpath>."
msgstr ""
"Podczas rozwiązywania I<oldpath> lub I<newpath> napotkano zbyt wiele "
"dowiązań symbolicznych."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EMLINK>"
msgstr "B<EMLINK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file referred to by I<oldpath> already has the maximum number of links "
"to it.  For example, on an B<ext4>(5)  filesystem that does not employ the "
"I<dir_index> feature, the limit on the number of hard links to a file is "
"65,000; on B<btrfs>(5), the limit is 65,535 links."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<oldpath> or I<newpath> was too long."
msgstr "I<oldpath> lub I<newpath> było zbyt długie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A directory component in I<oldpath> or I<newpath> does not exist or is a "
"dangling symbolic link."
msgstr ""
"Składnik katalogu w I<oldpath> lub I<newpath> nie istnieje, lub jest "
"wiszącym dowiązaniem symbolicznym."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "Brak pamięci jądra."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSPC>"
msgstr "B<ENOSPC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The device containing the file has no room for the new directory entry."
msgstr ""
"Na urządzeniu, zawierającym plik nie ma miejsca na kolejny wpis w katalogu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A component used as a directory in I<oldpath> or I<newpath> is not, in fact, "
"a directory."
msgstr ""
"Składnik I<oldpath> lub I<newpath> używany jako katalog nie jest w "
"rzeczywistości katalogiem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<oldpath> is a directory."
msgstr "I<oldpath> jest katalogiem."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The filesystem containing I<oldpath> and I<newpath> does not support the "
"creation of hard links."
msgstr ""
"System plików zawierający I<oldpath> i I<newpath> nie obsługuje tworzenia "
"twardych dowiązań."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM> (since Linux 3.6)"
msgstr "B<EPERM> (od Linuksa 3.6)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The caller does not have permission to create a hard link to this file (see "
"the description of I</proc/sys/fs/protected_hardlinks> in B<proc>(5))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<oldpath> is marked immutable or append-only.  (See B<ioctl_iflags>(2).)"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EROFS>"
msgstr "B<EROFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The file is on a read-only filesystem."
msgstr "Plik leży na systemie plików tylko dla odczytu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EXDEV>"
msgstr "B<EXDEV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<oldpath> and I<newpath> are not on the same mounted filesystem.  (Linux "
"permits a filesystem to be mounted at multiple points, but B<link>()  does "
"not work across different mounts, even if the same filesystem is mounted on "
"both.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following additional errors can occur for B<linkat>():"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"I<oldpath> (I<newpath>)  is relative but I<olddirfd> (I<newdirfd>)  is "
"neither B<AT_FDCWD> nor a valid file descriptor."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "An invalid flag value was specified in I<flags>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<AT_EMPTY_PATH> was specified in I<flags>, but the caller did not have the "
"B<CAP_DAC_READ_SEARCH> capability."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"An attempt was made to link to the I</proc/self/fd/NN> file corresponding to "
"a file descriptor created with"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "open(path, O_TMPFILE | O_EXCL, mode);\n"
msgstr "open(path, O_TMPFILE | O_EXCL, mode);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<open>(2)."
msgstr "Zobacz B<open>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"An attempt was made to link to a I</proc/self/fd/NN> file corresponding to a "
"file that has been deleted."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<oldpath> is a relative pathname and I<olddirfd> refers to a directory that "
"has been deleted, or I<newpath> is a relative pathname and I<newdirfd> "
"refers to a directory that has been deleted."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<oldpath> is relative and I<olddirfd> is a file descriptor referring to a "
"file other than a directory; or similar for I<newpath> and I<newdirfd>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<AT_EMPTY_PATH> was specified in I<flags>, I<oldpath> is an empty string, "
"and I<olddirfd> refers to a directory."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "WERSJE"

#.  more precisely: since Linux 1.3.56
#.  For example, the default Solaris compilation environment
#.  behaves like Linux, and contributors to a March 2005
#.  thread in the Austin mailing list reported that some
#.  other (System V) implementations did/do the same -- MTK, Apr 05
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"POSIX.1-2001 says that B<link>()  should dereference I<oldpath> if it is a "
"symbolic link.  However, since Linux 2.0, Linux does not do so: if "
"I<oldpath> is a symbolic link, then I<newpath> is created as a (hard) link "
"to the same symbolic link file (i.e., I<newpath> becomes a symbolic link to "
"the same file that I<oldpath> refers to).  Some other implementations behave "
"in the same manner as Linux.  POSIX.1-2008 changes the specification of "
"B<link>(), making it implementation-dependent whether or not I<oldpath> is "
"dereferenced if it is a symbolic link.  For precise control over the "
"treatment of symbolic links when creating a link, use B<linkat>()."
msgstr ""

#. type: SS
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "glibc 2.1."
msgid "glibc"
msgstr "glibc 2.1."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On older kernels where B<linkat>()  is unavailable, the glibc wrapper "
"function falls back to the use of B<link>(), unless the B<AT_SYMLINK_FOLLOW> "
"is specified.  When I<oldpath> and I<newpath> are relative pathnames, glibc "
"constructs pathnames based on the symbolic links in I</proc/self/fd> that "
"correspond to the I<olddirfd> and I<newdirfd> arguments."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<link>(2)"
msgid "B<link>()"
msgstr "B<link>(2)"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#.  SVr4 documents additional ENOLINK and
#.  EMULTIHOP error conditions; POSIX.1 does not document ELOOP.
#.  X/OPEN does not document EFAULT, ENOMEM or EIO.
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "SVr4, 4.3BSD, POSIX.1-2001."
msgid "SVr4, 4.3BSD, POSIX.1-2001 (but see VERSIONS)."
msgstr "SVr4, 4.3BSD, POSIX.1-2001."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<linkat>():"
msgid "B<linkat>()"
msgstr "B<linkat>():"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008.  Linux 2.6.16, glibc 2.4."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Hard links, as created by B<link>(), cannot span filesystems.  Use "
"B<symlink>(2)  if this is required."
msgstr ""
"Twarde dowiązania, tworzone z pomocą B<link>(), nie mogą wykraczać poza "
"jeden system plików. W takich sytuacjach można użyć funkcji B<symlink>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BŁĘDY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On NFS filesystems, the return code may be wrong in case the NFS server "
"performs the link creation and dies before it can say so.  Use B<stat>(2)  "
"to find out if the link got created."
msgstr ""
"Na systemach NFS, wartość zwracana może być nieprawidłowa w wypadku gdy "
"serwer NFS dokonuje tworzenia dowiązania i umiera przed zakomunikowaniem "
"tego faktu. Można użyć B<stat>(2) aby dowiedzieć się czy dowiązanie zostało "
"utworzone."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ln>(1), B<open>(2), B<rename>(2), B<stat>(2), B<symlink>(2), B<unlink>(2), "
"B<path_resolution>(7), B<symlink>(7)"
msgstr ""
"B<ln>(1), B<open>(2), B<rename>(2), B<stat>(2), B<symlink>(2), B<unlink>(2), "
"B<path_resolution>(7), B<symlink>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 lutego 2023 r."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"B<linkat>()  was added in Linux 2.6.16; library support was added in glibc "
"2.4."
msgstr ""

#.  SVr4 documents additional ENOLINK and
#.  EMULTIHOP error conditions; POSIX.1 does not document ELOOP.
#.  X/OPEN does not document EFAULT, ENOMEM or EIO.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<link>(): SVr4, 4.3BSD, POSIX.1-2001 (but see NOTES), POSIX.1-2008."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<linkat>(): POSIX.1-2008."
msgstr "B<linkat>(): POSIX.1-2008."

#. type: SS
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "glibc notes"
msgstr "Uwagi dla glibc"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "LINK"
msgstr "LINK"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definition of AT_* constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>fcntl.hE<gt>           >/* Definicja stałych AT_* */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.10:"
msgstr "Od glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-5
msgid "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"
msgstr "_POSIX_C_SOURCE\\ E<gt>=\\ 200809L"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Before glibc 2.10:"
msgstr "Przed glibc 2.10:"

#. type: Plain text
#: opensuse-leap-15-5
msgid "_ATFILE_SOURCE"
msgstr "_ATFILE_SOURCE"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Before kernel 2.6.18, the I<flags> argument was unused, and had to be "
"specified as 0."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"odpowiednio ustawiane jest I<errno>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"I<oldpath> and I<newpath> are not on the same mounted filesystem.  (Linux "
"permits a filesystem to be mounted at multiple points, but B<link>()  does "
"not work across different mount points, even if the same filesystem is "
"mounted on both.)"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "I<olddirfd> or I<newdirfd> is not a valid file descriptor."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "    open(path, O_TMPFILE | O_EXCL, mode);\n"
msgstr "    open(path, O_TMPFILE | O_EXCL, mode);\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<linkat>()  was added to Linux in kernel 2.6.16; library support was added "
"to glibc in version 2.4."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#.  more precisely: since kernel 1.3.56
#.  For example, the default Solaris compilation environment
#.  behaves like Linux, and contributors to a March 2005
#.  thread in the Austin mailing list reported that some
#.  other (System V) implementations did/do the same -- MTK, Apr 05
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"POSIX.1-2001 says that B<link>()  should dereference I<oldpath> if it is a "
"symbolic link.  However, since kernel 2.0, Linux does not do so: if "
"I<oldpath> is a symbolic link, then I<newpath> is created as a (hard) link "
"to the same symbolic link file (i.e., I<newpath> becomes a symbolic link to "
"the same file that I<oldpath> refers to).  Some other implementations behave "
"in the same manner as Linux.  POSIX.1-2008 changes the specification of "
"B<link>(), making it implementation-dependent whether or not I<oldpath> is "
"dereferenced if it is a symbolic link.  For precise control over the "
"treatment of symbolic links when creating a link, use B<linkat>()."
msgstr ""

#. type: SS
#: opensuse-leap-15-5
#, no-wrap
msgid "Glibc notes"
msgstr "Uwagi dla glibc"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
