# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Bartłomiej Sowa <bartowl@kki.net.pl>, 1998.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2003.
# Michał Kułach <michal.kulach@gmail.com>, 2013, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-06-27 19:49+0200\n"
"PO-Revision-Date: 2016-05-03 20:25+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "securetty"
msgstr "securetty"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 października 2022 r."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "securetty - file which lists terminals from which root can log in"
msgid "securetty - list of terminals on which root is allowed to login"
msgstr ""
"securetty - plik zawierający listę terminali z których może się logować root"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file I</etc/securetty> contains the names of terminals (one per line, "
"without leading I</dev/>)  which are considered secure for the transmission "
"of certain authentication tokens."
msgstr ""
"Plik I</etc/securetty> zawiera nazwy terminali (jedną na wiersz, bez "
"przedrostka I</dev/>) które są uważane za bezpieczne do transmisji "
"określonych tokenów uwierzytelniania."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"It is used by (some versions of)  B<login>(1)  to restrict the terminals on "
"which root is allowed to login.  See B<login.defs>(5)  if you use the shadow "
"suite."
msgstr ""
"Jest używany przez program (niektóre wersje) B<login>(1) do ograniczenia "
"terminali, na które może się zalogować root.  W przypadku korzystania z "
"shadow, zobacz B<login.defs>(5)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On PAM enabled systems, it is used for the same purpose by "
"B<pam_securetty>(8)  to restrict the terminals on which empty passwords are "
"accepted."
msgstr ""
"Na systemach korzystających z PAM, jest on używany do tego samego celu przez "
"B<pam_securetty>(8), aby ograniczyć terminale na których akceptowane są "
"puste hasła."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I</etc/securetty>"
msgstr "I</etc/securetty>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<login>(1), B<login.defs>(5), B<pam_securetty>(8)"
msgstr "B<login>(1), B<login.defs>(5), B<pam_securetty>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "SECURETTY"
msgstr "SECURETTY"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2015-03-29"
msgstr "29 marca 2015 r."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-5
msgid "securetty - file which lists terminals from which root can log in"
msgstr ""
"securetty - plik zawierający listę terminali z których może się logować root"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
