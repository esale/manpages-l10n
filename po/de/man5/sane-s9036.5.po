# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.2.0\n"
"POT-Creation-Date: 2023-06-27 19:48+0200\n"
"PO-Revision-Date: 2021-01-23 21:20+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "sane-s9036"
msgstr "sane-s9036"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "14 Jul 2008"
msgstr "14. Juli 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE Scanner Access Now Easy"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "sane-s9036 - SANE backend for Siemens 9036 flatbed scanners"
msgstr "sane-s9036 - SANE-Backend für die 9036-Flachbettscanner von Siemens"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<sane-s9036> library implements a SANE (Scanner Access Now Easy) "
"backend that provides access to Siemens 9036 flatbed scanners."
msgstr ""
"Die Bibliothek B<sane-s9036> implementiert ein SANE-(Scanner Access Now "
"Easy) Backend zum Zugriff auf die 9036-Flachbettscanner von Siemens."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DEVICE NAMES"
msgstr "GERÄTENAMEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "This backend expects device names of the form:"
msgstr "Dieses Backend erwartet Gerätenamen der folgenden Form:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<special>"
msgstr "I<Spezialdatei>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Where I<special> is the path-name for the special device that corresponds to "
"a SCSI scanner. The special device name must be a generic SCSI device or a "
"symlink to such a device.  The program B<sane-find-scanner>(1)  helps to "
"find out the correct device. Under Linux, such a device name could be I</dev/"
"sga> or I</dev/sge>, for example.  See B<sane-scsi>(5)  for details."
msgstr ""
"Die angegebene I<Spezialdatei> ist der Pfadname eines Spezialgeräts, das "
"einem SCSI-Scanner entspricht. Der Name des Spezialgeräts muss ein "
"generisches SCSI-Gerät oder ein symbolischer Link zu so einem Gerät sein. "
"Das Programm B<sane-find-scanner>(1) hilft Ihnen beim Ermitteln des "
"korrekten Geräts. Unter Linux könnte ein solcher Gerätename beispielsweise "
"I</dev/sga> oder I</dev/sge> sein. Siehe B<sane-scsi>(5) für Details."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/sane.d/s9036.conf>"
msgstr "I</etc/sane.d/s9036.conf>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The backend configuration file (see also description of B<SANE_CONFIG_DIR> "
"below)."
msgstr ""
"Die Backend-Konfigurationsdatei (siehe auch die nachfolgende Beschreibung "
"von B<SANE_CONFIG_DIR>)."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-s9036.a>"
msgstr "I</usr/lib/sane/libsane-s9036.a>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The static library implementing this backend."
msgstr "Die statische Bibliothek, die dieses Backend implementiert."

#. type: TP
#: archlinux
#, no-wrap
msgid "I</usr/lib/sane/libsane-s9036.so>"
msgstr "I</usr/lib/sane/libsane-s9036.so>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The shared library implementing this backend (present on systems that "
"support dynamic loading)."
msgstr ""
"Die dynamische Bibliothek, die dieses Backend implementiert (auf Systemen "
"verfügbar, die dynamisches Laden unterstützen)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "UMGEBUNGSVARIABLEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_CONFIG_DIR>"
msgstr "B<SANE_CONFIG_DIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This environment variable specifies the list of directories that may contain "
"the configuration file.  On *NIX systems, the directories are separated by a "
"colon (`:'), under OS/2, they are separated by a semi-colon (`;').  If this "
"variable is not set, the configuration file is searched in two default "
"directories: first, the current working directory (\".\") and then in I</etc/"
"sane.d>.  If the value of the environment variable ends with the directory "
"separator character, then the default directories are searched after the "
"explicitly specified directories.  For example, setting B<SANE_CONFIG_DIR> "
"to \"/tmp/config:\" would result in directories I<tmp/config>, I<.>, and I</"
"etc/sane.d> being searched (in this order)."
msgstr ""
"Diese Umgebungsvariable gibt eine Liste von Verzeichnissen an, die die "
"Konfigurationsdatei enthalten können. Auf *NIX-Systemen sind die "
"Verzeichnisse durch Doppelpunkte (:) getrennt, unter OS/2 durch Semikola "
"(;). Falls diese Variable nicht gesetzt ist, wird in zwei "
"Standardverzeichnissen nach der Konfigurationsdatei gesucht: zuerst im "
"aktuellen Arbeitsverzeichnis (.) und dann in I</etc/sane.d>. Falls der Wert "
"der Umgebungsvariable mit dem Verzeichnis-Trennzeichen endet, dann werden "
"die Standardverzeichnisse nach den explizit angegebenen Verzeichnissen "
"durchsucht. Wenn Sie beispielsweise B<SANE_CONFIG_DIR> auf »/tmp/config:« "
"setzen, wird in den Verzeichnissen »tmp/config«, ».« und »/etc/sane.d« "
"gesucht (in dieser Reihenfolge)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SANE_DEBUG_S9036>"
msgstr "B<SANE_DEBUG_S9036>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the library was compiled with debug support enabled, this environment "
"variable controls the debug level for this backend.  Higher debug levels "
"increase the verbosity of the output."
msgstr ""
"Falls die Bibliothek mit Debug-Unterstützung kompiliert wurde, steuert diese "
"Umgebungsvariable die Debug-Stufe für dieses Backend. Größere Werte erhöhen "
"die Ausführlichkeit der Ausgabe."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Example: export SANE_DEBUG_S9036=4"
msgstr "Beispiel: export SANE_DEBUG_S9036=4"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<sane>(7), B<sane-scsi>(5)"
msgstr "B<sane>(7), B<sane-scsi>(5)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Ingo Schneider"
msgstr "Ingo Schneider"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-s9036.a>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-s9036.a>"

#. type: TP
#: debian-bookworm debian-unstable
#, no-wrap
msgid "I</usr/lib/x86_64-linux-gnu/sane/libsane-s9036.so>"
msgstr "I</usr/lib/x86_64-linux-gnu/sane/libsane-s9036.so>"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-s9036.a>"
msgstr "I</usr/lib64/sane/libsane-s9036.a>"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib64/sane/libsane-s9036.so>"
msgstr "I</usr/lib64/sane/libsane-s9036.so>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This environment variable specifies the list of directories that may contain "
"the configuration file.  Under UNIX, the directories are separated by a "
"colon (`:'), under OS/2, they are separated by a semi-colon (`;').  If this "
"variable is not set, the configuration file is searched in two default "
"directories: first, the current working directory (\".\") and then in I</etc/"
"sane.d>.  If the value of the environment variable ends with the directory "
"separator character, then the default directories are searched after the "
"explicitly specified directories.  For example, setting B<SANE_CONFIG_DIR> "
"to \"/tmp/config:\" would result in directories I<tmp/config>, I<.>, and I</"
"etc/sane.d> being searched (in this order)."
msgstr ""
"Diese Umgebungsvariable gibt eine Liste von Verzeichnissen an, die die "
"Konfigurationsdatei enthalten können. Unter UNIX sind die Verzeichnisse "
"durch Doppelpunkte (:) getrennt, unter OS/2 durch Semikola (;). Falls diese "
"Variable nicht gesetzt ist, wird in zwei Standardverzeichnissen nach der "
"Konfigurationsdatei gesucht: zuerst im aktuellen Arbeitsverzeichnis (.) und "
"dann in I</etc/sane.d>. Falls der Wert der Umgebungsvariable mit dem "
"Verzeichnis-Trennzeichen endet, dann werden die Standardverzeichnisse nach "
"den explizit angegebenen Verzeichnissen durchsucht. Wenn Sie beispielsweise "
"B<SANE_CONFIG_DIR> auf »/tmp/config:« setzen, wird in den Verzeichnissen "
"»tmp/config«, ».« und »/etc/sane.d« gesucht (in dieser Reihenfolge)."
