# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2023-06-27 20:00+0200\n"
"PO-Revision-Date: 2023-02-18 08:14+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "user-keyring"
msgstr "user-keyring"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "user-keyring - per-user keyring"
msgstr "user-keyring - benutzerbezogener Schlüsselbund"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The user keyring is a keyring used to anchor keys on behalf of a user.  Each "
"UID the kernel deals with has its own user keyring that is shared by all "
"processes with that UID.  The user keyring has a name (description) of the "
"form I<_uid.E<lt>UIDE<gt>> where I<E<lt>UIDE<gt>> is the user ID of the "
"corresponding user."
msgstr ""
"Der Benutzerschlüsselbund ist ein Schlüsselbund, der im Auftrag von "
"Benutzern zur Verankerung von Schlüsseln verwandt wird. Jede UID, mit der "
"der Kernel umgeht, hat ihren eigenen Benutzerschlüsselbund, der von allen "
"Prozessen mit dieser UID gemeinsam verwandt wird. Der Benutzerschlüsselbund "
"hat einen Namen (die Beschreibung) der Form I<_uid.E<lt>UIDE<gt>>, wobei "
"I<E<lt>UIDE<gt>> die Benutzerkennung des entsprechenden Benutzers ist."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The user keyring is associated with the record that the kernel maintains for "
"the UID.  It comes into existence upon the first attempt to access either "
"the user keyring, the B<user-session-keyring>(7), or the B<session-"
"keyring>(7).  The keyring remains pinned in existence so long as there are "
"processes running with that real UID or files opened by those processes "
"remain open.  (The keyring can also be pinned indefinitely by linking it "
"into another keyring.)"
msgstr ""
"Der Benutzerschlüsselbund ist dem Datensatz zugeordnet, den der Kernel für "
"die UID verwaltet. Er beginnt mit dem ersten Zugriffsversuch auf den "
"Benutzerschlüsselbund, dem B<user-session-keyring>(7) oder dem B<session-"
"keyring>(7) zu existieren. Der Existenz des Schlüsselbunds bleibt befestigt, "
"solange wie Prozesse mit dieser realen UID laufen oder Dateien von diesen "
"Prozessen geöffnet sind. (Der Schlüsselbund kann auch endlos befestigt "
"werden, indem er mit einem anderen Schlüsselbund verbunden wird.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Typically, the user keyring is created by B<pam_keyinit>(8)  when a user "
"logs in."
msgstr ""
"Typischerweise wird der Benutzerschlüsselbund durch B<pam_keyinit>(8) "
"erstellt, wenn sich ein Benutzer anmeldet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The user keyring is not searched by default by B<request_key>(2).  When "
"B<pam_keyinit>(8)  creates a session keyring, it adds to it a link to the "
"user keyring so that the user keyring will be searched when the session "
"keyring is."
msgstr ""
"Der Benutzerschlüsselbund wird von B<request_key>(2) standardmäßig nicht "
"durchsucht. Wenn B<pam_keyinit>(8) einen Sitzungsschlüsselbund erstellt, "
"fügt er einen Verweis zu dem Benutzerschlüsselbund hinzu, so dass der "
"Benutzerschlüsselbund durchsucht wird, wenn der Sitzungschlüsselbund "
"durchsucht wird."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A special serial number value, B<KEY_SPEC_USER_KEYRING>, is defined that can "
"be used in lieu of the actual serial number of the calling process's user "
"keyring."
msgstr ""
"Ein besonderer Wert für die Seriennummer, B<KEY_SPEC_USER_SESSION_KEYRING>, "
"ist definiert, die an Stelle der tatsächlichen Seriennummer des "
"Benutzerschlüsselbundes des Benutzers des aufrufenden Prozesses verwandt "
"werden kann."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"From the B<keyctl>(1)  utility, 'B<@u>' can be used instead of a numeric key "
"ID in much the same way."
msgstr ""
"Bei dem Hilfswerkzeug B<keyctl>(1) kann »B<@u>« anstelle der numerischen "
"Schlüsselkennung fast genauso verwandt werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"User keyrings are independent of B<clone>(2), B<fork>(2), B<vfork>(2), "
"B<execve>(2), and B<_exit>(2)  excepting that the keyring is destroyed when "
"the UID record is destroyed when the last process pinning it exits."
msgstr ""
"Benutzerschlüsselbunde sind von B<clone>(2), B<fork>(2), B<vfork>(2), "
"B<execve>(2) und B<_exit>(2) unabhängig, außer dass der Schlüsselbund "
"zerstört wird, wenn der UID-Datensatz zerstört wird, wenn der letzte "
"Prozess, der ihn befestigt, sich beendet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If it is necessary for a key associated with a user to exist beyond the UID "
"record being garbage collected\\[em]for example, for use by a B<cron>(8)  "
"script\\[em]then the B<persistent-keyring>(7)  should be used instead."
msgstr ""
"Falls es notwendig ist, dass ein einem Benutzer zugeordneter Schlüssel nach "
"der Speicherbereinigung des UID-Datensatzes existieren muss \\[en] "
"beispielsweise für ein B<cron>(8)-Skript \\[en], sollte stattdessen "
"B<persistent-keyring>(7) verwandt werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If a user keyring does not exist when it is accessed, it will be created."
msgstr ""
"Falls ein Benutzerschlüsselbund beim Zugriff nicht existiert, wird er "
"erstellt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<keyctl>(1), B<keyctl>(3), B<keyrings>(7), B<persistent-keyring>(7), "
"B<process-keyring>(7), B<session-keyring>(7), B<thread-keyring>(7), B<user-"
"session-keyring>(7), B<pam_keyinit>(8)"
msgstr ""
"B<keyctl>(1), B<keyctl>(3), B<keyrings>(7), B<persistent-keyring>(7), "
"B<process-keyring>(7), B<session-keyring>(7), B<thread-keyring>(7), B<user-"
"session-keyring>(7), B<pam_keyinit>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "USER-KEYRING"
msgstr "USER-KEYRING"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-03-13"
msgstr "13. März 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"If it is necessary for a key associated with a user to exist beyond the UID "
"record being garbage collected\\(emfor example, for use by a B<cron>(8)  "
"script\\(emthen the B<persistent-keyring>(7)  should be used instead."
msgstr ""
"Falls es notwendig ist, dass ein einem Benutzer zugeordneter Schlüssel nach "
"der Speicherbereinigung des UID-Datensatzes existieren muss \\(en "
"beispielsweise für ein B<cron>(8)-Skript \\(en, sollte stattdessen "
"B<persistent-keyring>(7) verwandt werden."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
