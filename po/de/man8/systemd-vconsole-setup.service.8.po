# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2020,2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-07-25 20:02+0200\n"
"PO-Revision-Date: 2023-07-29 13:11+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SYSTEMD-VCONSOLE-SETUP\\&.SERVICE"
msgstr "SYSTEMD-VCONSOLE-SETUP\\&.SERVICE"

#. type: TH
#: archlinux fedora-38 mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr "systemd 253"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "systemd-vconsole-setup.service"
msgstr "systemd-vconsole-setup.service"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid ""
"systemd-vconsole-setup.service, systemd-vconsole-setup - Configure the "
"virtual consoles"
msgstr ""
"systemd-vconsole-setup.service, systemd-vconsole-setup - Konfiguration der "
"virtuellen Konsolen"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "systemd-vconsole-setup\\&.service"
msgstr "systemd-vconsole-setup\\&.service"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "B</usr/lib/systemd/systemd-vconsole-setup> [TTY]"
msgstr "B</usr/lib/systemd/systemd-vconsole-setup> [TTY]"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"systemd-vconsole-setup sets up and configures either all virtual consoles, "
"or \\(em if the optional I<TTY> parameter is provided \\(em a specific "
"one\\&. When the system is booting up, it\\*(Aqs called by B<systemd-"
"udevd>(8)  during VT console subsystem initialization\\&. Also, B<systemd-"
"localed.service>(8)  invokes it as needed when language or console changes "
"are made\\&. Internally, this program calls B<loadkeys>(1)  and "
"B<setfont>(8)\\&."
msgstr ""
"systemd-vconsole-setup konfiguriert entweder alle virtuellen Konsolen und "
"richtet diese ein, oder \\(em falls der optionale Parameter I<TTY> "
"bereitgestellt wird \\(em eine bestimmte\\&. Wenn das System startet, wird "
"sie durch B<systemd-udevd>(8) bei der Initialisierung des VT-"
"Konsolensubsystems aufgerufen\\&. Bei Bedarf ruft sie auch B<systemd-localed."
"service>(8) auf, wenn sich Sprach- oder Konsolenänderungen erfolgen\\&. "
"Intern ruft dieses Programm B<loadkeys>(1) und B<setfont>(8) auf\\&."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid ""
"Execute B<systemctl restart systemd-vconsole-setup\\&.service> in order to "
"apply any manual changes made to /etc/vconsole\\&.conf\\&."
msgstr ""
"Führen Sie B<systemctl restart systemd-vconsole-setup\\&.service> aus, um "
"alle manuell an /etc/vconsole\\&.conf vorgenommenen Änderungen anzuwenden\\&."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid ""
"See B<vconsole.conf>(5)  for information about the configuration files and "
"kernel command line options understood by this program\\&."
msgstr ""
"Siehe B<vconsole.conf>(5) für Informationen über die von diesem Programm "
"verstandenen Konfigurationsdateien und Kernelbefehlszeilen\\&."

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CREDENTIALS"
msgstr "ZUGANGSBERECHTIGUNGEN"

#. type: Plain text
#: archlinux fedora-38 mageia-cauldron
msgid ""
"B<systemd-vconsole-setup> supports the service credentials logic as "
"implemented by I<LoadCredential=>/I<SetCredential=> (see B<systemd.exec>(1)  "
"for details)\\&. The following credentials are used when passed in:"
msgstr ""
"B<systemd-vconsole-setup> unterstützt Dienstezugangsberechtigungslogiken, "
"wie die durch I<LoadCredential=>/I<SetCredential=> implementierten (siehe "
"B<systemd.exec>(1) für Details)\\&. Die folgenden Zugangsberechtigungen "
"werden verwandt, wenn sie hereingegeben werden:"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
msgid "I<vconsole\\&.keymap>, I<vconsole\\&.keymap_toggle>"
msgstr "I<vconsole\\&.keymap>, I<vconsole\\&.keymap_toggle>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The keymap (and toggle keymap) to apply\\&. The matching options in "
"vconsole\\&.conf and on the kernel command line take precedence over these "
"credentials\\&."
msgstr ""
"Die anzuwendende Tastaturzuordnungstabelle (und Umschalt-"
"Tastaturzuordnungstabelle)\\&. Die entsprechenden Optionen in vconsole\\&."
"conf und auf der Kernelbefehlszeile haben Vorrang vor diesen "
"Zugangsberechtigungen\\&."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Note the relationship to the I<firstboot\\&.keymap> credential understood by "
"B<systemd-firstboot.service>(8): both ultimately affect the same setting, "
"but I<firstboot\\&.keymap> is written into /etc/vconsole\\&.conf on first "
"boot (if not already configured), and then read from there by B<systemd-"
"vconsole-setup>, while I<vconsole\\&.keymap> is read on every boot, and is "
"not persisted to disk (but any configuration in vconsole\\&.conf will take "
"precedence if present)\\&."
msgstr ""
"Beachten Sie die Beziehung zwischen der von B<systemd-firstboot.service>(8) "
"verstandenen Zugangsberechtigung I<firstboot\\&.keymap>: Beide betreffen "
"letztendlich die gleiche Einstellung, aber I<firstboot\\&.keymap> wird beim "
"ersten Systemstart in /etc/vconsole\\&.conf geschrieben (falls es noch nicht "
"konfiguriert wurde) und dann durch B<systemd-vconsole-setup> von dort "
"gelesen, während I<vconsole\\&.keymap> bei jedem Systemstart gelesen wird "
"und sich nicht dauerhaft auf Platte befindet (allerdings hat jede vorhandene "
"Konfiguration in vconsole\\&.conf Vorrang)\\&."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"I<vconsole\\&.font>, I<vconsole\\&.font_map>, I<vconsole\\&.font_unimap>"
msgstr ""
"I<vconsole\\&.font>, I<vconsole\\&.font_map>, I<vconsole\\&.font_unimap>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"The console font settings to apply\\&. The matching options in vconsole\\&."
"conf and on the kernel command line take precedence over these "
"credentials\\&."
msgstr ""
"Die anzuwendenden Konsole-Schrifteinstellungen\\&. Die entsprechenden "
"Optionen in vconsole\\&.conf und auf der Kernelbefehlszeile haben Vorrang "
"vor diesen Zugangsberechtigungen\\&."

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid ""
"B<systemd>(1), B<vconsole.conf>(5), B<loadkeys>(1), B<setfont>(8), B<systemd-"
"localed.service>(8)"
msgstr ""
"B<systemd>(1), B<vconsole.conf>(5), B<loadkeys>(1), B<setfont>(8), B<systemd-"
"localed.service>(8)"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: Plain text
#: fedora-rawhide
msgid ""
"systemd-vconsole-setup sets up and configures either all virtual consoles, "
"or \\(em if the optional I<TTY> parameter is provided \\(em a specific "
"one\\&. When the system is booting up, systemd-vconsole-setup\\&.service is "
"called by B<systemd-udevd>(8)  during VT console subsystem "
"initialization\\&. Also, B<systemd-localed.service>(8)  invokes it as needed "
"when language or console changes are made\\&. Internally, this program calls "
"B<loadkeys>(1)  and B<setfont>(8)\\&."
msgstr ""
"systemd-vconsole-setup konfiguriert entweder alle virtuellen Konsolen und "
"richtet diese ein, oder \\(em falls der optionale Parameter I<TTY> "
"bereitgestellt wird \\(em eine bestimmte\\&. Wenn das System startet, wird "
"systemd-vconsole-setup\\&.service durch B<systemd-udevd>(8) bei der "
"Initialisierung des VT-Konsolensubsystems aufgerufen\\&. Bei Bedarf ruft sie "
"auch B<systemd-localed.service>(8) auf, wenn sich Sprach- oder "
"Konsolenänderungen erfolgen\\&. Intern ruft dieses Programm B<loadkeys>(1) "
"und B<setfont>(8) auf\\&."

# FIXME I<ImportCredential=> → I<ImportCredential=>/
#. type: Plain text
#: fedora-rawhide
msgid ""
"B<systemd-vconsole-setup> supports the service credentials logic as "
"implemented by I<ImportCredential=>I<LoadCredential=>/I<SetCredential=> (see "
"B<systemd.exec>(1)  for details)\\&. The following credentials are used when "
"passed in:"
msgstr ""
"B<systemd-vconsole-setup> unterstützt Dienstezugangsberechtigungslogiken, "
"wie die durch I<ImportCredential=>/I<LoadCredential=>/I<SetCredential=> "
"implementierten (siehe B<systemd.exec>(1) für Details)\\&. Die folgenden "
"Zugangsberechtigungen werden verwandt, wenn sie hereingegeben werden:"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"
