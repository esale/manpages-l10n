# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2015-2016.
# Helge Kreutzmann <debian@helgefjell.de>, 2018-2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.1.0\n"
"POT-Creation-Date: 2023-07-25 20:02+0200\n"
"PO-Revision-Date: 2020-08-30 07:09+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SYSTEMD-TIMEDATED\\&.SERVICE"
msgstr "SYSTEMD-TIMEDATED\\&.SERVICE"

#. type: TH
#: archlinux fedora-38 mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr "systemd 253"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "systemd-timedated.service"
msgstr "systemd-timedated.service"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"systemd-timedated.service, systemd-timedated - Time and date bus mechanism"
msgstr ""
"systemd-timedated.service, systemd-timedated - Busmechanismus für Zeit und "
"Datum"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "systemd-timedated\\&.service"
msgstr "systemd-timedated\\&.service"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid "/usr/lib/systemd/systemd-timedated"
msgstr "/usr/lib/systemd/systemd-timedated"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"systemd-timedated\\&.service is a system service that may be used as a "
"mechanism to change the system clock and timezone, as well as to enable/"
"disable network time synchronization\\&.  systemd-timedated is automatically "
"activated on request and terminates itself when it is unused\\&."
msgstr ""
"Systemd-timedated\\&.service ist ein Systemdienst, der als Mechanismus zum "
"Stellen der Uhr und Ändern der Zeitzone des Systems sowie zum Aktivieren "
"oder Deaktivieren des Zeitabgleichs über NTP verwendet werden kann\\&. "
"Systemd-timedated wird auf Anfrage automatisch aktiviert und beendet sich "
"selbst, wenn er nicht benötigt wird\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"The tool B<timedatectl>(1)  is a command line client to this service\\&."
msgstr ""
"Das Dienstprogramm B<timedatectl>(1) ist ein Befehlszeilenclient für diesen "
"Dienst\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"systemd-timedated currently offers access to the following four settings:"
msgstr ""
"Systemd-timedated bietet derzeit Zugriff auf die folgenden vier "
"Einstellungen:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "The system time"
msgstr "Die Systemzeit"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "The system timezone"
msgstr "Die Systemzeitzone"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"A boolean controlling whether the system RTC is in local or UTC timezone"
msgstr ""
"Ein logischer Wert, der steuert, ob sich die System-RTC in der lokalen oder "
"der UTC-Zeitzone befindet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"Whether the time synchronization service is enabled/started or disabled/"
"stopped, see next section\\&."
msgstr ""
"Ob der Zeitsynchronisationsdienst aktiviert/gestartet oder deaktiviert/"
"gestoppt ist, siehe nächsten Abschnitt\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"See B<org.freedesktop.timedate1>(5)  and B<org.freedesktop.LogControl1>(5)  "
"for information about the D-Bus API\\&."
msgstr ""
"Siehe B<org.freedesktop.timedate1>(5) und B<org.freedesktop.LogControl1>(5)  "
"für Information über das D-Bus API\\&."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "LIST OF NETWORK TIME SYNCHRONIZATION SERVICES"
msgstr "LISTE DER NETZWERK-ZEITSYNCHRONISATIONSDIENSTE"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
msgid ""
"B<systemd-timesyncd> will look for files with a \"\\&.list\" extension in "
"ntp-units\\&.d/ directories\\&. Each file is parsed as a list of unit names, "
"one per line\\&. Empty lines and lines with comments (\"#\") are ignored\\&. "
"Files are read from /usr/lib/systemd/ntp-units\\&.d/ and the corresponding "
"directories under /etc/, /run/, /usr/local/lib/\\&. Files in /etc/ override "
"files with the same name in /run/, /usr/local/lib/, and /usr/lib/\\&. Files "
"in /run/ override files with the same name under /usr/\\&. Packages should "
"install their configuration files in /usr/lib/ (distribution packages) or /"
"usr/local/lib/ (local installs)\\&."
msgstr ""
"B<systemd-timesyncd> schaut nach Dateien mit der Erweiterung »\\&.list« in "
"ntp-units\\&.d/-Verzeichnissen\\&. Jede Datei wird als Liste von Unit-Namen "
"ausgewertet, eine pro Zeile\\&. Leere Zeilen und Zeilen mit Kommentaren "
"(»#«) werden ignoriert\\&. Dateien werden aus /usr/lib/systemd/ntp-units\\&."
"d/ und den entsprechenden Verzeichnissen unter /etc/, /run/, /usr/local/lib/ "
"gelesen\\&. Dateien in /etc/ setzen Dateien mit dem gleichen Namen in /"
"run/, /usr/local/lib/ und /usr/lib/ außer Kraft\\&. Dateien in /run/ setzen "
"Dateien mit dem gleichen Namen in /usr/ außer Kraft\\&. Pakete sollten ihre "
"Konfigurationsdateien in /usr/lib/ (Distributionspakete) oder /usr/local/"
"lib/ (lokale Installationen) installieren\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<Example\\ \\&1.\\ \\&ntp-units\\&.d/ entry for systemd-timesyncd>"
msgstr "B<Beispiel\\ \\&1.\\ \\&ntp-units\\&.d/ Eintrag für Systemd-timesyncd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid ""
"# /usr/lib/systemd/ntp-units\\&.d/80-systemd-timesync\\&.list\n"
"systemd-timesyncd\\&.service\n"
msgstr ""
"# /usr/lib/systemd/ntp-units\\&.d/80-systemd-timesync\\&.list\n"
"systemd-timesyncd\\&.service\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"If the environment variable I<$SYSTEMD_TIMEDATED_NTP_SERVICES> is set, "
"B<systemd-timesyncd> will parse the contents of that variable as a colon-"
"separated list of unit names\\&. When set, this variable overrides the file-"
"based list described above\\&."
msgstr ""
"Falls die Umgebungsvariable I<$SYSTEMD_TIMEDATED_NTP_SERVICES> gesetzt ist, "
"wird B<systemd-timesyncd> die Inhalte dieser Variablen als Doppelpunkt-"
"getrennte Liste von Unit-Namen auswerten\\&. Wenn gesetzt, setzt diese "
"Variable die oben beschriebene Datei-basierte Liste außer Kraft\\&."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"B<Example\\ \\&2.\\ \\&An override that specifies that chronyd should be "
"used if available>"
msgstr ""
"B<Beispiel\\ \\&2.\\ \\&Eine Außerkraftsetzung, die festlegt, dass Chronyd "
"verwandt werden soll, falls verfügbar>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SYSTEMD_TIMEDATED_NTP_SERVICES=chronyd\\&.service:systemd-timesyncd\\&.service\n"
msgstr "SYSTEMD_TIMEDATED_NTP_SERVICES=chronyd\\&.service:systemd-timesyncd\\&.service\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"B<systemd>(1), B<timedatectl>(1), B<localtime>(5), B<hwclock>(8), B<systemd-"
"timesyncd>(8)"
msgstr ""
"B<systemd>(1), B<timedatectl>(1), B<localtime>(5), B<hwclock>(8), B<systemd-"
"timesyncd>(8)"

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr "systemd 252"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "/lib/systemd/systemd-timedated"
msgstr "/lib/systemd/systemd-timedated"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"B<systemd-timesyncd> will look for files with a \"\\&.list\" extension in "
"ntp-units\\&.d/ directories\\&. Each file is parsed as a list of unit names, "
"one per line\\&. Empty lines and lines with comments (\"#\") are ignored\\&. "
"Files are read from /usr/lib/systemd/ntp-units\\&.d/ and the corresponding "
"directories under /etc/, /run/, /usr/local/lib/\\&. Files in /etc/ override "
"files with the same name in /run/, /usr/local/lib/, and /lib/\\&. Files in /"
"run/ override files with the same name under /usr/\\&. Packages should "
"install their configuration files in /usr/lib/ (distribution packages) or /"
"usr/local/lib/ (local installs)\\&."
msgstr ""
"B<systemd-timesyncd> schaut nach Dateien mit der Erweiterung »\\&.list« in "
"ntp-units\\&.d/-Verzeichnissen\\&. Jede Datei wird als Liste von Unit-Namen "
"ausgewertet, eine pro Zeile\\&. Leere Zeilen und Zeilen mit Kommentaren "
"(»#«) werden ignoriert\\&. Dateien werden aus /usr/lib/systemd/ntp-units\\&."
"d/ und den entsprechenden Verzeichnissen unter /etc/, /run/, /usr/local/lib/ "
"gelesen\\&. Dateien in /etc/ setzen Dateien mit dem gleichen Namen in /"
"run/, /usr/local/lib/ und /lib/ außer Kraft\\&. Dateien in /run/ setzen "
"Dateien mit dem gleichen Namen in /usr/ außer Kraft\\&. Pakete sollten ihre "
"Konfigurationsdateien in /usr/lib/ (Distributionspakete) oder /usr/local/"
"lib/ (lokale Installationen) installieren\\&."

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 254"
msgstr "systemd 254"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"
