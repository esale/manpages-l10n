# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-06-27 19:35+0200\n"
"PO-Revision-Date: 2023-06-03 15:09+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: ds C+
#: mageia-cauldron
#, no-wrap
msgid "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"
msgstr "C\\v'-.1v'\\h'-1p'\\s-2+\\h'-1p'+\\s0\\v'.1v'\\h'-1p'"

#.  ========================================================================
#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "Title"
msgstr "Title"

#.  ========================================================================
#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "LILO-UUID-DISKID 8"
msgstr "LILO-UUID-DISKID 8"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "LILO-UUID-DISKID"
msgstr "LILO-UUID-DISKID"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "2015-11-22"
msgstr "22. November 2015"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "24.2"
msgstr "24.2"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "lilo documentation"
msgstr "Lilo-Dokumentation"

#. type: SH
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: mageia-cauldron
msgid ""
"lilo-uuid-diskid - convert boot / root options to diskid and uuid in lilo."
"conf."
msgstr ""
"lilo-uuid-diskid - Boot-/Root-Optionen in Diskid und UUID in lilo.conf "
"konvertieren."

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "Header"
msgstr "Header"

#. type: Plain text
#: mageia-cauldron
msgid "\\&B<lilo-uuid-diskid> [B<-h>] [B<-v>] [B<lilo.conf>]"
msgstr "\\&B<lilo-uuid-diskid> [B<-h>] [B<-v>] [B<lilo.conf>]"

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: mageia-cauldron
msgid ""
"This script looks for the boot block device or boot partition and create the "
"right diskid or uuid as boot option. Then it looks for all root partitions "
"and create the right uuids as root options."
msgstr ""
"Dieses Skript sucht nach Systemstart-Blockgeräten oder "
"Systemstartpartitionen und erstellt die richtige Diskid oder UUID als Boot-"
"Option. Dann sucht es nach allen Wurzelpartitionen und erstellt die "
"richtigen UUIDs als Root-Optionen."

#. type: Plain text
#: mageia-cauldron
msgid ""
"These conversions are necessary for use with newer kernel (E<gt>= 2.6.26) if "
"it use the libata module for parallel and serial \\s-1ATA\\s0 interfaces of "
"block devices (i. e. hard disks with \\s-1IDE\\s0 or \\s-1SATA\\s0 "
"interface, usbsticks)."
msgstr ""
"Diese Umwandlungen sind für neuere Kernel (E<gt>= 2.6.26) wichtig, falls "
"diese das Modul »libata« und die serielle »ATA«-Schnittstelle der "
"Blockgeräte verwenden (d.h. für Festplatten mit »IDE«- oder »SATA«-"
"Schnittstelle, USB-Sticks)."

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: IP
#: mageia-cauldron
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "Item"
msgstr "Item"

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "-h"
msgstr "-h"

#. type: Plain text
#: mageia-cauldron
msgid "Print a brief help."
msgstr "gibt eine kurze Hilfemeldung aus."

#. type: IP
#: mageia-cauldron
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "-v"
msgstr "-v"

#. type: Plain text
#: mageia-cauldron
msgid "Print verbose messages."
msgstr "gibt ausführliche Meldungen aus."

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: mageia-cauldron
msgid "Lines in the configuration file /etc/lilo.conf:"
msgstr "Zeilen in der Konfigurationsdatei /etc/lilo.conf:"

#. type: Plain text
#: mageia-cauldron
msgid ""
"\\& #boot = /dev/sda \\& boot = /dev/disk/by-id/ata-"
"SAMSUNG_SV1604N_S01FJ10X999999 \\& \\& #root = /dev/sda1 \\& root = "
"\"UUID=18843936-00f9-4df0-a373-000d05a5dd44\""
msgstr ""
"\\& #boot = /dev/sda \\& boot = /dev/disk/by-id/ata-"
"SAMSUNG_SV1604N_S01FJ10X999999 \\& \\& #root = /dev/sda1 \\& root = "
"\"UUID=18843936-00f9-4df0-a373-000d05a5dd44\""

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "COPYRIGHT and LICENSE"
msgstr "COPYRIGHT und LIZENZ"

#. type: Plain text
#: mageia-cauldron
msgid "Copyright (C) 2010-2014 Joachim Wiedorn"
msgstr "Copyright (C) 2010-2014 Joachim Wiedorn"

#. type: Plain text
#: mageia-cauldron
msgid ""
"This script is free software; you can redistribute it and/or modify it under "
"the terms of the \\s-1GNU\\s0 General Public License as published by the "
"Free Software Foundation; either version 2 of the License, or (at your "
"option) any later version."
msgstr ""
"Dieses Skript ist freie Software; Sie können es unter den Bedingungen der "
"\\s-1GNU\\s0 General Public License weitergeben und/oder verändern, so wie "
"sie von der Free Software Foundation veröffentlicht wurde; entweder in "
"Version 2 der Lizenz oder (nach Ihrem Ermessen) in jeder späteren Version."

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: mageia-cauldron
msgid "\\&B<lilo-uuid-diskid> was written by Joachim Wiedorn."
msgstr "\\&B<lilo-uuid-diskid> wurde von Joachim Wiedorn geschrieben."

#. type: Plain text
#: mageia-cauldron
msgid ""
"This manual page was written by Joachim Wiedorn E<lt>joodevel at joonet."
"deE<gt>."
msgstr ""
"Diese Handbuchseite wurde von Joachim Wiedorn E<lt>joodevel at joonet."
"deE<gt> geschrieben."

#. type: IX
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: mageia-cauldron
msgid "\\&B<lilo>(8), B<update-lilo>(8), B<liloconfig>(8)"
msgstr "\\&B<lilo>(8), B<update-lilo>(8), B<liloconfig>(8)"
