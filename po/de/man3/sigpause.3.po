# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# René Tschirley <gremlin@cs.tu-berlin.de>
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-06-27 19:51+0200\n"
"PO-Revision-Date: 2023-05-04 10:19+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sigpause"
msgstr "sigpause"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "sigpause - atomically release blocked signals and wait for interrupt"
msgstr ""
"sigpause - schaltet gezielt blockierte Signale frei und wartet auf einen "
"Interrupt"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int sigpause(int >I<sigmask>B<);  /* BSD (but see NOTES) */>\n"
msgstr "B<[[veraltet]] int sigpause(int >I<sigmask>B<);  /* BSD (siehe ANMERKUNGEN) */>\n"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int sigpause(int >I<sig>B<);      /* POSIX.1 / SysV / UNIX 95 */>\n"
msgstr "B<[[veraltet]] int sigpause(int >I<sig>B<);      /* POSIX.1 / SysV / UNIX 95 */>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Don't use this function.  Use B<sigsuspend>(2)  instead."
msgstr ""
"Verwenden Sie diese nicht Funktion, sondern stattdessen B<sigsuspend>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function B<sigpause>()  is designed to wait for some signal.  It changes "
"the process's signal mask (set of blocked signals), and then waits for a "
"signal to arrive.  Upon arrival of a signal, the original signal mask is "
"restored."
msgstr ""
"Die Funktion B<sigpause>() wurde entwickelt, um auf ein Signal warten. Sie "
"ändert die Signalmaske (den Satz blockierter Signale) des Prozesses und "
"wartet dann darauf, dass ein Signal eintrifft. Nach der Ankunft eines "
"Signals wird die ursprüngliche Signalmaske wiederhergestellt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If B<sigpause>()  returns, it was interrupted by a signal and the return "
"value is -1 with I<errno> set to B<EINTR>."
msgstr ""
"Wenn B<sigpause>() zurückkehrt, wurde sie durch ein Signal unterbrochen. Der "
"Rückgabewert ist -1; der Wert von I<errno> ist B<EINTR>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<sigpause>()"
msgstr "B<sigpause>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On Linux, this routine is a system call only on the Sparc (sparc64)  "
"architecture."
msgstr ""
"Unter Linux ist diese Routine nur auf der Sparc-Architektur (sparc64) ein "
"Systemaufruf."

#.  Libc4 and libc5 know only about the BSD version.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"glibc uses the BSD version if the B<_BSD_SOURCE> feature test macro is "
"defined and none of B<_POSIX_SOURCE>, B<_POSIX_C_SOURCE>, B<_XOPEN_SOURCE>, "
"B<_GNU_SOURCE>, or B<_SVID_SOURCE> is defined.  Otherwise, the System V "
"version is used, and feature test macros must be defined as follows to "
"obtain the declaration:"
msgstr ""
"Glibc verwendet die BSD-Version, wenn das Feature-Test-Makro B<_BSD_SOURCE> "
"und keines der Makros B<_POSIX_SOURCE>, B<_POSIX_C_SOURCE>, "
"B<_XOPEN_SOURCE>, B<_GNU_SOURCE> oder B<_SVID_SOURCE> definiert ist. "
"Anderenfalls verwendet sie die System-V-Version und Feature-Test-Makros "
"müssen wie folgt definiert sein, um die Deklarationen zu erhalten:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#.  || (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Since glibc 2.26: _XOPEN_SOURCE E<gt>= 500"
msgstr "Seit Glibc 2.26: _XOPEN_SOURCE E<gt>= 500"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "glibc 2.25 and earlier: _XOPEN_SOURCE"
msgstr "Glibc 2.25 und älter: _XOPEN_SOURCE"

#
#.  For the BSD version, one usually uses a zero
#.  .I sigmask
#.  to indicate that no signals are to be blocked.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Since glibc 2.19, only the System V version is exposed by I<E<lt>signal."
"hE<gt>>; applications that formerly used the BSD B<sigpause>()  should be "
"amended to use B<sigsuspend>(2)."
msgstr ""
"Seit Glibc 2.19 wird nur die System-V-Version von I<E<lt>signal.hE<gt>> "
"angezeigt. Zu Anwendungen, die früher B<sigpause>() von BSD verwendet haben, "
"sollte B<sigsuspend>(2) hinzugefügt werden."

#. #-#-#-#-#  archlinux: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  debian-bookworm: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#. #-#-#-#-#  debian-unstable: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#. #-#-#-#-#  fedora-38: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#. #-#-#-#-#  fedora-rawhide: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  mageia-cauldron: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: sigpause.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001.  Obsoleted in POSIX.1-2008."
msgstr "POSIX.1-2001. In POSIX.1-2008 als veraltet markiert."

#.  __xpg_sigpause: UNIX 95, spec 1170, SVID, SVr4, XPG
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The classical BSD version of this function appeared in 4.2BSD.  It sets the "
"process's signal mask to I<sigmask>.  UNIX 95 standardized the incompatible "
"System V version of this function, which removes only the specified signal "
"I<sig> from the process's signal mask.  The unfortunate situation with two "
"incompatible functions with the same name was solved by the B<\\"
"%sigsuspend>(2)  function, that takes a I<sigset_t\\ *> argument (instead of "
"an I<int>)."
msgstr ""
"Die klassische BSD-Version dieser Funktion erschien in 4.2BSD. Sie setzt die "
"Signalmaske des Prozesses auf I<sigmask>. UNIX 95 standardisierte die "
"inkompatible System-V-Version dieser Funktion, die nur das angegebene Signal "
"I<sig> aus der Signalmaske des Prozesses entfernt. Die unglückliche "
"Situation mit zwei unvereinbaren Funktionen mit dem gleichen Namen wurde von "
"der Funktion B<\\%sigsuspend>(2) gelöst. Ihr Argument hat den Typ "
"I<sigset_t\\ *> anstatt I<int>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<kill>(2), B<sigaction>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<sigblock>(3), B<sigvec>(3), B<feature_test_macros>(7)"
msgstr ""
"B<kill>(2), B<sigaction>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<sigblock>(3), B<sigvec>(3), B<feature_test_macros>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<int sigpause(int >I<sigmask>B<);  /* BSD (but see NOTES) */>\n"
msgstr "B<int sigpause(int >I<sigmask>B<);  /* BSD (siehe ANMERKUNGEN) */>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "B<int sigpause(int >I<sig>B<);      /* System V / UNIX 95 */>\n"
msgstr "B<int sigpause(int >I<sig>B<);      /* System V / UNIX 95 */>\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"The System V version of B<sigpause>()  is standardized in POSIX.1-2001.  It "
"is also specified in POSIX.1-2008, where it is marked obsolete."
msgstr ""
"Die System-V-Version von B<sigpause>() ist in POSIX.1-2001 standardisiert. "
"Er ist auch in POSIX.1-2008 spezifiziert, dort aber als veraltet markiert."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: SS
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "History"
msgstr "Geschichte"

#. type: SS
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "Linux notes"
msgstr "Linux-Anmerkungen"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "SIGPAUSE"
msgstr "SIGPAUSE"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#
#.  Libc4 and libc5 know only about the BSD version.
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Glibc uses the BSD version if the B<_BSD_SOURCE> feature test macro is "
"defined and none of B<_POSIX_SOURCE>, B<_POSIX_C_SOURCE>, B<_XOPEN_SOURCE>, "
"B<_GNU_SOURCE>, or B<_SVID_SOURCE> is defined.  Otherwise, the System V "
"version is used, and feature test macros must be defined as follows to "
"obtain the declaration:"
msgstr ""
"Glibc verwendet die BSD-Version, wenn das Feature-Test-Makro B<_BSD_SOURCE> "
"und keines der Makros B<_POSIX_SOURCE>, B<_POSIX_C_SOURCE>, "
"B<_XOPEN_SOURCE>, B<_GNU_SOURCE> oder B<_SVID_SOURCE> definiert ist. "
"Anderenfalls verwendet sie die System-V-Version und Feature-Test-Makros "
"müssen wie folgt definiert sein, um die Deklarationen zu erhalten:"

#. type: IP
#: opensuse-leap-15-5
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Glibc 2.25 and earlier: _XOPEN_SOURCE"
msgstr "Glibc 2.25 und älter: _XOPEN_SOURCE"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
