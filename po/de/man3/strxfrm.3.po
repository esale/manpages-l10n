# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2023-06-27 19:53+0200\n"
"PO-Revision-Date: 2023-02-17 15:15+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "strxfrm"
msgstr "strxfrm"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "strxfrm - string transformation"
msgstr "strxfrm - Zeichenkettenumwandlung"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr "B<#include E<lt>string.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<size_t strxfrm(char >I<dest>B<[restrict .>I<n>B<], const char >I<src>B<[restrict .>I<n>B<],>\n"
"B<               size_t >I<n>B<);>\n"
msgstr ""
"B<size_t strxfrm(char >I<Ziel>B<[restrict .>I<n>B<], const char >I<Quelle>B<[restrict .>I<n>B<],>\n"
"B<               size_t >I<n>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<strxfrm>()  function transforms the I<src> string into a form such "
"that the result of B<strcmp>(3)  on two strings that have been transformed "
"with B<strxfrm>()  is the same as the result of B<strcoll>(3)  on the two "
"strings before their transformation.  The first I<n> bytes of the "
"transformed string are placed in I<dest>.  The transformation is based on "
"the program's current locale for category B<LC_COLLATE>.  (See "
"B<setlocale>(3))."
msgstr ""
"Die Funktion B<strxfrm>() wandelt die Zeichenkette I<Quelle> in eine solche "
"Form um, dass das Ergebnis von B<strcmp>(3) von zwei Zeichenketten, die mit "
"B<strxfrm>() umgewandelt wurden, identisch zum Ergebnis von B<strcoll>(3) "
"mit den zwei Zeichenketten vor ihrer Umwandlung ist. Die ersten I<n> Byte "
"der umgewandelten Zeichenkette werden in I<Ziel> abgelegt. Die Umwandlung "
"basiert auf der aktuellen Locale des Programms für die Kategorie "
"B<LC_COLLATE> (siehe B<setlocale>(3))."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<strxfrm>()  function returns the number of bytes required to store the "
"transformed string in I<dest> excluding the terminating null byte "
"(\\[aq]\\e0\\[aq]).  If the value returned is I<n> or more, the contents of "
"I<dest> are indeterminate."
msgstr ""
"Die Funktion B<strxfrm>() liefert die Anzahl von Bytes zurück, die zur "
"Speicherung der umgewandelten Zeichenkette in I<Ziel> (ohne das "
"abschließende Nullbyte »\\e0« benötigt werden. Falls der zurückgelieferte "
"Wert I<n> oder mehr ist, ist der Inhalt von I<Ziel> unbestimmt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<strxfrm>()"
msgstr "B<strxfrm>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, C89, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, C89, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<memcmp>(3), B<setlocale>(3), B<strcasecmp>(3), B<strcmp>(3), "
"B<strcoll>(3), B<string>(3)"
msgstr ""
"B<memcmp>(3), B<setlocale>(3), B<strcasecmp>(3), B<strcmp>(3), "
"B<strcoll>(3), B<string>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "STRXFRM"
msgstr "STRXFRM"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-07-17"
msgstr "17. Juli 2016"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<size_t strxfrm(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>\n"
msgstr "B<size_t strxfrm(char *>I<Ziel>B<, const char *>I<Quelle>B<, size_t >I<n>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<strxfrm>()  function returns the number of bytes required to store the "
"transformed string in I<dest> excluding the terminating null byte "
"(\\(aq\\e0\\(aq).  If the value returned is I<n> or more, the contents of "
"I<dest> are indeterminate."
msgstr ""
"Die Funktion B<strxfrm>() liefert die Anzahl von Bytes zurück, die zur "
"Speicherung der umgewandelten Zeichenkette in I<Ziel> (ohne das "
"abschließende Nullbyte »\\e0«) benötigt werden. Falls der zurückgelieferte "
"Wert I<n> oder mehr ist, ist der Inhalt von I<Ziel> unbestimmt."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: Plain text
#: opensuse-leap-15-5
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<bcmp>(3), B<memcmp>(3), B<setlocale>(3), B<strcasecmp>(3), B<strcmp>(3), "
"B<strcoll>(3), B<string>(3)"
msgstr ""
"B<bcmp>(3), B<memcmp>(3), B<setlocale>(3), B<strcasecmp>(3), B<strcmp>(3), "
"B<strcoll>(3), B<string>(3)"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
