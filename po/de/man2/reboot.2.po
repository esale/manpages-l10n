# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Chris Leick <c.leick@vollbio.de>, 2010-2014.
# Helge Kreutzmann <debian@helgefjell.de>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.17.0\n"
"POT-Creation-Date: 2023-06-27 19:45+0200\n"
"PO-Revision-Date: 2023-06-01 21:02+0200\n"
"Last-Translator: Chris Leick <c.leick@vollbio.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "reboot"
msgstr "reboot"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "reboot - reboot or enable/disable Ctrl-Alt-Del"
msgstr "reboot - Neustart oder Strg-Alt-Entf ein-/ausschalten"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"/* Since Linux 2.1.30 there are symbolic names B<LINUX_REBOOT_*>\n"
"   for the constants and a fourth argument to the call: */\n"
msgstr ""
"/* Seit Linux 2.1.30 gibt es die symbolischen Namen B<LINUX_REBOOT_*>\n"
"   für die Konstanten und ein viertes Argument für den Aufruf: */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>linux/reboot.hE<gt>  >/* Definition of B<LINUX_REBOOT_*> constants */\n"
"B<#include E<lt>sys/syscall.hE<gt>   >/* Definition of B<SYS_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>linux/reboot.hE<gt>  >/* Definition der B<LINUX_REBOOT_*>-Konstanten */\n"
"B<#include E<lt>sys/syscall.hE<gt>   >/* Definition der B<SYS_*>-Konstanten */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int syscall(SYS_reboot, int >I<magic>B<, int >I<magic2>B<, int >I<cmd>B<, void *>I<arg>B<);>\n"
msgstr "B<int syscall(SYS_reboot, int >I<Magik>B<, int >I<Magik2>B<, int >I<Befehl>B<, void *>I<Arg>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"/* Under glibc and most alternative libc's (including uclibc, dietlibc,\n"
"   musl and a few others), some of the constants involved have gotten\n"
"   symbolic names B<RB_*>, and the library call is a 1-argument\n"
"   wrapper around the system call: */\n"
msgstr ""
"/* Unter Glibc und den meisten alternativen Libcs (darunter Uclibc,\n"
"   Dietlibc, Musl und einigen anderen) haben einige beteiligte Konstanten\n"
"   symbolische B<RB_*>-Namen bekommen und der Bibliotheksaufruf ist ein\n"
"   Ein-Argument-Wrapper des Systemaufrufs: */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/reboot.hE<gt>    >/* Definition of B<RB_*> constants */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/reboot.hE<gt>    >/* Definition der B<RB_*>-Konstanten */\n"
"B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int reboot(int >I<cmd>B<);>\n"
msgstr "B<int reboot(int >I<Befehl>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<reboot>()  call reboots the system, or enables/disables the reboot "
"keystroke (abbreviated CAD, since the default is Ctrl-Alt-Delete; it can be "
"changed using B<loadkeys>(1))."
msgstr ""
"Der B<reboot>()-Aufruf startet das System neu oder schaltet den Tastendruck "
"für den Neustart ein/aus (Strg-Alt-Entf, englisch CAD für Ctrl-Alt-Delete; "
"er kann mittels B<loadkeys>(1) geändert werden)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This system call fails (with the error B<EINVAL>)  unless I<magic> equals "
"B<LINUX_REBOOT_MAGIC1> (that is, 0xfee1dead) and I<magic2> equals "
"B<LINUX_REBOOT_MAGIC2> (that is, 0x28121969).  However, since Linux 2.1.17 "
"also B<LINUX_REBOOT_MAGIC2A> (that is, 0x05121996)  and since Linux 2.1.97 "
"also B<LINUX_REBOOT_MAGIC2B> (that is, 0x16041998)  and since Linux 2.5.71 "
"also B<LINUX_REBOOT_MAGIC2C> (that is, 0x20112000)  are permitted as values "
"for I<magic2>.  (The hexadecimal values of these constants are meaningful.)"
msgstr ""
"Dieser Systemaufruf schlägt (mit dem Fehler B<EINVAL>) fehl, außer wenn "
"I<Magik> gleich B<LINUX_REBOOT_MAGIC1> (0xfee1dead) und I<Magik2> gleich "
"B<LINUX_REBOOT_MAGIC2> (0x28121969) ist. Seit Linux 2.1.17 ist jedoch auch "
"B<LINUX_REBOOT_MAGIC2A> (0x05121996) und seit Linux 2.1.97 außerdem "
"B<LINUX_REBOOT_MAGIC2B> (0x16041998) und seit Linux 2.5.71 auch "
"B<LINUX_REBOOT_MAGIC2C> (0x20112000) als Wert für I<Magik2> erlaubt. (Die "
"hexadezimalen Werte dieser Konstanten haben eine Bedeutung.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The I<cmd> argument can have the following values:"
msgstr "Das Argument I<Befehl> kann die folgenden Werte haben:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_CAD_OFF>"
msgstr "B<LINUX_REBOOT_CMD_CAD_OFF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<RB_DISABLE_CAD>, 0).  CAD is disabled.  This means that the CAD keystroke "
"will cause a B<SIGINT> signal to be sent to init (process 1), whereupon this "
"process may decide upon a proper action (maybe: kill all processes, sync, "
"reboot)."
msgstr ""
"(B<RB_DISABLE_CAD>, 0). Strg-Alt-Delete wird ausgeschaltet. Dies bedeutet, "
"dass das Tastenkürzel Strg-Alt-Delete veranlasst, dass ein B<SIGINT>-Signal "
"an Init (Prozess 1) gesandt wird, woraufhin dieser Prozess über die richtige "
"Aktion entscheidet (möglicherweise: alle Prozesse beenden, Platten "
"synchronisieren, Neustart)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_CAD_ON>"
msgstr "B<LINUX_REBOOT_CMD_CAD_ON>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<RB_ENABLE_CAD>, 0x89abcdef).  CAD is enabled.  This means that the CAD "
"keystroke will immediately cause the action associated with "
"B<LINUX_REBOOT_CMD_RESTART>."
msgstr ""
"(B<RB_ENABLE_CAD>, 0x89abcdef). Strg-Alt-Delete wird eingeschaltet. Dies "
"bedeutet, dass das Tastenkürzel Strg-Alt-Delete sofort die mit "
"B<LINUX_REBOOT_CMD_RESTART> verbundene Aktion veranlassen wird."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_HALT>"
msgstr "B<LINUX_REBOOT_CMD_HALT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<RB_HALT_SYSTEM>, 0xcdef0123; since Linux 1.1.76).  The message \"System "
"halted.\" is printed, and the system is halted.  Control is given to the ROM "
"monitor, if there is one.  If not preceded by a B<sync>(2), data will be "
"lost."
msgstr ""
"(B<RB_HALT_SYSTEM>, 0xcdef0123; seit Linux 1.1.76). Die Nachricht »System "
"halted.« wird ausgegeben und das System wird angehalten. Falls vorhanden, "
"wird die Steuerung an den ROM-Monitor übergeben. Wenn kein B<sync>(2) "
"vorausgeht, werden Daten verlorengehen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_KEXEC>"
msgstr "B<LINUX_REBOOT_CMD_KEXEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<RB_KEXEC>, 0x45584543, since Linux 2.6.13).  Execute a kernel that has "
"been loaded earlier with B<kexec_load>(2).  This option is available only if "
"the kernel was configured with B<CONFIG_KEXEC>."
msgstr ""
"(B<RB_KEXEC>, 0x45584543, since Linux 2.6.13). Einen Kernel ausführen, der "
"vorher mit B<kexec_load>(2) geladen wurde. Diese Option ist nur verfügbar, "
"falls der Kernel mit B<CONFIG_KEXEC> konfiguriert wurde."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_POWER_OFF>"
msgstr "B<LINUX_REBOOT_CMD_POWER_OFF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<RB_POWER_OFF>, 0x4321fedc; since Linux 2.1.30).  The message \"Power down."
"\" is printed, the system is stopped, and all power is removed from the "
"system, if possible.  If not preceded by a B<sync>(2), data will be lost."
msgstr ""
"(B<RB_POWER_OFF>, 0x4321fedc; seit Linux 2.1.30). Die Nachricht »Power "
"down.« wird ausgegeben, das System wird gestoppt und jegliche Stromzufuhr "
"unterbrochen, wenn möglich. Falls kein B<sync>(2) vorausgeht, werden Daten "
"verlorengehen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_RESTART>"
msgstr "B<LINUX_REBOOT_CMD_RESTART>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<RB_AUTOBOOT>, 0x1234567).  The message \"Restarting system.\" is printed, "
"and a default restart is performed immediately.  If not preceded by a "
"B<sync>(2), data will be lost."
msgstr ""
"(B<RB_AUTOBOOT>, 0x1234567). Die Nachricht »Restarting system.« wird "
"ausgegeben und es wird sofort ein Standard-Neustart ausgeführt. Wenn kein "
"B<sync>(2) vorausgeht, werden Daten verlorengehen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_RESTART2>"
msgstr "B<LINUX_REBOOT_CMD_RESTART2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"(0xa1b2c3d4; since Linux 2.1.30).  The message \"Restarting system with "
"command \\[aq]%s\\[aq]\" is printed, and a restart (using the command string "
"given in I<arg>)  is performed immediately.  If not preceded by a "
"B<sync>(2), data will be lost."
msgstr ""
"(0xa1b2c3d4; seit Linux 2.1.30). Die Nachricht »Restarting system with "
"command \\(aq%s\\(aq« wird ausgegeben und sofort ein Neustart ausgeführt "
"(unter Verwendung der in I<Arg> angegebenen Zeichenkette). Wenn kein "
"B<sync>(2) vorausgeht, werden Daten verlorengehen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_SW_SUSPEND>"
msgstr "B<LINUX_REBOOT_CMD_SW_SUSPEND>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<RB_SW_SUSPEND>, 0xd000fce1; since Linux 2.5.18).  The system is suspended "
"(hibernated) to disk.  This option is available only if the kernel was "
"configured with B<CONFIG_HIBERNATION>."
msgstr ""
"(B<RB_SW_SUSPEND>, 0xd000fce1; seit Linux 2.5.18). Einen Kernel ausführen, "
"der vorher mit B<kexec_load>(2) geladen wurde. Diese Option ist nur "
"verfügbar, falls der Kernel mit B<CONFIG_KEXEC> konfiguriert wurde."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Only the superuser may call B<reboot>()."
msgstr "Nur der Superuser kann B<reboot>() aufrufen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The precise effect of the above actions depends on the architecture.  For "
"the i386 architecture, the additional argument does not do anything at "
"present (2.1.122), but the type of reboot can be determined by kernel "
"command-line arguments (\"reboot=...\") to be either warm or cold, and "
"either hard or through the BIOS."
msgstr ""
"Die genauen Auswirkungen der vorangehenden Aktionen hängen von der "
"Architektur ab. Für die i386-Architektur bewirkt das zusätzliche Argument "
"derzeit nichts (2.1.122), aber der Typ des Neustarts kann durch Kernel-"
"Befehlszeilenargumente (»reboot=…«) festgelegt werden, um entweder einen "
"Warm- oder Kaltstart entweder hart oder über das BIOS durchzuführen."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Behavior inside PID namespaces"
msgstr "Verhalten innerhalb von PID-Namensräumen"

#.  commit cf3f89214ef6a33fad60856bc5ffd7bb2fc4709b
#.  see also commit 923c7538236564c46ee80c253a416705321f13e3
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Since Linux 3.4, if B<reboot>()  is called from a PID namespace other than "
"the initial PID namespace with one of the I<cmd> values listed below, it "
"performs a \"reboot\" of that namespace: the \"init\" process of the PID "
"namespace is immediately terminated, with the effects described in "
"B<pid_namespaces>(7)."
msgstr ""
"Wird B<reboot>() in einem vom ursprünglichen PID-Namensraum verschiedenen "
"Namensraum mit einem der nachfolgend aufgeführten I<Befehl>-Werte "
"aufgerufen, dann führt es seit Linux 3.4 einen »Neustart« dieses "
"Namensraumes durch: der »init«-Prozess des PID-Namensraums wird sofort mit "
"den in B<pid_namespaces>(7) beschriebenen Effekten beendet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The values that can be supplied in I<cmd> when calling B<reboot>()  in this "
"case are as follows:"
msgstr ""
"Die Werte, die in I<Befehl> beim Aufruf von B<reboot>() übergeben werden "
"können, sind in diesem Fall die folgenden:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_RESTART>, B<LINUX_REBOOT_CMD_RESTART2>"
msgstr "B<LINUX_REBOOT_CMD_RESTART>, B<LINUX_REBOOT_CMD_RESTART2>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The \"init\" process is terminated, and B<wait>(2)  in the parent process "
"reports that the child was killed with a B<SIGHUP> signal."
msgstr ""
"Der »init«-Prozess wird beendet und B<wait>(2) im Elternprozess berichtet, "
"dass das Kind mit einem Signal B<SIGHUP> getötet wurde."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LINUX_REBOOT_CMD_POWER_OFF>, B<LINUX_REBOOT_CMD_HALT>"
msgstr "B<LINUX_REBOOT_CMD_POWER_OFF>, B<LINUX_REBOOT_CMD_HALT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The \"init\" process is terminated, and B<wait>(2)  in the parent process "
"reports that the child was killed with a B<SIGINT> signal."
msgstr ""
"Der »init«-Prozess wird beendet und B<wait>(2) im Elternprozess berichtet, "
"dass das Kind mit einem Signal B<SIGINT> getötet wurde."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For the other I<cmd> values, B<reboot>()  returns -1 and I<errno> is set to "
"B<EINVAL>."
msgstr ""
"Für andere Werte in I<Befehl> liefert B<reboot>() -1 zurück und I<errno> "
"wird auf B<EINVAL> gesetzt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"For the values of I<cmd> that stop or restart the system, a successful call "
"to B<reboot>()  does not return.  For the other I<cmd> values, zero is "
"returned on success.  In all cases, -1 is returned on failure, and I<errno> "
"is set to indicate the error."
msgstr ""
"Für die Werte von I<Befehl>, die das System stoppen oder neu starten, gibt "
"ein erfolgreicher Aufruf von B<reboot>() nichts zurück. Für die anderen "
"I<Befehl>-Werte wird bei Erfolg Null zurückgegeben. Bei einem Fehler wird "
"immer -1 zurückgegeben und I<errno> gesetzt, um den Fehler anzuzeigen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Problem with getting user-space data under B<LINUX_REBOOT_CMD_RESTART2>."
msgstr ""
"Problem bei der Abfrage von Daten aus dem Adressraum des Benutzers unter "
"B<LINUX_REBOOT_CMD_RESTART2>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Bad magic numbers or I<cmd>."
msgstr "falsche magische Zahlen oder I<Befehl>"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The calling process has insufficient privilege to call B<reboot>(); the "
"caller must have the B<CAP_SYS_BOOT> inside its user namespace."
msgstr ""
"Der aufrufende Prozess verfügt nicht über ausreichende Privilegien, um "
"B<reboot>() aufzurufen. Die Capability B<CAP_SYS_BOOT> wird innerhalb seines "
"Benutzernamensraums benötigt."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<systemctl>(1), B<systemd>(1), B<kexec_load>(2), B<sync>(2), "
"B<bootparam>(7), B<capabilities>(7), B<ctrlaltdel>(8), B<halt>(8), "
"B<shutdown>(8)"
msgstr ""
"B<systemctl>(1), B<systemd>(1), B<kexec_load>(2), B<sync>(2), "
"B<bootparam>(7), B<capabilities>(7), B<ctrlaltdel>(8), B<halt>(8), "
"B<shutdown>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-08"
msgstr "8. Februar 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"B<reboot>()  is Linux-specific, and should not be used in programs intended "
"to be portable."
msgstr ""
"B<reboot>() ist Linux-spezifisch und sollte nicht in portierbaren Programmen "
"benutzt werden."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "REBOOT"
msgstr "REBOOT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"/* Since kernel version 2.1.30 there are symbolic names LINUX_REBOOT_*\n"
"   for the constants and a fourth argument to the call: */\n"
msgstr ""
"/* Seit Kernel-Version 2.1.30 gibt es die symbolischen\n"
"   Namen LINUX_REBOOT_* für die Konstanten und ein viertes\n"
"   Argument für den Aufruf: */\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>linux/reboot.hE<gt>>"
msgstr "B<#include E<lt>linux/reboot.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<int reboot(int >I<magic>B<, int >I<magic2>B<, int >I<cmd>B<, void "
"*>I<arg>B<);>"
msgstr ""
"B<int reboot(int >I<Magik>B<, int >I<Magik2>B<, int >I<Befehl>B<, void "
"*>I<Arg>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"/* Under glibc and most alternative libc's (including uclibc, dietlibc,\n"
"   musl and a few others), some of the constants involved have gotten\n"
"   symbolic names RB_*, and the library call is a 1-argument\n"
"   wrapper around the system call: */\n"
msgstr ""
"/* Unter Glibc und der meisten alternativen Libcs (darunter Uclibc,\n"
"   Dietlibc, Musl und einige andere) haben einige beteiligte Konstanten\n"
"   symbolische RB_*-Namen bekommen und der Bibliotheksaufruf ist ein\n"
"   Ein-Argument-Wrapper des Systemaufrufs: */\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>sys/reboot.hE<gt>>"
msgstr "B<#include E<lt>sys/reboot.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int reboot(int >I<cmd>B<);>"
msgstr "B<int reboot(int >I<Befehl>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This system call fail (with the error B<EINVAL>)  unless I<magic> equals "
"B<LINUX_REBOOT_MAGIC1> (that is, 0xfee1dead) and I<magic2> equals "
"B<LINUX_REBOOT_MAGIC2> (that is, 672274793).  However, since 2.1.17 also "
"B<LINUX_REBOOT_MAGIC2A> (that is, 85072278)  and since 2.1.97 also "
"B<LINUX_REBOOT_MAGIC2B> (that is, 369367448)  and since 2.5.71 also "
"B<LINUX_REBOOT_MAGIC2C> (that is, 537993216)  are permitted as values for "
"I<magic2>.  (The hexadecimal values of these constants are meaningful.)"
msgstr ""
"Dieser Systemaufruf schlägt (mit dem Fehler B<EINVAL>) fehl, außer wenn "
"I<Magik> gleich B<LINUX_REBOOT_MAGIC1> (0xfee1dead) und I<Magik2> gleich "
"B<LINUX_REBOOT_MAGIC2> (672274793) ist. Seit 2.1.17 ist jedoch auch "
"B<LINUX_REBOOT_MAGIC2A> (85072278) und seit 2.1.97 außerdem "
"B<LINUX_REBOOT_MAGIC2B> (369367448) und seit 2.5.71 auch "
"B<LINUX_REBOOT_MAGIC2C> (537993216) als Wert für I<Magik2> erlaubt. (Die "
"hexadezimalen Werte dieser Konstanten haben eine Bedeutung.)"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"(0xa1b2c3d4; since Linux 2.1.30).  The message \"Restarting system with "
"command \\(aq%s\\(aq\" is printed, and a restart (using the command string "
"given in I<arg>)  is performed immediately.  If not preceded by a "
"B<sync>(2), data will be lost."
msgstr ""
"(0xa1b2c3d4; seit Linux 2.1.30). Die Nachricht »Restarting system with "
"command \\(aq%s\\(aq« wird ausgegeben und sofort ein Neustart ausgeführt "
"(unter Verwendung der in I<Arg> angegebenen Zeichenkette). Wenn kein "
"B<sync>(2) vorausgeht, werden Daten verlorengehen."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"For the values of I<cmd> that stop or restart the system, a successful call "
"to B<reboot>()  does not return.  For the other I<cmd> values, zero is "
"returned on success.  In all cases, -1 is returned on failure, and I<errno> "
"is set appropriately."
msgstr ""
"Für die Werte von I<Befehl>, die das System stoppen oder neu starten, gibt "
"ein erfolgreicher Aufruf von B<reboot>() nichts zurück. Für die anderen "
"I<Befehl>-Werte wird bei Erfolg Null zurückgegeben. Bei einem Fehler wird "
"immer -1 zurückgegeben und I<errno> entsprechend gesetzt."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
