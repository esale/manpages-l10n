# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-07-25 19:35+0200\n"
"PO-Revision-Date: 2022-07-23 16:22+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-EDITENV"
msgstr "GRUB-EDITENV"

#. type: TH
#: archlinux
#, no-wrap
msgid "July 2023"
msgstr "Јула 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.12rc1-1"
msgstr "ГРУБ 2:2.12rc1-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
#, fuzzy
#| msgid "Tool to edit environment block."
msgid "grub-editenv - edit GRUB environment block"
msgstr "Алат за уређивање блока окружења."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-editenv> [I<\\,OPTION\\/>...] I<\\,FILENAME COMMAND\\/>"
msgstr "B<grub-editenv> [I<\\,ОПЦИЈА\\/>...] I<\\,ДАТОТЕКА НАРЕДБА\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Tool to edit environment block."
msgstr "Алат за уређивање блока окружења."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Commands:"
msgstr "Наредбе:"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "create"
msgstr "create"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Create a blank environment block file."
msgstr "Прави празну датотеку блока окружења."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "list"
msgstr "list"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "List the current variables."
msgstr "Исписује текуће променљиве."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "set [NAME=VALUE ...]"
msgstr "set [НАЗИВ=ВРЕДНОСТ ...]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set variables."
msgstr "Поставља променљиве."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "unset [NAME ...]"
msgstr "unset [НАЗИВ ...]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Delete variables."
msgstr "Брише променљиве."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Options:"
msgstr "Опције:"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "приказује овај списак помоћи"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "приказује кратку поруку коришћења"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "исписује опширне поруке."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "исписује издање програма"

#. type: Plain text
#: archlinux
msgid "If FILENAME is `-', the default value //boot/grub/grubenv is used."
msgstr "Ако је ДАТОТЕКА `-', користи се основна вредност //boot/grub/grubenv."

#. type: Plain text
#: archlinux
msgid ""
"There is no `delete' command; if you want to delete the whole environment "
"block, use `rm //boot/grub/grubenv'."
msgstr ""
"Нема наредбе „delete“; ако желите да обришете читав блок окружења, користите "
"„rm //boot/grub/grubenv“."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Грешке пријавите на: E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-reboot>(8), B<grub-set-default>(8)"
msgstr "B<grub-reboot>(8), B<grub-set-default>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-editenv> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-editenv> programs are properly installed "
"at your site, the command"
msgstr ""
"Потпуна документација за B<grub-editenv> је одржавана као Тексинфо "
"упутство.  Ако су B<info> и B<grub-editenv> исправно инсталирани на вашем "
"сајту, наредба"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-editenv>"
msgstr "B<info grub-editenv>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "треба да вам да приступ потпуном упутству."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "April 2023"
msgstr "Априла 2023"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "ГРУБ 2.06-13"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"If FILENAME is `-', the default value I<\\,/boot/grub/grubenv\\/> is used."
msgstr ""
"Ако је ДАТОТЕКА `-', користи се основна вредност I<\\,/boot/grub/grubenv\\/>."

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"There is no `delete' command; if you want to delete the whole environment "
"block, use `rm /boot/grub/grubenv'."
msgstr ""
"Нема наредбе „delete“; ако желите да обришете читав блок окружења, користите "
"„rm /boot/grub/grubenv“."
