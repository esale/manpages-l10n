# Create header, using MET/MEST
timestamp=$(TZ='Europe/Moscow' date "+%d %m %Y, %H:%M")

# Name of Columns
cname_name="Название"
cname_percent="Процент"
cname_missing_strings="Переведено менее 80%"
cname_statistics="Статистика"
cname_packet="Пакет"
cname_manpages="Справочные страницы"
cname_date="Дата"

cname_onepageuntranslated="1 файл переведен не полностью."
cname_severalpagesuntranslated="файла(ов) переведены не полностью."

cname_intotal1="Всего "
cname_intotal2=" файла(ов) не переведены."

cname_webtitle="Перевод страниц руководства на русский язык"
cname_overviewlink="Обзор по русскому языку"
# For overview of (untranslated) pages
cname_nottranslated="Список файлов, которые не переведены на русский язык"
cname_explanation="Следующие страницы переведены на другой язык или языки, а на русский — нет"
