# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:20+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "bcmp"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "bcmp - compare byte sequences"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>strings.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int bcmp(const void >I<s1>B<[.>I<n>B<], const void >I<s2>B<[.>I<n>B<], size_t >I<n>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<bcmp>()  is identical to B<memcmp>(3); use it instead."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "4.3BSD.  Marked as LEGACY in POSIX.1-2001; removed in POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<memcmp>(3)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-01-07"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"4.3BSD.  This function is deprecated (marked as LEGACY in POSIX.1-2001); "
"POSIX.1-2008 removes the specification of B<bcmp>()."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "BCMP"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-03-13"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<int bcmp(const void *>I<s1>B<, const void *>I<s2>B<, size_t >I<n>B<);>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<bcmp>()  function compares the two byte sequences I<s1> and I<s2> of "
"length I<n> each.  If they are equal, and in particular if I<n> is zero, "
"B<bcmp>()  returns 0.  Otherwise, it returns a nonzero result."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<bcmp>()  function returns 0 if the byte sequences are equal, otherwise "
"a nonzero result is returned."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid "B<bcmp>()"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"4.3BSD.  This function is deprecated (marked as LEGACY in POSIX.1-2001): use "
"B<memcmp>(3)  in new programs.  POSIX.1-2008 removes the specification of "
"B<bcmp>()."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<bstring>(3), B<memcmp>(3), B<strcasecmp>(3), B<strcmp>(3), B<strcoll>(3), "
"B<strncasecmp>(3), B<strncmp>(3)"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
