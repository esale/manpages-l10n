# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "remquo"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "remquo, remquof, remquol - remainder and part of quotient"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double remquo(double >I<x>B<, double >I<y>B<, int *>I<quo>B<);>\n"
"B<float remquof(float >I<x>B<, float >I<y>B<, int *>I<quo>B<);>\n"
"B<long double remquol(long double >I<x>B<, long double >I<y>B<, int *>I<quo>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<remquo>(), B<remquof>(), B<remquol>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions compute the remainder and part of the quotient upon division "
"of I<x> by I<y>.  A few bits of the quotient are stored via the I<quo> "
"pointer.  The remainder is returned as the function result."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The value of the remainder is the same as that computed by the "
"B<remainder>(3)  function."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The value stored via the I<quo> pointer has the sign of I<x\\~/\\~y> and "
"agrees with the quotient in at least the low order 3 bits."
msgstr ""

#
#.  A possible application of this function might be the computation
#.  of sin(x). Compute remquo(x, pi/2, &quo) or so.
#.  glibc, UnixWare: return 3 bits
#.  MacOS 10: return 7 bits
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For example, I<remquo(29.0,\\ 3.0)> returns -1.0 and might store 2.  Note "
"that the actual quotient might not fit in an integer."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On success, these functions return the same value as the analogous functions "
"described in B<remainder>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If I<x> or I<y> is a NaN, a NaN is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<x> is an infinity, and I<y> is not a NaN, a domain error occurs, and a "
"NaN is returned."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<y> is zero, and I<x> is not a NaN, a domain error occurs, and a NaN is "
"returned."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"See B<math_error>(7)  for information on how to determine whether an error "
"has occurred when calling these functions."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following errors can occur:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Domain error: I<x> is an infinity or I<y> is 0, and the other argument is not a NaN"
msgstr ""

#.  .I errno
#.  is set to
#.  .BR EDOM .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "An invalid floating-point exception (B<FE_INVALID>)  is raised."
msgstr ""

#. #-#-#-#-#  archlinux: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  debian-unstable: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  fedora-38: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: http://sources.redhat.com/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: remquo.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . Is it intentional that these functions do not set errno?
#.  Bug raised: https://www.sourceware.org/bugzilla/show_bug.cgi?id=6802
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These functions do not set I<errno>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<remquo>(),\n"
"B<remquof>(),\n"
"B<remquol>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.1.  C99, POSIX.1-2001."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<fmod>(3), B<logb>(3), B<remainder>(3)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "These functions were added in glibc 2.1."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "REMQUO"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Link with I<-lm>."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The value stored via the I<quo> pointer has the sign of I<x\\ /\\ y> and "
"agrees with the quotient in at least the low order 3 bits."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "These functions first appeared in glibc in version 2.1."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
