# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:51+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SLIST"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. SLIST_FOREACH_FROM,
#. SLIST_FOREACH_FROM_SAFE,
#. SLIST_FOREACH_SAFE,
#. SLIST_REMOVE_AFTER,
#. SLIST_SWAP
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"SLIST_EMPTY, SLIST_ENTRY, SLIST_FIRST, SLIST_FOREACH, SLIST_HEAD, "
"SLIST_HEAD_INITIALIZER, SLIST_INIT, SLIST_INSERT_AFTER, SLIST_INSERT_HEAD, "
"SLIST_NEXT, SLIST_REMOVE, SLIST_REMOVE_HEAD - implementation of a singly "
"linked list"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/queue.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<SLIST_ENTRY(TYPE);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<SLIST_HEAD(HEADNAME, TYPE);>\n"
"B<SLIST_HEAD SLIST_HEAD_INITIALIZER(SLIST_HEAD >I<head>B<);>\n"
"B<void SLIST_INIT(SLIST_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int SLIST_EMPTY(SLIST_HEAD *>I<head>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void SLIST_INSERT_HEAD(SLIST_HEAD *>I<head>B<,>\n"
"B<                        struct TYPE *>I<elm>B<, SLIST_ENTRY >I<NAME>B<);>\n"
"B<void SLIST_INSERT_AFTER(struct TYPE *>I<listelm>B<,>\n"
"B<                        struct TYPE *>I<elm>B<, SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<struct TYPE *SLIST_FIRST(SLIST_HEAD *>I<head>B<);>\n"
"B<struct TYPE *SLIST_NEXT(struct TYPE *>I<elm>B<, SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .BI "SLIST_FOREACH_FROM(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME );
#.  .PP
#.  .BI "SLIST_FOREACH_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#.  .BI "SLIST_FOREACH_FROM_SAFE(struct TYPE *" var ", SLIST_HEAD *" head ,
#.  .BI "                        SLIST_ENTRY " NAME ", struct TYPE *" temp_var );
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<SLIST_FOREACH(struct TYPE *>I<var>B<, SLIST_HEAD *>I<head>B<, SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#.  .BI "void SLIST_REMOVE_AFTER(struct TYPE *" elm ,
#.  .BI "                        SLIST_ENTRY " NAME );
#.  .PP
#.  .BI "void SLIST_SWAP(SLIST_HEAD *" head1 ", SLIST_HEAD *" head2 ,
#.  .BI "                        SLIST_ENTRY " NAME );
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void SLIST_REMOVE(SLIST_HEAD *>I<head>B<, struct TYPE *>I<elm>B<,>\n"
"B<                        SLIST_ENTRY >I<NAME>B<);>\n"
"B<void SLIST_REMOVE_HEAD(SLIST_HEAD *>I<head>B<,>\n"
"B<                        SLIST_ENTRY >I<NAME>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "These macros define and operate on doubly linked lists."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In the macro definitions, I<TYPE> is the name of a user-defined structure, "
"that must contain a field of type I<SLIST_ENTRY>, named I<NAME>.  The "
"argument I<HEADNAME> is the name of a user-defined structure that must be "
"declared using the macro B<SLIST_HEAD>()."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Creation"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"A singly linked list is headed by a structure defined by the "
"B<SLIST_HEAD>()  macro.  This structure contains a single pointer to the "
"first element on the list.  The elements are singly linked for minimum space "
"and pointer manipulation overhead at the expense of O(n) removal for "
"arbitrary elements.  New elements can be added to the list after an existing "
"element or at the head of the list.  An I<SLIST_HEAD> structure is declared "
"as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SLIST_HEAD(HEADNAME, TYPE) head;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"where I<struct HEADNAME> is the structure to be defined, and I<struct TYPE> "
"is the type of the elements to be linked into the list.  A pointer to the "
"head of the list can later be declared as:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "struct HEADNAME *headp;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "(The names I<head> and I<headp> are user selectable.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_ENTRY>()  declares a structure that connects the elements in the "
"list."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_HEAD_INITIALIZER>()  evaluates to an initializer for the list "
"I<head>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<SLIST_INIT>()  initializes the list referenced by I<head>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_EMPTY>()  evaluates to true if there are no elements in the list."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Insertion"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_INSERT_HEAD>()  inserts the new element I<elm> at the head of the "
"list."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_INSERT_AFTER>()  inserts the new element I<elm> after the element "
"I<listelm>."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Traversal"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_FIRST>()  returns the first element in the list, or NULL if the list "
"is empty."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<SLIST_NEXT>()  returns the next element in the list."
msgstr ""

#.  .PP
#.  .BR SLIST_FOREACH_FROM ()
#.  behaves identically to
#.  .BR SLIST_FOREACH ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found SLIST element and begins the loop at
#.  .I var
#.  instead of the first element in the SLIST referenced by
#.  .IR head .
#.  .Pp
#.  .BR SLIST_FOREACH_SAFE ()
#.  traverses the list referenced by
#.  .I head
#.  in the forward direction, assigning each element in
#.  turn to
#.  .IR var .
#.  However, unlike
#.  .BR SLIST_FOREACH ()
#.  here it is permitted to both remove
#.  .I var
#.  as well as free it from within the loop safely without interfering with the
#.  traversal.
#.  .PP
#.  .BR SLIST_FOREACH_FROM_SAFE ()
#.  behaves identically to
#.  .BR SLIST_FOREACH_SAFE ()
#.  when
#.  .I var
#.  is NULL, else it treats
#.  .I var
#.  as a previously found SLIST element and begins the loop at
#.  .I var
#.  instead of the first element in the SLIST referenced by
#.  .IR head .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_FOREACH>()  traverses the list referenced by I<head> in the forward "
"direction, assigning each element in turn to I<var>."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Removal"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<SLIST_REMOVE>()  removes the element I<elm> from the list."
msgstr ""

#.  .PP
#.  .BR SLIST_REMOVE_AFTER ()
#.  removes the element after
#.  .I elm
#.  from the list.
#.  Unlike
#.  .IR SLIST_REMOVE ,
#.  this macro does not traverse the entire list.
#.  .SS Other features
#.  .BR SLIST_SWAP ()
#.  swaps the contents of
#.  .I head1
#.  and
#.  .IR head2 .
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_REMOVE_HEAD>()  removes the element I<elm> from the head of the "
"list.  For optimum efficiency, elements being removed from the head of the "
"list should explicitly use this macro instead of the generic "
"B<SLIST_REMOVE>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_EMPTY>()  returns nonzero if the list is empty, and zero if the list "
"contains at least one entry."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_FIRST>(), and B<SLIST_NEXT>()  return a pointer to the first or next "
"I<TYPE> structure, respectively."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_HEAD_INITIALIZER>()  returns an initializer that can be assigned to "
"the list I<head>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "BSD."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "4.4BSD."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<SLIST_FOREACH>()  doesn't allow I<var> to be removed or freed within the "
"loop, as it would interfere with the traversal.  B<SLIST_FOREACH_SAFE>(), "
"which is present on the BSDs but is not present in glibc, fixes this "
"limitation by allowing I<var> to safely be removed from the list and freed "
"from within the loop without interfering with the traversal."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stddef.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>sys/queue.hE<gt>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"struct entry {\n"
"    int data;\n"
"    SLIST_ENTRY(entry) entries;             /* Singly linked list */\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SLIST_HEAD(slisthead, entry);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    struct entry *n1, *n2, *n3, *np;\n"
"    struct slisthead head;                  /* Singly linked list\n"
"                                               head */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    SLIST_INIT(&head);                      /* Initialize the queue */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    n1 = malloc(sizeof(struct entry));      /* Insert at the head */\n"
"    SLIST_INSERT_HEAD(&head, n1, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    n2 = malloc(sizeof(struct entry));      /* Insert after */\n"
"    SLIST_INSERT_AFTER(n1, n2, entries);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    SLIST_REMOVE(&head, n2, entry, entries);/* Deletion */\n"
"    free(n2);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    n3 = SLIST_FIRST(&head);\n"
"    SLIST_REMOVE_HEAD(&head, entries);      /* Deletion from the head */\n"
"    free(n3);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    for (unsigned int i = 0; i E<lt> 5; i++) {\n"
"        n1 = malloc(sizeof(struct entry));\n"
"        SLIST_INSERT_HEAD(&head, n1, entries);\n"
"        n1-E<gt>data = i;\n"
"    }\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"                                            /* Forward traversal */\n"
"    SLIST_FOREACH(np, &head, entries)\n"
"        printf(\"%i\\en\", np-E<gt>data);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    while (!SLIST_EMPTY(&head)) {           /* List deletion */\n"
"        n1 = SLIST_FIRST(&head);\n"
"        SLIST_REMOVE_HEAD(&head, entries);\n"
"        free(n1);\n"
"    }\n"
"    SLIST_INIT(&head);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<insque>(3), B<queue>(7)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"Not in POSIX.1, POSIX.1-2001, or POSIX.1-2008.  Present on the BSDs (SLIST "
"macros first appeared in 4.4BSD)."
msgstr ""
