# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:43+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "MKFS.CRAMFS"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "mkfs.cramfs - make compressed ROM file system"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<mkfs.cramfs> [options] I<directory file>"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Files on cramfs file systems are zlib-compressed one page at a time to allow "
"random read access. The metadata is not compressed, but is expressed in a "
"terse representation that is more space-efficient than conventional file "
"systems."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The file system is intentionally read-only to simplify its design; random "
"write access for compressed files is difficult to implement. cramfs ships "
"with a utility (B<mkcramfs>(8)) to pack files into new cramfs images."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "File sizes are limited to less than 16 MB."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Maximum file system size is a little under 272 MB. (The last file on the "
"file system must begin before the 256 MB block, but can extend past it.)"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "ARGUMENTS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The I<directory> is simply the root of the directory tree that we want to "
"generate a compressed filesystem out of."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The I<file> will contain the cram file system, which later can be mounted."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-v>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Enable verbose messaging."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-E>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Treat all warnings as errors, which are reflected as command exit status."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-b> I<blocksize>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Use defined block size, which has to be divisible by page size."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-e> I<edition>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Use defined file system edition number in superblock."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-N> I<big, little, host>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Use defined endianness. Value defaults to I<host>."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-i> I<file>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Insert a I<file> to cramfs file system."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-n> I<name>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Set name of the cramfs file system."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-p>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Pad by 512 bytes for boot code."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-s>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"This option is ignored. Originally the B<-s> turned on directory entry "
"sorting."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-z>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Make explicit holes."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<0>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "success"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<8>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "operation error, such as unable to allocate memory"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<fsck.cramfs>(8), B<mount>(8)"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<mkfs.cramfs> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr ""
