# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 20:01+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SYSTEMD-SYSEXT"
msgstr ""

#. type: TH
#: archlinux fedora-38 mageia-cauldron
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "systemd-sysext"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"systemd-sysext, systemd-sysext.service - Activates System Extension Images"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "B<systemd-sysext> [OPTIONS...] COMMAND"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "systemd-sysext\\&.service\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"B<systemd-sysext> activates/deactivates system extension images\\&. System "
"extension images may \\(en dynamically at runtime \\(em extend the /usr/ "
"and /opt/ directory hierarchies with additional files\\&. This is "
"particularly useful on immutable system images where a /usr/ and/or /opt/ "
"hierarchy residing on a read-only file system shall be extended temporarily "
"at runtime without making any persistent modifications\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"System extension images should contain files and directories similar in "
"fashion to regular operating system tree\\&. When one or more system "
"extension images are activated, their /usr/ and /opt/ hierarchies are "
"combined via \"overlayfs\" with the same hierarchies of the host OS, and the "
"host /usr/ and /opt/ overmounted with it (\"merging\")\\&. When they are "
"deactivated, the mount point is disassembled \\(em again revealing the "
"unmodified original host version of the hierarchy (\"unmerging\")\\&. "
"Merging thus makes the extension\\*(Aqs resources suddenly appear below the /"
"usr/ and /opt/ hierarchies as if they were included in the base OS image "
"itself\\&. Unmerging makes them disappear again, leaving in place only the "
"files that were shipped with the base OS image itself\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"Files and directories contained in the extension images outside of the /usr/ "
"and /opt/ hierarchies are I<not> merged, and hence have no effect when "
"included in a system extension image\\&. In particular, files in the /etc/ "
"and /var/ included in a system extension image will I<not> appear in the "
"respective hierarchies after activation\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"System extension images are strictly read-only, and the host /usr/ and /opt/ "
"hierarchies become read-only too while they are activated\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"System extensions are supposed to be purely additive, i\\&.e\\&. they are "
"supposed to include only files that do not exist in the underlying basic OS "
"image\\&. However, the underlying mechanism (overlayfs) also allows "
"overlaying or removing files, but it is recommended not to make use of "
"this\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "System extension images may be provided in the following formats:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "Plain directories or btrfs subvolumes containing the OS tree"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"Disk images with a GPT disk label, following the \\m[blue]B<Discoverable "
"Partitions Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Disk images lacking a partition table, with a naked Linux file system (e\\&."
"g\\&. erofs, squashfs or ext4)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"These image formats are the same ones that B<systemd-nspawn>(1)  supports "
"via its B<--directory=>/B<--image=> switches and those that the service "
"manager supports via B<RootDirectory=>/B<RootImage=>\\&. Similar to them "
"they may optionally carry Verity authentication information\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"System extensions are automatically looked for in the directories /etc/"
"extensions/, /run/extensions/, /var/lib/extensions/, /usr/lib/extensions/ "
"and /usr/local/lib/extensions/\\&. The first two listed directories are not "
"suitable for carrying large binary images, however are still useful for "
"carrying symlinks to them\\&. The primary place for installing system "
"extensions is /var/lib/extensions/\\&. Any directories found in these search "
"directories are considered directory based extension images, any files with "
"the \\&.raw suffix are considered disk image based extension images\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"During boot OS extension images are activated automatically, if the systemd-"
"sysext\\&.service is enabled\\&. Note that this service runs only after the "
"underlying file systems where system extensions may be located have been "
"mounted\\&. This means they are not suitable for shipping resources that are "
"processed by subsystems running in earliest boot\\&. Specifically, OS "
"extension images are not suitable for shipping system services or B<systemd-"
"sysusers>(8)  definitions\\&. See the \\m[blue]B<Portable Services "
"Documentation>\\m[]\\&\\s-2\\u[2]\\d\\s+2 for a simple mechanism for "
"shipping system services in disk images, in a similar fashion to OS "
"extensions\\&. Note the different isolation on these two mechanisms: while "
"system extension directly extend the underlying OS image with additional "
"files that appear in a way very similar to as if they were shipped in the OS "
"image itself and thus imply no security isolation, portable services imply "
"service level sandboxing in one way or another\\&. The systemd-sysext\\&."
"service service is guaranteed to finish start-up before basic\\&.target is "
"reached; i\\&.e\\&. at the time regular services initialize (those which do "
"not use I<DefaultDependencies=no>), the files and directories system "
"extensions provide are available in /usr/ and /opt/ and may be accessed\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Note that there is no concept of enabling/disabling installed system "
"extension images: all installed extension images are automatically activated "
"at boot\\&. However, you can place an empty directory named like the "
"extension (no \\&.raw) in /etc/extensions/ to \"mask\" an extension with the "
"same name in a system folder with lower precedence\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"A simple mechanism for version compatibility is enforced: a system extension "
"image must carry a /usr/lib/extension-release\\&.d/extension-release\\&."
"I<$name> file, which must match its image name, that is compared with the "
"host os-release file: the contained I<ID=> fields have to match unless "
"\"_any\" is set for the extension\\&. If the extension I<ID=> is not "
"\"_any\", the I<SYSEXT_LEVEL=> field (if defined) has to match\\&. If the "
"latter is not defined, the I<VERSION_ID=> field has to match instead\\&. If "
"the extension defines the I<ARCHITECTURE=> field and the value is not "
"\"_any\" it has to match the kernel\\*(Aqs architecture reported by "
"B<uname>(2)  but the used architecture identifiers are the same as for "
"I<ConditionArchitecture=> described in B<systemd.unit>(5)\\&. System "
"extensions should not ship a /usr/lib/os-release file (as that would be "
"merged into the host /usr/ tree, overriding the host OS version data, which "
"is not desirable)\\&. The extension-release file follows the same format and "
"semantics, and carries the same content, as the os-release file of the OS, "
"but it describes the resources carried in the extension image\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "USES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"The primary use case for system images are immutable environments where "
"debugging and development tools shall optionally be made available, but not "
"included in the immutable base OS image itself (e\\&.g\\&.  B<strace>(1)  "
"and B<gdb>(1)  shall be an optionally installable addition in order to make "
"debugging/development easier)\\&. System extension images should not be "
"misunderstood as a generic software packaging framework, as no dependency "
"scheme is available: system extensions should carry all files they need "
"themselves, except for those already shipped in the underlying host system "
"image\\&. Typically, system extension images are built at the same time as "
"the base OS image \\(em within the same build system\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"Another use case for the system extension concept is temporarily overriding "
"OS supplied resources with newer ones, for example to install a locally "
"compiled development version of some low-level component over the immutable "
"OS image without doing a full OS rebuild or modifying the nominally "
"immutable image\\&. (e\\&.g\\&. \"install\" a locally built package with "
"B<DESTDIR=/var/lib/extensions/mytest make install && systemd-sysext "
"refresh>, making it available in /usr/ as if it was installed in the OS "
"image itself\\&.) This case works regardless if the underlying host /usr/ is "
"managed as immutable disk image or is a traditional package manager "
"controlled (i\\&.e\\&. writable) tree\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "COMMANDS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "The following commands are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<status>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"When invoked without any command verb, or when B<status> is specified the "
"current merge status is shown, separately for both /usr/ and /opt/\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<merge>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Merges all currently installed system extension images into /usr/ and /opt/, "
"by overmounting these hierarchies with an \"overlayfs\" file system "
"combining the underlying hierarchies with those included in the extension "
"images\\&. This command will fail if the hierarchies are already merged\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<unmerge>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Unmerges all currently installed system extension images from /usr/ and /"
"opt/, by unmounting the \"overlayfs\" file systems created by B<merge> "
"prior\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<refresh>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"A combination of B<unmerge> and B<merge>: if already mounted the existing "
"\"overlayfs\" instance is unmounted temporarily, and then replaced by a new "
"version\\&. This command is useful after installing/removing system "
"extension images, in order to update the \"overlayfs\" file system "
"accordingly\\&. If no system extensions are installed when this command is "
"executed, the equivalent of B<unmerge> is executed, without establishing any "
"new \"overlayfs\" instance\\&. Note that currently there\\*(Aqs a brief "
"moment where neither the old nor the new \"overlayfs\" file system is "
"mounted\\&. This implies that all resources supplied by a system extension "
"will briefly disappear \\(em even if it exists continuously during the "
"refresh operation\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<list>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "A brief list of installed extension images is shown\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<--root=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Operate relative to the specified root directory, i\\&.e\\&. establish the "
"\"overlayfs\" mount not on the top-level host /usr/ and /opt/ hierarchies, "
"but below some specified root directory\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<--force>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"When merging system extensions into /usr/ and /opt/, ignore version "
"incompatibilities, i\\&.e\\&. force merging regardless of whether the "
"version information included in the extension images matches the host or "
"not\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<--no-pager>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "Do not pipe output into a pager\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<--no-legend>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"Do not print the legend, i\\&.e\\&. column headers and the footer with "
"hints\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "B<--json=>I<MODE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid ""
"Shows output formatted as JSON\\&. Expects one of \"short\" (for the "
"shortest possible output without any redundant whitespace or line breaks), "
"\"pretty\" (for a pretty version of the same, with indentation and line "
"breaks) or \"off\" (to turn off JSON output, the default)\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "On success, 0 is returned\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<systemd>(1), B<systemd-nspawn>(1)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "Discoverable Partitions Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"\\%https://uapi-group.org/specifications/specs/"
"discoverable_partitions_specification"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid " 2."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Portable Services Documentation"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5
msgid "\\%https://systemd.io/PORTABLE_SERVICES"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid ""
"Disk images lacking a partition table, with a naked Linux file system (e\\&."
"g\\&. squashfs or ext4)"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"During boot OS extension images are activated automatically, if the systemd-"
"sysext\\&.service is enabled\\&. Note that this service runs only after the "
"underlying file systems where system extensions may be located have been "
"mounted\\&. This means they are not suitable for shipping resources that are "
"processed by subsystems running in earliest boot\\&. Specifically, OS "
"extension images are not suitable for shipping system services or B<systemd-"
"sysusers>(8)  definitions\\&. See \\m[blue]B<Portable "
"Services>\\m[]\\&\\s-2\\u[2]\\d\\s+2 for a simple mechanism for shipping "
"system services in disk images, in a similar fashion to OS extensions\\&. "
"Note the different isolation on these two mechanisms: while system extension "
"directly extend the underlying OS image with additional files that appear in "
"a way very similar to as if they were shipped in the OS image itself and "
"thus imply no security isolation, portable services imply service level "
"sandboxing in one way or another\\&. The systemd-sysext\\&.service service "
"is guaranteed to finish start-up before basic\\&.target is reached; i\\&."
"e\\&. at the time regular services initialize (those which do not use "
"I<DefaultDependencies=no>), the files and directories system extensions "
"provide are available in /usr/ and /opt/ and may be accessed\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "\\%https://systemd.io/DISCOVERABLE_PARTITIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "Portable Services"
msgstr ""

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd 254"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"systemd-sysext, systemd-sysext.service, systemd-confext, systemd-confext."
"service - Activates System Extension Images"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid "B<systemd-confext> [OPTIONS...] COMMAND"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "systemd-confext\\&.service\n"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"System extensions are searched for in the directories /etc/extensions/, /run/"
"extensions/ and /var/lib/extensions/\\&. The first two listed directories "
"are not suitable for carrying large binary images, however are still useful "
"for carrying symlinks to them\\&. The primary place for installing system "
"extensions is /var/lib/extensions/\\&. Any directories found in these search "
"directories are considered directory based extension images; any files with "
"the \\&.raw suffix are considered disk image based extension images\\&. When "
"invoked in the initrd, the additional directory /\\&.extra/sysext/ is "
"included in the directories that are searched for extension images\\&. Note "
"however, that by default a tighter image policy applies to images found "
"there, though, see below\\&. This directory is populated by B<systemd-"
"stub>(7)  with extension images found in the system\\*(Aqs EFI System "
"Partition\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"The B<systemd-confext> concept follows the same principle as the B<systemd-"
"sysext>(1)  functionality but instead of working on /usr and /opt, "
"B<confext> will extend only /etc\\&. Files and directories contained in the "
"confext images outside of the /etc/ hierarchy are I<not> merged, and hence "
"have no effect when included in the image\\&. Formats for these images are "
"of the same as sysext images\\&. The merged hierarchy will be mounted with "
"\"nosuid\" and (if not disabled via B<--noexec=false>)  \"noexec\"\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"Confexts are looked for in the directories /run/confexts/, /var/lib/"
"confexts/, /usr/lib/confexts/ and /usr/local/lib/confexts/\\&. The first "
"listed directory is not suitable for carrying large binary images, however "
"is still useful for carrying symlinks to them\\&. The primary place for "
"installing configuration extensions is /var/lib/confexts/\\&. Any "
"directories found in these search directories are considered directory based "
"confext images; any files with the \\&.raw suffix are considered disk image "
"based confext images\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"Again, just like sysext images, the confext images will contain a /etc/"
"extension-release\\&.d/extension-release\\&.I<$name> file, which must match "
"the image name (with the usual escape hatch of xattr), and again with "
"content being one or more of I<ID=>, I<VERSION_ID=>, and "
"I<CONFEXT_LEVEL>\\&. Confext images will then be checked and matched against "
"the base OS layer\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"For the confext case, the OSConfig project aims to perform runtime "
"reconfiguration of OS services\\&. Sometimes, there is a need to swap "
"certain configuration parameter values or restart only a specific service "
"without deployment of new code or a complete OS deployment\\&. In other "
"words, we want to be able to tie the most frequently configured options to "
"runtime updateable flags that can be changed without a system reboot\\&. "
"This will help reduce servicing times when there is a need for changing the "
"OS configuration\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"The following commands are understood by both the sysext and confext "
"concepts:"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"When invoked without any command verb, or when B<status> is specified the "
"current merge status is shown, separately (for both /usr/ and /opt/ of "
"sysext and for /etc/ of confext)\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"Merges all currently installed system extension images into /usr/ and /opt/, "
"by overmounting these hierarchies with an \"overlayfs\" file system "
"combining the underlying hierarchies with those included in the extension "
"images\\&. This command will fail if the hierarchies are already merged\\&. "
"For confext, the merge happens into the /etc/ directory instead\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"Unmerges all currently installed system extension images from /usr/ and /"
"opt/ for sysext and /etc/, for confext, by unmounting the \"overlayfs\" file "
"systems created by B<merge> prior\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"Operate relative to the specified root directory, i\\&.e\\&. establish the "
"\"overlayfs\" mount not on the top-level host /usr/ and /opt/ hierarchies "
"for sysext or /etc/ for confext, but below some specified root directory\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"When merging system extensions into /usr/ and /opt/ for sysext and /etc/ for "
"confext, ignore version incompatibilities, i\\&.e\\&. force merging "
"regardless of whether the version information included in the images matches "
"the host or not\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid "B<--image-policy=>I<policy>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"Takes an image policy string as argument, as per B<systemd.image-"
"policy>(7)\\&. The policy is enforced when operating on system extension "
"disk images\\&. If not specified defaults to "
"\"root=verity+signed+encrypted+unprotected+absent:"
"usr=verity+signed+encrypted+unprotected+absent\" for system extensions, i\\&."
"e\\&. only the root and /usr/ file systems in the image are used\\&. For "
"configuration extensions defaults to "
"\"root=verity+signed+encrypted+unprotected+absent\"\\&. When run in the "
"initrd and operating on a system extension image stored in the /\\&.extra/"
"sysext/ directory a slightly stricter policy is used by default: "
"\"root=signed+absent:usr=signed+absent\", see above for details\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid "B<--noexec=>I<BOOL>"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid ""
"When merging configuration extensions into /etc/ the \"MS_NOEXEC\" mount "
"flag is used by default\\&. This option can be used to disable it\\&."
msgstr ""

#. type: Plain text
#: debian-unstable fedora-rawhide
msgid "B<systemd>(1), B<systemd-nspawn>(1), B<systemd-stub>(7)"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "systemd 249"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<systemd-sysext> [OPTIONS...]"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"System extensions are supposed to be purely additive, i\\&.e\\&. they are "
"supposed to include only files that do not exist in the underlying basic OS "
"image\\&. However, the underlying mechanism (overlayfs) also allows removing "
"files, but it is recommended not to make use of this\\&."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"These image formats are the same ones that B<systemd-nspawn>(1)  supports "
"via it\\*(Aqs B<--directory=>/B<--image=> switches and those that the "
"service manager supports via B<RootDirectory=>/B<RootImage=>\\&. Similar to "
"them they may optionally carry Verity authentication information\\&."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"During boot OS extension images are activated automatically, if the systemd-"
"sysext\\&.service is enabled\\&. Note that this service runs only after the "
"underlying file systems where system extensions are searched are mounted\\&. "
"This means they are not suitable for shipping resources that are processed "
"by subsystems running in earliest boot\\&. Specifically, OS extension images "
"are not suitable for shipping system services or B<systemd-sysusers>(8)  "
"definitions\\&. See \\m[blue]B<Portable Services>\\m[]\\&\\s-2\\u[2]\\d\\s+2 "
"for a simple mechanism for shipping system services in disk images, in a "
"similar fashion to OS extensions\\&. Note the different isolation on these "
"two mechanisms: while system extension directly extend the underlying OS "
"image with additional files that appear in a way very similar to as if they "
"were shipped in the OS image itself and thus imply no security isolation, "
"portable services imply service level sandboxing in one way or another\\&. "
"The systemd-sysext\\&.service service is guaranteed to finish start-up "
"before basic\\&.target is reached; i\\&.e\\&. at the time regular services "
"initialize (those which do not use I<DefaultDependencies=no>), the files and "
"directories system extensions provide are available in /usr/ and /opt/ and "
"may be accessed\\&."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Note that there is no concept of enabling/disabling installed system "
"extension images: all installed extension images are automatically activated "
"at boot\\&."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"A simple mechanism for version compatibility is enforced: a system extension "
"image must carry a /usr/lib/extension-release\\&.d/extension-release\\&."
"I<$name> file, which must match its image name, that is compared with the "
"host os-release file: the contained I<ID=> fields have to match, as well as "
"the I<SYSEXT_LEVEL=> field (if defined)\\&. If the latter is not defined, "
"the I<VERSION_ID=> field has to match instead\\&. System extensions should "
"not ship a /usr/lib/os-release file (as that would be merged into the host /"
"usr/ tree, overriding the host OS version data, which is not desirable)\\&. "
"The extension-release file follows the same format and semantics, and "
"carries the same content, as the os-release file of the OS, but it describes "
"the resources carried in the extension image\\&."
msgstr ""
