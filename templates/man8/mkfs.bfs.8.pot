# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:43+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "MKFS.BFS"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "mkfs.bfs - make an SCO bfs filesystem"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<mkfs.bfs> [options] I<device> [I<block-count>]"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid ""
"B<mkfs.bfs> creates an SCO bfs filesystem on a block device (usually a disk "
"partition or a file accessed via the loop device)."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid ""
"The I<block-count> parameter is the desired size of the filesystem, in "
"blocks. If nothing is specified, the entire partition will be used."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<-N>, B<--inodes> I<number>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid ""
"Specify the desired I<number> of inodes (at most 512). If nothing is "
"specified, some default number in the range 48-512 is picked depending on "
"the size of the partition."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<-V>, B<--vname> I<label>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "Specify the volume I<label>. I have no idea if/where this is used."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<-F>, B<--fname> I<name>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "Specify the filesystem I<name>. I have no idea if/where this is used."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "Explain what is being done."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<-c>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "This option is silently ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<-l>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"Print version and exit.  Option B<-V> only works as B<--version> when it is "
"the only option."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid ""
"The exit status returned by B<mkfs.bfs> is 0 when all went well, and 1 when "
"something went wrong."
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "B<mkfs>(8)"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid ""
"The B<mkfs.bfs> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Display version information and exit. Option B<-V> only works as B<--"
"version> when it is the only option."
msgstr ""
