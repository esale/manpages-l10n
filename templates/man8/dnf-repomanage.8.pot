# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:30+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DNF-REPOMANAGE"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Jan 22, 2023"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "4.3.1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "dnf-plugins-core"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "dnf-repomanage - DNF repomanage Plugin"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Manage a repository or a simple directory of rpm packages."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<dnf repomanage [E<lt>optional-optionsE<gt>] [E<lt>optionsE<gt>] "
"E<lt>pathE<gt>>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<repomanage> prints newest or older packages in a repository specified by "
"E<lt>pathE<gt> for easy piping to xargs or similar programs. In case "
"E<lt>pathE<gt> doesn\\(aqt contain a valid repodata, it is searched for rpm "
"packages which are then used instead.  If the repodata are present, "
"I<repomanage> uses them as the source of truth, it doesn\\(aqt verify that "
"they match the present rpm packages. In fact, I<repomanage> can run with "
"just the repodata, no rpm packages are needed."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In order to work correctly with modular packages, E<lt>pathE<gt> has to "
"contain repodata with modular metadata. If modular content is present, "
"I<repomanage> prints packages from newest or older stream versions in "
"addition to newest or older non-modular packages."
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Options"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"All general DNF options are accepted, see I<Options> in B<dnf(8)> for "
"details."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following options set what packages are displayed. These options are "
"mutually exclusive, i.e. only one can be specified. If no option is "
"specified, the newest packages are shown."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--old>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Show older packages (for a package or a stream show all versions except the "
"newest one)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--oldonly>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Show older packages (same as --old, but exclude the newest packages even "
"when it\\(aqs included in the older stream versions)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--new>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Show newest packages."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following options control how packages are displayed in the output:"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s, --space>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Print resulting set separated by space instead of newline."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-k E<lt>keep-numberE<gt>, --keep E<lt>keep-numberE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Limit the resulting set to newest B<E<lt>keep-numberE<gt>> packages."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Display newest packages in current repository (directory):"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"^\".ft C$\n"
"dnf repomanage --new .\n"
"^\".ft P$\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Display 2 newest versions of each package in \"home\" directory:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"^\".ft C$\n"
"dnf repomanage --new --keep 2 ~/\n"
"^\".ft P$\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Display oldest packages separated by space in current repository (directory):"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"^\".ft C$\n"
"dnf repomanage --old --space .\n"
"^\".ft P$\n"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide opensuse-tumbleweed
msgid "2023, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: fedora-38
#, no-wrap
msgid "Apr 05, 2023"
msgstr ""

#. type: TH
#: fedora-38
#, no-wrap
msgid "4.4.0"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "Jun 15, 2023"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "4.4.1"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Sep 26, 2022"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: mageia-cauldron
msgid "2014, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Nov 03, 2021"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "4.0.24"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: opensuse-leap-15-5
msgid "2021, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "Feb 24, 2023"
msgstr ""
