# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 20:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "UUIDD"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "2023-03-23"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "uuidd - UUID generation daemon"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<uuidd> [options]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"The B<uuidd> daemon is used by the UUID library to generate universally "
"unique identifiers (UUIDs), especially time-based UUIDs, in a secure and "
"guaranteed-unique fashion, even in the face of large numbers of threads "
"running on different CPUs trying to grab UUIDs."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<-C>, B<--cont-clock> I<opt_arg>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Activate continuous clock handling for time based UUIDs. B<uuidd> could use "
"all possible clock values, beginning with the daemon\\(cqs start time. The "
"optional argument can be used to set a value for the max_clock_offset. This "
"gurantees, that a clock value of a UUID will always be within the range of "
"the max_clock_offset. \\*(Aq-C\\*(Aq or \\*(Aq--cont-clock\\*(Aq enables the "
"feature with a default max_clock_offset of 2 hours. \\*(Aq-"
"CE<lt>NUME<gt>[hd]\\*(Aq or \\*(Aq--cont-clock=E<lt>NUME<gt>[hd]\\*(Aq "
"enables the feature with a max_clock_offset of NUM seconds. In case of an "
"appended h or d, the NUM value is read in hours or days. The minimum value "
"is 60 seconds, the maximum value is 365 days."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-d>, B<--debug>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"Run B<uuidd> in debugging mode. This prevents B<uuidd> from running as a "
"daemon."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-F>, B<--no-fork>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "Do not daemonize using a double-fork."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-k>, B<--kill>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "If currently a uuidd daemon is running, kill it."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-n>, B<--uuids> I<number>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"When issuing a test request to a running B<uuidd>, request a bulk response "
"of I<number> UUIDs."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-P>, B<--no-pid>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "Do not create a pid file."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-p>, B<--pid> I<path>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Specify the pathname where the pid file should be written. By default, the "
"pid file is written to I<{runstatedir}/uuidd/uuidd.pid>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-q>, B<--quiet>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "Suppress some failure messages."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-r>, B<--random>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Test uuidd by trying to connect to a running uuidd daemon and request it to "
"return a random-based UUID."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-S>, B<--socket-activation>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Do not create a socket but instead expect it to be provided by the calling "
"process. This implies B<--no-fork> and B<--no-pid>. This option is intended "
"to be used only with B<systemd>(1). It needs to be enabled with a configure "
"option."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-s>, B<--socket> I<path>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Make uuidd use this pathname for the unix-domain socket. By default, the "
"pathname used is I<{runstatedir}/uuidd/request>. This option is primarily "
"for debugging purposes, since the pathname is hard-coded in the B<libuuid> "
"library."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-T>, B<--timeout> I<number>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "Make B<uuidd> exit after I<number> seconds of inactivity."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-t>, B<--time>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Test B<uuidd> by trying to connect to a running uuidd daemon and request it "
"to return a time-based UUID."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "Start up a daemon, print 42 random keys, and then stop the daemon:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid ""
"uuidd -p /tmp/uuidd.pid -s /tmp/uuidd.socket\n"
"uuidd -d -r -n 42 -s /tmp/uuidd.socket\n"
"uuidd -d -k -s /tmp/uuidd.socket\n"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "The B<uuidd> daemon was written by"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<uuid>(3), B<uuidgen>(1)"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"The B<uuidd> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2023-07-09"
msgstr ""

#. type: TH
#: fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-08-04"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Run uuidd in debugging mode. This prevents uuidd from running as a daemon."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Output version information and exit."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display help screen and exit."
msgstr ""
