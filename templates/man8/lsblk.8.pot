# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:41+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "LSBLK"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "2022-08-04"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "lsblk - list block devices"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<lsblk> [options] [I<device>...]"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<lsblk> lists information about all available or the specified block "
"devices. The B<lsblk> command reads the B<sysfs> filesystem and B<udev db> "
"to gather information. If the udev db is not available or B<lsblk> is "
"compiled without udev support, then it tries to read LABELs, UUIDs and "
"filesystem types from the block device. In this case root permissions are "
"necessary."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The command prints all block devices (except RAM disks) in a tree-like "
"format by default. Use B<lsblk --help> to get a list of all available "
"columns."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The default output, as well as the default output from options like B<--fs> "
"and B<--topology>, is subject to change. So whenever possible, you should "
"avoid using default outputs in your scripts. Always explicitly define "
"expected columns by using B<--output> I<columns-list> and B<--list> in "
"environments where a stable output is required."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Note that B<lsblk> might be executed in time when B<udev> does not have all "
"information about recently added or modified devices yet. In this case it is "
"recommended to use B<udevadm settle> before B<lsblk> to synchronize with "
"udev."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The relationship between block devices and filesystems is not always one-to-"
"one. The filesystem may use more block devices, or the same filesystem may "
"be accessible by more paths. This is the reason why B<lsblk> provides "
"MOUNTPOINT and MOUNTPOINTS (pl.) columns. The column MOUNTPOINT displays "
"only one mount point (usually the last mounted instance of the filesystem), "
"and the column MOUNTPOINTS displays by multi-line cell all mount points "
"associated with the device."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<-A>, B<--noempty>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Don\\(cqt print empty devices."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-a>, B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"Disable all built-in filters and list all empty devices and RAM disk devices "
"too."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-b>, B<--bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-D>, B<--discard>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Print information about the discarding capabilities (TRIM, UNMAP) for each "
"device."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-d>, B<--nodeps>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Do not print holder devices or slaves. For example, B<lsblk --nodeps /dev/"
"sda> prints information about the sda device only."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-E>, B<--dedup> I<column>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Use I<column> as a de-duplication key to de-duplicate output tree. If the "
"key is not available for the device, or the device is a partition and "
"parental whole-disk device provides the same key than the device is always "
"printed."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The usual use case is to de-duplicate output on system multi-path devices, "
"for example by B<-E WWN>."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-e>, B<--exclude> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Exclude the devices specified by the comma-separated I<list> of major device "
"numbers. Note that RAM disks (major=1) are excluded by default if B<--all> "
"is not specified. The filter is applied to the top-level devices only. This "
"may be confusing for B<--list> output format where hierarchy of the devices "
"is not obvious."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-f>, B<--fs>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Output info about filesystems. This option is equivalent to B<-o NAME,FSTYPE,"
"FSVER,LABEL,UUID,FSAVAIL,FSUSE%,MOUNTPOINTS>. The authoritative information "
"about filesystems and raids is provided by the B<blkid>(8) command."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-I>, B<--include> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Include devices specified by the comma-separated I<list> of major device "
"numbers. The filter is applied to the top-level devices only. This may be "
"confusing for B<--list> output format where hierarchy of the devices is not "
"obvious."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-i>, B<--ascii>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Use ASCII characters for tree formatting."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Use JSON output format. It\\(cqs strongly recommended to use B<--output> and "
"also B<--tree> if necessary."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-l>, B<--list>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Produce output in the form of a list. The output does not provide "
"information about relationships between devices and since version 2.34 every "
"device is printed only once if B<--pairs> or B<--raw> not specified (the "
"parsable outputs are maintained in backwardly compatible way)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-M>, B<--merge>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Group parents of sub-trees to provide more readable output for RAIDs and "
"Multi-path devices. The tree-like output is required."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-m>, B<--perms>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Output info about device owner, group and mode. This option is equivalent to "
"B<-o NAME,SIZE,OWNER,GROUP,MODE>."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-n>, B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Do not print a header line."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-o>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns. The columns may affect tree-like output. The default is "
"to use tree for the column \\(aqNAME\\(aq (see also B<--tree>)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The default list of columns may be extended if I<list> is specified in the "
"format I<+list> (e.g., B<lsblk -o +UUID>)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-O>, B<--output-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Output all available columns."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-P>, B<--pairs>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"Produce output in the form of key=\"value\" pairs. The output lines are "
"still ordered by dependencies. All potentially unsafe value characters are "
"hex-escaped (\\(rsxE<lt>codeE<gt>). See also option B<--shell>."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-p>, B<--paths>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Print full device paths."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Produce output in raw format. The output lines are still ordered by "
"dependencies. All potentially unsafe characters are hex-escaped "
"(\\(rsxE<lt>codeE<gt>) in the NAME, KNAME, LABEL, PARTLABEL and MOUNTPOINT "
"columns."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-S>, B<--scsi>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Output info about SCSI devices only. All partitions, slaves and holder "
"devices are ignored."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-s>, B<--inverse>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Print dependencies in inverse order. If the B<--list> output is requested "
"then the lines are still ordered by dependencies."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-T>, B<--tree>[B<=>I<column>]"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Force tree-like output format. If I<column> is specified, then a tree is "
"printed in the column. The default is NAME column."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-t>, B<--topology>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Output info about block-device topology. This option is equivalent to"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<-o NAME,ALIGNMENT,MIN-IO,OPT-IO,PHY-SEC,LOG-SEC,ROTA,SCHED,RQ-SIZE,RA,"
"WSAME>."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print version and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-w>, B<--width> I<number>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Specifies output width as a number of characters. The default is the number "
"of the terminal columns, and if not executed on a terminal, then output "
"width is not restricted at all by default. This option also forces B<lsblk> "
"to assume that terminal control characters and unsafe characters are not "
"allowed. The expected use-case is for example when B<lsblk> is used by the "
"B<watch>(1) command."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-x>, B<--sort> I<column>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Sort output lines by I<column>. This option enables B<--list> output format "
"by default. It is possible to use the option B<--tree> to force tree-like "
"output and than the tree branches are sorted by the I<column>."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<-y>, B<--shell>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"The column name will be modified to contain only characters allowed for "
"shell variable identifiers, for example, MIN_IO and FSUSE_PCT instead of MIN-"
"IO and FSUSE%. This is usable, for example, with B<--pairs>. Note that this "
"feature has been automatically enabled for B<--pairs> in version 2.37, but "
"due to compatibility issues, now it\\(cqs necessary to request this behavior "
"by B<--shell>."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-z>, B<--zoned>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print the zone related information for each device."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<--sysroot> I<directory>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Gather data for a Linux instance other than the instance from which the "
"B<lsblk> command is issued. The specified directory is the system root of "
"the Linux instance to be inspected. The real device nodes in the target "
"directory can be replaced by text files with udev attributes."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "0"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "success"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "1"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "failure"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "32"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "none of specified devices found"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "64"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "some specified devices found, some not found"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LSBLK_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "enables B<lsblk> debug output."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LIBBLKID_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "enables B<libblkid> debug output."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LIBMOUNT_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "enables B<libmount> debug output."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LIBSMARTCOLS_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "enables B<libsmartcols> debug output."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "B<LIBSMARTCOLS_DEBUG_PADDING>=on"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "use visible padding characters."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"For partitions, some information (e.g., queue attributes) is inherited from "
"the parent device."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"The B<lsblk> command needs to be able to look up each block device by major:"
"minor numbers, which is done by using I</sys/dev/block>. This sysfs block "
"directory appeared in kernel 2.6.27 (October 2008). In case of problems with "
"a new enough kernel, check that B<CONFIG_SYSFS> was enabled at the time of "
"the kernel build."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<ls>(1), B<blkid>(8), B<findmnt>(8)"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<lsblk> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Also list empty devices and RAM disk devices."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Print the SIZE column in bytes rather than in a human-readable format."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Produce output in the form of key=\"value\" pairs. The output lines are "
"still ordered by dependencies. All potentially unsafe value characters are "
"hex-escaped (\\(rsxE<lt>codeE<gt>). The key (variable name) will be modified "
"to contain only characters allowed for a shell variable identifiers, for "
"example, MIN_IO and FSUSE_PCT instead of MIN-IO and FSUSE%."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Print the zone model for each device."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "LSBLK_DEBUG=all"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBBLKID_DEBUG=all"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBMOUNT_DEBUG=all"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBSMARTCOLS_DEBUG=all"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBSMARTCOLS_DEBUG_PADDING=on"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<lsblk> command needs to be able to look up each block device by major:"
"minor numbers, which is done by using I</sys/dev/block>. This sysfs block "
"directory appeared in kernel 2.6.27 (October 2008). In case of problems with "
"a new enough kernel, check that CONFIG_SYSFS was enabled at the time of the "
"kernel build."
msgstr ""
