# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:53+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SWAPON"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"swapon, swapoff - enable/disable devices and files for paging and swapping"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<swapon> [options] [I<specialfile>...]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<swapoff> [B<-va>] [I<specialfile>...]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<swapon> is used to specify devices on which paging and swapping are to "
"take place."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The device or file used is given by the I<specialfile> parameter. It may be "
"of the form B<-L> I<label> or B<-U> I<uuid> to indicate a device by label or "
"uuid."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Calls to B<swapon> normally occur in the system boot scripts making all swap "
"devices available, so that the paging and swapping activity is interleaved "
"across several devices and files."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<swapoff> disables swapping on the specified devices and files. When the B<-"
"a> flag is given, swapping is disabled on all known swap devices and files "
"(as found in I</proc/swaps> or I</etc/fstab>)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-a>, B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"All devices marked as \"swap\" in I</etc/fstab> are made available, except "
"for those with the \"noauto\" option. Devices that are already being used as "
"swap are silently skipped."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-d>, B<--discard>[B<=>I<policy>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Enable swap discards, if the swap backing device supports the discard or "
"trim operation. This may improve performance on some Solid State Devices, "
"but often it does not. The option allows one to select between two available "
"swap discard policies:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--discard=once>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"to perform a single-time discard operation for the whole swap area at "
"swapon; or"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--discard=pages>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"to asynchronously discard freed swap pages before they are available for "
"reuse."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"If no policy is selected, the default behavior is to enable both discard "
"types. The I</etc/fstab> mount options B<discard>, B<discard=once>, or "
"B<discard=pages> may also be used to enable discard flags."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-e>, B<--ifexists>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Silently skip devices that do not exist. The I</etc/fstab> mount option "
"B<nofail> may also be used to skip non-existing device."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-f>, B<--fixpgsz>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Reinitialize (exec mkswap) the swap space if its page size does not match "
"that of the current running kernel. B<mkswap>(8) initializes the whole "
"device and does not check for bad blocks."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-L> I<label>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Use the partition that has the specified I<label>. (For this, access to I</"
"proc/partitions> is needed.)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-o>, B<--options> I<opts>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid ""
"Specify swap options by an I<fstab>-compatible comma-separated string. For "
"example:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<swapon -o pri=1,discard=pages,nofail /dev/sda2>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The I<opts> string is evaluated last and overrides all other command line "
"options."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-p>, B<--priority> I<priority>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Specify the priority of the swap device. I<priority> is a value between -1 "
"and 32767. Higher numbers indicate higher priority. See B<swapon>(2) for a "
"full description of swap priorities. Add B<pri=>I<value> to the option field "
"of I</etc/fstab> for use with B<swapon -a>. When no priority is defined, it "
"defaults to -1."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-s>, B<--summary>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Display swap usage summary by device. Equivalent to B<cat /proc/swaps>. This "
"output format is DEPRECATED in favour of B<--show> that provides better "
"control on output data."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--show>[B<=>I<column>...]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Display a definable table of swap areas. See the B<--help> output for a list "
"of available columns."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--output-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Output all available columns."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Do not print headings when displaying B<--show> output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Display B<--show> output without aligning table columns."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Display swap size in bytes in B<--show> output instead of in user-friendly "
"units."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-U> I<uuid>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Use the partition that has the specified I<uuid>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Be verbose."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<swapoff> has the following exit status values since v2.36:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<0>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "success"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<2>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "system has insufficient memory to stop swapping (OOM)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<4>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "B<swapoff>(2) syscall failed for another reason"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<8>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "non-B<swapoff>(2) syscall system error (out of memory, ...)"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<16>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "usage or syntax error"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<32>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "all swapoff failed on B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<64>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "some swapoff succeeded on B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The command B<swapoff --all> returns 0 (all succeeded), 32 (all failed), or "
"64 (some failed, some succeeded)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"+ The old versions before v2.36 has no documented exit status, 0 means "
"success in all versions."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "B<LIBMOUNT_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "enables B<libmount> debug output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "B<LIBBLKID_DEBUG>=all"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "enables B<libblkid> debug output."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "I</dev/sd??>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "standard paging devices"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "I</etc/fstab>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "ascii filesystem description table"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Files with holes"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The swap file implementation in the kernel expects to be able to write to "
"the file directly, without the assistance of the filesystem. This is a "
"problem on files with holes or on copy-on-write files on filesystems like "
"Btrfs."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Commands like B<cp>(1) or B<truncate>(1) create files with holes. These "
"files will be rejected by B<swapon>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Preallocated files created by B<fallocate>(1) may be interpreted as files "
"with holes too depending of the filesystem. Preallocated swap files are "
"supported on XFS since Linux 4.18."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The most portable solution to create a swap file is to use B<dd>(1) and I</"
"dev/zero>."
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Btrfs"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid ""
"Swap files on Btrfs are supported since Linux 5.0 on files with B<nocow> "
"attribute. See the B<btrfs>(5) manual page for more details."
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NFS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Swap over B<NFS> may not work."
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Suspend"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid ""
"B<swapon> automatically detects and rewrites a swap space signature with old "
"software suspend data (e.g., B<S1SUSPEND>, B<S2SUSPEND>, ...). The problem "
"is that if we don\\(cqt do it, then we get data corruption the next time an "
"attempt at unsuspending is made."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "The B<swapon> command appeared in 4.0BSD."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<swapoff>(2), B<swapon>(2), B<fstab>(5), B<init>(8), B<fallocate>(1), "
"B<mkswap>(8), B<mount>(8), B<rc>(8)"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The B<swapon> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Specify swap options by an fstab-compatible comma-separated string. For "
"example:"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "swapoff syscall failed for another reason"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "non-swapoff syscall system error (out of memory, ...)"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBMOUNT_DEBUG=all"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "LIBBLKID_DEBUG=all"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Swap files on Btrfs are supported since Linux 5.0 on files with nocow "
"attribute. See the B<btrfs>(5) manual page for more details."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<swapon> automatically detects and rewrites a swap space signature with old "
"software suspend data (e.g., S1SUSPEND, S2SUSPEND, ...). The problem is that "
"if we don\\(cqt do it, then we get data corruption the next time an attempt "
"at unsuspending is made."
msgstr ""
