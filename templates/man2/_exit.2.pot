# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:26+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "_exit"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "_exit, _Exit - terminate the calling process"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<[[noreturn]] void _exit(int >I<status>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<[[noreturn]] void _Exit(int >I<status>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<_Exit>():"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<_exit>()  terminates the calling process \"immediately\".  Any open file "
"descriptors belonging to the process are closed.  Any children of the "
"process are inherited by B<init>(1)  (or by the nearest \"subreaper\" "
"process as defined through the use of the B<prctl>(2)  "
"B<PR_SET_CHILD_SUBREAPER> operation).  The process's parent is sent a "
"B<SIGCHLD> signal."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The value I<status & 0xFF> is returned to the parent process as the "
"process's exit status, and can be collected by the parent using one of the "
"B<wait>(2)  family of calls."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The function B<_Exit>()  is equivalent to B<_exit>()."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These functions do not return."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<_exit>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<_Exit>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "B<_Exit>()  was introduced by C99."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For a discussion on the effects of an exit, the transmission of exit status, "
"zombie processes, signals sent, and so on, see B<exit>(3)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function B<_exit>()  is like B<exit>(3), but does not call any functions "
"registered with B<atexit>(3)  or B<on_exit>(3).  Open B<stdio>(3)  streams "
"are not flushed.  On the other hand, B<_exit>()  does close open file "
"descriptors, and this may cause an unknown delay, waiting for pending output "
"to finish.  If the delay is undesired, it may be useful to call functions "
"like B<tcflush>(3)  before calling B<_exit>().  Whether any pending I/O is "
"canceled, and which pending I/O may be canceled upon B<_exit>(), is "
"implementation-dependent."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "C library/kernel differences"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The text above in DESCRIPTION describes the traditional effect of "
"B<_exit>(), which is to terminate a process, and these are the semantics "
"specified by POSIX.1 and implemented by the C library wrapper function.  On "
"modern systems, this means termination of all threads in the process."
msgstr ""

#.  _exit() is used by pthread_exit() to terminate the calling thread
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"By contrast with the C library wrapper function, the raw Linux B<_exit>()  "
"system call terminates only the calling thread, and actions such as "
"reparenting child processes or sending B<SIGCHLD> to the parent process are "
"performed only if this is the last thread in the thread group."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Up to glibc 2.3, the B<_exit>()  wrapper function invoked the kernel system "
"call of the same name.  Since glibc 2.3, the wrapper function invokes "
"B<exit_group>(2), in order to terminate all of the threads in a process."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<execve>(2), B<exit_group>(2), B<fork>(2), B<kill>(2), B<wait>(2), "
"B<wait4>(2), B<waitpid>(2), B<atexit>(3), B<exit>(3), B<on_exit>(3), "
"B<termios>(3)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-01-22"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD.  The function B<_Exit>()  was "
"introduced by C99."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "_EXIT"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-05-03"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<void _exit(int >I<status>B<);>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>stdlib.hE<gt>>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<void _Exit(int >I<status>B<);>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The function B<_exit>()  terminates the calling process \"immediately\".  "
"Any open file descriptors belonging to the process are closed.  Any children "
"of the process are inherited by B<init>(1)  (or by the nearest \"subreaper\" "
"process as defined through the use of the B<prctl>(2)  "
"B<PR_SET_CHILD_SUBREAPER> operation).  The process's parent is sent a "
"B<SIGCHLD> signal."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The value I<status & 0377> is returned to the parent process as the "
"process's exit status, and can be collected using one of the B<wait>(2)  "
"family of calls."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"In glibc up to version 2.3, the B<_exit>()  wrapper function invoked the "
"kernel system call of the same name.  Since glibc 2.3, the wrapper function "
"invokes B<exit_group>(2), in order to terminate all of the threads in a "
"process."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
