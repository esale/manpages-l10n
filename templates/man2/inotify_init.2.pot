# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:32+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "inotify_init"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "inotify_init, inotify_init1 - initialize an inotify instance"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/inotify.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int inotify_init(void);>\n"
"B<int inotify_init1(int >I<flags>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For an overview of the inotify API, see B<inotify>(7)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<inotify_init>()  initializes a new inotify instance and returns a file "
"descriptor associated with a new inotify event queue."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<flags> is 0, then B<inotify_init1>()  is the same as "
"B<inotify_init>().  The following values can be bitwise ORed in I<flags> to "
"obtain different behavior:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<IN_NONBLOCK>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the B<O_NONBLOCK> file status flag on the open file description (see "
"B<open>(2))  referred to by the new file descriptor.  Using this flag saves "
"extra calls to B<fcntl>(2)  to achieve the same result."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<IN_CLOEXEC>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Set the close-on-exec (B<FD_CLOEXEC>)  flag on the new file descriptor.  See "
"the description of the B<O_CLOEXEC> flag in B<open>(2)  for reasons why this "
"may be useful."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On success, these system calls return a new file descriptor.  On error, -1 "
"is returned, and I<errno> is set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "(B<inotify_init1>())  An invalid value was specified in I<flags>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The user limit on the total number of inotify instances has been reached."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Insufficient kernel memory is available."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<inotify_init>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux 2.6.13, glibc 2.4."
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<inotify_init1>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux 2.6.27, glibc 2.9."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<inotify_add_watch>(2), B<inotify_rm_watch>(2), B<inotify>(7)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"B<inotify_init>()  first appeared in Linux 2.6.13; library support was added "
"in glibc 2.4.  B<inotify_init1>()  was added in Linux 2.6.27; library "
"support was added in glibc 2.9."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "These system calls are Linux-specific."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "INOTIFY_INIT"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Set the B<O_NONBLOCK> file status flag on the new open file description.  "
"Using this flag saves extra calls to B<fcntl>(2)  to achieve the same result."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<inotify_init>()  first appeared in Linux 2.6.13; library support was added "
"to glibc in version 2.4.  B<inotify_init1>()  was added in Linux 2.6.27; "
"library support was added to glibc in version 2.9."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
