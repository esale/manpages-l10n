# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "removexattr"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "removexattr, lremovexattr, fremovexattr - remove an extended attribute"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/xattr.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int removexattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<);>\n"
"B<int lremovexattr(const char\\ *>I<path>B<, const char\\ *>I<name>B<);>\n"
"B<int fremovexattr(int >I<fd>B<, const char\\ *>I<name>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Extended attributes are I<name>:I<value> pairs associated with inodes "
"(files, directories, symbolic links, etc.).  They are extensions to the "
"normal attributes which are associated with all inodes in the system (i.e., "
"the B<stat>(2)  data).  A complete overview of extended attributes concepts "
"can be found in B<xattr>(7)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<removexattr>()  removes the extended attribute identified by I<name> and "
"associated with the given I<path> in the filesystem."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<lremovexattr>()  is identical to B<removexattr>(), except in the case of a "
"symbolic link, where the extended attribute is removed from the link itself, "
"not the file that it refers to."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<fremovexattr>()  is identical to B<removexattr>(), only the extended "
"attribute is removed from the open file referred to by I<fd> (as returned by "
"B<open>(2))  in place of I<path>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"An extended attribute name is a null-terminated string.  The I<name> "
"includes a namespace prefix; there may be several, disjoint namespaces "
"associated with an individual inode."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On failure, -1 is returned and I<errno> is "
"set to indicate the error."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<ENODATA>"
msgstr ""

#.  .RB ( ENOATTR
#.  is defined to be a synonym for
#.  .BR ENODATA
#.  in
#.  .IR <attr/attributes.h> .)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The named attribute does not exist."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSUP>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Extended attributes are not supported by the filesystem, or are disabled."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "In addition, the errors documented in B<stat>(2)  can also occur."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#.  .SH AUTHORS
#.  Andreas Gruenbacher,
#.  .RI < a.gruenbacher@computer.org >
#.  and the SGI XFS development team,
#.  .RI < linux-xfs@oss.sgi.com >.
#.  Please send any bug reports or comments to these addresses.
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux 2.4, glibc 2.3."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<getfattr>(1), B<setfattr>(1), B<getxattr>(2), B<listxattr>(2), B<open>(2), "
"B<setxattr>(2), B<stat>(2), B<symlink>(7), B<xattr>(7)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"These system calls have been available since Linux 2.4; glibc support is "
"provided since glibc 2.3."
msgstr ""

#.  .SH AUTHORS
#.  Andreas Gruenbacher,
#.  .RI < a.gruenbacher@computer.org >
#.  and the SGI XFS development team,
#.  .RI < linux-xfs@oss.sgi.com >.
#.  Please send any bug reports or comments to these addresses.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "These system calls are Linux-specific."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "REMOVEXATTR"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2015-05-07"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/xattr.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On failure, -1 is returned and I<errno> is "
"set appropriately."
msgstr ""

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<ENOATTR>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The named attribute does not exist.  (B<ENOATTR> is defined to be a synonym "
"for B<ENODATA> in I<E<lt>attr/xattr.hE<gt>>.)"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"These system calls have been available on Linux since kernel 2.4; glibc "
"support is provided since version 2.3."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
