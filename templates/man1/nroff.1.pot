# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:45+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: SY
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "\\%nroff"
msgstr ""

#. type: TH
#: archlinux
#, no-wrap
msgid "18 July 2023"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "groff 1.23.0"
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "Name"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "\\%nroff - format documents with I<groff> for TTY (terminal) devices"
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "Synopsis"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"[B<-bcCEhikpRStUVz>] [B<-d\\~>I<ctext>] [B<-d\\~>I<string>B<=>I<text>] [B<-"
"K\\~>I<fallback-encoding>] [B<-m\\~>I<macro-package>] [B<-M\\~>I<macro-"
"directory>] [B<-n\\~>I<page-number>] [B<-o\\~>I<page-list>] [B<-"
"P\\~>I<postprocessor-argument>] [B<-r\\~>I<cnumeric-expression>] [B<-"
"r\\~>I<register>B<=>I<numeric-expression>] [B<-T\\~>I<output-device>] [B<-"
"w\\~>I<warning-category>] [B<-W\\~>I<warning-category>] [I<file\\~>.\\|.\\|.]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-v>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--version>"
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "Description"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "I<\\%nroff> formats documents written in the"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "language for typewriter-like devices such as terminal emulators."
msgstr ""

#.  GNU
#.  AT&T
#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "GNU I<nroff> emulates the AT&T I<nroff> command using"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "I<\\%nroff> generates output via"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"I<groff>'s terminal output driver, which needs to know the character "
"encoding scheme used by the device."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Consequently, acceptable arguments to the B<-T> option are B<ascii>, "
"B<latin1>, B<utf8>, and B<cp1047>; any others are ignored."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"If neither the I<\\%GROFF_TYPESETTER> environment variable nor the B<-T> "
"command-line option (which overrides the environment variable)  specifies a "
"(valid) device, I<\\%nroff> consults the locale to select an appropriate "
"output device."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "It first tries the"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"program, then checks several locale-related environment variables; see "
"section \\[lq]Environment\\[rq] below."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If all of the foregoing fail, B<-Tascii> is implied."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"The B<-b>, B<-c>, B<-C>, B<-d>, B<-E>, B<-i>, B<-m>, B<-M>, B<-n>, B<-o>, B<-"
"r>, B<-U>, B<-w>, B<-W>, and B<-z> options have the effects described in"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"B<-c> and B<-h> imply \\[lq]B<-P-c>\\[rq] and \\[lq]B<-P-h>\\[rq], "
"respectively; B<-c> is also interpreted directly by I<\\%troff>."
msgstr ""

#.  AT&T
#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"In addition, this implementation ignores the AT&T I<nroff> options B<-e>, B<-"
"q>, and B<-s> (which are not implemented in I<groff>)."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"The options B<-k>, B<-K>, B<-p>, B<-P>, B<-R>, B<-t>, and B<-S> are "
"documented in"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"B<-V> causes I<\\%nroff> to display the constructed I<groff> command on the "
"standard output stream, but does not execute it."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"B<-v> and B<--version> show version information about I<\\%nroff> and the "
"programs it runs, while B<--help> displays a usage message; all exit "
"afterward."
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "Exit status"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"I<\\%nroff> exits with error status\\~B<2> if there was a problem parsing "
"its arguments, with status\\~B<0> if any of the options B<-V>, B<-v>, B<--"
"version>, or B<--help> were specified, and with the status of I<groff> "
"otherwise."
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "Environment"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"Normally, the path separator in environment variables ending with I<PATH> is "
"the colon; this may vary depending on the operating system."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "For example, Windows uses a semicolon instead."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<GROFF_BIN_PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"is a colon-separated list of directories in which to search for the I<groff> "
"executable before searching in I<PATH>."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "If unset, I</usr/\\:\\%bin> is used."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<GROFF_TYPESETTER>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "specifies the default output device for I<groff>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<LC_ALL>"
msgstr ""

#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<LC_CTYPE>"
msgstr ""

#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<LANG>"
msgstr ""

#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<LESSCHARSET>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"are pattern-matched in this order for contents matching standard character "
"encodings supported by I<groff> in the event no B<-T> option is given and "
"I<\\%GROFF_TYPESETTER> is unset, or the values specified are invalid."
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "Files"
msgstr ""

#. type: TP
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "I</usr/\\:\\%share/\\:\\%groff/\\:\\%1.23.0/\\:\\%tmac/\\:\\%tty-char\\:.tmac>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "defines fallback definitions of I<roff> special characters."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"These definitions more poorly optically approximate typeset output than "
"those of I<tty.tmac> in favor of communicating semantic information."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "I<nroff> loads it automatically."
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "Notes"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "Pager programs like"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid "and"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide
msgid ""
"may require command-line options to correctly handle some output sequences; "
"see"
msgstr ""

#.  ====================================================================
#. type: SH
#: archlinux debian-unstable fedora-rawhide
#, no-wrap
msgid "See also"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NROFF"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "7 March 2023"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "groff 1.22.4"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "nroff - use groff to format documents for TTY devices"
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: SY
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "nroff"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"[B<-CchipStUv>] [B<-d>I<cs>] [B<-M>I<dir>] [B<-m>I<name>] [B<-n>I<num>] [B<-"
"o>I<list>] [B<-r>I<cn>] [B<-T>I<name>] [B<-W>I<warning>] [B<-w>I<warning>] "
"[I<file> \\&.\\|.\\|.\\&]"
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<nroff> formats documents written in the I<roff>(7)  language for "
"typewriter-like devices such as terminal emulators."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"GNU I<nroff> emulates the traditional Unix I<nroff> command using "
"I<groff>(1)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<nroff> generates output via I<grotty>(1), I<groff>'s TTY output device, "
"which needs to know the character encoding scheme used by the terminal."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"If neither the I<\\%GROFF_TYPESETTER> environment variable nor the B<-T> "
"command-line option (which overrides the environment variable)  specifies a "
"(valid) device, I<nroff> consults the locale to select an appropriate output "
"device."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"It first tries the I<locale>(1)  program, then checks several locale-related "
"environment variables; see \\(lqENVIRONMENT\\(rq, below."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Whitespace is not permitted between an option and its argument."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The B<-h> and B<-c> options are equivalent to I<grotty>'s options B<-h> "
"(using tabs in the output) and B<-c> (using the old output scheme instead of "
"SGR escape sequences)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The B<-d>, B<-C>, B<-i>, B<-M>, B<-m>, B<-n>, B<-o>, B<-r>, B<-w>, and B<-W> "
"options have the effect described in I<troff>(1)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"In addition, I<nroff> ignores B<-e>, B<-q>, and B<-s> (which are not "
"implemented in I<troff>)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The options B<-p> (pic), B<-t> (tbl), B<-S> (safer), and B<-U> (unsafe) are "
"passed to I<groff>."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<-v> and B<--version> show version information, while B<--help> displays a "
"usage message; all exit afterward."
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "If unset, I</usr/\\:bin> is used."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"are pattern-matched in this order for standard character encodings supported "
"by I<groff> in the event no B<-T> option is given and I<GROFF_TYPESETTER> is "
"unset."
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Character definitions in the file I</usr/\\:share/\\:groff/\\:1.22.4/\\:tmac/"
"\\:tty-char.tmac> are loaded to replace unrepresentable glyphs."
msgstr ""

#.  ====================================================================
#. type: SH
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "I<groff>(1), I<troff>(1), I<grotty>(1), I<locale>(1), I<roff>(7)"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "14 July 2023"
msgstr ""

#. type: TH
#: fedora-38
#, no-wrap
msgid "19 January 2023"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "10 July 2023"
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "23 November 2018"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "7 February 2022"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "16 March 2023"
msgstr ""
