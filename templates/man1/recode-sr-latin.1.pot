# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:50+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RECODE-SR-LATIN"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "June 2023"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide
#, no-wrap
msgid "GNU gettext-tools 0.22"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "recode-sr-latin - convert Serbian text from Cyrillic to Latin script"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<recode-sr-latin> [I<\\,OPTION\\/>]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Recode Serbian text from Cyrillic to Latin script.  The input text is read "
"from standard input.  The converted text is output to standard output."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Informative output:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Danilo Segan and Bruno Haible."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report bugs in the bug tracker at E<lt>https://savannah.gnu.org/projects/"
"gettextE<gt> or by email to E<lt>bug-gettext@gnu.orgE<gt>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide
msgid ""
"Copyright \\(co 2006-2023 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The full documentation for B<recode-sr-latin> is maintained as a Texinfo "
"manual.  If the B<info> and B<recode-sr-latin> programs are properly "
"installed at your site, the command"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<info recode-sr-latin>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "February 2023"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GNU gettext-tools 0.21"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable opensuse-leap-15-5
msgid ""
"Copyright \\(co 2006-2020 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""

#. type: TH
#: debian-unstable
#, no-wrap
msgid "July 2023"
msgstr ""

#. type: TH
#: fedora-38 mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "October 2022"
msgstr ""

#. type: TH
#: fedora-38 mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "GNU gettext-tools 0.21.1"
msgstr ""

#. type: Plain text
#: fedora-38 mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 2006-2022 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "April 2020"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU gettext-tools 0.20.2"
msgstr ""
