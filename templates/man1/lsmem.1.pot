# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:41+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "LSMEM"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "lsmem - list the ranges of available memory with their online status"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<lsmem> [options]"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<lsmem> command lists the ranges of available memory with their online "
"status. The listed memory blocks correspond to the memory block "
"representation in sysfs. The command also shows the memory block size and "
"the amount of memory in online and offline state."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"The default output is compatible with original implementation from s390-"
"tools, but it\\(cqs strongly recommended to avoid using default outputs in "
"your scripts. Always explicitly define expected columns by using the B<--"
"output> option together with a columns list in environments where a stable "
"output is required."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<lsmem> command lists a new memory range always when the current memory "
"block distinguish from the previous block by some output column. This "
"default behavior is possible to override by the B<--split> option (e.g., "
"B<lsmem --split=ZONES>). The special word \"none\" may be used to ignore all "
"differences between memory blocks and to create as large as possible "
"continuous ranges. The opposite semantic is B<--all> to list individual "
"memory blocks."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Note that some output columns may provide inaccurate information if a split "
"policy forces B<lsmem> to ignore differences in some attributes. For example "
"if you merge removable and non-removable memory blocks to the one range than "
"all the range will be marked as non-removable on B<lsmem> output."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Not all columns are supported on all systems. If an unsupported column is "
"specified, B<lsmem> prints the column but does not provide any data for it."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Use the B<--help> option to see the columns description."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-a>, B<--all>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"List each individual memory block, instead of combining memory blocks with "
"similar attributes."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-b>, B<--bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print the sizes in bytes rather than in a human-readable format."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"By default, the unit, sizes are expressed in, is byte, and unit prefixes are "
"in power of 2^10 (1024). Abbreviations of symbols are exhibited truncated in "
"order to reach a better readability, by exhibiting alone the first letter of "
"them; examples: \"1 KiB\" and \"1 MiB\" are respectively exhibited as \"1 "
"K\" and \"1 M\", then omitting on purpose the mention \"iB\", which is part "
"of these abbreviations."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Use JSON output format."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-n>, B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Do not print a header line."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-o>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns. The default list of columns may be extended if I<list> is "
"specified in the format B<+>I<list> (e.g., B<lsmem -o +NODE>)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<--output-all>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Output all available columns."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-P>, B<--pairs>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Produce output in the form of key=\"value\" pairs. All potentially unsafe "
"value characters are hex-escaped (\\(rsxE<lt>codeE<gt>)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Produce output in raw format. All potentially unsafe characters are hex-"
"escaped (\\(rsxE<lt>codeE<gt>)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-S>, B<--split> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid ""
"Specify which columns (attributes) use to split memory blocks to ranges. The "
"supported columns are STATE, REMOVABLE, NODE and ZONES, or \"none\". The "
"other columns are silently ignored. For more details see B<DESCRIPTION> "
"above."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-s>, B<--sysroot> I<directory>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Gather memory data for a Linux instance other than the instance from which "
"the B<lsmem> command is issued. The specified I<directory> is the system "
"root of the Linux instance to be inspected."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print version and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<--summary>[=I<when>]"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"This option controls summary lines output. The optional argument I<when> can "
"be B<never>, B<always> or B<only>. If the I<when> argument is omitted, it "
"defaults to B<\"only\">. The summary output is suppressed for B<--raw>, B<--"
"pairs> and B<--json>."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<lsmem> was originally written by Gerald Schaefer for s390-tools in Perl. "
"The C version for util-linux was written by Clemens von Mann, Heiko Carstens "
"and Karel Zak."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<chmem>(8)"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<lsmem> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The default output compatible with original implementation from s390-tools, "
"but it\\(cqs strongly recommended to avoid using default outputs in your "
"scripts. Always explicitly define expected columns by using the B<--output> "
"option together with a columns list in environments where a stable output is "
"required."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Print the SIZE column in bytes rather than in a human-readable format."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Specify which columns (attributes) use to split memory blocks to ranges. The "
"supported columns are STATE, REMOVABLE, NODE and ZONES, or \"none\". The "
"other columns are silently ignored. For more details see DESCRIPTION above."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr ""
