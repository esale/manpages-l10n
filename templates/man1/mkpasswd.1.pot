# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:43+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "MKPASSWD"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "2019-12-30"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "Marco d'Itri"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "Debian GNU/Linux"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "mkpasswd - Overfeatured front end to crypt(3)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<mkpasswd> I<PASSWORD> [I<SALT>]"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<mkpasswd> encrypts the given password with the B<crypt>(3)  libc function, "
"using the given salt."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-S>I<, >"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<--salt=>I<STRING> Use the I<STRING> as salt. If it begins with I<$> then "
"it will be passed straight to B<crypt>(3)  without any checks."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-R>I<, >"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<--rounds=>I<NUMBER> Use I<NUMBER> rounds. This argument is ignored if the "
"method chosen does not support variable rounds. For the OpenBSD Blowfish "
"method this is the logarithm of the number of rounds.  The behavior is "
"undefined if this option is used without I<--method>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-m>I<, >"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<--method=>I<TYPE> Compute the password using the I<TYPE> method.  If "
"I<TYPE> is I<help> then the list of available methods is printed.  If "
"I<TYPE> begins and end with I<$> characters then the string is passed to "
"I<crypt_gensalt>(3)  as-is."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-5>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Like I<--method=md5crypt>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-P .IR NUM ,  .BI --password-fd= NUM>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Read the password from file descriptor I<NUM> instead of using "
"I<getpass>(3).  If the file descriptor is not connected to a tty then no "
"other text than the hashed password is printed on stdout."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--stdin>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Like I<--password-fd=0>."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "MKPASSWD_OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"A list of options which will be evaluated before the ones specified on the "
"command line."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"If the I<--stdin> option is used then passwords containing some control "
"characters may not be read correctly."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-tumbleweed
msgid "This program suffers of a bad case of featuritis."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"I<passwd>(1), I<passwd>(5), I<crypt>(3), I<crypt>(5), I<crypt_gensalt>(3), "
"I<getpass>(3)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"B<mkpasswd> and this man page were written by Marco d'Itri E<lt>I<md@linux."
"it>E<gt> and are licensed under the terms of the GNU General Public License, "
"version 2 or later."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<mkpasswd> and this man page were written by Marco d'Itri E<lt>I<md@linux."
"it>E<gt> and are licensed under the terms of the GNU General Public License, "
"version 2 or higher."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "This programs suffers of a bad case of featuritis."
msgstr ""
