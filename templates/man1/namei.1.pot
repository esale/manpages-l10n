# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-07-25 19:45+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAMEI"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "2022-08-04"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "namei - follow a pathname until a terminal point is found"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<namei> [options] I<pathname>..."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<namei> interprets its arguments as pathnames to any type of Unix file "
"(symlinks, files, directories, and so forth). B<namei> then follows each "
"pathname until an endpoint is found (a file, a directory, a device node, "
"etc). If it finds a symbolic link, it shows the link, and starts following "
"it, indenting the output to show the context."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"This program is useful for finding \"too many levels of symbolic links\" "
"problems."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"For each line of output, B<namei> uses the following characters to identify "
"the file type found:"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid ""
"   f: = the pathname currently being resolved\n"
"    d = directory\n"
"    l = symbolic link (both the link and its contents are output)\n"
"    s = socket\n"
"    b = block device\n"
"    c = character device\n"
"    p = FIFO (named pipe)\n"
"    - = regular file\n"
"    ? = an error of some kind\n"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"B<namei> prints an informative message when the maximum number of symbolic "
"links this system can have has been exceeded."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-l>, B<--long>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Use the long listing format (same as B<-m -o -v>)."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-m>, B<--modes>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Show the mode bits of each file type in the style of B<ls>(1), for example "
"\\(aqrwxr-xr-x\\(aq."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-n>, B<--nosymlinks>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Don\\(cqt follow symlinks."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-o>, B<--owners>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Show owner and group name of each file."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-v>, B<--vertical>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Vertically align the modes and owners."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-x>, B<--mountpoints>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"Show mountpoint directories with a \\(aqD\\(aq rather than a \\(aqd\\(aq."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "To be discovered."
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "The original B<namei> program was written by"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "The program was rewritten by Karel Zak"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "B<ls>(1), B<stat>(1), B<symlink>(7)"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm fedora-38 opensuse-leap-15-5
msgid ""
"The B<namei> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr ""
