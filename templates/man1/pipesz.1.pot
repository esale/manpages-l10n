# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-05-03 10:32+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "PIPESZ"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "2023-03-23"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "util-linux 2.39-rc3"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "pipesz - set or examine pipe and FIFO buffer sizes"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<pipesz> [options] [--set I<size>] [--] [I<command> [argument] ...]"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<pipesz> [options] --get"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Pipes and FIFOs maintain an internal buffer used to transfer data between "
"the read end and the write end. In some cases, the default size of this "
"internal buffer may not be appropriate. This program provides facilities to "
"set and examine the size of these buffers."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"The B<--set> operation sets pipe buffer sizes. If it is specified, it must "
"be specified with an explicit I<size>. Otherwise, it is implied and the size "
"is read from B</proc/sys/fs/pipe-max-size>. The kernel may adjust I<size> as "
"described in B<fcntl>(2). To determine the actual buffer sizes set, use the "
"B<--verbose> option. If neither B<--file> nor B<--fd> are specified, B<--"
"set> acts on standard output."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"The B<--set> operation permits an optional I<command> to execute after "
"setting the pipe buffer sizes. This command is executed with the adjusted "
"pipes."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"The B<--get> operation outputs data in a tabular format. The first column is "
"the name of the pipe as passed to B<pipesz>. File descriptors are named as "
"\"fd I<N>\". The second column is the size, in bytes, of the pipe\\(cqs "
"internal buffer. The third column is the number of unread bytes currently in "
"the pipe. The columns are separated by tabs (\\*(Aq\\(rst\\*(Aq, ASCII 09h). "
"If B<--verbose> is specified, a descriptive header is also emitted. If "
"neither B<--file> nor B<--fd> are specified, B<--get> acts on standard input."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Unless the B<--check> option is specified, B<pipesz> does I<not> exit if it "
"encounters an error while manipulating a file or file descriptor. This "
"allows B<pipesz> to be used generically without fear of disrupting the "
"execution of pipelines should the type of certain files be later changed. "
"For minimal disruption, the B<--quiet> option prevents warnings from being "
"emitted in these cases."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"The kernel imposes limits on the amount of pipe buffer space unprivileged "
"processes can use, though see B<BUGS> below. The kernel will also refuse to "
"shrink a pipe buffer if this would cause a loss of buffered data. See "
"B<pipe>(7) for additional details."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"B<pipesz> supports specifying multiple short options consecutively, in the "
"usual B<getopt>(3) fashion. The first non-option argument is interpreted as "
"I<command>. If I<command> might begin with \\*(Aq-\\*(Aq, use \\*(Aq--\\*(Aq "
"to separate it from arguments to B<pipesz>. In shell scripts, it is good "
"practice to use \\*(Aq--\\*(Aq when parameter expansion is involved. "
"B<pipesz> itself does not read from standard input and does not write to "
"standard output unless B<--get>, B<--help>, or B<--version> are specified."
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-g>, B<--get>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Report the size of pipe buffers to standard output and exit. As a special "
"behavior, if neither B<--file> nor B<--fd> are specified, standard input is "
"examined. It is an error to specify this option in combination with B<--set>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-s>, B<--set> I<size>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Set the size of the pipe buffers, in bytes. This option may be suffixed by "
"I<K>, I<M>, I<G>, I<KiB>, I<MiB>, or I<GiB> to indicate multiples of 1024. "
"Fractional values are supported in this case. Additional suffixes are "
"supported but are unlikely to be useful. If this option is not specified, a "
"default value is used, as described above. If this option is specified "
"multiple times, a warning is emitted and only the last-specified I<size> is "
"used. As a special behavior, if neither B<--file> nor B<--fd> are specified, "
"standard output is adjusted. It is an error to specify this option in "
"combination with B<--get>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-f>, B<--file> I<path>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Set the buffer size of the FIFO or pipe at I<path>, relative to the current "
"working directory. You may specify this option multiple times to affect "
"different files, and you may do so in combination with B<--fd>. Generally, "
"this option is used with FIFOs, but it will also operate on anonymous pipes "
"such as those found in B</proc/PID/fd>. Changes to the buffer size of FIFOs "
"are not preserved across system restarts."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-n>, B<--fd> I<fd>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Set the buffer size of the pipe or FIFO passed to B<pipesz> as the specified "
"file descriptor number. You may specify this option multiple times to affect "
"different file descriptors, and you may do so in combination with B<--file>. "
"Shorthand options are provided for the common cases of fd 0 (stdin), fd 1 "
"(stdout), and fd 2 (stderr). These should suffice in most cases."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-i>, B<--stdin>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Shorthand for B<--fd 0>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-o>, B<--stdout>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Shorthand for B<--fd 1>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-e>, B<--stderr>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Shorthand for B<--fd 2>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-c>, B<--check>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Exit, without executing I<command>, in case of any error while manipulating "
"a file or file descriptor. The default behavior if this is not specified is "
"to emit a warning to standard error and continue."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-q>, B<--quiet>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Do not diagnose non-fatal errors to standard error. This option does not "
"affect the normal output of B<--get>, B<--verbose>, B<--help>, or B<--"
"version>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-v>, B<--verbose>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"If specified with B<--get>, B<pipesz> will emit a descriptive header above "
"the table. Otherwise, if specified, B<pipesz> will print the actual buffer "
"sizes set by the kernel to standard error."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "Print version and exit."
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<pipesz> B<dd> if=I<file> bs=1M | ..."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Runs B<dd>(1) with an expanded standard output pipe, allowing it to avoid "
"context switches when piping around large blocks."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<pipesz> -s1M -cf I</run/my-service.fifo>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Sets the pipe buffer size of a service FIFO to 1,048,576 bytes. If the "
"buffer size could not be set, B<pipesz> exits with an error."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<echo> hello | B<pipesz> -g"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Prints the size of pipe used by the shell to pass input to B<pipesz>. Since "
"B<pipesz> does not read standard input, it may also report 6 unread bytes in "
"the pipe, depending on relative timings."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"B<find> /proc/I<PID>/fd -exec B<pipesz> -gqf \\*(Aq{}\\*(Aq \\*(Aq;\\*(Aq"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Prints the size and number of unread bytes of all pipes in use by I<PID>. If "
"some pipes are routinely full, B<pipesz> might be able to mitigate a "
"processing bottleneck."
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Linux supports adjusting the size of pipe buffers since kernel 2.6.35. This "
"release also introduced B</proc/sys/fs/pipe-max-size>."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"This program uses B<fcntl>(2) B<F_GETPIPE_SZ>/B<F_SETPIPE_SZ> to get and set "
"pipe buffer sizes."
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"This program uses B<ioctl>(2) B<FIONREAD> to report the amount of unread "
"data in pipes. If for some reason this fails, the amount of unread data is "
"reported as 0."
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "BUGS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Before Linux 4.9, some bugs affect how certain resource limits are enforced "
"when setting pipe buffer sizes. See B<pipe>(7) for details."
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "B<pipe>(7)"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: fedora-rawhide
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"The B<pipesz> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
